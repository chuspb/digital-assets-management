/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.s3backend;

import java.io.NotActiveException;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.servlet.http.HttpServletRequest;

import org.iavante.sling.commons.distributionserver.IDistributionServer;

/**
 * Manage S3 connections and services.
 * 
 * @scr.property name="service.description" value="IAVANTE - S3backend"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */
public interface S3backend extends IDistributionServer {

	/**
	 * Get a url to access to rooms, download or streaming a media object.
	 * Depending of the media server the URL can work or not by credentials. If
	 * the server does'nt support streaming must raise a NotActiveException.
	 * 
	 * @param media
	 *            is a string that reference the media object at the content
	 *            server
	 * @throws NotActiveException
	 * @return a well-formed URL to download/streaming the media object
	 */
	public String getUrl(String media) throws NotActiveException;

	/**
	 * Get a filename from a shared location and upload it to S3 service.
	 * 
	 * @param fileName
	 * @return a message with operation's results
	 */
	public String upload(String fileName);

	/**
	 * Get a filename from a shared location and upload it to S3 service.
	 * 
	 * @param fileName
	 * @param bucket
	 *            we can give a specified bucket
	 * @return a message with operation's results
	 */
	public String upload(String fileName, String bucket);

	/**
	 * Upload a file name from a shared location and some metadata asociated to
	 * the node
	 * 
	 * @param contentNode
	 * @return a message with operaiton's results
	 */
	public String upload(Node contentNode);

	/**
	 * Configures the distribution server
	 * 
	 * @param prop_dict
	 *            The properties with the configuration
	 */
	public void configure(Map<String, String> prop_dict);

	/**
	 * Get the player for this node.
	 * 
	 * @param req
	 * @param url
	 * @param node
	 * @return The player
	 */
	public String get_player(HttpServletRequest req,
			HashMap<String, Object> kwargs);

}
