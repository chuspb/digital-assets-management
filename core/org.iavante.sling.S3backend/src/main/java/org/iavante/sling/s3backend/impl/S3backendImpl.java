/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.s3backend.impl;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.Node;
import javax.servlet.http.HttpServletRequest;

import org.iavante.sling.commons.services.PortalSetup;
import org.iavante.sling.s3backend.S3backend;
import org.jets3t.service.S3Service;
import org.jets3t.service.S3ServiceException;
import org.jets3t.service.acl.AccessControlList;
import org.jets3t.service.impl.rest.httpclient.RestS3Service;
import org.jets3t.service.model.S3Bucket;
import org.jets3t.service.model.S3Object;
import org.jets3t.service.security.AWSCredentials;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see org.iavante.sling.s3backend.S3backend
 * @scr.component immediate="true"
 * @scr.service interface="org.iavante.sling.s3backend.S3backend"
 * @scr.property name="service.description" value="IAVANTE - S3backend Impl"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.property name="service.type" value="distributionserver"
 * @scr.property name="service.typeimpl" value="s3"
 */
public class S3backendImpl implements S3backend, Serializable {
	private static final long serialVersionUID = 1L;

	/** @scr.reference */
	private PortalSetup portalSetup;

	/** Default Logger. */
	private static final Logger log = LoggerFactory
			.getLogger(S3backendImpl.class);

	/** S3 Service. */
	private S3Service s3service;

	/** S3 Credentials. */
	private static AWSCredentials awsCredentials;

	/** Public read */
	private String public_read = "";

	/** Bucket name */
	private String bucketName = "";

	/** Expiration time */
	private String expirationTime = "";

	/** Player */
	private String player = "";

	/** Player playlist layout */
	private String player_playlist_layout = "";

	/** Player autostart */
	private String player_autostart = "";

	/** Player width */
	private String player_width = "";

	/** Player height */
	private String player_height = "";

	/** Player repeat */
	private String player_repeat = "";

	/** Player plugins */
	private String player_plugins = "";

	/** Player player_plugin_playlist */
	private String player_plugin_playlist = "";

	/** Player player_plugin_playlist_params */
	private String player_plugin_playlist_params = "";

	/** Existing services list */
	Map existingServices = new HashMap();

	protected void activate(ComponentContext context) {

		// Your Amazon Web Services (AWS) login credentials are required to
		// manage
		// S3 accounts.
		// These credentials are stored in an AWSCredentials object:
		Map<String, String> properties = portalSetup
				.get_config_properties("/s3backend");

		String key = null;
		String secret = null;

		assert properties.containsKey("bucket") == true;
		assert properties.containsKey("expirationTime") == true;

		public_read = properties.get("public_read");
		bucketName = properties.get("bucket");
		expirationTime = properties.get("expirationTime");
		player = properties.get("player");
		player_playlist_layout = properties.get("player_playlist_layout");
		player_autostart = properties.get("player_autostart");
		player_width = properties.get("player_width");
		player_height = properties.get("player_height");
		player_repeat = properties.get("player_repeat");
		player_plugins = properties.get("player_plugins");
		player_plugin_playlist = properties.get("player_plugin_playlist");
		player_plugin_playlist_params = properties
				.get("player_plugin_playlist_params");

		if (properties.containsKey("key"))
			key = properties.get("key");
		if (properties.containsKey("secret"))
			secret = properties.get("secret");

		awsCredentials = new AWSCredentials(key, secret);

		// To communicate with S3, create a class that implements an S3Service.
		// We will use the REST/HTTP implementation based on HttpClient, as this
		// is
		// the most
		// robust implementation provided with jets3t.
		try {
			s3service = new RestS3Service(awsCredentials);
		} catch (Exception e) {
			assert false;
			e.printStackTrace();
		}
	}

	public void log(String text) {
		log.error(text);
	}

	public S3backendImpl() {

	}

	/*
	 * Constructor. Used when we get the service via de S3ServiceFactory
	 */
	public S3backendImpl(Map<String, String> prop_dict) {
		initialize(prop_dict);
	}

	/*
	 * @see org.iavante.sling.s3backend.S3backend
	 */
	public String getUrl(String object) {

		try {
			/*
			 * the object is called to verify the existence of object temporaly
			 * off for performance
			 */

			String result = "";
			// No public read so we have to calculate a signed url
			if ("0".equals(public_read)) {
				Integer expTime = Integer.valueOf(expirationTime).intValue();
				result = s3service.createSignedGetUrl(bucketName, object,
						awsCredentials, calculateExpiryTime(expTime));
			}

			// Public read activated so we have to give a public url
			else {

				// This sentence was used with jets3 version < 0.7.x
				// String bucket_url =
				// s3service.generateS3HostnameForBucket(bucketName, false);

				// Use this sentences when upgrade jets3 to 0.7.x
				String bucket_url = s3service.generateS3HostnameForBucket(
						bucketName, false);
				if (!bucket_url.contains("http") == true) {
					bucket_url = "http://" + bucket_url;
				}

				if (!bucket_url.endsWith("/")) {
					bucket_url = bucket_url + "/";
				}
				result = bucket_url + object;
			}
			if (result.contains("https") == true) {
				result = result.replace("https", "http");
			}
			return urlAddExtension(object, result);
		} catch (S3ServiceException e) {
			log.error("Error getting tickect from S3");
			return "";
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			return "";
		}
	}

	/*
	 * @see org.iavante.sling.s3backend.S3backend#upload
	 */
	public String upload(Node contentNode) {
		return null;
	}

	private String urlAddExtension(String file, String url) {
		String ext = extractFileExtension(file);
		if (ext == null)
			return url;
		else {
			return url + "&ext=." + ext;
		}
	}

	/**
	 * Extracts extension from the file name. Dot is not included in the
	 * returned string.
	 */
	public static String extractFileExtension(String fileName) {
		int dotInd = fileName.lastIndexOf('.');

		// if dot is in the first position,
		// we are dealing with a hidden file rather than an extension
		return (dotInd > 0 && dotInd < fileName.length()) ? fileName
				.substring(dotInd + 1) : null;
	}

	/**
	 * @see org.iavante.sling.s3backend.S3backend
	 */
	public String upload(String filepath) {
		String results = null;

		try {
			// get the bucket
			S3Bucket bucket = s3service.getBucket(bucketName);

			// create an object with the file data
			File fileData = new File(filepath);
			S3Object fileObject = new S3Object(bucket, fileData);

			// Public read so we have to give public read permissions
			if ("1".equals(public_read)) {
				fileObject.setAcl(AccessControlList.REST_CANNED_PUBLIC_READ);
			}

			// put the data on S3
			s3service.putObject(bucket, fileObject);
			if (log.isInfoEnabled())
				log.info("Successfully uploaded object - " + fileObject);
			results = "200";

		} catch (S3ServiceException e) {
			log.error("Error uploading file to S3");
			return "500";

		} catch (NoSuchAlgorithmException e) {
			log.error("Error uploading file to S3");
			e.printStackTrace();
		} catch (IOException e) {
			log.error("Error getting data from file");
			e.printStackTrace();
		}

		return results;
	}

	/**
	 * @see org.iavante.sling.s3backend.S3backend
	 */
	public String upload(String filepath, String bucket_name) {
		String results = null;

		try {
			// get the bucket
			S3Bucket bucket = s3service.getBucket(bucket_name);

			// create an object with the file data
			File fileData = new File(filepath);
			S3Object fileObject = new S3Object(bucket, fileData);

			// put the data on S3
			s3service.putObject(bucket, fileObject);
			if (log.isInfoEnabled())
				log.info("Successfully uploaded object - " + fileObject);
			results = "200";

		} catch (S3ServiceException e) {
			log.error("Error uploading file to S3");
			return "500";

		} catch (NoSuchAlgorithmException e) {
			log.error("Error uploading file to S3");
			e.printStackTrace();
		} catch (IOException e) {
			log.error("Error getting data from file");
			e.printStackTrace();
		}
		return results;
	}

	@Override
	public void configure(Map<String, String> prop_dict) {
		if (log.isInfoEnabled())
			log.info("Custom configuration");
		initialize(prop_dict);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.iavante.sling.s3backend.S3backend#get_player(javax.servlet.ServletRequest
	 * , java.util.HashMap)
	 */
	public String get_player(HttpServletRequest req,
			HashMap<String, Object> kwargs) {
		String player_ds = get_ds_player(req, kwargs);
		String custom_player = get_override_request_player(req, player_ds);
		return custom_player;
	}

	private void initialize(Map<String, String> prop_dict) {
		String key = (String) prop_dict.get("key");
		String secret = (String) prop_dict.get("secret");

		if (key != null && secret != null) {
			awsCredentials = null;
			awsCredentials = new AWSCredentials(key, secret);
			try {
				s3service = new RestS3Service(awsCredentials);
			} catch (Exception e) {
				assert false;
				e.printStackTrace();
			}
		}
		if (prop_dict.get("bucket") != null)
			bucketName = (String) prop_dict.get("bucket");

		if (prop_dict.get("expirationTime") != null)
			expirationTime = (String) prop_dict.get("expirationTime");

		if (prop_dict.get("public_read") != null)
			public_read = (String) prop_dict.get("public_read");

		if (prop_dict.get("player") != null)
			player = (String) prop_dict.get("player");

		if (prop_dict.get("player_playlist_layout") != null)
			player_playlist_layout = (String) prop_dict
					.get("player_playlist_layout");

		if (prop_dict.get("player_autostart") != null)
			player_autostart = (String) prop_dict.get("player_autostart");

		if (prop_dict.get("player_width") != null)
			player_width = (String) prop_dict.get("player_width");

		if (prop_dict.get("player_height") != null)
			player_height = (String) prop_dict.get("player_height");

		if (prop_dict.get("player_repeat") != null) {
			player_repeat = (String) prop_dict.get("player_repeat");
		}

		if (prop_dict.get("player_plugins") != null)
			player_plugins = (String) prop_dict.get("player_plugins");

		if (prop_dict.get("player_plugin_playlist") != null)
			player_plugin_playlist = (String) prop_dict
					.get("player_plugin_playlist");

		if (prop_dict.get("player_plugin_playlist") != null)
			player_plugin_playlist = (String) prop_dict
					.get("player_plugin_playlist");

		if (prop_dict.get("player_plugin_playlist_params") != null)
			player_plugin_playlist_params = (String) prop_dict
					.get("player_plugin_playlist_params");

	}

	/**
	 * Configure the player with the distribution server settings and channel
	 * settings. The channel settings override the default distribution server
	 * properties
	 * 
	 * @param req
	 * @param kwargs
	 * @return
	 */
	private String get_ds_player(HttpServletRequest req,
			HashMap<String, Object> kwargs) {
		String player_ds = this.player;
		String playlist = "none";

		player_ds = player_ds.replace("[[video_url]]", kwargs.get("video_url")
				.toString());
		player_ds = player_ds.replace("[[tvicon]]", kwargs.get("tvicon")
				.toString());

		player_ds = player_ds.replace("[[player_plugins]]", player_plugins);

		if ((Boolean) kwargs.get("support_playlist")) {
			//player_ds = player_ds.replace("[[player_plugins]]", player_plugins
			//		+ "," + player_plugin_playlist + "&"
			//		+ player_plugin_playlist_params);
			
		    playlist = player_playlist_layout;
		}

		player_ds = player_ds.replace("[[player_plugin_playlist_params]]", player_plugin_playlist_params);
        player_ds = player_ds.replace("[[player_playlist_layout]]", playlist);      
		player_ds = player_ds.replace("[[player_autostart]]", player_autostart);
		player_ds = player_ds.replace("[[player_height]]", player_height);
		player_ds = player_ds.replace("[[player_width]]", player_width);
		player_ds = player_ds.replace("[[player_repeat]]", player_repeat);

		return player_ds;
	}

	/**
	 * Configure the player with the request settings. The request settings
	 * override the ds and channel settings.
	 * 
	 * @param req
	 * @param player
	 * @return
	 */
	private String get_override_request_player(HttpServletRequest req,
			String player) {

		if (req.getParameter("player_autostart") != null) {
			String autostart = req.getParameter("player_autostart");
			player = player.replaceFirst("autostart\\=[a-zA-Z]*", "autostart="
					+ autostart);
		}

		if (req.getParameter("player_playlist_layout") != null) {
			;
			String playlist_layout = req.getParameter("player_playlist_layout");
			player = player.replaceFirst("playlist\\=[a-zA-Z]*", "playlist="
					+ playlist_layout);
		}

		if (req.getParameter("player_repeat") != null) {
			String repeat = req.getParameter("player_repeat");
			player = player.replaceFirst("repeat\\=[a-zA-Z]*", "repeat="
					+ repeat);
		}

		if (req.getParameter("player_plugins") != null) {
			String plugins = req.getParameter("player_plugins");
			player = player.replaceFirst("plugins\\=[a-zA-Z|,|-|0-9]*",
					"plugins=" + plugins);
		}
		if (req.getParameter("player_plugin_playlist") != null) {
			String plugins = req.getParameter("player_plugins");
			String playerPluginPlaylist = req
					.getParameter("player_plugin_playlist");
			String playerPluginPlaylistParams = req
					.getParameter("player_plugin_playlist_params");
			player = player.replaceFirst("plugins\\=[a-zA-Z|,|-|0-9]*",
					"plugins=" + plugins + "," + playerPluginPlaylist + "&"
							+ playerPluginPlaylistParams);
		}

		if (req.getParameter("player_height") != null) {
			String height = req.getParameter("player_height");
			player = player.replaceFirst("height\\=\\'[0-9]*\\'", "height='"
					+ height + "'");
		}

		if (req.getParameter("player_width") != null) {
			String width = req.getParameter("player_width");
			player = player.replaceFirst("width=\\'[0-9]*\\'", "width='"
					+ width + "'");
		}

		return player;

	}

	/**
	 * Delete an object within a bucket
	 * 
	 * @param objectKey
	 * @return
	 */
	private String delete(String objectKey) {
		String results = null;

		try {
			// get the bucket
			S3Bucket bucket = s3service.getBucket(bucketName);

			// put the data on S3
			s3service.deleteObject(bucket, objectKey);
			results = "Successfully deleted object - " + objectKey;

		} catch (S3ServiceException e) {
			log.error("Error deleting file from S3");
			return "";

		}

		return results;
	}

	// --------------------- Internal -------------------

	/**
	 * Calculate the expiration time from now.
	 * 
	 * @param secondsUntilExpiry
	 * @return the date and time when signed URLs should expire, calculated by
	 *         adding the number of seconds until expiry to the current time.
	 */
	protected Date calculateExpiryTime(int secondsUntilExpiry) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.SECOND, secondsUntilExpiry);
		return cal.getTime();
	}
}
