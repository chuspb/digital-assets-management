/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.searcher.services.impl;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.xmlio.out.XMLWriter;

public class SearcherXmlWriter implements Serializable {
	private static final long serialVersionUID = 1L;

	/** Search result iterator. */
	private Iterator<HashMap<String, String>> resultIt;

	/** Result counter. */
	private long count;

	/** Writer */
	private PrintWriter writer;

	// ----------- Constructors ------------------

	public SearcherXmlWriter() {
	}

	public SearcherXmlWriter(ArrayList<HashMap<String, String>> results,
			PrintWriter respwriter, int rows) {
		count = rows;
		resultIt = results.iterator();
		writer = respwriter;
	}

	/**
	 * Parse to XML format.
	 */
	public void writeOutput() {
		/* Out format: XML */

		try {
			final XMLWriter w = new XMLWriter(writer);
			w.writeXMLDeclaration();
			w.setPrettyPrintMode(true);
			w.writeStartTag(
					"<Result xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">",
					true);
			if (resultIt.hasNext()) {
				HashMap<String, String> counter = resultIt.next();
				w.writeElementWithPCData("<counter>", counter.get("count"),
						"</counter>");
				w.writeStartTag("<items>");
				while (resultIt.hasNext() && count != 0) {
					HashMap<String, String> row = resultIt.next();

					w.writeStartTag("<item>");

					Set<String> keys = row.keySet();
					Iterator<String> it = keys.iterator();
					int count = 0;

					while (it.hasNext()) {
						String key = it.next();
						if ("channels".compareToIgnoreCase(key) == 0) {
							w.writeStartTag("<" + key + ">");
							for (String channels : row.get(key).split("&&")) {
								if (channels.length() != 0) {
									w.writeStartTag("<channel>");
									w.writeElementWithCData("<path>", channels.split("&")[0],
											"</path>");
									w.writeElementWithCData("<title>", channels.split("&")[1],
											"</title>");
									w.writeEndTag("</channel>");
								}
							}
							w.writeEndTag("</" + key + ">");
						} else
							w.writeElementWithCData("<" + key + ">", row.get(key), "</" + key
									+ ">");
					}
					w.writeEndTag("</item>");
					count--;
				}
				w.writeEndTag("</items>");
			}
			w.writeEndTag("</Result>", true);

		} catch (IOException je) {
			je.printStackTrace();
		}
	}

	/**
	 * Returns the content type.
	 * 
	 * @return
	 */
	public String getContentType() {
		return "text/xml";
	}
}
