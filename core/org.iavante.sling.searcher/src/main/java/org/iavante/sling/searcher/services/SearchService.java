/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.searcher.services;

import java.util.ArrayList;
import java.util.HashMap;

import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;

/**
 * Search in the JCR.
 * 
 * @scr.property name="service.description" value="IAVANTE - Search Service"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */

public interface SearchService {

	/**
	 * Make a search to the JCR from string object more info in
	 * http://info.iavante.es/display/GAD2/API+de+busqueda.
	 * 
	 * @param searchstring
	 *          string to be searched
	 * @return a ArrayList<HashMap<String,String>> object where each map is a
	 *         result of the query with the keys: path,resourceType and exceptr
	 * @throws ValueFormatException
	 * @throws IllegalStateException
	 * @throws RepositoryException
	 */
	public ArrayList<HashMap<String, String>> search(String searchstring)
			throws ValueFormatException, IllegalStateException, RepositoryException;

	/**
	 * Make a search to the JCR from string object more info in
	 * http://info.iavante.es/display/GAD2/API+de+busqueda.
	 * 
	 * @param searchstring
	 *          string to be searched
	 * @param operator
	 *          logical operator in search
	 * @return a ArrayList<HashMap<String,String>> object where each map is a
	 *         result of the query with the keys: path,resourceType and exceptr
	 * @throws ValueFormatException
	 * @throws IllegalStateException
	 * @throws RepositoryException
	 */
	public ArrayList<HashMap<String, String>> search(String searchstring,
			String operator) throws ValueFormatException, IllegalStateException,
			RepositoryException;

	/**
	 * Make a search to the JCR from string object more info in
	 * http://info.iavante.es/display/GAD2/API+de+busqueda.
	 * 
	 * @param searchstring
	 *          string to be searched
	 * @param path
	 *          is the path of query descent
	 * @param fullinfo
	 *          if true will return full info from result nodes
	 * @return a ArrayList<HashMap<String,String>> object where each map is a
	 *         result of the query with the keys: path,resourceType, ...
	 * @throws ValueFormatException
	 * @throws IllegalStateException
	 * @throws RepositoryException
	 */
	public ArrayList<HashMap<String, String>> search(String searchstring,
			String path, boolean fullinfo) throws ValueFormatException,
			IllegalStateException, RepositoryException;

	/**
	 * Make a search to the JCR from string object more info in
	 * http://info.iavante.es/display/GAD2/API+de+busqueda.
	 * 
	 * @param searchstring
	 *          string to be searched
	 * @param path
	 *          is the path list of query descent
	 * @param fullinfo
	 *          if true will return full info from result nodes
	 * @return a ArrayList<HashMap<String,String>> object where each map is a
	 *         result of the query with the keys: path,resourceType, ...
	 * @throws ValueFormatException
	 * @throws IllegalStateException
	 * @throws RepositoryException
	 */
	public ArrayList<HashMap<String, String>> search(String searchstring,
			String[] path, boolean fullinfo) throws ValueFormatException,
			IllegalStateException, RepositoryException;

	/**
	 * Make a search to the JCR from string object more info in
	 * http://info.iavante.es/display/GAD2/API+de+busqueda.
	 * 
	 * @param searchstring
	 *          string to be searched
	 * @param path
	 *          is the path list of query descent
	 * @param fullinfo
	 *          if true will return full info from result nodes
	 * @param operator
	 *          the logical operator in search
	 * @return a ArrayList<HashMap<String,String>> object where each map is a
	 *         result of the query with the keys: path,resourceType, ...
	 * @throws ValueFormatException
	 * @throws IllegalStateException
	 * @throws RepositoryException
	 */
	public ArrayList<HashMap<String, String>> search(String searchstring,
			String[] path, boolean fullinfo, String operator)
			throws ValueFormatException, IllegalStateException, RepositoryException;

	/**
	 * Make a search to the JCR from string object more info in
	 * http://info.iavante.es/display/GAD2/API+de+busqueda.
	 * 
	 * @param searchstring
	 *          string to be searched
	 * @param path
	 *          is the path of query descent
	 * @param fullinfo
	 *          if true will return full info from result nodes
	 * @param offset
	 *          the first element of query result to return
	 * @param items
	 *          number of items to return
	 * @param order
	 *          if the order is descending or ascending, the default is descending
	 * @param order_by
	 *          the field to order: title, path, author....
	 * @return a ArrayList<HashMap<String,String>> object where each map is a
	 *         result of the query with the keys: path,resourceType,exceptr ...
	 * @throws ValueFormatException
	 * @throws IllegalStateException
	 * @throws RepositoryException
	 */
	public ArrayList<HashMap<String, String>> search(String searchstring,
			String path, boolean fullinfo, int offset, int items, String order,
			String order_by) throws ValueFormatException, IllegalStateException,
			RepositoryException;

	/**
	 * Make a search to the JCR from string object more info in
	 * http://info.iavante.es/display/GAD2/API+de+busqueda.
	 * 
	 * @param searchstring
	 *          string to be searched
	 * @param path
	 *          is the path of query descent
	 * @param fullinfo
	 *          if true will return full info from result nodes
	 * @param offset
	 *          the first element of query result to return
	 * @param items
	 *          number of items to return
	 * @param order
	 *          if the order is descending or ascending, the default is descending
	 * @param order_by
	 *          the field to order: title, path, author....
	 * @param operator
	 *          the logical operator used in search
	 * @param related
	 *          if the search has to be related, -1 not, and other value is the
	 *          related ratio
	 * @return a ArrayList<HashMap<String,String>> object where each map is a
	 *         result of the query with the keys: path,resourceType,exceptr ...
	 * @throws ValueFormatException
	 * @throws IllegalStateException
	 * @throws RepositoryException
	 */
	public ArrayList<HashMap<String, String>> search(String searchstring,
			String path, boolean fullinfo, int offset, int items, String order,
			String order_by, String operator, double related)
			throws ValueFormatException, IllegalStateException, RepositoryException;

	/**
	 * Make a search filter by a resourceType.
	 * 
	 * @param searchstring
	 *          to be searched
	 * @param resourceType
	 *          string with the resourceType to filter
	 * @return a ArrayList<HashMap<String,String>> object where each map is a
	 *         result of the query with the keys: path,resourceType, ...
	 * @throws ValueFormatException
	 * @throws IllegalStateException
	 * @throws RepositoryException
	 */
	public ArrayList<HashMap<String, String>> searchFilteredByResourceType(
			String searchstring, String resourceType) throws ValueFormatException,
			IllegalStateException, RepositoryException;

	/**
	 * Make a search filter by a resourceType.
	 * 
	 * @param searchstring
	 *          to be searched
	 * @param resourceType
	 *          string with the resourceType to filter
	 * @param fullinfo
	 *          if true will return full info from result nodes
	 * @return a ArrayList<HashMap<String,String>> object where each map is a
	 *         result of the query with the keys: path,resourceType, ...
	 * @throws ValueFormatException
	 * @throws IllegalStateException
	 * @throws RepositoryException
	 */
	public ArrayList<HashMap<String, String>> searchFilteredByResourceType(
			String searchstring, String resourceType, boolean fullinfo)
			throws ValueFormatException, IllegalStateException, RepositoryException;

	/**
	 * Make a search filter by a resourceType.
	 * 
	 * @param searchstring
	 *          to be searched
	 * @param resourceType
	 *          string with the resourceType to filter
	 * @param fullinfo
	 *          if true will return full info from result nodes
	 * @return a ArrayList<HashMap<String,String>> object where each map is a
	 *         result of the query with the keys: path,resourceType, ...
	 * @throws ValueFormatException
	 * @throws IllegalStateException
	 * @throws RepositoryException
	 */
	public ArrayList<HashMap<String, String>> searchFilteredByResourceType(
			String searchstring, String resourceType, int fullinfo)
			throws ValueFormatException, IllegalStateException, RepositoryException;

	/**
	 * Make a search filter by a resourceType.
	 * 
	 * @param searchstring
	 *          to be searched
	 * @param path
	 *          is the path of query descent
	 * @param resourceType
	 *          string with the resourceType to filter
	 * @param fullinfo
	 *          if true will return full info from result nodes
	 * @param offset
	 *          the first element of query result to return
	 * @param items
	 *          number of items to return
	 * @param order
	 *          if the order is descending or ascending, the default is descending
	 * @param order_by
	 *          the field to order: title, path, author....
	 * @param operator
	 *          logical operator in search
	 * @param related
	 *          if the search has to be related, -1 not, and other value is the
	 *          related ratio
	 * @return a ArrayList<HashMap<String,String>> object where each map is a
	 *         result of the query with the keys: path,resourceType,...
	 * @throws ValueFormatException
	 * @throws IllegalStateException
	 * @throws RepositoryException
	 */
	public ArrayList<HashMap<String, String>> searchFilteredByResourceType(
			String searchstring, String path, String resourceType, boolean fullinfo,
			int offset, int items, String order, String order_by, String operator,
			double related) throws ValueFormatException, IllegalStateException,
			RepositoryException;
}
