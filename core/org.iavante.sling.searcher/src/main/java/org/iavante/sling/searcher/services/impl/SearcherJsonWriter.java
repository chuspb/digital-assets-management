/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.searcher.services.impl;

import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.io.JSONWriter;
import org.apache.sling.servlets.get.impl.helpers.JsonRendererServlet;

/**
 * Write the search result in JSON format.
 */
public class SearcherJsonWriter implements Serializable {
	private static final long serialVersionUID = 1L;

	/** Search result iterator. */
	private Iterator<HashMap<String, String>> resultIt;

	/** Result counter. */
	private long count;

	/** Writer */
	PrintWriter writer;

	// ----------- Constructors ------------------
	public SearcherJsonWriter() {
	}

	public SearcherJsonWriter(ArrayList<HashMap<String, String>> results,
			PrintWriter respwriter, int rows) {

		count = rows;
		resultIt = results.iterator();
		writer = respwriter;
	}

	/**
	 * Parse to JSON format.
	 */
	public void writeOutput() {
		/* Out format: JSON */

		try {
			final JSONWriter w = new JSONWriter(writer);
			w.object();
			w.key("Result");
			w.object(); // counter
			w.key("counter");

			if (resultIt.hasNext()) {
				HashMap<String, String> counter = resultIt.next();
				w.value(counter.get("count"));
				w.key("results");

				w.array();
				while (resultIt.hasNext() && count != 0) {
					HashMap<String, String> row = resultIt.next();

					Set<String> keys = row.keySet();
					Iterator<String> it = keys.iterator();
					w.object();

					while (it.hasNext()) {
						String key = it.next();
						if ("channels".compareToIgnoreCase(key) == 0) {
							w.key(key);
							w.array();
							for (String channels : row.get(key).split("&&")) {
								if (channels.length() != 0) {
									w.object();
									w.key("channel");
									w.array();
									w.object();
									w.key("path");
									w.value(channels.split("&")[0]);
									w.endObject();
									w.object();
									w.key("title");
									w.value(channels.split("&")[1]);
									w.endObject();
									w.endArray();
									w.endObject();
								}
							}
							w.endArray();
						} else {
							w.key(key);
							w.value(row.get(key));
						}
					}
					w.endObject();
					count--;
				}
				w.endArray();
				w.endObject(); // endresults
			} else {
				w.value("0");
				w.key("results");
				w.array();
				w.endArray();
			}
			w.endObject(); // result

		} catch (JSONException je) {
			je.printStackTrace();
		}
	}

	public void writeSimpleOutput() {
		/* Out format: JSON */

		try {
			final JSONWriter w = new JSONWriter(writer);
			w.array();
			resultIt.next();
			while (resultIt.hasNext() && count != 0) {
				HashMap<String, String> row = resultIt.next();
				w.object();
				Set<String> keys = row.keySet();
				Iterator<String> it = keys.iterator();

				while (it.hasNext()) {
					String key = it.next();
					w.key(key);
					w.value(row.get(key));
				}
				w.endObject();
				count--;
			}
			w.endArray();

		} catch (JSONException je) {
			je.printStackTrace();
		}
	}

	/**
	 * Returns the content type.
	 * 
	 * @return
	 */
	public String getContentType() {
		return JsonRendererServlet.responseContentType;
	}
}
