/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.searcher.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.io.PrintWriter;

/**
 * Search in the JCR.
 * 
 * @scr.property name="service.description"
 *               value="IAVANTE - Print Search Service"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */
public interface SearchPrintService {

	/**
	 * write to the printer the formated output of the values in result with the
	 * format <b>format</b>
	 * 
	 * @param format
	 *          the output format, can be json or xml
	 * @param result
	 *          the list with the results of query
	 * @param printer
	 *          the printer to write to
	 * @param rows
	 *          number of rows to print
	 */
	public void writeOutput(String format,
			ArrayList<HashMap<String, String>> result, PrintWriter printer, int rows);

	/**
	 * write to the printer the formated output of the values in result with the
	 * format <b>format</b> in a simple way
	 * 
	 * @param format
	 *          the output format, can be json or xml
	 * @param result
	 *          the list with the results of query
	 * @param printer
	 *          the printer to write to
	 * @param rows
	 *          number of rows to print
	 */
	public void writeSimpleOutput(String format,
			ArrayList<HashMap<String, String>> result, PrintWriter printer, int rows);

	/**
	 * return the content type of the format
	 * 
	 * @param format
	 *          the format to get the content type
	 * @return a string with the contentType of the format. Ex: text/xml
	 */
	public String getContentType(String format);

}
