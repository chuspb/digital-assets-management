<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page language="java" contentType="text/json; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.lang.String"%>
<%@ page import="java.util.Enumeration"%>
<%@ page import="javax.jcr.*,org.apache.sling.api.resource.Resource"%>
<%@ page import="org.iavante.sling.searcher.services.SearchService"%>
<%@ page import="org.iavante.sling.searcher.services.SearchPrintService"%>
<%@ page import="org.iavante.sling.commons.services.AuthToolsService"%>
<%@ page import="org.apache.sling.engine.auth.AuthenticationInfo"%>
<%@ page import="java.io.PrintWriter"%>
<%@ page import="org.apache.commons.codec.binary.Base64"%>

<%@ taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>

<%@page import="java.net.URLDecoder"%><sling:defineObjects />

<%
/** Number of rows */
int rows = 100;

/** Properties of service */
Map<String, String> searcherProperties = null;
searcherProperties = sling.getService(org.iavante.sling.commons.services.PortalSetup.class).get_config_properties("/searcher");

// Get request parameters
Enumeration<String> enu = request.getParameterNames();
String par = null;
String enc = null;
String spath = null;
String rstype = null;
String order = null;
String orderBy = null;
double related = -1;
String operator = "and";
int offset = -1;
int items = -1;
boolean fullinfo = false;
/** predefined output format */
String outformat = "json";

ArrayList<HashMap<String, String>> result = new ArrayList<HashMap<String, String>>();

/* Credentials */
AuthenticationInfo credentials = sling.getService(org.iavante.sling.commons.services.AuthToolsService.class).extractAuthentication(request);

SimpleCredentials authInfo = null;
if (credentials != null)
	authInfo = (SimpleCredentials) credentials.getCredentials();


assert searcherProperties != null;
assert authInfo != null;

while (enu.hasMoreElements()) {
	String name = enu.nextElement();
	if (name.compareToIgnoreCase("key") == 0)
		par = request.getParameter(name);
	else if (name.compareToIgnoreCase("encoding") == 0)
		enc = request.getParameter(name);
	else if (name.compareToIgnoreCase("path") == 0)
		spath = request.getParameter(name);
	else if (name.compareToIgnoreCase("type") == 0)
		rstype = request.getParameter(name);
	else if (name.compareToIgnoreCase("format") == 0) {
		outformat = request.getParameter("format").toLowerCase();
		if (outformat.compareToIgnoreCase("xml") != 0
				&& outformat.compareToIgnoreCase("json") != 0) {
			outformat = "json";
		} else if (outformat.compareToIgnoreCase("xml") == 0) {
			outformat = "xml";
		}
	} else if (name.compareToIgnoreCase("fullinfo") == 0) {
		if (request.getParameter("fullinfo").compareToIgnoreCase("true") == 0)
			fullinfo = true;
		else
			fullinfo = false;
	} else if (name.compareToIgnoreCase("order") == 0) {
		order = request.getParameter("order");
	} else if (name.compareToIgnoreCase("order_by") == 0) {
		orderBy = request.getParameter("order_by");

	} else if (name.compareToIgnoreCase("offset") == 0) {
		try {
			offset = Integer.valueOf(request.getParameter("offset"));
		} catch (java.lang.NumberFormatException ex) {
			offset = -1;
		}
	} else if (name.compareToIgnoreCase("items") == 0) {
		try {
			items = Integer.valueOf(request.getParameter("items"));
		} catch (java.lang.NumberFormatException ex) {
			items = -1;
		}

	}else if(name.compareToIgnoreCase("operator")==0){
		operator = request.getParameter(name);
	
	}else if(name.compareToIgnoreCase("related")==0){
		try{
			related = Double.valueOf(request.getParameter(name));
		} catch (java.lang.NumberFormatException ex) {
			related = 0.8;
		}
	}
}
if (par == null)
	par = "";
if (enc != null){
	if (enc.compareToIgnoreCase("base64") == 0) {
		par = new String(Base64.decodeBase64(par.getBytes()));
	} else if ( enc.compareToIgnoreCase("urlencode") == 0) {
		par = URLDecoder.decode(par,"UTF-8");
	}else {
		par = "";
	}
}
if (spath == null)
	spath = searcherProperties.get("scope") + "/" + authInfo.getUserID();

try {
	if (rstype == null) {
		result = sling.getService(org.iavante.sling.searcher.services.SearchService.class).search(par, spath, fullinfo, offset, items,
				order, orderBy,operator,related);
	} else {
		result = sling.getService(org.iavante.sling.searcher.services.SearchService.class).searchFilteredByResourceType(par, spath, rstype,
				fullinfo, offset, items, order, orderBy, operator,related);
	}
} catch (ValueFormatException e2) {
	e2.printStackTrace();
} catch (IllegalStateException e2) {
	e2.printStackTrace();
} catch (RepositoryException e2) {	
	e2.printStackTrace();
}

/* Write the output */
sling.getService(SearchPrintService.class).writeOutput(outformat,result,response.getWriter(),rows);
response.setContentType(sling.getService(SearchPrintService.class).getContentType(outformat));
response.setCharacterEncoding("UTF-8");


%>