/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.searcher;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Iterator;

import static org.junit.Assert.*;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.*;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.*;

/**
 * Test search bundle
 * 
 */
public class SearcherServiceTestIT {

	static private final String HOSTVAR = "SLINGHOST";
	static private String HOSTPREDEF = "localhost:8888";
	static private String SLING_URL = "http://";
	static private String SEARCH_URL = "/content/search";
	static private final String COLLECTIONS_URL = "/content/colecciones";
//	static private final String TITLE = "Test_Search_Title";
	static private final String SOURCE_TITLE = "Fuente1";
	static private final String DESCRIPTION = "descripcion aproximada";
	static private final String TITLE_CONTENT = "tests_search_title";
	static private final String SCHEMA = "default";
	static private final String COLLECTION = "admin";
	static private final String SEARCH_KEY = "descripcion";
	static private final String SEARCH_KEY_BASE64 = "ZGVzY3JpcGNpb24=";
	static private final String TYPE_CONTENT = "gad%2Fcontent";
	static private final String TYPE_SOURCE = "gad%2Fsource";
	static private final String SOURCEPATH = "%2Fcontent%2Fconfig";

	static private Credentials defaultcreds;
	static private List<String> authPrefs = new ArrayList<String>(2);

	static HttpClient client;

	@org.junit.BeforeClass
	static public void initialize() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		/* get env params */
		Map<String, String> envs = System.getenv();
		Set<String> keys = envs.keySet();

		Iterator<String> it = keys.iterator();
		boolean hashost = false;
		while (it.hasNext()) {
			String key = (String) it.next();
			if (key.compareToIgnoreCase(HOSTVAR) == 0) {
				SLING_URL = SLING_URL + (String) envs.get(key);
				hashost = true;
				break;
			}
		}
		if (hashost == false)
			SLING_URL = SLING_URL + HOSTPREDEF;
		
		
		/* make http connection */
		client = new HttpClient();
		authPrefs.add(AuthPolicy.DIGEST);
		authPrefs.add(AuthPolicy.BASIC);
		defaultcreds = new UsernamePasswordCredentials("admin", "admin");
		PostMethod post_create_col;

		/* tests if the collection exists */
		GetMethod get_create_col = new GetMethod(SLING_URL + COLLECTIONS_URL + "/"
				+ COLLECTION);

		get_create_col.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);
		try {
			client.executeMethod(get_create_col);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		if (get_create_col.getStatusCode() != 404) {
			deletecontent();
		}

		/* create a collection */
		post_create_col = new PostMethod(SLING_URL + COLLECTIONS_URL + "/*");

		post_create_col.setDoAuthentication(true);

		NameValuePair[] data_create_col = {
				new NameValuePair("sling:resourceType", "gad/collection"),
				new NameValuePair("title", COLLECTION),
				new NameValuePair("subtitle", ""), new NameValuePair("schema", SCHEMA),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };
		post_create_col.setRequestBody(data_create_col);
		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);
		try {
			client.executeMethod(post_create_col);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals("Creando la coleccion", 201, post_create_col.getStatusCode());
		post_create_col.releaseConnection();
		// collection created
		// Get the content
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		/* create a content */
		post_create_col = new PostMethod(SLING_URL + COLLECTIONS_URL + "/"
				+ COLLECTION + "/contents/*");

		post_create_col.setDoAuthentication(true);

		NameValuePair[] data_create_col_content = {
				new NameValuePair("sling:resourceType", "gad/content"),
				new NameValuePair("title", TITLE_CONTENT),
				new NameValuePair("author", "test_autor@tests.com"),
				new NameValuePair("schema", SCHEMA),
				new NameValuePair("description", DESCRIPTION),
				new NameValuePair("origin", "final cut"),
				new NameValuePair("lang", "es"),
				new NameValuePair("tags", "recetas equilibradas"),
				new NameValuePair("tags@TypeHint", "String[]"),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };
		post_create_col.setRequestBody(data_create_col_content);
		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);
		try {

			client.executeMethod(post_create_col);

		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals("Creando contenido", 201, post_create_col.getStatusCode());
		post_create_col.releaseConnection();
		// Content created
		// Get the content
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

		/* create a source */
		post_create_col = new PostMethod(SLING_URL + COLLECTIONS_URL + "/"
				+ COLLECTION + "/contents/tests_search_title/sources/*");

		post_create_col.setDoAuthentication(true);

		NameValuePair[] data_create_col_source = {
				new NameValuePair("sling:resourceType", "gad/source"),
				new NameValuePair("title", SOURCE_TITLE),
				new NameValuePair("author", "test_autor@tests.com"),
				new NameValuePair("schema", SCHEMA),
				new NameValuePair("description", DESCRIPTION),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };
		post_create_col.setRequestBody(data_create_col_source);
		// post.setDoAuthentication(true);
		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);
		try {

			client.executeMethod(post_create_col);

		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		// Get the content

		assertEquals("Creando fuente", 201, post_create_col.getStatusCode());
		post_create_col.releaseConnection();
		// Source created
		
		/* send to review */
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		post_create_col = new PostMethod(SLING_URL + COLLECTIONS_URL + "/"
				+ COLLECTION + "/contents/"+TITLE_CONTENT);

		post_create_col.setDoAuthentication(true);

		NameValuePair[] data_send_review = {
				new NameValuePair(":operation", "copy"),
				new NameValuePair(":dest","/content/catalogo/pendientes/"),
				new NameValuePair("replace", "true")};
		post_create_col.setRequestBody(data_send_review);
		// post.setDoAuthentication(true);
		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);
		try {

			client.executeMethod(post_create_col);

		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		// test if sent

		assertEquals("Mandando a revision", 201, post_create_col.getStatusCode());
		post_create_col.releaseConnection();
		// send to review finished

	}

	@org.junit.AfterClass
	static public void deletecontent() {
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		/* delete collection */
		PostMethod post_create_col = new PostMethod(SLING_URL + COLLECTIONS_URL
				+ "/" + COLLECTION);

		post_create_col.setDoAuthentication(true);

		NameValuePair[] data_create_col = { new NameValuePair(":operation",
				"delete"), };
		post_create_col.setRequestBody(data_create_col);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);
		try {

			client.executeMethod(post_create_col);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals("Deleting content", 200, post_create_col.getStatusCode());
		post_create_col.releaseConnection();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}

	}

	@org.junit.Test
	public void test_emptySearchAscii() {

		// Empty search
		String param = "?key=jasndkjasjkdn897hdioa";
		String body = null;
		GetMethod empty_search = new GetMethod(SLING_URL + SEARCH_URL + param);

		empty_search.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		try {
			client.executeMethod(empty_search);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertEquals("emptySearchAscii", 200, empty_search.getStatusCode());
		try {
			body = empty_search.getResponseBodyAsString();
			assertTrue("emptySearch", body.contains("[]"));
		} catch (IOException e) {
			assert false;
			e.printStackTrace();
		}

		empty_search.releaseConnection();

	}

	@org.junit.Test
	public void test_emptySearchBase64() {

		/*
		 * Empty search key ascii: calksmdasda87sd98asdklajsh key base64:
		 * Y2Fsa3NtZGFzZGE4N3NkOThhc2RrbGFqc2g=
		 */

		String param = "?key=Y2Fsa3NtZGFzZGE4N3NkOThhc2RrbGFqc2g=&encoding=base64";
		String body = null;
		GetMethod empty_search = new GetMethod(SLING_URL + SEARCH_URL + param);

		empty_search.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		try {
			client.executeMethod(empty_search);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertEquals(200, empty_search.getStatusCode());
		try {
			body = empty_search.getResponseBodyAsString();
			assertTrue("Empty search with base64", body.contains("[]"));
		} catch (IOException e) {
			assert false;
			e.printStackTrace();
		}

		empty_search.releaseConnection();

	}

	@org.junit.Test
	public void test_SearchAscii() {

		// Empty search
		String param = "?key=" + SEARCH_KEY + "&type=" + TYPE_CONTENT;
		String body = null;
		GetMethod empty_search = new GetMethod(SLING_URL + SEARCH_URL + param);

		empty_search.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		try {
			client.executeMethod(empty_search);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertEquals(200, empty_search.getStatusCode());
		try {
			body = empty_search.getResponseBodyAsString();
			assertTrue(
					"SearchAscii comparing response path",
					body
							.contains("{\"Result\":{\"counter\":\"1\",\"results\":[{\"path\":\"/content/colecciones/admin/contents/tests_search_title\",\"extract\":"));
			assertTrue("SearchAscii comparing response resourcetype", body
					.contains("\"gad/content\""));
		} catch (IOException e) {
			assertFalse(true);

		}

		empty_search.releaseConnection();

	}

	@org.junit.Test
	public void test_SearchBase64() {

		// Empty search
		String param = "?key=" + SEARCH_KEY_BASE64 + "&encoding=base64" + "&type="
				+ TYPE_CONTENT;
		String body = null;
		GetMethod empty_search = new GetMethod(SLING_URL + SEARCH_URL + param);

		empty_search.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		try {
			client.executeMethod(empty_search);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertEquals(200, empty_search.getStatusCode());
		try {
			body = empty_search.getResponseBodyAsString();
			assertTrue(
					"SearchBase64 comparing response path",
					body
							.contains("{\"Result\":{\"counter\":\"1\",\"results\":[{\"path\":\"/content/colecciones/admin/contents/tests_search_title\",\"extract\":"));
			assertTrue("SearchBase64 comparing response resource type", body
					.contains("\"resourceType\":\"gad/content\"}]"));
		} catch (IOException e) {
			assertFalse(true);

		}

		empty_search.releaseConnection();

	}

	@org.junit.Test
	public void test_SearchType_content() {

		// Empty search
		String param = "?key=fuente&type=" + TYPE_CONTENT
				+ "&path=/content/colecciones";
		String body = null;
		GetMethod searchmethod = new GetMethod(SLING_URL + SEARCH_URL + param);

		searchmethod.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		try {
			client.executeMethod(searchmethod);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertEquals(200, searchmethod.getStatusCode());
		try {
			body = searchmethod.getResponseBodyAsString();
			assertTrue(
					"SearchContent comparing response path",
					body
							.contains("\"Result\":{\"counter\":\"1\",\"results\":[{\"path\":\"/content/colecciones/admin/contents/tests_search_title"));
			assertTrue("SearchContent comparing response resource type", body
					.contains("\"resourceType\":\"gad/content\"}]"));
		} catch (IOException e) {
			assertFalse(true);

		}

		searchmethod.releaseConnection();

	}

	@org.junit.Test
	public void test_SearchType_source() {

		// Empty search
		String param = "?key=" + SOURCE_TITLE + "&type=" + TYPE_SOURCE;
		String body = null;
		GetMethod searchmethod = new GetMethod(SLING_URL + SEARCH_URL + param);

		searchmethod.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		try {
			client.executeMethod(searchmethod);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertEquals(200, searchmethod.getStatusCode());
		try {
			body = searchmethod.getResponseBodyAsString();
			assertTrue("SearchSource comparing response path", body
					.contains("[{\"path\":\"/content/colecciones/" + COLLECTION
							+ "/contents/tests_search_title/sources/fuente1\""));
			assertTrue("SearchSource comparing response resourcetype", body
					.contains("\"resourceType\":\"gad/source\"}]"));
		} catch (IOException e) {
			assertFalse(true);

		}

		searchmethod.releaseConnection();

	}

	@org.junit.Test
	public void test_SearchType_source_path() {

		// Empty search
		String param = "?key=" + SEARCH_KEY + "&type=" + TYPE_SOURCE + "&path="
				+ SOURCEPATH;
		String body = null;
		GetMethod searchmethod = new GetMethod(SLING_URL + SEARCH_URL + param);

		searchmethod.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		try {
			client.executeMethod(searchmethod);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertEquals(200, searchmethod.getStatusCode());
		try {
			body = searchmethod.getResponseBodyAsString();
			assertTrue("SearchSource comparing response", body.contains("[]"));
		} catch (IOException e) {
			assertFalse(true);

		}

		searchmethod.releaseConnection();

	}

	@org.junit.Test
	public void test_SearchType_xml_output() {

		// Empty search
		String param = "?key=" + SEARCH_KEY + "&format=xml";
		String body = null;
		GetMethod searchmethod = new GetMethod(SLING_URL + SEARCH_URL + param);

		searchmethod.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		try {
			client.executeMethod(searchmethod);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertEquals(200, searchmethod.getStatusCode());
		try {
			body = searchmethod.getResponseBodyAsString();
			assertTrue("SearchSource comparing response 1", body
					.contains("<?xml version=\"1.0\"?>"));
			assertTrue(
					"SearchSource comparint response 2",
					body
							.contains("<path>/content/colecciones/admin/contents/tests_search_title/sources/fuente1</path>"));
			assertTrue(
					"SearchSource comparint response 3",
					body
							.contains("<resourceType>gad/source</resourceType>"));
			assertTrue(
					"SearchSource comparint response 4",
					body
							.contains("<extract><![CDATA["));
			assertTrue(
					"SearchSource comparint response 5",
					body
							.contains("<counter>2</counter>"));
		} catch (IOException e) {
			assertFalse(true);

		}
		searchmethod.releaseConnection();
	}

	@org.junit.Test
	public void test_SearchType_xml_output_type() {

		// Empty search
		String param = "?key=" + SEARCH_KEY + "&format=xml&type=" + TYPE_CONTENT;
		String body = null;
		GetMethod searchmethod = new GetMethod(SLING_URL + SEARCH_URL + param);

		searchmethod.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		try {
			client.executeMethod(searchmethod);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertEquals(200, searchmethod.getStatusCode());
		try {
			body = searchmethod.getResponseBodyAsString();
			assertTrue("SearchSource comparing response 1", body
					.contains("<?xml version=\"1.0\"?>"));
			assertTrue(
					"SearchSource comparint response 2",
					body
							.contains("<path>/content/colecciones/admin/contents/tests_search_title</path>"));
			assertTrue(
					"SearchSource comparint response 3",
					body
							.contains("<resourceType>gad/content</resourceType>"));
			assertTrue(
					"SearchSource comparint response 4",
					body
							.contains("<extract><![CDATA["));
			assertTrue(
					"SearchSource comparint response 5",
					body
							.contains("<counter>1</counter>"));
		} catch (IOException e) {
			assertFalse(true);

		}
		searchmethod.releaseConnection();
	}

	@org.junit.Test
	public void test_Search_fullinfo() {

		// Empty search
		String param = "?key=" + SEARCH_KEY + "&type=" + TYPE_CONTENT
				+ "&fullinfo=true&offset=0";
		String body = null;
		GetMethod searchmethod = new GetMethod(SLING_URL + SEARCH_URL + param);

		searchmethod.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		try {
			client.executeMethod(searchmethod);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertEquals(200, searchmethod.getStatusCode());
		try {
			body = searchmethod.getResponseBodyAsString();
			assertTrue("SearchSource assert hits", body.contains("hits"));
			assertTrue("SearchSource assert schema", body.contains("schema"));
		} catch (IOException e) {
			assertFalse(true);

		}
		searchmethod.releaseConnection();
	}

	@org.junit.Test
	public void test_Search_items() {

		// Empty search
		String param = "?key=" + SEARCH_KEY + "&items=1&format=xml";
		String body = null;
		GetMethod searchmethod = new GetMethod(SLING_URL + SEARCH_URL + param);
		
		searchmethod.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		try {
			client.executeMethod(searchmethod);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertEquals(200, searchmethod.getStatusCode());
		try {
			body = searchmethod.getResponseBodyAsString();
			assertTrue("SearchItems 1", body.contains("<counter>1</counter>"));
			assertTrue(
					"SearchItems 2",
					body
							.contains("<path>/content/colecciones/admin/contents/tests_search_title/sources/fuente1</path>"));
			assertTrue("SearchItems 3", body.contains("<?xml version=\"1.0\"?>"));

		} catch (IOException e) {
			assertFalse(true);

		}
		searchmethod.releaseConnection();
	}

	@org.junit.Test
	public void test_fullinfo_channels_xml() {

		// Empty search
		String param = "?key=" + TITLE_CONTENT + "&format=xml&fullinfo=true&path=/content/catalogo/&type=gad/revision";
		String body = null;
		GetMethod searchmethod = new GetMethod(SLING_URL + SEARCH_URL + param);
		
		searchmethod.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		try {
			client.executeMethod(searchmethod);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertEquals(200, searchmethod.getStatusCode());
		try {
			body = searchmethod.getResponseBodyAsString();
			
			assertTrue("fullinfo channels xml 1", body.contains("<counter>1</counter>"));
			assertTrue(
					"fullinfo channels xml 2",
					body
							.contains("<channels>\n      </channels>"));
			assertTrue("fullinfo channels xml 3", body.contains("<?xml version=\"1.0\"?>"));

		} catch (IOException e) {
			assertFalse(true);

		}
		searchmethod.releaseConnection();
	}
	
	@org.junit.Test
	public void test_fullinfo_channels_json() {

		// Empty search
		String param = "?key=" + TITLE_CONTENT + "&format=json&fullinfo=true&path=/content/catalogo/&type=gad/revision";
		String body = null;
		GetMethod searchmethod = new GetMethod(SLING_URL + SEARCH_URL + param);
		
		searchmethod.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		try {
			client.executeMethod(searchmethod);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertEquals(200, searchmethod.getStatusCode());
		try {
			body = searchmethod.getResponseBodyAsString();
			
			assertTrue("fullinfo channels json 1", body.contains("\"counter\":\"1\""));
			assertTrue(
					"fullinfo channels json 2",
					body
							.contains("\"channels\":[]"));

		} catch (IOException e) {
			assertFalse(true);

		}
		searchmethod.releaseConnection();
	}
	
	@org.junit.Test
	public void test_related_search() {

		// Empty search
		String param = "?key=" + TITLE_CONTENT + "&path=/content/colecciones&related=0.2&format=xml";
		String body = null;
		GetMethod searchmethod = new GetMethod(SLING_URL + SEARCH_URL + param);
		
		searchmethod.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		try {
			client.executeMethod(searchmethod);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertEquals(200, searchmethod.getStatusCode());
		try {
			body = searchmethod.getResponseBodyAsString();
			System.out.println(body);
			assertTrue("fullinfo related 1", body.contains("<counter>6</counter>"));

		} catch (IOException e) {
			assertFalse(true);

		}
		searchmethod.releaseConnection();
	}

}