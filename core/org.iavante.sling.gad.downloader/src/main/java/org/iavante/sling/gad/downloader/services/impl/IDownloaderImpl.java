/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.downloader.services.impl;

import java.io.NotActiveException;
import java.util.HashMap;

import javax.jcr.Node;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.iavante.sling.gad.downloader.services.IDownloader;
import org.iavante.sling.commons.services.PortalSetup;
import org.iavante.sling.commons.services.ServiceProvider;

/**
 * @see org.iavante.sling.gad.downloader.servlet.services.IDownloader
 * @scr.component inmediate="true"
 * @scr.service 
 *              interface="org.iavante.sling.gad.downloader.services.IDownloader"
 * @scr.property name="service.description" value="IAVANTE - Downloader Impl"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.property name="service.type" value="distributionserver"
 * @scr.property name="service.typeimpl" value="downloader"
 */
public class IDownloaderImpl implements IDownloader {

	/** @scr.reference */
	private PortalSetup portalSetup;

	/** @scr.reference */
	private ServiceProvider serviceProvider;

	/** Default Logger. */
	private static final Logger log = LoggerFactory
			.getLogger(IDownloaderImpl.class);

	/** Downloader url */
	private String downloader_url = "";

	/** Scape char **/
	private String url_scape_char = "aAa";

	protected void activate(ComponentContext context) {
		this.downloader_url = portalSetup.get_config_properties("/downloader").get(
				"url");
	}

	/*
	 * @see
	 * org.iavante.sling.gad.downloader.services.IDownloader#getUrl(java.lang.
	 * String)
	 */
	public String getUrl(String media) throws NotActiveException {

		String url = "";
		String encTicket = "";

		try {
			encTicket = serviceProvider.get_service_ticket("downloader");
			log.info(encTicket);
			encTicket = new String(Base64.encodeBase64(encTicket.getBytes()));
			log.info(encTicket);
		} catch (Exception e) {
			e.printStackTrace();
		}

		url = this.downloader_url + "/?v=" + media + url_scape_char + encTicket
				+ url_scape_char;
		return url;
	}

	/*
	 * @see
	 * org.iavante.sling.gad.downloader.services.IDownloader#upload(java.lang.
	 * String)
	 */
	public String upload(String fileName) {
		return "200";
	}

	/*
	 * @see org.iavante.sling.s3backend.S3backend#upload
	 */
	public String upload(Node contentNode) {
		return null;
	}

	/*
	 * @see org.iavante.sling.gad.downloader.services.IDownloader
	 */
	public String get_player(HttpServletRequest req,
			HashMap<String, Object> kwargs) {
		return "";
	}

}
