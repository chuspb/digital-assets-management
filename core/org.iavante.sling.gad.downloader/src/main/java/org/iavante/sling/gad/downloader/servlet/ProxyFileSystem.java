/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.downloader.servlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;

import org.apache.commons.codec.binary.Base64;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.iavante.sling.commons.services.EncryptionService;
import org.iavante.sling.commons.services.PortalSetup;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The servlet returns the file required from the files system.
 * 
 * @scr.component immediate="true" metatype="no"
 * @scr.service interface="javax.servlet.Servlet"
 * @scr.property name="service.description" value="Proxy File System"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.property name="sling.servlet.paths" value="/system/proxy"
 */
@SuppressWarnings("serial")
public class ProxyFileSystem
		extends SlingSafeMethodsServlet {

	/** @scr.reference */
	private PortalSetup portalSetup;

	/** @scr.reference */
	private EncryptionService encryptionTool;

	/** Names of configuration properties. */
	private final String UPLOADER_FILE_FOLDER = "uploader_file_folder";
	private final String DOWNLOADER_TOKEN = "token";
	private final String VALID_PERIOD_TIME = "expiration_time";

	/** Scape character for urls. */
	private String urlScapeChar = "aAa";

	/** Uploader file folder. */
	private String uploaderFileFolder = "";

	/** Uploader properties. */
	private Map<String, String> uploaderProperties = null;

	/** Downloader properties. */
	private Map<String, String> downloaderProperties = null;

	private static final Logger log = LoggerFactory
			.getLogger(ProxyFileSystem.class);

	public void log(String text) {
		log.error(text);
	}
	
	protected void activate(ComponentContext context) {	
		
		// Get config properties
		this.uploaderProperties = portalSetup.get_config_properties("/uploader");
		this.downloaderProperties = portalSetup
				.get_config_properties("/downloader");
		this.uploaderFileFolder = uploaderProperties.get(UPLOADER_FILE_FOLDER);		
	}

	@Override
	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException, IOException {		

		// Get parameters
		String param = request.getParameter("v");
		String videoFile = param.substring(0, param.indexOf(urlScapeChar));
		String ticket = param.substring(param.indexOf(urlScapeChar)
				+ urlScapeChar.length(), param.indexOf(urlScapeChar, param
				.indexOf(urlScapeChar)
				+ urlScapeChar.length()));
		ticket = new String(Base64.decodeBase64(ticket.getBytes()));

		if (videoFile == null) {
			response.setStatus(SlingHttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			response.sendError(SlingHttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			log.error("Video path empty");
		} else {
			String videoPath = this.uploaderFileFolder + "/" + videoFile;
			log.info("Video path: " + videoPath);

			if (this.ticked_is_valid(ticket)) {

				log.info("Ticket valido");
				// The ticket is valid
				if (new File(videoPath).exists()) {
					ServletOutputStream out = response.getOutputStream();

					// Return the file
					try {
						if (videoPath.endsWith(".flv"))
							response.setContentType("video/x-flv");
						else {
							String contentType = getServletContext().getMimeType(videoPath);
							response.setContentType(contentType);
						}
						response.setHeader("Content-Disposition", "attachment; filename="
								+ videoFile);
						this.returnFile(videoPath, out);

					} catch (FileNotFoundException e) {
						// The file doesn't exists
						response.setStatus(SlingHttpServletResponse.SC_NOT_FOUND);
						log.error("File not found");
					} catch (IOException e) {
						// Error reading the file
						response
								.setStatus(SlingHttpServletResponse.SC_INTERNAL_SERVER_ERROR);
						log.error("IO Exception");
					}
				} else {
					response.setStatus(SlingHttpServletResponse.SC_NOT_FOUND);
				}
			} else {
				// The ticket is invalid
				log.error("Invalid ticket");
				response.setStatus(SlingHttpServletResponse.SC_FORBIDDEN);
			}
		}
	}

	// ---------------- Internal -----------------------

	/**
	 * Send the contents of the file to the output stream.
	 * 
	 * @param filename
	 *          The file path
	 * @param out
	 *          Response output stream
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private void returnFile(String filename, OutputStream out)
			throws FileNotFoundException, IOException {
		// A FileInputStream is for bytes
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(filename);
			byte[] buf = new byte[4 * 1024]; // 4K buffer
			int bytesRead;
			while ((bytesRead = fis.read(buf)) != -1) {
				out.write(buf, 0, bytesRead);
			}
		} finally {
			if (fis != null)
				fis.close();
			out.close();
			out.flush();
		}
	}

	/**
	 * Checks if ticket is valid
	 * 
	 * @param ticket
	 *          The ticket to validate
	 * @returns If ticket is valid or not
	 */
	private boolean ticked_is_valid(String ticket) {

		// Get downloader properties
		String downloaderToken = this.downloaderProperties.get(DOWNLOADER_TOKEN);
		String validPeriodTime = this.downloaderProperties.get(VALID_PERIOD_TIME);

		// Desencrypt the ticket
		String ticket_encoded = ticket;

		String decryptTicket = "";
		try {
			decryptTicket = encryptionTool.decryptMessage(ticket_encoded);
		} catch (Exception e) {
			// The ticket is not valid. Desencryption exception
			e.printStackTrace();
			log.error("Ticket not valid. Desencryption error:" + e.toString());
			return false;
		}

		String[] campo = decryptTicket.split(":");
		String token = campo[0];
		String timestamp = campo[1];

		// Check token correct
		if (!token.equals(downloaderToken)) {
			return false;
		}

		// Time spent since the ticket was created in millisencods
		Long time_spent = new Date().getTime() - Long.parseLong(timestamp);

		// Current time in milliseconds
		Long valid_period_milliseconds = Long.parseLong(validPeriodTime) * 1000;

		// Compare times
		if (time_spent < valid_period_milliseconds) {
			log.error("PERIOD: " + valid_period_milliseconds.toString());
			log.error("TIME SPENT: " + time_spent);
			return true;
		}

		log.error("Ticket out of date");
		return false;
	}
}
