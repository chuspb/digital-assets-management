/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.authentication;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthPolicy;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;

/**
 * Test content creation and schema association
 */
public class AuthenticationFilterIT
		extends TestCase {

	private Credentials defaultcreds;
	private List authPrefs = new ArrayList(2);

	private final String GET_URL = "/colecciones/admin-test";
	private final String ADMIN_URL = "/content/colecciones/it-col.admin";
	private final String DOWNLOADER_URL = "/system/proxy/?v=&ticket=2312asdasd5212342361661";
	private final String CROSSDOMAIN_URL = "/crossdomain.xml";

	private final String HOSTVAR = "SLINGHOST";
	private final String HOSTPREDEF = "localhost:8888";
	private String SLING_URL = "http://";

	HttpClient client;

	@org.junit.Before
	protected void setUp() {

		Map<String, String> envs = System.getenv();
		Set<String> keys = envs.keySet();

		Iterator<String> it = keys.iterator();
		boolean hashost = false;
		while (it.hasNext()) {
			String key = (String) it.next();

			if (key.compareTo(HOSTVAR) == 0) {
				SLING_URL = SLING_URL + (String) envs.get(key);
				hashost = true;
			}
		}

		if (hashost == false)
			SLING_URL = SLING_URL + HOSTPREDEF;

		client = new HttpClient();
	}

	@org.junit.After
	protected void tearDown() {
	}

	@org.junit.Test
	public void test_get_authenticated() {

		// Try to get a collection
		authPrefs.add(AuthPolicy.DIGEST);
		authPrefs.add(AuthPolicy.BASIC);
		defaultcreds = new UsernamePasswordCredentials("admin", "admin");

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		HttpMethod get = new GetMethod(SLING_URL + GET_URL);
		try {
			client.executeMethod(get);
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertEquals(get.getStatusCode(), 404);
	}

	@org.junit.Test
	public void test_get_not_authenticated() {

		// Try to get a collection
		authPrefs.add(AuthPolicy.DIGEST);
		authPrefs.add(AuthPolicy.BASIC);
		defaultcreds = new UsernamePasswordCredentials("noauthenticated",
				"noauthenticated");

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		HttpMethod get = new GetMethod(SLING_URL + GET_URL);
		try {
			client.executeMethod(get);
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		assertEquals(get.getStatusCode(), 403);
	}

	
	@org.junit.Test
	public void test_admin_url() {

		// Try to get a collection
		authPrefs.add(AuthPolicy.DIGEST);
		authPrefs.add(AuthPolicy.BASIC);
		defaultcreds = new UsernamePasswordCredentials("noauthenticated",
				"noauthenticated");

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		HttpMethod get = new GetMethod(SLING_URL + ADMIN_URL);
		try {
			client.executeMethod(get);
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		assertTrue((get.getStatusCode() != 403));
	}

	
	@org.junit.Test
	public void test_downloader_url() {

		// Try to get a collection
		authPrefs.add(AuthPolicy.DIGEST);
		authPrefs.add(AuthPolicy.BASIC);
		defaultcreds = new UsernamePasswordCredentials("noauthenticated",
				"noauthenticated");

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		HttpMethod get = new GetMethod(SLING_URL + DOWNLOADER_URL);
		try {
			client.executeMethod(get);
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		assertTrue((get.getStatusCode() != 403));
	}
	
	@org.junit.Test
	public void test_crossdomain_url() {

		HttpMethod get = new GetMethod(SLING_URL + CROSSDOMAIN_URL);
		try {
			client.executeMethod(get);
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		assertTrue((get.getStatusCode() != 403));
	}

}