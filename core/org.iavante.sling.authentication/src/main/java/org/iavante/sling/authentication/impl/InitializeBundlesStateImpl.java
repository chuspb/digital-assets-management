/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.authentication.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.iavante.sling.authentication.InitializeBundlesState;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleException;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see org.iavante.sling.authentication.InitializeBundlesState
 * @scr.component immediate="true" metatype="false"
 * @scr.property name="service.description"
 *               value="IAVANTE - Initialize Bundles State Impl"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.service 
 * interface="org.iavante.sling.authentication.InitializeBundlesState"
 */
public class InitializeBundlesStateImpl implements InitializeBundlesState,
		Serializable {
	private static final long serialVersionUID = 1L;
	
	/** Default log. */
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	/** Sling Component Context. */
	private ComponentContext myComponentContext;
	
	/** Array of installed bundles. */
	private Bundle[] bundlesInstalled;

	protected void activate(ComponentContext context) {
		myComponentContext = context;
		updateBundlesIsntalled();
		
		List<String> sn = new ArrayList();
		sn.add("org.apache.sling.httpauth");
		stopBundles(sn);
	}
	
	/*
	 * @see org.iavante.sling.initialconfig.services.InitializeBundlesState
	 */
	public void stopBundles(List symbolicNames) {
		
		for (int i=0; i<symbolicNames.size(); i++) {
			String sn = (String) symbolicNames.get(i);
			Bundle bundle = getBundle(sn);
			try {
				bundle.stop();
			} catch (BundleException e) {
				e.printStackTrace();
			}
			if (log.isInfoEnabled())
				log.info("stopBundles, Bundle: " + sn + " stopped");
		}
	}
	
	// -------------------- Internal------------------------
	/**
	 * This method update the list of bundles installed
	 */
	private void updateBundlesIsntalled() {
		bundlesInstalled = myComponentContext.getBundleContext().getBundles();
	}

	/**
	 * Get a bundle given its symbolic name
	 * 
	 * @param symbolicName
	 * @return the bundle
	 */
	private Bundle getBundle(String symbolicName) {
		Bundle bundle = null;
		boolean seguir = true;
		int i = 0;
		while (i < bundlesInstalled.length && seguir) {
			if (bundlesInstalled[i].getSymbolicName().startsWith(symbolicName)) {
				bundle = bundlesInstalled[i];
				seguir = false;
			}
			i++;
		}
		return bundle;
	}

}
