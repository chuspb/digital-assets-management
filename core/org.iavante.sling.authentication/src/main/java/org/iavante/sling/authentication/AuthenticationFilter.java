/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.authentication;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

import javax.jcr.SimpleCredentials;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.engine.auth.AuthenticationInfo;
import org.iavante.sling.commons.services.AuthToolsService;
import org.iavante.sling.commons.services.LDAPConnector;
import org.iavante.sling.commons.services.PortalSetup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * AuthenticationFilter
 * 
 * @scr.component immediate="true" metatype="no"
 * @scr.property name="service.description"
 *               value="IAV Authentication Request Filter"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.property name="filter.scope" value="request" private="true"
 * @scr.property name="filter.order" value="-2147483648" type="Integer"
 *               private="true"
 * @scr.service
 */

public class AuthenticationFilter implements Filter, Serializable {
	private static final long serialVersionUID = 1L;

	/** default log */
	private final Logger log = LoggerFactory.getLogger(getClass());

	/** @scr.reference */
	private LDAPConnector ldapService;

	/** @scr.reference */
	private PortalSetup portalSetup;

	/** @scr.reference */
	private AuthToolsService authTools;

	/** Downloader properties */
	Map<String, String> downloaderProperties = null;

	/** Admin suffix */
	private final String ADMIN_SUFFIX = ".admin";

	/** Downloader url */
	String downloaderUrl = "";

	/** Downloader relative path */
	String downloaderPath = "";

	/** Admin prefix */
	private final String ADMIN_PREFIX = "administration";

	
	public void init(FilterConfig filterConfig) {

		downloaderProperties = portalSetup.get_config_properties("/downloader");

		downloaderUrl = downloaderProperties.get("url");

		String[] downloader_path_splited = downloaderUrl.split("://")[1]
				.split("/");

		for (int i = 1; i < downloader_path_splited.length; i++) {
			downloaderPath += "/" + downloader_path_splited[i];
		}
	}


	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {

		SlingHttpServletRequest request = (SlingHttpServletRequest) req;
		SlingHttpServletResponse response = (SlingHttpServletResponse) res;

		AuthenticationInfo credentials = authTools.extractAuthentication(request);

		SimpleCredentials authInfo;

		String path = request.getPathInfo();

		boolean authenticated = false;

		// If it is a webdav acces we delegate the authorization to the
		// WebDavServlet
		if (portalSetup.isWebDavMethod(request.getMethod())) {
			if (log.isInfoEnabled())
				log.info("Is a webdav method");
			chain.doFilter(request, response);
			authenticated = true;
		}

		// If it is an admin path we delegate the autorization to the
		// AdminAuthenticationFilter
		else if ((path.endsWith(ADMIN_SUFFIX))
				|| (path.startsWith("/" + ADMIN_PREFIX))
				|| (path.startsWith("/apps/" + ADMIN_PREFIX))
				|| (path.endsWith("crossdomain.xml"))
				|| (path.endsWith("playlist.xml"))
				|| (path.endsWith("smil.xml"))
				|| (path.endsWith("xspf.xml"))
				|| (path.startsWith("/content/upload"))) {
			if (log.isInfoEnabled())
				log.info("Is an administration url");
			chain.doFilter(request, response);
			authenticated = true;
		}

		// If it is a downloader path we leave the request to pass because it has
		// its own authorization mechanism based in tickets
		else if (path.contains(downloaderPath)) {
			if (portalSetup.isBundleInstalled("org.iavante.sling.gad.downloader")) {
				if (log.isInfoEnabled())
					log.info("Is Downloader url");
				chain.doFilter(request, response);
				authenticated = true;
			}
		}

		// The rest of request
		else {

			if (credentials != null) {
				authInfo = (SimpleCredentials) credentials.getCredentials();
				authenticated = ldapService.checkUser(authInfo.getUserID(),
						new String(authInfo.getPassword()));
				if (authenticated) {
					chain.doFilter(request, response);
					if (log.isInfoEnabled())
						log.info("User authenticated");
				}
			}
		}

		if (!authenticated) {

			if (log.isInfoEnabled())
				log.info("User not authenticated");
			response.setStatus(403);
			response.sendError(403);
		}
	}
	
	
	public void destroy() {
	}
}
