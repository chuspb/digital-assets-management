<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page trimDirectiveWhitespaces="true"%>
<?xml version="1.0" encoding="UTF-8"?>
<Result xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<%@ page language="java" contentType="text/xml; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.Node"%>
<%@ page import="javax.jcr.NodeIterator"%>
<%@ page import="javax.jcr.query.QueryManager"%>
<%@ page import="javax.jcr.query.Query"%>
<%@ page import="org.iavante.sling.gad.content.ContentTools"%>
<%@ page import="org.iavante.sling.gad.source.SourceTools"%>
<%@ taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />

<%
	ArrayList etiquetas = new ArrayList();
	etiquetas.add("title");
	etiquetas.add("file");
%>
<%
String ORDER_BY = "order_by";
String ORDER = "order";
String OFFSET = "offset";
String ITEMS = "items";
String MIMETYPE = "mimetype";
String SCHEMATYPE = "schematype";
String TYPE = "type";
boolean nodeorder = false;
String ordering = "";


// Order validation
String order_by = null;
if (request.getParameter(ORDER_BY) != null) {
    order_by = request.getParameter(ORDER_BY);   
}

String order = null;
if (request.getParameter(ORDER) != null) {
    order = request.getParameter(ORDER);
}

String schematype = "";
if (request.getParameter(SCHEMATYPE) != null) {
	schematype = request.getParameter(SCHEMATYPE);
}

if (schematype.equals("playlist"))
	nodeorder = true;

else{

	
	if ( order_by != null || order != null){
		if (order_by == null)
			order_by = "title";
		if (order == null)
			order = "descending";
		ordering =  "order by @" + order_by + " " + order;
	}
}

NodeIterator it = null;
String query_str = "";

if (nodeorder)  {
	it = currentNode.getNodes();
}
else{

	QueryManager queryManager = currentNode.getSession().getWorkspace().getQueryManager();
	Query query;
	
	query_str = "/jcr:root/" + request.getPathInfo() + "/element(*, nt:unstructured)[@sling:resourceType = 'gad/source'";
	if (schematype.equals("schema")) {
		query_str = query_str + " and @schematype = 'schema'";
	}
	
	if(request.getParameter(MIMETYPE) != null) {
		 query_str =  query_str + " and @mimetype = '" + request.getParameter(MIMETYPE) + "'";
	}
	
	if(request.getParameter(TYPE) != null) {
		query_str =  query_str + " and @type = '" + request.getParameter(TYPE) + "'";
	}
	
	query_str = query_str + "]";
	query_str = query_str + ordering;
		
	query = queryManager.createQuery(query_str, "xpath");

	it = query.execute().getNodes();
}

%>

<%
if (request.getParameter(OFFSET) != null) {
    long skip = Long.parseLong(request.getParameter(OFFSET));
    while (skip > 0 && it.hasNext()) {
        it.next();
        skip--;
    }
}

long count = -1;
if (request.getParameter(ITEMS) != null) {
    count = Long.parseLong(request.getParameter(ITEMS));
}
%>

<query><%=query_str%></query>
<count><%= Long.toString(it.getSize())%></count>
<support_playlist><% if(currentNode.hasProperty("support_playlist")){ %><%=currentNode.getProperty("support_playlist").getValue().getString()%><%}%></support_playlist>
<sources>
<%
while(it.hasNext() && count != 0){
Node node = it.nextNode();
try{
	
	// Continue if it is no a source
	if (node.getProperty("sling:resourceType").getValue().getString().compareToIgnoreCase("gad/source") != 0)
		continue;
	
	// Continue if we only want playlist sources
	if ((nodeorder) && (!sling.getService(org.iavante.sling.gad.source.SourceTools.class).is_a_playlist_source(node))) {
		continue;
	}
}
catch(Exception e){
	continue;
}
%>
<source>
	<%for(int i=0; i<etiquetas.size(); i++){
		String tagName = (String) etiquetas.get(i);%>
	<<%= tagName%>><%if(node.hasProperty(tagName)){%>
	<%=node.getProperty(tagName).getValue().getString()%>
	<%}%>
	</<%= tagName%>>
	<%}%>
	<path><%=node.getPath() %></path>
	<resourceType><% if(node.hasProperty("sling:resourceType")){ %><%=node.getProperty("sling:resourceType").getValue().getString()%><%}%></resourceType>
	<length><% if(node.hasProperty("length")){ %><%=node.getProperty("length").getValue().getString()%><%}%></length>
	<schematype><% if(node.hasProperty("schematype")){ %><%=node.getProperty("schematype").getValue().getString()%><%}%></schematype>
	<mimetype><% if(node.hasProperty("mimetype")){ %><%=node.getProperty("mimetype").getValue().getString()%><%}%></mimetype>
	<type><% if(node.hasProperty("type")){ %><%=node.getProperty("type").getValue().getString()%><%}%></type>
	<urlInternal><![CDATA[<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).get_url(request, node)%>]]></urlInternal>
	<urlExternal><![CDATA[<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).getS3Url(node)%>]]></urlExternal>
	<% if(node.hasNode("transcriptions")){
		Node transcriptionNode = node.getNode("transcriptions");
		NodeIterator it0 = transcriptionNode.getNodes();
		%><resources><%
		while(it0.hasNext()){
			Node nodeLang = it0.nextNode();
			NodeIterator it1 = nodeLang.getNodes();
			%><<%=nodeLang.getName()%>><%
			while(it1.hasNext()){
				Node nodeTransType = it1.nextNode(); 
				NodeIterator it2 = nodeTransType.getNodes();
				while(it2.hasNext()){
					Node source = it2.nextNode();
					%><<%=nodeTransType.getName()%>><url><![CDATA[<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).getS3Url(source)%>]]></url></<%=nodeTransType.getName()%>><%
				}
			}
			%></<%=nodeLang.getName()%>><%
		}
		%></resources><%
	%>
<%}%>
</source>
<%count--; %>
<%}%>
</sources>

</Result>
