<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@page contentType="text/json; charset=UTF-8"%>

<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.Node"%>
<%@ page import="javax.jcr.NodeIterator"%>
<%@ taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />
<%
	ArrayList etiquetas = new ArrayList();
	etiquetas.add("title");
	etiquetas.add("file");
	
%>
{"Result": {
"transformations": [<%String y ="";int z = 1;
NodeIterator it = currentNode.getNodes();
while(it.hasNext()){Node node = it.nextNode(); y = Long.toString(z++);%>
{<% for(int i=0; i<etiquetas.size(); i++){String tagName = (String) etiquetas.get(i);%>"<%= tagName%>": "<% if(node.hasProperty(tagName)){ %><%=node.getProperty(tagName).getValue().getString()%><%}%>", <%}%>"path": "<%=node.getPath() %>", "resourceType": "<% if(node.hasProperty("sling:resourceType")){ %><%=node.getProperty("sling:resourceType").getValue().getString()%><%}%>"}<% if(!y.equals(Long.toString(it.getSize()))){%>,<%}%><%}%>
]
}}
