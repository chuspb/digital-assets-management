<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<?xml version="1.0" encoding="UTF-8"?>
<Result xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/xml; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.Node"%>
<%@ page import="javax.jcr.NodeIterator"%>
<%@ page import="javax.jcr.query.QueryManager"%>
<%@ page import="javax.jcr.query.Query"%>
<%@ page import=" javax.jcr.Property"%>
<%@ page import="javax.jcr.Value"%>
<%@ page import="org.iavante.sling.gad.content.ContentTools"%>
<%@ taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />
<%
String ORDER_BY = "order_by";
String ORDER = "order";
String OFFSET = "offset";
String ITEMS = "items";
String FILTER = "filter";
String filter = null;
String CHANNEL = "channel";
String LANGUAGE = "lang";
String PATH = "path";
String channel = null;
Integer MAX_LEN_DESCRIPTION = 400;
Integer length = 0;

String order_by = "title";
if (request.getParameter(ORDER_BY) != null) {
    order_by = request.getParameter(ORDER_BY);   
}

String order = "ascending";
if (request.getParameter(ORDER) != null) {
    order = request.getParameter(ORDER);
}

String lang = "";
if (request.getParameter(LANGUAGE) != null) {
	lang = request.getParameter(LANGUAGE);
}

/* Get all paths */
String path[] = null;
if (request.getParameter(PATH) != null){
    path = request.getParameterValues(PATH);
}

String description = "";
String tags = "";
Integer values_length = 0;

if (path != null) {
    values_length = path.length;
}
%>
<count><%=values_length%></count>
<contents>
<%

/* Iter over all paths */
for (int i=0;i<values_length;i++) {
    
    String node_path = path[i];
    
    // Remove the first and ending slash
    if (node_path.startsWith("/")) node_path = node_path.substring(1, node_path.length());
    if (node_path.endsWith("/")) node_path = node_path.substring(0,node_path.length()-1);
    
    // Get the name of the revision node in the path
    String node_name;
    if (node_path.contains("/")) {
        String[] path_splited = node_path.split("/");
        node_name = path_splited[path_splited.length -1];
    }
    else {
        node_name = node_path;
    }%> 
    <content>    
    <%
    
    // Node exists
    if (currentNode.hasNode(node_name)) {
        Node node = currentNode.getNode(node_name);%>
        <title><%if(node.hasProperty("title")){ %>
    	<%=node.getProperty("title").getValue().getString()%>
    	<%}%></title>
        <path><%=node.getPath()%></path>
        <resourceType><%if(node.hasProperty("sling:resourceType")){ %>
    	<%=node.getProperty("sling:resourceType").getValue().getString()%>
    	<%}%></resourceType>

       	<% if(node.hasProperty("description")) { 
             description = node.getProperty("description").getValue().getString();
    	     if (description.length() > MAX_LEN_DESCRIPTION) {
    	       description = description.substring(0, MAX_LEN_DESCRIPTION);
    	     }
    	   }
    	%>
    	<description><%=description%></description>
    	<%
    	if (node.hasProperty("tags")) {
    	     Property prop = node.getProperty("tags");
    	     if (prop.getDefinition().isMultiple()) {
    			Value[] values = prop.getValues();		
    			for (int j=0; j<values.length;j++) {
    			    tags = tags + values[j].getString() + ",";
    			}	
    	     }
    	     else {
    	    	 tags = prop.getValue().getString();
    	     }

    	     if (tags.endsWith(",")) {
    	    	 tags = tags.substring(0, tags.length() - 1);
    	     }
    	}
    	%>	
    	<tags><%=tags%></tags>
        <categories>
    	<%
    	if (node.hasProperty("categories")) {
    	    Property prop = node.getProperty("categories");
    	    if (prop.getDefinition().isMultiple()) {
    			Value[] values = prop.getValues();		
    			for (int j=0; j<values.length;j++) {
    			    String category_path = values[j].getString();
    			    Node root_node = currentNode.getSession().getRootNode();
    			    Node cat_node = null;
    			    if (root_node.hasNode(category_path.substring(1))) {
    			    	cat_node = root_node.getNode(category_path.substring(1));
    			    	String title = "";
    			    	if (cat_node.hasProperty("title")) {
    			    		title = cat_node.getProperty("title").getValue().getString();
    			    		%>			    		
          <category>
             <title><%=title%></title>
             <path><%=category_path%></path>
          </category><%
    			    	}				    	
    			    }
    			}	
    	    }	    
    	}
    	%>
    	</categories>
    	<votes_figures>
            <%String number_votes = sling.getService(org.iavante.sling.gad.content.ContentTools.class).number_of_votes(node);%>
            <%String sum_votes = sling.getService(org.iavante.sling.gad.content.ContentTools.class).number_of_votes_points(node);%>
            <number><%=number_votes%></number>
            <sum><%=sum_votes%></sum>
        </votes_figures>	
    	<author><% if(node.hasProperty("author")) { %><%=node.getProperty("author").getValue().getString()%><%}%></author>
    	<schema><% if(node.hasProperty("schema")) { %><%=node.getProperty("schema").getValue().getString()%><%}%></schema>
    	<hits><% if(node.hasProperty("hits")) { %><%=node.getProperty("hits").getValue().getString()%><%}%></hits>
    	<thumbnail><![CDATA[<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).getThumbUrl(node)%>]]></thumbnail>
    	<created><% if(node.hasProperty("jcr:created")) { %><%=node.getProperty("jcr:created").getValue().getString()%><%}%></created>
    	<createdBy><% if(node.hasProperty("jcr:createdBy")) { %><%=node.getProperty("jcr:createdBy").getValue().getString()%><%}%></createdBy>
    	<lastModified><% if(node.hasProperty("jcr:lastModified")) { %><%=node.getProperty("jcr:lastModified").getValue().getString()%><%}%></lastModified>
        <lastModifiedBy><% if(node.hasProperty("jcr:lastModifiedBy")) { %><%=node.getProperty("jcr:lastModifiedBy").getValue().getString()%><%}%></lastModifiedBy>
    <%}
    
    // Node doesn´t exist
    else {%>
        <title>not-exists</title>
        <path>404</path>
    <%}%>
</content>
<%}// End for%>
</contents>	
</Result>