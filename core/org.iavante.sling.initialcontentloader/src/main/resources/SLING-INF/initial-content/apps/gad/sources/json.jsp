<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page contentType="text/json; charset=UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.Node"%>
<%@ page import="javax.jcr.NodeIterator"%>
<%@ page import="javax.jcr.query.QueryManager"%>
<%@ page import="javax.jcr.query.Query"%>
<%@ page import="org.iavante.sling.gad.content.ContentTools"%>
<%@ taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />
<%
	ArrayList etiquetas = new ArrayList();
	etiquetas.add("title");
	etiquetas.add("file");
%>

<%
String ORDER_BY = "order_by";
String ORDER = "order";
String OFFSET = "offset";
String ITEMS = "items";
String MIMETYPE = "mimetype";

String PathInfo = request.getPathInfo();
PathInfo = PathInfo.replace(".json", "");


String order_by = "title";
if (request.getParameter(ORDER_BY) != null) {
    order_by = request.getParameter(ORDER_BY);   
}

String order = "descending";
if (request.getParameter(ORDER) != null) {
    order = request.getParameter(ORDER);
}

QueryManager queryManager = currentNode.getSession().getWorkspace().getQueryManager();
Query query;

if(request.getParameter(MIMETYPE) != null) {
	query = queryManager.createQuery("/jcr:root/" + PathInfo + "/element(*, nt:unstructured)[@sling:resourceType = 'gad/source' and @mimetype = '" + request.getParameter(MIMETYPE) + "'] order by @" + order_by + " " + order, "xpath");
}
else {
    query = queryManager.createQuery("/jcr:root/" + PathInfo + "/element(*, nt:unstructured)[@sling:resourceType = 'gad/source'] order by @" + order_by + " " + order, "xpath");
}
NodeIterator it = query.execute().getNodes();
String y ="";
int z = 1;
%>
{"Result": {
"count": <%= Long.toString(it.getSize())%>,
"sources": [
<%while(it.hasNext()){
Node node = it.nextNode();%>
<%y = Long.toString(z++); %>{
"urlInternal": "<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).get_url(request, node)%>",
"urlExternal": "<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).getS3Url(node)%>", 
<% for(int i=0; i<etiquetas.size(); i++){String tagName = (String) etiquetas.get(i);%>"<%= tagName%>": "<% if(node.hasProperty(tagName)){ %><%=node.getProperty(tagName).getValue().getString()%><%}%>", <%}%> "path": "<%=node.getPath() %>", "resourceType": "<% if(node.hasProperty("sling:resourceType")){ %><%=node.getProperty("sling:resourceType").getValue().getString()%>"<%}%>}<% if(!y.equals(Long.toString(it.getSize()))){%>,<%}%>
<%}%>]
}}

