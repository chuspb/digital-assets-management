<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<?xml version="1.0" encoding="UTF-8"?>
<Result xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
<%@ page trimDirectiveWhitespaces="true"%>
<%@ page language="java" contentType="text/xml; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.Node"%>
<%@ page import="javax.jcr.NodeIterator"%>
<%@ page import="javax.jcr.query.QueryManager"%>
<%@ page import="javax.jcr.query.Query"%>
<%@ page import=" javax.jcr.Property"%>
<%@ page import="javax.jcr.Value"%>
<%@ page import="org.iavante.sling.gad.content.ContentTools"%>
<%@ taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />
<%
String ORDER_BY = "order_by";
String ORDER = "order";
String OFFSET = "offset";
String ITEMS = "items";
String FILTER = "filter";
String filter = null;
String CHANNEL = "channel";
String LANGUAGE = "lang";
String SCHEMA = "schema";
String channel = null;
Integer MAX_LEN_DESCRIPTION = 1000;
Integer length = 0;

String order_by = "title";
if (request.getParameter(ORDER_BY) != null) {
    order_by = request.getParameter(ORDER_BY);   
}

String order = "ascending";
if (request.getParameter(ORDER) != null) {
    order = request.getParameter(ORDER);
}

String schema = "";
if (request.getParameter(SCHEMA) != null) {
	  schema = request.getParameter(SCHEMA);
}

String lang = "";
if (request.getParameter(LANGUAGE) != null) {
	lang = request.getParameter(LANGUAGE);
}

// channel parameter
if (request.getParameter(CHANNEL) != null) {
    channel = request.getParameter(CHANNEL);
}

/* filter parameter
	possible values are: 
		notpublished, published, outdated, updated
		
	by default: notpublished 
	
	requeriments: channel parameter has been set
*/

if (channel != null)
	if (request.getParameter(FILTER) != null) {
	    filter = request.getParameter(FILTER);
	}
	else
		filter = "notpublished";
		

QueryManager queryManager = currentNode.getSession().getWorkspace().getQueryManager();

String query_str = "/jcr:root/" + request.getPathInfo() + "/element(*, nt:unstructured)[@sling:resourceType = 'gad/revision'";

if (lang != "") {
	query_str = query_str + " and @lang = '" + lang + "'";
}   

if (schema != "") {
	query_str = query_str + " and @schema = '" + schema + "'";
} 

query_str = query_str + "]";
query_str = query_str + " order by @" + order_by + " " + order;

Query query = queryManager.createQuery(query_str, "xpath");

NodeIterator itnode = query.execute().getNodes();
ArrayList<Node> filterednodes=null;

if (filter != null){
	length = 0;
	filterednodes = new ArrayList<Node>();
	Query querychannel = queryManager.createQuery("/jcr:root/content/canales/" + channel + "/contents/element(*, nt:unstructured)[@sling:resourceType = 'gad/revision']", "xpath");
	NodeIterator itchannel = querychannel.execute().getNodes();

	Node encatalogo = null;
	Node encanal	= null;
	boolean published = false;
	boolean updated   = false;
	Float verscat,verscha = null;
	
	while(itnode.hasNext()){
		encatalogo = itnode.nextNode();
		published = false;
		updated   = false;
		itchannel = querychannel.execute().getNodes();
		
		while(itchannel.hasNext()){
			encanal = itchannel.nextNode();

			if(encanal.getProperty("gaduuid").getString().compareToIgnoreCase(encatalogo.getProperty("gaduuid").getString()) == 0){
				published = true;
				break;
			}
		}
		if (published == false){
			if("notpublished".compareToIgnoreCase(filter)==0){ 
				filterednodes.add(encatalogo);
			}
		}	
		else{
			if("published".compareToIgnoreCase(filter) == 0){
				filterednodes.add(encatalogo);
			}
			else{
				try{
					verscat = Float.parseFloat(encatalogo.getProperty("version").getString());
					try{
						verscha = Float.parseFloat(encanal.getProperty("version").getString());

						if (verscat > verscha)
							updated = false;
						else
							updated = true;
					}catch(Exception e2){
						updated = false;
					}
				}catch(Exception e){
					updated = true;
				}
				if("updated".compareToIgnoreCase(filter) == 0 && updated == true){
					filterednodes.add(encatalogo);
				}
				else 
					if("outdated".compareToIgnoreCase(filter) == 0 && updated == false){
						filterednodes.add(encatalogo);
					}
				
			}
		}
	}
	length = filterednodes.size();

}
java.util.Iterator it = null;
/* query presentation */
if(filter!=null){
	it = filterednodes.iterator();
	%>
	<count><%= Integer.toString(filterednodes.size())%></count>
	<%
}
else{
	it = (java.util.Iterator) itnode;
%>
<count><%= Long.toString(itnode.getSize())%></count>
<%}
if (request.getParameter(OFFSET) != null) {
    long skip = Long.parseLong(request.getParameter(OFFSET));
    while (skip > 0 && it.hasNext()) {
        it.next();
        skip--;
    }
}

long count = -1;
if (request.getParameter(ITEMS) != null) {
    count = Long.parseLong(request.getParameter(ITEMS));
}

String description = "";
String votes = "";
%>

<contents>
<%
while (it.hasNext() && count != 0) {
Node node = (Node) it.next(); 
String tags = "";
%>
<content>   
  <title><%if(node.hasProperty("title")) { %><%=node.getProperty("title").getValue().getString() %><% } %></title>
	<path><%=node.getPath()%></path>
	<resourceType><%if(node.hasProperty("sling:resourceType")){ %>
	<%=node.getProperty("sling:resourceType").getValue().getString()%>
	<%}%></resourceType>
	
   	<% if(node.hasProperty("description")) { 
         description = node.getProperty("description").getValue().getString();
	     if (description.length() > MAX_LEN_DESCRIPTION) {
	       description = description.substring(0, MAX_LEN_DESCRIPTION);
	     }
	   }
	%>
	<description><%=description%></description>
	<%
	if (node.hasProperty("tags")) {
	     Property prop = node.getProperty("tags");
	     if (prop.getDefinition().isMultiple()) {
			Value[] values = prop.getValues();		
			for (int j=0; j<values.length;j++) {
			    tags = tags + values[j].getString() + ",";
			}	
	     }
	     else {
	    	 tags = prop.getValue().getString();
	     }
	     
	     if (tags.endsWith(",")) {
	    	 tags = tags.substring(0, tags.length() - 1);
	     }
	}
	%>	
	<tags><%=tags%></tags>
	<%
	
	if (node.hasProperty("votes")) {
	     Property prop = node.getProperty("votes");
	     if (prop.getDefinition().isMultiple()) {
			Value[] values = prop.getValues();		
			votes = "";
			for (int j=0; j<values.length;j++) {
			    votes = votes + values[j].getString() + ",";
			}	
	     }
	     else {
	    	 votes = prop.getValue().getString();
	     }
	     
	     if (votes.endsWith(",")) {
	    	 votes = votes.substring(0, votes.length() - 1);
	     }
	}
	%>	
	<votes><%=votes%></votes>
	<votes_figures>
    <%String number_votes = sling.getService(org.iavante.sling.gad.content.ContentTools.class).number_of_votes(node);%>
    <%String sum_votes = sling.getService(org.iavante.sling.gad.content.ContentTools.class).number_of_votes_points(node);%>
    <number><%=number_votes%></number>
    <sum><%=sum_votes%></sum>
  </votes_figures>
	<categories>
	<%
	if (node.hasProperty("categories")) {
	    Property prop = node.getProperty("categories");
	    if (prop.getDefinition().isMultiple()) {
			Value[] values = prop.getValues();		
			for (int j=0; j<values.length;j++) {
			    String category_path = values[j].getString();
			    Node root_node = currentNode.getSession().getRootNode();
			    Node cat_node = null;
			    if (root_node.hasNode(category_path.substring(1))) {
			    	cat_node = root_node.getNode(category_path.substring(1));
			    	String title = "";
			    	if (cat_node.hasProperty("title")) {
			    		title = cat_node.getProperty("title").getValue().getString();
			    		%>			    		
					    <category>
					      <title><%=title%></title>
					      <path><%=category_path%></path>
					    </category><%
			    	}				    	
			    }
			}	
	    }	    
	}
	%>
	</categories>
	<% 
    String urlGif = sling.getService(org.iavante.sling.gad.content.ContentTools.class).getExternalUrl(node, "thumbnail_preview_3");
  %>
  <urlGif><![CDATA[<%=urlGif%>]]></urlGif>	
	<author><% if(node.hasProperty("author")) { %><%=node.getProperty("author").getValue().getString()%><%}%></author>
	<schema><% if(node.hasProperty("schema")) { %><%=node.getProperty("schema").getValue().getString()%><%}%></schema>
	<hits><% if(node.hasProperty("hits")) { %><%=node.getProperty("hits").getValue().getString()%><%}%></hits>
	<thumbnail><![CDATA[<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).getThumbUrl(node)%>]]></thumbnail>
	<created><% if(node.hasProperty("jcr:created")) { %><%=node.getProperty("jcr:created").getValue().getString()%><%}%></created>
	<createdBy><% if(node.hasProperty("jcr:createdBy")) { %><%=node.getProperty("jcr:createdBy").getValue().getString()%><%}%></createdBy>
	<lastModified><% if(node.hasProperty("jcr:lastModified")) { %><%=node.getProperty("jcr:lastModified").getValue().getString()%><%}%></lastModified>
  <lastModifiedBy><% if(node.hasProperty("jcr:lastModifiedBy")) { %><%=node.getProperty("jcr:lastModifiedBy").getValue().getString()%><%}%></lastModifiedBy>
</content>
<%count--; %>
<%}%>
</contents>
</Result>