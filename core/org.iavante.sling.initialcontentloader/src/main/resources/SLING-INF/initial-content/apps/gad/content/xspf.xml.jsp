
<%
	/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page contentType="application/smil; charset=UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="javax.jcr.Node"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="javax.jcr.*,org.apache.sling.api.resource.Resource"%>
<%@ page import="org.iavante.sling.gad.content.ContentTools"%>
<%@ page import="org.iavante.sling.commons.services.ServiceProvider"%>
<%@ taglib prefix="sling"
	uri="http://sling.apache.org/taglibs/sling/1.0"%>
<%@ page
	import="javax.jcr.*,javax.jcr.Property,javax.jcr.Value,org.apache.sling.api.resource.Resource"%>
<%@ page import="org.iavante.sling.gad.content.ContentTools"%>
<sling:defineObjects />

<%
	String ticket = request.getParameter("ticket");

	if (ticket == null) {
		response.setStatus(response.SC_FORBIDDEN);
	} else {
		boolean is_valid = sling.getService(org.iavante.sling.commons.services.ServiceProvider.class).ticket_is_valid("downloader", ticket);

		if (is_valid) {
			if (request.getParameter("storage") == null
					|| request.getParameter("storage").compareTo("external") == 0) {

				String contentTitle = "";
				String contentOwner = "";
				String contentDescription = "";
				String contentVideoPreviewDefaultUrl = "";
				String contentThumbPreviewDefaultUrl = "";
				String contentSubtitleDefaultUrl = "";
				String sourceSubtitleDefaultUrl = "";
				String contentCierrePreviewUrl = "";
				String S3Url = "";
				String endingVideoUrl = "";
				String schema = "";
				String contentType = "";
				String contentThumbPreviewUrl = "";
				Boolean needEnding = false;
				try {
					if (currentNode.hasProperty("title")) {
						contentTitle = currentNode.getProperty("title").getValue().getString();
					}

					if (currentNode.hasProperty("description")) {
						contentDescription = currentNode.getProperty("description").getValue().getString();
					}

					if (currentNode.hasProperty("author")) {
						contentOwner = currentNode.getProperty("author").getValue().getString();
					}

					if (currentNode.hasProperty("schema")) {
						schema = currentNode.getProperty("schema").getValue().getString();
					}

					if (currentNode.hasProperty("needEnding")
							&& currentNode.getProperty("needEnding").getValue().getString().compareToIgnoreCase("1") == 0) {
						needEnding = true;
					}

					contentThumbPreviewDefaultUrl = sling.getService(org.iavante.sling.gad.content.ContentTools.class).getThumbUrl(currentNode);

					contentSubtitleDefaultUrl = sling.getService(org.iavante.sling.gad.content.ContentTools.class).getSubtitleUrl(currentNode, null);

					contentVideoPreviewDefaultUrl = sling.getService(org.iavante.sling.gad.content.ContentTools.class).getS3Url(currentNode);

					endingVideoUrl = sling.getService(org.iavante.sling.gad.content.ContentTools.class).getEndingVideoUrl(currentNode);

				} catch (ValueFormatException e) {
					e.printStackTrace();
					
				} catch (IllegalStateException e) {
					e.printStackTrace();
					
				} catch (PathNotFoundException e) {
					e.printStackTrace();
					
				} catch (RepositoryException e) {
					e.printStackTrace();
					
				}
%>
				<%="<playlist version='1' xmlns='http://xspf.org/ns/0/' xmlns:jwplayer='http://developer.longtailvideo.com/trac/wiki/FlashFormats'>"%>
				<%="<title>"%><%=contentTitle%><%="</title>"%>
				<%="<annotation>"%><%=contentDescription%><%="</annotation>"%>

				<%="<tracklist>"%>

				<%if (schema.equals("default")) {%>

					<%="<track>"%>
					<%="<title>" + contentTitle + "</title>"%>
					<%="<creator>" + contentOwner + "</creator>"%>
					<%="<annotation>" + contentDescription + "</annotation>"%>
					<%="<location>" + contentVideoPreviewDefaultUrl + "</location>"%>
					<%="<image>" + contentThumbPreviewDefaultUrl + "</image>"%>
					<%if (contentSubtitleDefaultUrl.equals("") || contentSubtitleDefaultUrl.equals(null)) {
						//here we can put a alternative subtitle if doesn't exist the default.
						//xspfstring+="<jwplayer:captions.file>"+"fixed subtitle:"+""+"</jwplayer:captions.file>";
					} else {%>
						<%="<jwplayer:captions.file>" + contentSubtitleDefaultUrl + "</jwplayer:captions.file>"%>
					<%}%>
					<%="</track>"%>
				<%} //content default%>

				<%if (schema.equals("playlist")) {%>
					<%
					NodeIterator it = currentNode.getNode("sources").getNodes();
					int x = 0;
					while (it.hasNext()) {
						Node nodeFuente = it.nextNode();

						if ((nodeFuente.hasProperty("file"))) {%>
						<%
						try {
							if (nodeFuente.hasProperty("title")) {
								contentTitle = nodeFuente.getProperty("title").getValue().getString();
							}

							if (nodeFuente.hasProperty("description")) {
								contentDescription = nodeFuente.getProperty("description").getValue().getString();
							}

							if (nodeFuente.hasProperty("author")) {
								contentOwner = nodeFuente.getProperty("author").getValue().getString();
							}
							
							if (nodeFuente.hasProperty("type")) {
								contentType = nodeFuente.getProperty("type").getValue().getString();
							}

							contentVideoPreviewDefaultUrl = sling.getService(org.iavante.sling.gad.content.ContentTools.class).getS3Url(nodeFuente);

							contentThumbPreviewUrl = sling.getService(org.iavante.sling.gad.content.ContentTools.class).getThumbPreviewUrl(nodeFuente,0);

							sourceSubtitleDefaultUrl = sling.getService(org.iavante.sling.gad.content.ContentTools.class).getSubtitleUrl(nodeFuente, null);

							} catch (ValueFormatException e) {
								e.printStackTrace();
								
							} catch (IllegalStateException e) {
								e.printStackTrace();
								
							} catch (PathNotFoundException e) {
								e.printStackTrace();
								
							} catch (RepositoryException e) {
								e.printStackTrace();
								
							}

							if (contentType.equals("video")) {
								x++;
								String itemThumb = contentThumbPreviewUrl;
								if (x == 1) {
									itemThumb = contentThumbPreviewDefaultUrl;
								}%>
								<%="<track>"%>
								<%="<title>" + contentTitle + "</title>"%>
								<%="<creator>" + contentOwner + "</creator>"%>
								<%="<annotation>" + "Contenido " + x + "." + "</annotation>"%>
								<%="<image>" + itemThumb + "</image>"%>
								<%="<location>" + contentVideoPreviewDefaultUrl + "</location>"%>
								<%
								if (sourceSubtitleDefaultUrl.equals("") || sourceSubtitleDefaultUrl.equals(null)) {
									//here we can put a alternative subtitle if doesn't exist the default.
									//xspfstring+="<jwplayer:captions.file>"+"fixed subtitle:"+""+"</jwplayer:captions.file>";
								} else { %>
									<%="<jwplayer:captions.file>" + sourceSubtitleDefaultUrl + "</jwplayer:captions.file>"%>
								<%}%>
								<%="</track>"%>
							<%} //end is video%>
						<%} //has file%>
					<%} //while of iteration%>
				<%} //end playlist%>
			<%
			if (!needEnding) {
				//here we can put a alternative Ending Video
			} else {%>
				<%="<track>"%>
				<%="<title>" + "Cierre de Canal" + "</title>"%>
				<%="<creator>" + "GAD Channel Settings" + "</creator>"%>
				<%="<annotation>" + "GAD Channel Settings" + "</annotation>"%>
				<%="<location>" + endingVideoUrl + "</location>"%>
				<%="</track>"%>
			<%} //end Ending Video%>

			<%="</tracklist>"%>
			<%="</playlist>"%>

		<% // end storage=null
		}	else {
			if (request.getParameter("storage").compareTo("internal") == 0) {%>
				<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).getXSPF(currentNode, "external")%>
			<%} //end if extorage internal
		} // end valid
	} //end else ticket not null
} //end else ticket not null
%>
