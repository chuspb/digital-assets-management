<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page contentType="application/smil; charset=UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.*, org.apache.sling.api.resource.Resource"%>
<%@ page import="org.iavante.sling.gad.content.ContentTools"%>
<%@ page import="org.iavante.sling.commons.services.ServiceProvider"%>
<%@ taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />

<%
String ticket = request.getParameter("ticket");

if (ticket == null) {
	response.setStatus( response.SC_FORBIDDEN );
}
else {
	boolean is_valid = sling.getService(org.iavante.sling.commons.services.ServiceProvider.class).ticket_is_valid("downloader", ticket);
	
	
	 
	if (is_valid) {	
	  if (request.getParameter("storage") == null || request.getParameter("storage") .compareTo("external") == 0){
			sling.getService(org.iavante.sling.gad.content.ContentTools.class).getPlaylist(currentNode,"external");
	  }
	  else{
	    if (request.getParameter("storage").compareTo("internal") == 0) {
		  sling.getService(org.iavante.sling.gad.content.ContentTools.class).getPlaylist(currentNode,"internal");
	    }
	  }
	}
	else {
	    response.setStatus( response.SC_FORBIDDEN );
	}	
}
%>