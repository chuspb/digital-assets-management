/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.cpsies.services.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.NotActiveException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.jcr.AccessDeniedException;
import javax.jcr.ItemNotFoundException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;
import javax.servlet.http.HttpServletRequest;

import org.iavante.sling.commons.services.DistributionServiceProvider;
import org.iavante.sling.commons.services.PortalSetup;
import org.iavante.sling.gad.cpsies.services.ICpsIesShare;
import org.jets3t.service.S3Service;
import org.jets3t.service.S3ServiceException;
import org.jets3t.service.acl.AccessControlList;
import org.jets3t.service.impl.rest.httpclient.RestS3Service;
import org.jets3t.service.model.S3Bucket;
import org.jets3t.service.model.S3Object;
import org.jets3t.service.security.AWSCredentials;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.XmlRpcException; //import org.apache.commons.codec.binary.Base64;

import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see org.iavante.sling.gad.cpsies.services.ICpsIesShare
 * @scr.component immediate="true"
 * @scr.property name="service.description"
 *               value="IAVANTE - CPS-IES Share Integration Implementation"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.service interface="org.iavante.sling.gad.cpsies.services.ICpsIesShare"
 * @scr.property name="service.type" value="distributionserver"
 * @scr.property name="service.typeimpl" value="cps"
 */
public class CpsIesShareImpl implements ICpsIesShare, Serializable {

	private static final long serialVersionUID = 1L;

	/** @scr.reference */
	private PortalSetup portalSetup;

	/** @scr.reference */
	DistributionServiceProvider myDSProvider = null;

	/** File prop */
	private final String FILE_PROP = "file";

	/** Sources folder */
	private final String SOURCES_FOLDER = "sources";

	/** CPS Server Proxy */
	private String cps_server_proxy = "";

	/** S3 Service. */
	private S3Service s3service;

	/** S3 Credentials. */
	private static AWSCredentials awsCredentials;

	/** S3 Bucket */
	private String bucket = null;

	/** CPS Property */
	private String CPS_PROP = "cps";

	/** Save the created content rpath **/
	private String CPS_CONTENT_RPATH = "cps_rpath";

	/** User **/
	private String user;

	/** Pass */
	private String pass;

	/** CPS Publish space */
	private String space = "";

	/** File system path */
	private String file_path = "";

	/** Default Logger. */
	private static final Logger log = LoggerFactory
			.getLogger(CpsIesShareImpl.class);

	/**
	 * Activate method
	 * 
	 * @param context
	 */
	protected void activate(ComponentContext context) {

		// Properties
		Map<String, String> cpsiesProperties = portalSetup
				.get_config_properties("/cpsies");
		Map<String, String> globalProperties = portalSetup
				.get_config_properties("/sling");
		Map<String, String> s3properties = portalSetup
				.get_config_properties("/s3backend");

		this.user = cpsiesProperties.get("cpsUser");
		this.pass = cpsiesProperties.get("cpsPass");
		this.bucket = s3properties.get("bucket");
		String url = cpsiesProperties.get("url");

		this.file_path = globalProperties.get("default_file_folder");

		if (!url.endsWith("/")) {
			url = url + "/";
		}

		if (!this.file_path.endsWith("/")) {
			this.file_path = this.file_path + "/";
		}

		this.cps_server_proxy = url + "portal_remote_controller";
		this.space = cpsiesProperties.get("space");

		if (!this.space.endsWith("/")) {
			this.space = this.space + "/";
		}

		String key = null;
		String secret = null;
		if (s3properties.containsKey("key"))
			key = s3properties.get("key");
		if (s3properties.containsKey("secret"))
			secret = s3properties.get("secret");

		awsCredentials = new AWSCredentials(key, secret);

		// To communicate with S3, create a class that implements an S3Service.
		// We will use the REST/HTTP implementation based on HttpClient, as this is
		// the most
		// robust implementation provided with jets3t.
		try {
			s3service = new RestS3Service(awsCredentials);
		} catch (Exception e) {
			assert false;
			e.printStackTrace();
		}
		if (log.isInfoEnabled())
			log.info("Service activated");
	}

	/*
	 * @see
	 * org.iavante.sling.gad.cpsies.services.ICpsIesShare#upload(javax.jcr.Node)
	 */
	public String upload(Node content) {

		if (log.isInfoEnabled())
			log.info("Upload content to CPS");

		// Configure the xmlrpcclient
		XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
		config.setBasicUserName(user);
		config.setBasicPassword(pass);

		try {
			if (log.isInfoEnabled())
				log.info("Proxy: " + cps_server_proxy);
			config.setServerURL(new URL(cps_server_proxy));
		} catch (MalformedURLException e) {

			e.printStackTrace();
		}

		XmlRpcClient client = new XmlRpcClient();
		client.setConfig(config);

		// CPS Video Properties
		String portal_type = "Video";
		String title = "";
		String author = "";
		String language = "es";

		String thumb_file = null;
		byte[] thumb_file_byte = null;

		String subtitle_file = null;
		byte[] subtitle_file_byte = null;

		String content_rpath = "";
		String emissions_rpath = "";
		String subtitles_rpath = "";

		// Maybe the content (video in cps) has already been created
		try {
			if (content.hasProperty(CPS_PROP)) {
				content_rpath = content.getProperty(CPS_PROP).getValue().getString();
				emissions_rpath = content_rpath + "/emissions/";
				subtitles_rpath = content_rpath + "/subtitles/";
				if (log.isInfoEnabled())
					log.info("Content already created in CPS");
			}

			// Or we have to create the content (video in cps) object
			else {
				if (log.isInfoEnabled())
					log.info("Creating content");
				try {
					if (content.hasProperty("title")) {
						title = content.getProperty("title").getValue().getString();
					}
					if (content.hasProperty("author")) {
						author = content.getProperty("author").getValue().getString();
					}
					if (log.isInfoEnabled())
						log.info("Author " + author + " Title " + title);
				} catch (ValueFormatException e1) {

					e1.printStackTrace();
				} catch (IllegalStateException e1) {

					e1.printStackTrace();
				} catch (PathNotFoundException e1) {

					e1.printStackTrace();
				} catch (RepositoryException e1) {

					e1.printStackTrace();
				}

				// Get the channel thumbnail transformation
				Node channel;
				String image_trans = "";
				try {
					channel = content.getParent().getParent();
					if (channel.hasProperty("distribution_format_image")) {
						image_trans = channel.getProperty("distribution_format_image")
								.getValue().getString();
						if (log.isInfoEnabled())
							log.info("Image trans:" + image_trans);
					}
				} catch (ItemNotFoundException e2) {

					e2.printStackTrace();
				} catch (AccessDeniedException e2) {

					e2.printStackTrace();
				} catch (RepositoryException e2) {

					e2.printStackTrace();
				}

				// Get the thumbnail default
				Node thumb_trans = null;
				try {
					thumb_trans = content.getNode("sources").getNode("thumbnail_default")
							.getNode("transformations").getNode(image_trans);
					if ((thumb_trans.hasProperty(FILE_PROP))
							&& (!"".equals(thumb_trans.getProperty(FILE_PROP).getValue()
									.getString()))) {
						thumb_file = thumb_trans.getProperty(FILE_PROP).getValue()
								.getString();
					}
				} catch (PathNotFoundException e2) {

					e2.printStackTrace();
				} catch (RepositoryException e2) {

					e2.printStackTrace();
				}

				if (!"".equals(thumb_file)) {
					try {
						thumb_file_byte = getBytesFromFile(new File(this.file_path
								+ thumb_file));
					} catch (IOException e2) {

						e2.printStackTrace();
					}
				}

				if (log.isInfoEnabled())
					log.info("Thumb file: " + thumb_file);
				// Get the subtitle default
				Node subtitle_trans = null;
				try {
					if (content.hasNode("sources/subtitle_default")) {
						subtitle_trans = content.getNode("sources").getNode(
								"subtitle_default").getNode("transformations").getNode(
								"preview");
						if ((subtitle_trans.hasProperty(FILE_PROP))
								&& (!"".equals(subtitle_trans.getProperty(FILE_PROP).getValue()
										.getString()))) {
							subtitle_file = subtitle_trans.getProperty(FILE_PROP).getValue()
									.getString();
						}
					}
				} catch (PathNotFoundException e2) {

					e2.printStackTrace();
				} catch (RepositoryException e2) {

					e2.printStackTrace();
				}

				if (!"".equals(subtitle_file)) {
					try {
						subtitle_file_byte = getBytesFromFile(new File(this.file_path
								+ subtitle_file));
					} catch (IOException e2) {

						e2.printStackTrace();
					}
				}

				if (log.isInfoEnabled())
					log.info("Subtitle file" + subtitle_file);

				Map<String, String> doc_def = new HashMap<String, String>();
				doc_def.put("Title", title);
				doc_def.put("author", author);
				doc_def.put("Language", language);
				if (log.isInfoEnabled())
					log.info("Content title: " + title + " Author: " + author
							+ "Language :" + language);
				Object[] params = new Object[] { new String(portal_type), doc_def,
						new String(this.space) };

				try {
					content_rpath = (String) client.execute("createDocument", params);
					content.setProperty(CPS_CONTENT_RPATH, content_rpath);
					content.save();
					if (log.isInfoEnabled())
						log.info("Content created: " + content_rpath);
				} catch (XmlRpcException e) {

					e.printStackTrace();
				}

				// Create emissions and subtitles folder
				Map<String, String> doc_def_sub = new HashMap<String, String>();
				doc_def_sub.put("Title", "Subtitles");

				Map<String, String> doc_def_emi = new HashMap<String, String>();
				doc_def_emi.put("Title", "Emissions");

				Object[] params_sub = new Object[] { new String("Subtitles"),
						doc_def_sub, new String(content_rpath), new Integer(0) };
				Object[] params_emision = new Object[] { new String("Emissions"),
						doc_def_emi, new String(content_rpath), new Integer(1) };

				try {
					subtitles_rpath = (String) client.execute("createDocument",
							params_sub);
					if (log.isInfoEnabled())
						log.info("Subtitles folder created");
					emissions_rpath = (String) client.execute("createDocument",
							params_emision);
					if (log.isInfoEnabled())
						log.info("Emissions folder created");
				} catch (XmlRpcException e) {

					e.printStackTrace();
				}

				content.setProperty(CPS_PROP, content_rpath);
				content.save();
				if (log.isInfoEnabled())
					log.info("Content saved");

				// Create subtitle
				if (!"".equals(subtitle_file)) {
					Map<String, Object> doc_def_subtitle = new HashMap<String, Object>();
					doc_def_subtitle.put("Title", "es");
					doc_def_subtitle.put("file", subtitle_file_byte);
					doc_def_subtitle.put("file_name", subtitle_file);
					doc_def_subtitle.put("file_key", "file");

					if (log.isInfoEnabled())
						log.info("Subtitle title: " + title);
					params = new Object[] { new String("Subtitle"), doc_def_subtitle,
							new String(subtitles_rpath) };

					try {
						String subtitle_rpath = (String) client.execute("createDocument",
								params);
						if (log.isInfoEnabled())
							log.info("Subtitle created at: " + subtitle_rpath);
					} catch (XmlRpcException e) {

						e.printStackTrace();
					}
				}

			}
		} catch (ValueFormatException e3) {

			e3.printStackTrace();
		} catch (IllegalStateException e3) {

			e3.printStackTrace();
		} catch (PathNotFoundException e3) {

			e3.printStackTrace();
		} catch (RepositoryException e3) {

			e3.printStackTrace();
		}

		NodeIterator transformations = null;
		try {
			transformations = content.getNode(SOURCES_FOLDER).getNode(
					"fuente_default").getNode("transformations").getNodes();
		} catch (PathNotFoundException e) {

			e.printStackTrace();
		} catch (RepositoryException e) {

			e.printStackTrace();
		}

		// Iter over all transformations and register at CPS.
		while (transformations.hasNext()) {
			Node transformation = transformations.nextNode();
			try {
				if (transformation.getName().contains("ies")) {
					try {
						if (!transformation.hasProperty(CPS_PROP)
								&& (transformation.hasProperty(FILE_PROP))
								&& (!"".equals(transformation.getProperty(FILE_PROP).getValue()
										.getString()))) {

							if (log.isInfoEnabled())
								log.info("Transformation: " + transformation.getPath());
							String file = transformation.getProperty(FILE_PROP).getValue()
									.getString();
							String length = "0:00";
							if (transformation.getParent().getParent().hasProperty("length")) {
								length = transformation.getParent().getParent().getProperty(
										"length").getValue().getString();
							}

							String url = null;
							try {
								url = this.getUrl(file);
								if (log.isInfoEnabled())
									log.info("Url de la emision: " + url);
							} catch (NotActiveException e) {

								e.printStackTrace();
							}

							String quality = "medium";
							String transformation_name = transformation.getName();

							if (transformation_name.contains("low"))
								quality = "low";
							if (transformation_name.contains("medium"))
								quality = "medium";
							if (transformation_name.contains("high"))
								quality = "high";

							Map<String, Object> emi_def = new HashMap<String, Object>();
							emi_def.put("Title", transformation_name);
							emi_def.put("file_url", url);
							emi_def.put("length", length);
							emi_def.put("quality", quality);

							if ((thumb_file_byte != null) && (thumb_file_byte.length > 0)) {
								emi_def.put("file", thumb_file_byte);
								emi_def.put("file_name", thumb_file);
								emi_def.put("file_key", new String("image"));
								emi_def.put("file_type", new String("Image"));
							}

							Object[] param_emi = new Object[] { new String("Emission"),
									emi_def, new String(emissions_rpath), new Integer(0) };
							try {
								if (log.isInfoEnabled())
									log.info("Creating emission: " + emissions_rpath);
								String rpath_emission = (String) client.execute(
										"createDocument", param_emi);
								if (log.isInfoEnabled())
									log.info("Emission created in CPS: " + rpath_emission);
								transformation.setProperty(CPS_PROP, rpath_emission);
								transformation.save();
								if (log.isInfoEnabled())
									log.info("Emission: " + rpath_emission);
							} catch (XmlRpcException e) {

								e.printStackTrace();
							}
						}
					} catch (ValueFormatException e) {

						e.printStackTrace();
					} catch (IllegalStateException e) {

						e.printStackTrace();
					} catch (PathNotFoundException e) {

						e.printStackTrace();
					} catch (RepositoryException e) {

						e.printStackTrace();
					}
				}
			} catch (RepositoryException e) {

				e.printStackTrace();
			}
		}

		// Publish the content
		Map<String, String> rpaths_to_publish = new HashMap<String, String>();
		rpaths_to_publish.put("sections/videos", "");

		Object[] param_publish = new Object[] { new String(content_rpath),
				rpaths_to_publish };
		try {
			if (log.isInfoEnabled())
				log.info("Publishing content");
			String rpath_published_content = (String) client.execute(
					"publishDocument", param_publish);
			if (log.isInfoEnabled())
				log.info("Content published at " + rpath_published_content);
		} catch (XmlRpcException e) {

			e.printStackTrace();
		}
		return content_rpath;
	}

	/*
	 * @see org.iavante.sling.gad.cpsies.services#getUrl
	 */
	public String getUrl(String media) throws NotActiveException {
		String bucket_url = s3service.generateS3HostnameForBucket(this.bucket);
		if (!bucket_url.contains("http") == true) {
			bucket_url = "http://" + bucket_url;
		}

		if (!bucket_url.endsWith("/")) {
			bucket_url = bucket_url + "/";
		}

		return bucket_url + media;

	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.iavante.sling.gad.cpsies.services.ICpsIesShare#upload(java.lang.String)
	 */
	public String upload(String fileName) {
		String results = null;
		try {
			// get the bucket
			S3Bucket bucket = s3service.getBucket(this.bucket);

			// create an object with the file data
			File fileData = new File(fileName);
			S3Object fileObject = new S3Object(bucket, fileData);
			fileObject.setAcl(AccessControlList.REST_CANNED_PUBLIC_READ);
			// put the data on S3
			s3service.putObject(bucket, fileObject);
			if (log.isInfoEnabled())
				log.info("Successfully uploaded object - " + fileObject);
			results = "200";

		} catch (S3ServiceException e) {
			log.error("Error uploading file to S3");
			return "500";
		} catch (NoSuchAlgorithmException e) {
			log.error("Error uploading file to S3");
			e.printStackTrace();
		} catch (IOException e) {
			log.error("Error getting data from file");
			e.printStackTrace();
		}
		return results;
	}

	/*
	 * (non-Javadoc)
	 * @see
	 * org.iavante.sling.gad.cpsies.services.ICpsIesShare#get_player(ServletRequest
	 * , HashMap)
	 */
	public String get_player(HttpServletRequest req,
			HashMap<String, Object> kwargs) {

		return "";
	}

	/**
	 * Returns a byte array from a file
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	private static byte[] getBytesFromFile(File file) throws IOException {
		InputStream is = new FileInputStream(file);

		// Get the size of the file
		long length = file.length();

		if (length > Integer.MAX_VALUE) {
			// File is too large
		}
		// Create the byte array to hold the data
		byte[] bytes = new byte[(int) length];
		// Read in the bytes
		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length
				&& (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
			offset += numRead;
		}

		// Ensure all the bytes have been read in
		if (offset < bytes.length) {
			throw new IOException("Could not completely read file " + file.getName());
		}

		// Close the input stream and return bytes
		is.close();
		return bytes;
	}
}