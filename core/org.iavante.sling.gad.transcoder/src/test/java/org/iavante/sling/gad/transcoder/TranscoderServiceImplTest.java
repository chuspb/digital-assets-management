/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.transcoder;

import java.util.Map;
import java.util.Iterator;
import javax.jcr.Node;
import javax.jcr.Session;

import org.iavante.sling.gad.transcoder.impl.*;

import org.apache.sling.commons.testing.jcr.RepositoryTestBase;
import org.apache.sling.jcr.api.SlingRepository;

public class TranscoderServiceImplTest extends RepositoryTestBase {

    SlingRepository repo;
    Session session;
    private Node rootNode;

    private final String trans_url = "http://1.2.3.4";
    private final String send_conversion_path = "/content/components/transcodingServer/jobs/pending";

	private TranscoderServiceImpl trans_service;
	
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    @Override
    protected void setUp() throws Exception {
    	
    	super.setUp();
    	session = getSession();
        rootNode = getSession().getRootNode();  
        
        // Add content node
        Node content_node = rootNode.addNode("content", "nt:unstructured");
        
        // Add Preview conversion type node
        Node preview_type = content_node.addNode("components", "nt:unstructured").addNode("transcoder", "nt:unstructured").addNode("preview", "nt:unstructured");
        
        preview_type.setProperty("description", "Description test");
        preview_type.setProperty("command", "test");
        preview_type.setProperty("params", "-test -test");
    
        // Add transcoder config node
        Node transcoder_config_node = content_node.addNode("config", "nt:unstructured").addNode("transcoder", "nt:unstructured");
        transcoder_config_node.setProperty("url", trans_url);
        transcoder_config_node.setProperty("send_conversion_path", send_conversion_path);
        
        rootNode.save();
        session.save();     
        
        trans_service = new TranscoderServiceImpl(rootNode);
    }	
	
	/**
	 * Tests if the preview type exists. The preview conversion type is mandatory for all 
	 * transcoder implementations
	 */
/*	public void test_ConversionTypes() {
		
		Map<String, String> conversion_types = trans_service.getConversionTypes(null);		
		boolean preview_exists = false;		
		Iterator it = conversion_types.entrySet().iterator();
		
		while (it.hasNext()) {
			Map.Entry<String, String> entry = (Map.Entry)it.next();			
			if (entry.getKey().toString().equals("preview")) {
				preview_exists = true;
			}			
		}		
		assertTrue(preview_exists);
	}*/
	
	/**
	 * Test if we can get a conversion properties
	 */
	public void test_get_Conversion() {
		
		Map<String, String> conversion_props = trans_service.getConversion("preview");
		String command = conversion_props.get("command");
		assertEquals(command, "test");
	}
	
	/**
	 * Tests if the conversion task has been sent and recived by the transcoder
	 */
	public void test_sendConversionTask() {
		
		String conversion_type = "preview";
		String source_location = "/test/input_file.avi";
		String target_location = "/test/output_file.avi";
		String notification_url = "http://test.iavante.es/";
		String external_storage_url = "http://test_storage.iavante.es/";
		String external_storage_server = "S3";
		int response_status_code = trans_service.sendConversionTask(conversion_type, source_location, target_location, notification_url, external_storage_server, external_storage_url);
		
		assertEquals(response_status_code, 405);
	}
}
