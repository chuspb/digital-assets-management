/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.transcoder;

import java.io.*;
import java.util.List;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.*;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.*;

/**
 * Test content creation and schema association
 */
public class TranscoderServiceImplIT
		extends TestCase {

	private final String HOSTVAR = "SLINGHOST";
	private final String HOSTPREDEF = "localhost:8888";
	private String SLING_URL = "http://";

	private List authPrefs;
	private HttpClient client;
	private Credentials defaultcreds;

	private String CONFIG_FOLDER = "config";
	private String PREVIEW_TRANSFORMATION = "preview";
	private String TRANSCODER_FOLDER = "transcoder";
	private String COMPONENTS_FOLDER = "components";

	protected void setUp() {

		Map<String, String> envs = System.getenv();
		Set<String> keys = envs.keySet();

		Iterator<String> it = keys.iterator();
		boolean hashost = false;
		while (it.hasNext()) {
			String key = (String) it.next();

			if (key.compareTo(HOSTVAR) == 0) {
				SLING_URL = SLING_URL + (String) envs.get(key);
				hashost = true;
			}
		}
		if (hashost == false)
			SLING_URL = SLING_URL + HOSTPREDEF;

		client = new HttpClient();

		defaultcreds = new UsernamePasswordCredentials("admin", "admin");

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);
	}

	protected void tearDown() {

	}

	/**
	 * Tests if config properties are created
	 **/
	public void test_transcoder_properties() {

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Get transcoder config folder
		HttpMethod get_trans_config = new GetMethod(SLING_URL + "/" + "content"
				+ "/" + CONFIG_FOLDER + "/" + TRANSCODER_FOLDER + "/url");
		get_trans_config.setDoAuthentication(true);
		try {
			client.executeMethod(get_trans_config);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertEquals(200, get_trans_config.getStatusCode());
		get_trans_config.releaseConnection();
	}

	/**
	 * Tests if the transcoder folder is created and the preview transformation
	 * type
	 **/
	public void test_transcoder_folder() {

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Get transcoder folder
		HttpMethod get_trans_folder = new GetMethod(SLING_URL + "/" + "content"
				+ "/" + COMPONENTS_FOLDER + "/" + TRANSCODER_FOLDER + "/"
				+ PREVIEW_TRANSFORMATION + ".html");
		get_trans_folder.setDoAuthentication(true);
		try {
			client.executeMethod(get_trans_folder);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertEquals(200, get_trans_folder.getStatusCode());
		get_trans_folder.releaseConnection();
	}
}