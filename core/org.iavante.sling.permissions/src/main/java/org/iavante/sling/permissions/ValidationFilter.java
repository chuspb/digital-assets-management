/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.permissions;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.jcr.SimpleCredentials;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.engine.auth.AuthenticationInfo;
import org.apache.sling.jcr.api.SlingRepository;
import org.iavante.sling.commons.services.AuthToolsService;
import org.iavante.sling.commons.services.PortalSetup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This filter is suscribed to POST requests and it checks if the
 * sling:resourceType property of the request is allowed in the requested path.
 * 
 * @scr.component immediate="true" metatype="no"
 * @scr.property name="service.description" value="IAV GAD Validation Filter"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.property name="filter.scope" value="request" private="true"
 * @scr.property name="filter.order" value="1" type="Integer" private="true"
 * @scr.service
 */
public class ValidationFilter implements Filter, Serializable {
	private static final long serialVersionUID = 1L;

	/** Delimiter. */
	private String DELIMITER = ",";

	/** Default Logger. */
	private final Logger log = LoggerFactory.getLogger(getClass());

	/** @scr.reference */
	private SlingRepository repository;

	/** @scr.reference */
	private PortalSetup portalSetup;

	/** @scr.reference */
	private AuthToolsService authTools;

	/** Admin suffix. */
	private final String ADMIN_SUFFIX = ".admin";

	/** Admin prefix. */
	private final String ADMIN_PREFIX = "administration";

	/** Hierarchy properties. */
	private Map<String, String> hierarchyProperties = null;

	/** sling properties. */
	private Map<String, String> slingProperties = null;

	public String getRepository() {
		return repository.getDescriptor(SlingRepository.REP_NAME_DESC);
	}

	/*
	 * Does nothing. This filter has not initialization requirement.
	 * @see javax.servlet.Filter
	 */
	public void init(FilterConfig filterConfig) {
		this.hierarchyProperties = portalSetup.get_config_properties("/hierarchy");
		this.slingProperties = portalSetup.get_config_properties("/sling");
	}

	/*
	 * Checks the request Authorization Headers and use this information to
	 * authenticate against LDAP.
	 * @see javax.servlet.Filter
	 */
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {

		boolean resolved = false;
		SlingHttpServletRequest request = (SlingHttpServletRequest) req;
		SlingHttpServletResponse response = (SlingHttpServletResponse) res;

		AuthenticationInfo credentials = authTools.extractAuthentication(request);
		SimpleCredentials authInfo = new SimpleCredentials("any", "any"
				.toCharArray());
		String slingUser = "admin";
		if (slingProperties.get("sling_user") != null) {
			slingUser = slingProperties.get("sling_user");
		}
		if (credentials != null) {
			authInfo = (SimpleCredentials) credentials.getCredentials();
		}

		boolean isAdminPath = false;
		String requestedPath = request.getRequestURI();
		log.debug("requested_path:" + requestedPath);
		if ((requestedPath.endsWith(ADMIN_SUFFIX))
				|| (requestedPath.startsWith("/" + ADMIN_PREFIX))
				|| (requestedPath.startsWith("/apps/" + ADMIN_PREFIX))
				|| (requestedPath.startsWith("/content/upload"))) {
			isAdminPath = true;
		}

		int isAdmin = slingUser.compareTo(authInfo.getUserID());

		if ((request.getMethod() == "POST") && (isAdmin != 0) && !isAdminPath) {

			String operation = request.getParameter(":operation");
			String order = request.getParameter(":order");
			if ((operation != null) || (order != null)) {
				chain.doFilter(request, response);
			} else {
				String resourceType = request.getParameter("sling:resourceType");
				String absolutePath = request.getPathInfo().substring(1);

				List<String> pathList = Arrays.asList(absolutePath.split("/"));

				String container = pathList.get(pathList.size() - 2);

				String allowedTypes = hierarchyProperties.get(container) + DELIMITER;
				String[] resourceTypes = allowedTypes.split(",");

				if ((resourceTypes == null) || (resourceType == null)) {
					// Unauthorized
					log.error("POST invalid");
					response.setStatus(401);
					response.sendError(401);
				} else {
					for (String type : resourceTypes) {
						if (resourceType.equals(type)) {

							chain.doFilter(request, response);
							resolved = true;
						}
					}
					if (resolved == false) {
						if (log.isInfoEnabled())
							log.info("Forbiden " + resourceType + " in " + absolutePath);
						response.setStatus(response.SC_FORBIDDEN);
					}
				}
			}
		} else {
			chain.doFilter(request, response);
		}

	}

	/*
	 * Does nothing. This filter has not destroy requirement.
	 * @see javax.servlet.Filter
	 */
	public void destroy() {
	}

}
