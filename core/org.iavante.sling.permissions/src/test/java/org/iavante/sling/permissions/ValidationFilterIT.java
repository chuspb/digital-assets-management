/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.permissions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthPolicy;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.PostMethod;

/**
 * Test validation filter
 */
public class ValidationFilterIT
		extends TestCase {

	private String CATALOG_URL = "/content/catalogo/";
	private String COLLECTIONS_URL = "/content/colecciones/";
	private String CONTENTS_FOLDER = "contents";

	private String location;
	private String title;
	private String schema;
	private String slug_content;
	private Credentials defaultcreds;
	private Credentials anonymouscreds;
	private List authPrefs = new ArrayList(2);
	private String slug_collection;

	private final String HOSTVAR = "SLINGHOST";
	private final String HOSTPREDEF = "localhost:8888";
	private String SLING_URL = "http://";

	HttpClient client;

	@org.junit.Before
	public void setUp() {

		Map<String, String> envs = System.getenv();
		Set<String> keys = envs.keySet();

		Iterator<String> it = keys.iterator();
		boolean hashost = false;
		while (it.hasNext()) {
			String key = (String) it.next();

			if (key.compareTo(HOSTVAR) == 0) {
				SLING_URL = SLING_URL + (String) envs.get(key);
				hashost = true;
			}
		}
		if (hashost == false)
			SLING_URL = SLING_URL + HOSTPREDEF;

		client = new HttpClient();
		title = "Test case content";
		schema = "default";
		slug_content = "test_case_content";
		slug_collection = "admin";
		authPrefs.add(AuthPolicy.DIGEST);
		authPrefs.add(AuthPolicy.BASIC);
		defaultcreds = new UsernamePasswordCredentials("admin", "admin");
		anonymouscreds = new UsernamePasswordCredentials("anonymous", "anonymous");

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		PostMethod post_create_col = new PostMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection);

		post_create_col.setDoAuthentication(true);

		NameValuePair[] data_create_col = {
				new NameValuePair("sling:resourceType", "gad/collection"),
				new NameValuePair("title", title), new NameValuePair("subtitle", ""),
				new NameValuePair("schema", schema),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };
		post_create_col.setRequestBody(data_create_col);

		try {
			client.executeMethod(post_create_col);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		// Collection created
		post_create_col.releaseConnection();
	}

	@org.junit.After
	public void tearDown() {

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Delete the collection
		PostMethod post_delete = new PostMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection);
		NameValuePair[] data_delete = { new NameValuePair(":operation", "delete"), };

		post_delete.setDoAuthentication(true);
		post_delete.setRequestBody(data_delete);

		try {
			client.executeMethod(post_delete);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		post_delete.releaseConnection();
	}

	@org.junit.Test
	public void test_createContent_ok() {

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		PostMethod post_create_ok = new PostMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content);
		post_create_ok.setDoAuthentication(true);
		NameValuePair[] data_create_ok = {
				new NameValuePair("sling:resourceType", "gad/content"),
				new NameValuePair("title", this.title),
				new NameValuePair("schema", this.schema),
				new NameValuePair(
						"description",
						"Content description generated by test case. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."),
				new NameValuePair("author", "Test case"),
				new NameValuePair("origin", "Test case"),
				new NameValuePair("lang", "es"),
				new NameValuePair("tags", "test case"),
				new NameValuePair("tags@TypeHint", "String[]"),
				new NameValuePair("state", "pending"),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };

		post_create_ok.setRequestBody(data_create_ok);
		post_create_ok.setDoAuthentication(true);
		try {
			client.executeMethod(post_create_ok);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		// Content created
		assertEquals(201, post_create_ok.getStatusCode());
		post_create_ok.releaseConnection();
	}

	@org.junit.Test
	public void test_createContent_bad() {

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		PostMethod post_create_bad = new PostMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content);
		post_create_bad.setDoAuthentication(true);

		NameValuePair[] data_create_bad = {
				new NameValuePair("sling:resourceType", "gad/source"),
				new NameValuePair("title", this.title),
				new NameValuePair("schema", this.schema),
				new NameValuePair(
						"description",
						"Content description generated by test case. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."),
				new NameValuePair("author", "Test case"),
				new NameValuePair("origin", "Test case"),
				new NameValuePair("lang", "es"),
				new NameValuePair("tags", "test, case"),
				new NameValuePair("state", "pending"),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };

		post_create_bad.setRequestBody(data_create_bad);
		post_create_bad.setDoAuthentication(true);
		try {
			client.getState().setCredentials(AuthScope.ANY, anonymouscreds);
			client.executeMethod(post_create_bad);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} finally {
			client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		}

		// Content created bad
		assertEquals(403, post_create_bad.getStatusCode());
		post_create_bad.releaseConnection();
	}
}