<?xml version="1.0" encoding="UTF-8"?>
<Result xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

<%@ page language="java" contentType="text/xml; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.*,
        org.apache.sling.api.resource.Resource"
%>
<%@ page import="org.iavante.sling.commons.services.DistributionServiceProvider"%>
<%@ page import="org.iavante.sling.commons.distributionserver.*"%>
<%@ taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />
<value>
<%  
String test = request.getParameter("test");
String pepas = "SIN VALOR";

if (test.compareToIgnoreCase("listdistributionservers") == 0){
	List<String> services = sling.getService(org.iavante.sling.commons.services.DistributionServiceProvider.class).list_distribution_servers();
	pepas = services.toString();
}

if (test.compareToIgnoreCase("getdistributionserver") == 0){
	IDistributionServer services = sling.getService(org.iavante.sling.commons.services.DistributionServiceProvider.class).get_distribution_server("s3");
	pepas = services.getUrl("test_12345.flv");
}
%>
<%=pepas%>
</value>
</Result>