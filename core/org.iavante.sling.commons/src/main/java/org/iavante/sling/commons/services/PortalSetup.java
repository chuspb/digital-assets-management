/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.commons.services;

import java.util.List;
import java.util.Map;

import org.osgi.framework.Bundle;

/**
 * Utilities with bundles and node properties
 * 
 * @scr.property name="service.description" value="IAVANTE -Portalsetup service"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */

public interface PortalSetup {

	/**
	 * Returns configuration properties.
	 * 
	 * @param type
	 *          The type of properties to return. /searcher, /uploader,
	 *          /authentication
	 * @return Map with properties and values
	 */
	public Map<String, String> get_config_properties(String type);

	/**
	 * Check if the given method is WebDav related.
	 * 
	 * @param method
	 *          the method (request.getMethod())
	 * @return method is WebDav related
	 */
	public boolean isWebDavMethod(String method);

	/**
	 * Check if a specific bundle is installed
	 * 
	 * @param symbolicName
	 *          The name of the bundle i.e.:
	 *          "org.iavante.sling.gad.administration"
	 * @return boolean true or false
	 */
	public boolean isBundleInstalled(String symbolicName);

	/**
	 * Return the Bundles that works like Distribution Servers To define a
	 * Distribution bundle is necessary to add a new label in "pom.xml" please see
	 * the Bundles types documentation for more details.
	 * 
	 * @return List of bundles
	 */
	public List<Bundle> getDistributionBundles();

	/**
	 * Return the Bundles that works like Transcoding Servers To define a
	 * Transcoding bundle is necessary to add a new label in "pom.xml" please see
	 * the Bundles types documentation for more details.
	 * 
	 * @return List of bundles
	 */
	public List<Bundle> getTranscodingBundles();

}
