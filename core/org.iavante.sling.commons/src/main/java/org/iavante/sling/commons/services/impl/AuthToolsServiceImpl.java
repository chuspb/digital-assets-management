/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.commons.services.impl;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;

import javax.jcr.SimpleCredentials;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;
import org.apache.sling.engine.auth.AuthenticationInfo;
import org.iavante.sling.commons.services.AuthToolsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see org.iavante.sling.commons.services.AuthToolsService
 * @scr.component immediate="true"
 * @scr.property name="service.description"
 *               value="IAVANTE Authentication Tools Service Impl"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.service interface="org.iavante.sling.commons.services.AuthToolsService"
 */
public class AuthToolsServiceImpl implements AuthToolsService, Serializable {
	private static final long serialVersionUID = 1L;

	/** Default log. */
	private final Logger log = LoggerFactory.getLogger(getClass());

	/** To force logout. */
	private static final String NOT_LOGGED_IN_USER = "__forced_logout_user__";

	/** Header Authorization. */
	private static final String HEADER_AUTHORIZATION = "Authorization";

	/** Authentication scheme. */
	private static final String AUTHENTICATION_SCHEME_BASIC = "Basic";

	
	public AuthToolsServiceImpl() {
	}

	/*
	 * @see org.iavante.sling.commons.services.AuthToolsService
	 */
	@Override
	public AuthenticationInfo extractAuthentication(HttpServletRequest request) {

		// Return immediately if the header is missing
		String authHeader = request.getHeader(HEADER_AUTHORIZATION);
		log.info("Auth header" + authHeader);
		if (authHeader == null || authHeader.length() == 0) {

			// try to fall back to cookies
			Cookie[] cookies = request.getCookies();
			if (cookies != null) {
				for (int i = 0; i < cookies.length; i++) {
					if (HEADER_AUTHORIZATION.equalsIgnoreCase(cookies[i].getName())) {
						authHeader = cookies[i].getValue();
						break;
					}
				}
			}

			// If still no authentication, return null
			if (authHeader == null || authHeader.length() == 0) {
				return null;
			}
		}

		// Get the authType (Basic, Digest) and authInfo (user/password) from
		// the header
		authHeader = authHeader.trim();
		int blank = authHeader.indexOf(' ');
		if (blank <= 0) {
			return null;
		}
		String authType = authHeader.substring(0, blank);
		String authInfo = authHeader.substring(blank).trim();

		// Check whether authorization type matches
		if (!authType.equalsIgnoreCase(AUTHENTICATION_SCHEME_BASIC)) {
			return null;
		}

		// Base64 decode and split on colon

		// we cannot use default base64, since we need iso encoding
		// (nb: ISO-8859-1 is required as per API spec to be available)
		String decoded;
		try {
			byte[] encoded = authInfo.getBytes("ISO-8859-1");
			byte[] bytes = Base64.decodeBase64(encoded);
			decoded = new String(bytes, "ISO-8859-1");
		} catch (UnsupportedEncodingException uee) {
			// unexpected
			log.error("extractAuthentication: Cannot en/decode authentication info",
					uee);
			return null;
		}

		SimpleCredentials creds;
		int colIdx = decoded.indexOf(':');
		if (colIdx < 0) {
			creds = new SimpleCredentials(decoded, new char[0]);
		} else {
			creds = new SimpleCredentials(decoded.substring(0, colIdx), decoded
					.substring(colIdx + 1).toCharArray());
		}

		if (NOT_LOGGED_IN_USER.equals(creds.getUserID())) {
			return null;
		}

		return new AuthenticationInfo(HttpServletRequest.BASIC_AUTH, creds);
	}
	
	
	/*
	 * @see org.iavante.sling.commons.services.AuthToolsService
	 */
	public String extractUserPass(HttpServletRequest request) {

		// Return immediately if the header is missing
		String authHeader = request.getHeader(HEADER_AUTHORIZATION);
		log.info("Auth header" + authHeader);
		if (authHeader == null || authHeader.length() == 0) {

			// try to fall back to cookies
			Cookie[] cookies = request.getCookies();
			if (cookies != null) {
				for (int i = 0; i < cookies.length; i++) {
					if (HEADER_AUTHORIZATION.equalsIgnoreCase(cookies[i].getName())) {
						authHeader = cookies[i].getValue();
						break;
					}
				}
			}

			// If still no authentication, return null
			if (authHeader == null || authHeader.length() == 0) {
				return null;
			}
		}

		// Get the authType (Basic, Digest) and authInfo (user/password) from
		// the header
		authHeader = authHeader.trim();
		int blank = authHeader.indexOf(' ');
		if (blank <= 0) {
			return null;
		}
		String authType = authHeader.substring(0, blank);
		String authInfo = authHeader.substring(blank).trim();

		// Check whether authorization type matches
		if (!authType.equalsIgnoreCase(AUTHENTICATION_SCHEME_BASIC)) {
			return null;
		}

		// Base64 decode and split on colon

		// we cannot use default base64, since we need iso encoding
		// (nb: ISO-8859-1 is required as per API spec to be available)
		String decoded;
		try {
			byte[] encoded = authInfo.getBytes("ISO-8859-1");
			byte[] bytes = Base64.decodeBase64(encoded);
			decoded = new String(bytes, "ISO-8859-1");
		} catch (UnsupportedEncodingException uee) {
			// unexpected
			log.error("extractAuthentication: Cannot en/decode authentication info",
					uee);
			return null;
		}

//		UsernamePasswordCredentials creds;
//		int colIdx = decoded.indexOf(':');
//		if (colIdx < 0) {
//			creds = new UsernamePasswordCredentials(decoded);
//		} else {
//			creds = new UsernamePasswordCredentials(decoded.substring(0, colIdx), decoded
//					.substring(colIdx + 1));
//		}
//
//		return creds;
		
		return decoded;
	}

}
