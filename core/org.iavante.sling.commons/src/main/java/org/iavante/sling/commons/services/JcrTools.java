/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.commons.services;

import javax.jcr.Node;
import javax.jcr.Property;
import javax.jcr.RepositoryException;

/**
 * Special operations against JCR.
 * 
 * @scr.property name="service.description" value="IAVANTE - JCR Tools"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */
public interface JcrTools {

	/**
	 * Copy the <code>src</code> node into the <code>dstParent</code> node. The
	 * name of the newly created node is set to <code>name</code>.
	 * <p>
	 * This method does a recursive (deep) copy of the subtree rooted at the
	 * source node to the destination. Any protected child nodes and and
	 * properties are not copied.
	 * 
	 * @param src
	 *          The node to copy to the new location
	 * @param dstParent
	 *          The node into which the <code>src</code> node is to be copied
	 * @param name
	 *          The name of the newly created node. If this is <code>null</code>
	 *          the new node gets the same name as the <code>src</code> node.
	 * @throws RepositoryException
	 *           May be thrown in case of any problem copying the content.
	 */
	public void copy(Node src, Node dstParent, String name)
			throws RepositoryException;

	/**
	 * Copy the <code>src</code> property into the <code>dstParent</code> node.
	 * The name of the newly created property is set to <code>name</code>.
	 * <p>
	 * If the source property is protected, this method does nothing.
	 * 
	 * @param src
	 *          The property to copy to the new location
	 * @param dstParent
	 *          The node into which the <code>src</code> property is to be copied
	 * @param name
	 *          The name of the newly created property. If this is
	 *          <code>null</code> the new property gets the same name as the
	 *          <code>src</code> property.
	 * @throws RepositoryException
	 *           May be thrown in case of any problem copying the content.
	 */
	public void copy(Property src, Node dstParent, String name)
			throws RepositoryException;
}
