/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.commons.services;

import java.util.Map;

/**
 * Transcoder interface. This methods must be implemented by any transcoder.
 * 
 * @scr.property name="service.description" value="IAVANTE - Transcoder Service"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */
public interface ITranscoderService {

	/**
	 * Returns the name of the vendor implementation
	 * 
	 * @return The vendor name as String
	 */
	public String getVendor();

	/**
	 * Returns the implementation description
	 * 
	 * @return The description as String
	 */
	public String getDescription();

	/**
	 * Get available conversions types For example:
	 * <ul>
	 * <li>preview : Medium quality flash</li>
	 * <li>mobile: 3gp 128kbps</li>
	 * </ul>
	 * 
	 * @param type
	 * @return The available conversion types as Map
	 */
	public Map<String, String> getConversionTypes(String type);

	/**
	 * Get conversion type properties
	 * 
	 * @param conversion_type
	 * @return Map with the conversion properties
	 */
	public Map<String, String> getConversion(String conversion_type);

	/**
	 * Send a conversion task type
	 * 
	 * @param conversion_type
	 *          The type of conversion
	 * @param source_location
	 *          The input file filesystem path
	 * @param target_location
	 *          The output file filesystem path
	 * @param notification_url
	 *          Notify to this url when the conversion is done
	 * @param external_storage
	 *          Upload the conversion result to this external storage
	 * @return The response status code.
	 */
	public int sendConversionTask(String conversion_type, String source_location,
			String target_location, String notification_url,
			String external_storage_url, String external_storage_server);

	/**
	 * Send a conversion task type
	 * 
	 * @param conversion_type
	 *          The type of conversion
	 * @param source_location
	 *          The input file filesystem path
	 * @param target_location
	 *          The output file filesystem path
	 * @param notification_url
	 *          Notify to this url when the conversion is done
	 * @param external_storage
	 *          Upload the conversion result to this external storage
	 * @param params
	 *          Replace default type conversion params
	 * @return The response status code.
	 */
	public int sendConversionTask2(String conversion_type, String source_location,
			String target_location, String notification_url,
			String external_storage_url, String external_storage_server, String params);
	

	/**
	 * Send a conversion task type
	 * 
	 * @param conversion_type
	 *          The type of conversion
	 * @param source_location
	 *          The input file filesystem path
	 * @param target_location
	 *          The output file filesystem path
	 * @param notification_url
	 *          Notify to this url when the conversion is done
	 * @param external_storage
	 *          Upload the conversion result to this external storage
	 * @param params
	 *          Replace default type conversion params
	 * @param ds_custom_props
	 *          ds_custom_props param in json         
	 * @return The response status code.
	 */
	public int sendConversionTask3(String conversion_type, String source_location,
			String target_location, String notification_url,
			String external_storage_url, String external_storage_server, String params,
			String ds_custom_props);

}
