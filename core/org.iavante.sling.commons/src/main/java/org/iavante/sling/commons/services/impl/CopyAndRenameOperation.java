/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.commons.services.impl;

import javax.jcr.AccessDeniedException;
import javax.jcr.InvalidItemStateException;
import javax.jcr.ItemExistsException;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.nodetype.NoSuchNodeTypeException;
import javax.jcr.version.VersionException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.servlets.HtmlResponse;
import org.apache.sling.jcr.api.SlingRepository;
import org.apache.sling.servlets.post.SlingPostOperation;
import org.apache.sling.servlets.post.SlingPostProcessor;

import org.iavante.sling.commons.services.JcrTools;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @scr.component metatype="no" immediate="true"
 * @scr.service
 * @scr.property name="sling.post.operation" value="copyandrename"
 */
public class CopyAndRenameOperation implements SlingPostOperation {
 
  /** @scr.reference */
  private JcrTools jcr_tool;
	
  /** @scr.reference */
  private SlingRepository repository;
  
	/** Default Logger */
	private static final Logger log = LoggerFactory
			.getLogger(CopyAndRenameOperation.class);
  
  private Session session;
  
  /** Title prop */
  private final String TITLE_PROP = "title";
  
  /** Source resource type */
  private final String SOURCE_RESOURCE_TYPE = "gad/source";
  
  /** Schema type prop */
  private final String SCHEMA_TYPE_PROP = "schematype";
  
  protected void activate(ComponentContext context) {
	  try {
		  session = repository.loginAdministrative(null);
	  } catch (RepositoryException e) {
		  e.printStackTrace();
 	  }
  }

  public void run(SlingHttpServletRequest request, HtmlResponse response,
	    	SlingPostProcessor[] processors) {
		
    String msg = "";
	  String from = request.getPathInfo();
	  String dest = request.getParameter(":dest");
	
	  Node rootNode = null;
	  String dest_name = null;
	  Node dest_node = null;
	  Node dest_parent = null;
	  Node from_node = null;
	  String dest_title = "";
	  
	  String first_resourcetype = null;
    String second_resourcetype = null;
	
	  try {
	    rootNode = session.getRootNode();
	    dest_node = rootNode.getNode(dest.substring(1));
	    from_node = rootNode.getNode(from.substring(1));
	  
	    first_resourcetype = null;
	    second_resourcetype = null;	    
	  
	    if ((from_node.hasProperty("sling:resourceType")) && 
	    		(dest_node.hasProperty("sling:resourceType"))) {
		    first_resourcetype = from_node.getProperty("sling:resourceType").getValue().getString();
		    second_resourcetype = dest_node.getProperty("sling:resourceType").getValue().getString();

			  if (!first_resourcetype.equals(second_resourcetype)) {
				  msg = "sling:resourceTypes not equals";
				  if (log.isInfoEnabled()) log.info(msg);
			    response.setStatus(412, msg);
		    }
	    }
	  
	    else {
	  	  msg = "sling:resourceTypes not found";
	  	  if (log.isInfoEnabled()) log.info(msg);
		      response.setStatus(412, msg);
	    }	  
	  
	    dest_name = dest_node.getName();
	    dest_parent = dest_node.getParent();		
	    
	    if ((dest_node.hasProperty(TITLE_PROP)) && (!"".equals(dest_node.getProperty(TITLE_PROP)))) {
		      dest_title = dest_node.getProperty(TITLE_PROP).getValue().getString();
	    }
	  
	  } catch (RepositoryException e) {
	      e.printStackTrace();
	  }
	
	  try {
	    jcr_tool.copy(from_node, dest_parent, dest_name);
	    //if (log.isInfoEnabled()) log.info("Copied: " + from_node.getPath() + 
	  	//  	" to: " + dest_node.getPath() + " with name: " + dest_name);
	  } catch (RepositoryException e) {
  	   e.printStackTrace();
	  }
	
	  try {
		  dest_parent.save();
	  } catch (AccessDeniedException e) {
		  e.printStackTrace();
	  } catch (ItemExistsException e) {
		  e.printStackTrace();
	  } catch (ConstraintViolationException e) {
		  e.printStackTrace();
	  } catch (InvalidItemStateException e) {
		  e.printStackTrace();
	  } catch (VersionException e) {
		  e.printStackTrace();
	  } catch (LockException e) {
		  e.printStackTrace();
	  } catch (NoSuchNodeTypeException e) {
		  e.printStackTrace();
	  } catch (RepositoryException e) {
		  e.printStackTrace();
	  }		
	  
	  // Chage the dest title with the from title
	  if (!"".equals(dest_title)) {
	  	try {
				dest_node = rootNode.getNode(dest.substring(1));
				dest_node.setProperty(TITLE_PROP, dest_title);	
				dest_node.save();
				if (log.isInfoEnabled()) log.info("Assign old title");
			} catch (ValueFormatException e) {
				e.printStackTrace();
			} catch (VersionException e) {
				e.printStackTrace();
			} catch (LockException e) {
				e.printStackTrace();
			} catch (ConstraintViolationException e) {
				e.printStackTrace();
			} catch (PathNotFoundException e) {
				e.printStackTrace();
			} catch (RepositoryException e) {
				e.printStackTrace();
			}
	  }
	  
		if (second_resourcetype.equals(SOURCE_RESOURCE_TYPE)) {
			try {
				dest_node.setProperty(SCHEMA_TYPE_PROP, "schema");
				dest_node.save();	
				if (log.isInfoEnabled()) log.info("Copyandrename finish");
			} catch (ValueFormatException e) {
				e.printStackTrace();
			} catch (VersionException e) {
				e.printStackTrace();
			} catch (LockException e) {
				e.printStackTrace();
			} catch (ConstraintViolationException e) {
				e.printStackTrace();
			} catch (RepositoryException e) {
				e.printStackTrace();
			}
		}		
  } 
}
