/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.commons.services;

import java.util.ArrayList;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * Operations against LDAP server
 * 
 * @scr.property name="service.description" value="IAVANTE - LDAP Connector"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */
public interface LDAPConnector {

	/**
	 * Check the authentication with user:passwd
	 * 
	 * @param userID
	 * @param passWord
	 * @return if user exists
	 */
	public boolean checkUser(String userID, String passWord);

	/**
	 * Check the authentication with user:path
	 * 
	 * @param userID
	 * @param uri
	 * @return if user exists
	 */
	public boolean checkUserByPath(String userID, String uri);

	/**
	 * Connect to LDAP server
	 */
	public void connectLdapServer();

	/**
	 * Create a group in LDAP server
	 * 
	 * @param cn
	 *          common name
	 */
	public void createGroup(String cn);

	/**
	 * Check if a user is member of a group given its path
	 * 
	 * @param uid
	 * @param path
	 * @return if a user is member of a group given its path
	 */
	public boolean isMemberOf(String uid, String path);

	/**
	 * This method returns a list of Channels where the user can Publish
	 * 
	 * @param uid
	 *          User identification
	 * @return List of Channels where the user can Publish
	 */
	public ArrayList<String> getAuthorizedChannels(ServletRequest request,
			ServletResponse response);
}
