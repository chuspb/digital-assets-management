/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.commons.services.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.iavante.sling.commons.distributionserver.IDistributionServer;
import org.iavante.sling.commons.services.DistributionServiceProvider;
import org.iavante.sling.commons.services.PortalSetup;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see org.iavante.sling.commons.services.DistributionServiceProvider
 * @scr.component immediate="true"
 * @scr.property name="service.description"
 *               value="IAVANTE - Distribution Service Provider Impl"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.service 
 *              interface="org.iavante.sling.commons.services.DistributionServiceProvider"
 */
public class DistributionServiceProviderImpl implements
		DistributionServiceProvider, Serializable {
	private static final long serialVersionUID = 1L;

	/** @scr.reference */
	private PortalSetup portalSetup;

	/** Logger. */
	private final Logger log = LoggerFactory.getLogger(getClass());

	/** Component context. */
	private ComponentContext myContext;

	/** Bundle context. */
	private BundleContext myBundleContext;

	public void log(String text) {
		log.error(text);
	}

	protected void activate(ComponentContext context) {
		myContext = context;
		myBundleContext = myContext.getBundleContext();
	}

	/*
	 * @see org.iavante.sling.commons.services.DistributionServiceProvider
	 */
	public IDistributionServer get_distribution_server(String type) {
		List<Bundle> bundles = portalSetup.getDistributionBundles();
		Iterator<Bundle> it = bundles.iterator();
		Bundle bu = null;
		IDistributionServer distrsrv = null;

		while (it.hasNext()) {
			bu = it.next();
			ServiceReference[] sr = bu.getRegisteredServices();
			for (int i = 0; i < sr.length; i++) {
				for (int k = 0; k < sr[i].getPropertyKeys().length; k++) {
					if (sr[i].getPropertyKeys()[k].compareToIgnoreCase("service.type") == 0) {
						if (((String) sr[i].getProperty("service.type"))
								.compareToIgnoreCase("distributionserver") == 0) {
							if (((String) sr[i].getProperty("service.typeimpl"))
									.compareToIgnoreCase(type) == 0) {
								distrsrv = (IDistributionServer) myBundleContext
										.getService(sr[i]);
								assert distrsrv != null;
							}
						}
					}
				}
			}
		}
		return distrsrv;
	}

	/*
	 * @see org.iavante.sling.commons.services.DistributionServiceProvider
	 */
	public List<String> list_distribution_servers() {

		List<Bundle> bundles = portalSetup.getDistributionBundles();
		List<String> listout = new ArrayList<String>();
		Iterator<Bundle> it = bundles.iterator();
		Bundle bu = null;

		while (it.hasNext()) {
			bu = it.next();
			ServiceReference[] sr = bu.getRegisteredServices();
			for (int i = 0; i < sr.length; i++) {
				for (int k = 0; k < sr[i].getPropertyKeys().length; k++) {
					if (sr[i].getPropertyKeys()[k].compareToIgnoreCase("service.type") == 0) {
						if (((String) sr[i].getProperty("service.type"))
								.compareToIgnoreCase("distributionserver") == 0) {
							listout.add((String) sr[i].getProperty("service.typeimpl"));
						}
					}
				}
			}
		}
		return listout;
	}

	/**
	 * List the available distribution services symbolic names
	 * 
	 * @return a list of strings with the name of servers
	 */
	public List<String> list_simbolic_names_distribution_services() {

		List<Bundle> bundles = portalSetup.getDistributionBundles();
		List<String> listout = new ArrayList<String>();
		Iterator<Bundle> it = bundles.iterator();
		Bundle bu = null;

		while (it.hasNext()) {
			bu = it.next();
			ServiceReference[] sr = bu.getRegisteredServices();
			for (int i = 0; i < sr.length; i++) {
				for (int k = 0; k < sr[i].getPropertyKeys().length; k++) {
					if (sr[i].getPropertyKeys()[k].compareToIgnoreCase("service.type") == 0) {
						if (((String) sr[i].getProperty("service.type"))
								.compareToIgnoreCase("distributionserver") == 0) {
							listout.add((String) sr[i].getProperty("service.typeimpl"));
						}
					}
				}
			}
		}
		return listout;
	}
}
