/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.commons.services.impl;

import java.io.Serializable;

import org.iavante.sling.commons.services.JcrTools;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.nodetype.NodeType;
import javax.jcr.NodeIterator;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;

import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see org.iavante.sling.commons.services.JcrTools
 * @scr.component immediate="true"
 * @scr.property name="service.description"
 *               value="IAVANTE - JCR Tools Service Impl"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.service interface="org.iavante.sling.commons.services.JcrTools"
 */

public class JcrToolsImpl implements JcrTools, Serializable {
	private static final long serialVersionUID = 1L;

	/** Default log. */
	private final Logger log = LoggerFactory.getLogger(getClass());

	protected void activate(ComponentContext context) {
	}

	/*
	 * @see org.iavante.sling.commons.services.JcrTools
	 */
	public void copy(Node src, Node dstParent, String name)
			throws RepositoryException {

		// log.info("copy, srcNode:" + src.getPath());
		// log.info("copy, parentNode:" + dstParent.getPath());

		// ensure destination name
		if (name == null) {
			name = src.getName();
		}

		// ensure new node creation
		if (dstParent.hasNode(name)) {
			dstParent.getNode(name).remove();
		}

		// create new node
		Node dst = dstParent.addNode(name, src.getPrimaryNodeType().getName());
		for (NodeType mix : src.getMixinNodeTypes()) {
			dst.addMixin(mix.getName());
		}

		boolean establishAuthor = true;
		// copy the properties
		for (PropertyIterator iter = src.getProperties(); iter.hasNext();) {
			Property prop = iter.nextProperty();
			copy(prop, dst, null);
			if ("jcr:primaryType".compareTo(prop.getName()) == 0
					&& "nt:file".compareTo(prop.getValue().getString()) == 0) {
				establishAuthor = false;
			}
		}

//		log.info("copy, establishAuthor:" + establishAuthor);

		// inherit 'author' parent property

		if (establishAuthor && dstParent.hasProperty("author")) {
			copy(dstParent.getProperty("author"), dst, null);
		}

		// copy the child nodes
		for (NodeIterator iter = src.getNodes(); iter.hasNext();) {
			Node n = iter.nextNode();
			if (!n.getDefinition().isProtected()) {
				copy(n, dst, null);
			}
		}
	}

	/*
	 * @see org.iavante.sling.commons.services.JcrTools
	 */
	public void copy(Property src, Node dstParent, String name)
			throws RepositoryException {
		if (!src.getDefinition().isProtected()) {
			if (name == null) {
				name = src.getName();
			}

			// ensure new property creation
			if (dstParent.hasProperty(name)) {
				dstParent.getProperty(name).remove();
			}

			if (src.getDefinition().isMultiple()) {
				dstParent.setProperty(name, src.getValues());
			} else {
				dstParent.setProperty(name, src.getValue());
//				log.info("copy, property:" + src.getName() + ", value:"
//						+ src.getValue().getString());
			}

		}
	}

}
