/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.gad.administration.impl;

import java.io.IOException;
import java.util.Map;

import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingSafeMethodsServlet;
import org.iavante.sling.commons.services.PortalSetup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * NOT USED, JUST AN EXPERIMENTAL CASE.
 * Receive a file as parameter.
 * 
 * @scr.component immediate="true" metatype="no"
 * @scr.property name="service.description" value="Admin Upload Servlet"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.property name="sling.servlet.paths" value="/content/uploadServlet"
 * @scr.service interface="javax.servlet.Servlet"
 */
@SuppressWarnings("serial")
public class AdminUploadServlet
		extends SlingSafeMethodsServlet {

	/** @scr.reference */
	private PortalSetup portalSetupTool;

	/** Admin properties. */
	private Map<String, String> adminProperties = null;

	/** Default logger. */
	private static final Logger log = LoggerFactory
			.getLogger(AdminLogoutServlet.class);

	public void log(String text) {
		log.error(text);
	}

	protected void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException, IOException {
		processRequest(response);
	}

	protected void doPost(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException, IOException {
		processRequest(response);
	}

	private void processRequest(SlingHttpServletResponse response)
			throws IOException {
		response.getWriter().print("This is a test");
	}
}