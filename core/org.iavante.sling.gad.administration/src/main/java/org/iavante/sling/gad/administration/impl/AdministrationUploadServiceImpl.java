/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.gad.administration.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.iavante.sling.commons.services.PortalSetup;
import org.iavante.sling.gad.administration.AdministrationUploadService;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see org.iavante.sling.gad.administration.AdministrationUploadService
 * @scr.component immediate="true"
 * @scr.property name="service.description"
 *               value="IAVANTE - Administration Uploader Service Impl"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.service 
 * interface="org.iavante.sling.gad.administration.AdministrationUploadService"
 */
public class AdministrationUploadServiceImpl implements
		AdministrationUploadService, Serializable {
	private static final long serialVersionUID = 1L;

	/** Default log. */
	private final Logger log = LoggerFactory.getLogger(getClass());

	/** @scr.reference */
	private PortalSetup portal_setup;

	/** Authentication properties. */
	private Map<String, String> slingProperties = null;

	/** Uploader properties */
	private Map<String, String> uploaderProperties = null;

	/** Do upload operations. */
	UploadOperations up = null;

	protected void activate(ComponentContext context) throws IOException {

		if (log.isInfoEnabled())
			log.info("activate, ini");

		// Getting global configuration service
		try {
			slingProperties = portal_setup.get_config_properties("/sling");
			AbstractHttpOperation.HTTP_BASE_URL = slingProperties.get("sling_url");
			uploaderProperties = portal_setup.get_config_properties("/uploader");
			if (log.isInfoEnabled()){
				log.info("activate, AbstractHttpOperation.HTTP_BASE_URL= "
						+ AbstractHttpOperation.HTTP_BASE_URL);
				log.info("activate, Credentials= "
						+ slingProperties.get("sling_user") + ":" + slingProperties.get("sling_pass"));
				}
		} catch (Exception e) {
			log.error("activate, error getting parameters from config, "
					+ e.getMessage());
			AbstractHttpOperation.HTTP_BASE_URL = "http://localhost:8888";
			log.error("activate, using default sling_url="
					+ AbstractHttpOperation.HTTP_BASE_URL);
		}

		// Creating the object that assigns permissions and create users
		up = new UploadOperations(slingProperties, uploaderProperties);

		if (log.isInfoEnabled())
			log.info("activate, end");

	}

	/*
	 * @see org.iavante.sling.gad.administration.AdministrationUploadService
	 */
	public String uploadFile(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		if (log.isInfoEnabled())
			log.info("uploadFile, ini");

		String location = "";
		String rt = request.getParameter("rt");
		if(rt==null){
			rt = "gad/source";
		}

		try {

			// 1. Create source (POST a sling)
			// curl -i -F"title=<filename>" -F"sling:resourceType=gad/source"
			// http://hhhh:pppp/<content>
			location = up.createSourceInCore(request, rt);

			if (location != null) {

				// 2. Send binary data to uploader with location header
				// curl -i -F"data=@x.avi" -F"filename=x.avi" -F"content=/"
				up.sendSourceToUploader(request, location,
						AbstractHttpOperation.HTTP_BASE_URL, rt);

			}

		} catch (UploadException e) {
			response.sendError(HttpServletResponse.SC_CONFLICT, e.getMessage());
			e.printStackTrace();
		}

		String results = "Location=" + location;

		response.setStatus(HttpServletResponse.SC_OK);
		response.getWriter().println("Location=" + location);

		if (log.isInfoEnabled())
			log.info("uploadFile, end");
		return results;

	}

}
