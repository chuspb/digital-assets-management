/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.gad.administration.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import javax.jcr.SimpleCredentials;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.engine.auth.AuthenticationInfo;
import org.iavante.sling.commons.services.AuthToolsService;
import org.iavante.sling.commons.services.EncryptionService;
import org.iavante.sling.commons.services.LDAPConnector;
import org.iavante.sling.commons.services.PortalSetup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Admin Authentication Filter.
 * 
 * @scr.component immediate="true" metatype="false"
 * @scr.property name="service.description"
 *               value="IAV Admin Authentication Request Filter"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.property name="filter.scope" value="request" private="true"
 * @scr.property name="filter.order" value="-2147" type="Integer" private="true"
 * @scr.service
 */
public class AdminAuthenticationFilter implements Filter, Serializable {
	private static final long serialVersionUID = 1L;

	/** Default log. */
	private final Logger log = LoggerFactory.getLogger(getClass());

	/** @scr.reference */
	private LDAPConnector ldapService;

	/** @scr.reference */
	private EncryptionService encryptionTool;

	/** @scr.reference */
	private PortalSetup portalSetupTool;

	/** @scr.reference */
	private AuthToolsService authTools;

	/** Name of the session variable. */
	private final String GAD_SESSION_ID = "gad_id";

	/** Admin suffix. */
	private final String ADMIN_SUFFIX = ".admin";

	/** Admin prefix. */
	private final String ADMIN_PREFIX = "administration";

	/** Redirection url. */
	private String loginUrl = "";

	/** Admin home url. */
	private String adminHome = "";

	/** Session timeout. */
	private int sessionTimeout = 3600;

	/** Admin properties. */
	private Map<String, String> adminProperties = null;

	/** Class variables. */
	private static final String NOT_LOGGED_IN_USER = "__forced_logout_user__";
	private static final String HEADER_AUTHORIZATION = "Authorization";
	private static final String AUTHENTICATION_SCHEME_BASIC = "Basic";

	/*
	 * Get administration properties.
	 * @see javax.servlet.Filter
	 */
	public void init(FilterConfig filterConfig) {
		this.adminProperties = this.portalSetupTool
				.get_config_properties("/administration");
		loginUrl = adminProperties.get("login_url");
		adminHome = adminProperties.get("admin_home_url");
		try {
			sessionTimeout = Integer.parseInt(adminProperties.get("session_timeout"));
		} catch (Exception e) {
			log.info("Error tomando valor de session_timeout in admin");
		}
	}

	/*
	 * Checks the request Authorization Headers and use this information to
	 * authenticate against LDAP.
	 * @see javax.servlet.Filter
	 */
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {

		SlingHttpServletRequest request = (SlingHttpServletRequest) req;
		SlingHttpServletResponse response = (SlingHttpServletResponse) res;

		// Extract authentication credentials
		AuthenticationInfo credentials;
		credentials = authTools.extractAuthentication(request);
		SimpleCredentials authInfo;

		boolean authenticated = false;
		boolean allowed = false;

		String path = request.getPathInfo();

		// Administration login
		if ((request.getPathInfo().equals(loginUrl + "/"))
				|| (request.getPathInfo().equals(loginUrl))) {
			if (request.getSession().getAttribute(GAD_SESSION_ID) != null
					&& request.getParameter("sessionout") == null) {
				response.sendRedirect(adminHome);
			} else {
				chain.doFilter(request, response);
			}
		}

		// Static content
		else if (path.startsWith("/apps/" + ADMIN_PREFIX + "/static/")) {
			allowed = true;
			authenticated = true;
			chain.doFilter(request, response);
		}

		// Administration pages
		else if (path.endsWith(ADMIN_SUFFIX)
				|| path.startsWith("/" + ADMIN_PREFIX + "/search")
				|| path.startsWith("/" + ADMIN_PREFIX + "/upload")
				|| (path.startsWith("/content/upload"))) {
			log.info("Administration filter");

			// Basic Http autentication
			if (credentials != null) {

				log.info("Authenticate with username and password");
				// Get username and password
				authInfo = (SimpleCredentials) credentials.getCredentials();

				// Authenticat agains Ldap
				authenticated = ldapService.checkUser(authInfo.getUserID(),
						new String(authInfo.getPassword()));

				// Check if the user is admin
				allowed = is_admin(authInfo.getUserID());

				if ((authenticated) && (allowed)) {

					log.info("User authenticated username and password");
					HttpSession session = request.getSession(true);
					String session_ticket = authInfo.getUserID() + ":"
							+ new String(authInfo.getPassword()) + ":"
							+ new Date().getTime();
					String session_ticket_enc = "";
					try {
						session_ticket_enc = encryptionTool.encryptMessage(session_ticket);
					} catch (Exception e) {
						e.printStackTrace();
					}

					session.setAttribute(GAD_SESSION_ID, session_ticket_enc);
					session.setMaxInactiveInterval(sessionTimeout);
				}

				chain.doFilter(request, response);
			}

			// Maybe the user has a session key
			else if (request.getSession().getAttribute(GAD_SESSION_ID) != null) {

				log.info("User authenticated with session key");
				HttpSession session = request.getSession();

				String session_string_enc = session.getAttribute(GAD_SESSION_ID)
						.toString();
				String session_string = "";
				try {
					session_string = encryptionTool.decryptMessage(session_string_enc);

					String[] session_info_list = session_string.split(":");
					String username = session_info_list[0];
					String password = session_info_list[1];

					// Authenticate agains ldap service
					authenticated = ldapService.checkUser(username, password);

					// Check if the user is admin
					allowed = is_admin(username);

					if ((authenticated) && (allowed)) {
						chain.doFilter(request, response);
						log.info("Usuario autenticado session key");
					}

				} catch (javax.crypto.BadPaddingException e) {
					log.error("Error desencrypting session key: " + e.toString());

				} catch (Exception e) {
					log.error("Error authenticating in administration: "
							+ e.toString());
				}
			}
			if ((!authenticated) || (!allowed)) {
				log.error("User not authenticated or allowed");
				response.sendRedirect(loginUrl + "?sessionout=yes");
			}
		}

		// Does nothing
		else {
			chain.doFilter(request, response);
		}

	}

	/*
	 * Does nothing. This filter has not destroyal requirement.
	 * @see javax.servlet.Filter#destroy()
	 */
	public void destroy() {
	}

	// --------------------- internal -------------------------

	/**
	 * Returns true or false if the user is admin or not.
	 * 
	 * @param The
	 *          user
	 * @returns The user is admin or not
	 */
	private boolean is_admin(String user) {
		return ldapService.isMemberOf(user, "gad/admin/cualquiercosa");

	}

}