<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ page import="java.lang.String"%>
<%@ page import="java.util.Map"%>
<%@ page import="javax.jcr.*, org.apache.sling.api.resource.Resource"%>
<%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>


<sling:defineObjects />

<%

	String url = request.getRequestURL().toString();
	String uri = request.getRequestURI();
	String aux = url.replaceAll(uri,"");
	pageContext.setAttribute("urlBase", aux);	
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Interfaz de Admnistraci&oacute;n</title>
	<link rel="stylesheet" href="/apps/administration/static/css/blue.css">
	<script type="text/javascript" src="/apps/administration/static/js/webtoolkit.base64.js"></script>
	
	<script type="text/javascript">

		//Devuelve un objeto XmlHttp
		function getXmlHttpObject(){
			var xmlHttp = null;
			// Mozilla, Opera, Safari, Internet Explorer 7
			if (typeof XMLHttpRequest != 'undefined') {
			    xmlHttp = new XMLHttpRequest();
			}
			if (!xmlHttp) {
			    // Internet Explorer 6
			    try {
			        xmlHttp  = new ActiveXObject("Msxml2.XMLHTTP");
			    } catch(e) {
			        try {
			            xmlHttp  = new ActiveXObject("Microsoft.XMLHTTP");
			        } catch(e) {
			            xmlHttp  = null;
			        }
			    }
			}
			return xmlHttp;
		}

		//Encode user:password and sends form
		function checkLogin(){

			var userInfo = document.getElementById("j_username").value + ':' + document.getElementById("j_password").value;
			var auth = 'Basic ' + Base64.encode(userInfo);

			var xmlHttp = getXmlHttpObject();

	    xmlHttp.open("GET", document.getElementById("loginForm").action, true);
		//Send the proper header information along with the request
	    xmlHttp.setRequestHeader("Authorization", auth);
	    xmlHttp.onreadystatechange = function(){            
	    	switch(xmlHttp.readyState) {
	        	case 4:
	            	if(xmlHttp.status!=200) {
	                	alert("error checkLogin:"+xmlHttp.status);
	                	document.getElementById("message").innerHTML = 'No autenticado' 
	                }else{ 
		                document.getElementById("message").innerHTML = 'Autenticado';
		                document.location.href = document.getElementById("loginForm").action;
	                }
	                break;
	        
	            default:
	                return false;
	            	break;     
	        }
	    };
	    xmlHttp.send(null);	
		}

		function checkIntro(oEvento){
			var iAscii; 
			
			if (oEvento.which){ 
				iAscii = oEvento.which;
			} 
			else{
				if (oEvento.keyCode){ 
					iAscii = oEvento.keyCode;
				}
				else {
					return false;
				}
			} 

			if (iAscii == 13){ 
				checkLogin();
			} 

			return true; 
		}
		
	</script>
	
</head>

<body>

	<img alt="IAVANTE" src="/apps/administration/static/images/logo_iavante.png" align="right">
	<br><br><h1 align="center">Gestor de activos digitales</h1>
	
	<%
		if (request.getParameter("sessionout") != null){ 
			if (request.getParameter("sessionout").contentEquals("yes") == true ){
			
			%>	
			
			<h5 align="center" style="color:red">Tiempo de sesión excedido. Vuelva a hacer login para continuar.</h5>
			
<%			
			}
			else if (request.getParameter("sessionout").contentEquals("logout") == true){
				%>
			<h5 align="center" style="color:red">Logout correcto. Vuelva a hacer login <br/>para ingresar de nuevo en la plataforma.</h5>
				<%
			}
		}
%>
	
	<form method="post" id="loginForm" name="loginForm" action="/content/colecciones.admin">
		<table id="tablaMarco" width="60%" align="center">
		
			<tr>
				<td width="30%" align="right">User:</td>
				<td width="70%" ><input size="20" type="text" id="j_username" name="j_username" maxlength="25"></td>
			</tr>
			<tr>
				<td align="right">Password:</td>
				<td><input size="20" type="password" id="j_password" name="j_password" maxlength="25" onkeypress="javascript:checkIntro(event)"></td>
			</tr>
			<tr>
				<td colspan="2"><div id="message" name="message"></div></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="button" name="loginButton" value="Login" onclick="javascript:checkLogin()">&nbsp;<input type="reset" name="reset" value="Clear">
				</td>
			</tr>
		</table>
	</form>
	
	<div id="Footer">
		<%@ include file="/apps/administration/footer/footer.jsp" %>
	</div>
	
</body>