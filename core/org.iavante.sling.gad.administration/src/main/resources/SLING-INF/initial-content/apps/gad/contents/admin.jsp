<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.*"%>
<%@ page import="javax.jcr.Node"%>
<%@ page import="javax.jcr.NodeIterator"%>
<%@ page import="javax.jcr.query.QueryManager"%>
<%@ page import="javax.jcr.query.Query"%>
<%@ page import="org.apache.sling.api.resource.Resource"%>
<%@ page import="org.iavante.sling.gad.content.ContentTools"%>

<%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>

<sling:defineObjects />

<%
String url = request.getRequestURL().toString();
String uri = request.getRequestURI();
String aux = url.replaceAll(uri,"");
pageContext.setAttribute("urlBase", aux);
%>

<html>

<head>
	<%@ include file="/apps/administration/header/html_header.jsp" %>
</head>

<body>

<%@ include file="/apps/administration/header/header.jsp" %>

<div id="maincontainer">

   	<div id="container">

   	<div id="propiedades">	
	<% PropertyIterator it = currentNode.getProperties(); 
		while(it.hasNext()){
			Property p = it.nextProperty();
			if (!p.getDefinition().isMultiple()) {%>
			<input type="text" id="<%=p.getName()%>" value="<%=p.getValue().getString()%>" disabled="disabled" /><br /><%}}%>
	</div>

	<div id="subcontainer">
		<div id="maintitle">Contenidos</div><br />


<%
String ORDER_BY = "order_by";
String ORDER = "order";
String OFFSET = "offset";
String ITEMS = "items";
String PathInfo = request.getPathInfo();

PathInfo = PathInfo.replace(".admin", "");

String order_by = "title";
if (request.getParameter(ORDER_BY) != null) {
    order_by = request.getParameter(ORDER_BY);}

String order = "descending";
if (request.getParameter(ORDER) != null) {
    order = request.getParameter(ORDER);}

QueryManager queryManager = currentNode.getSession().getWorkspace().getQueryManager();
Query query = queryManager.createQuery("/jcr:root/" + PathInfo + "/element(*, nt:unstructured)[@sling:resourceType = 'gad/content'] order by @" + order_by + " " + order, "xpath");
NodeIterator it2 = query.execute().getNodes();%>

<%
if (request.getParameter(OFFSET) != null) {
    long skip = Long.parseLong(request.getParameter(OFFSET));
    while (skip > 0 && it.hasNext()) {
        it2.next();
        skip--;
    }
}
long count = -1;
if (request.getParameter(ITEMS) != null) {
    count = Long.parseLong(request.getParameter(ITEMS));
} %>

<div id="separador">
	<%
		Integer x=0;
		while (it2.hasNext() && count != 0) {
			x++;
			Node node = it2.nextNode(); 
	%>
 
  <% if(x!=1){%><div id="separador2"></div> <%}%>

 	<a class="url2" href="<%=node.getPath() %>.admin">&nbsp;&nbsp;&nbsp;
 	<%
 	String content_id = "";
 	if (node.hasProperty("title")) { content_id = node.getProperty("title").getValue().getString(); }
 	else { content_id = node.getName(); }
 	%>
 	<b><%=content_id%></b></a><br />
 	<a class="delete" href="javascript:eliminar('<%=node.getPath()%>')">eliminar<a/><br />
	<%count--; %><%}%>
</div>



	</div><!-- End sources -->
     </div><!-- End data -->

  <div id="menu">
        
          <div id="crear">
		<input type="button" class="newcontent" onclick="javascript:window.location='<%=currentNode.getPath()%>.create.admin'" />
	  </div> 
  </div>

</div><!--End maincontainer-->

	<%@ include file="/apps/administration/footer/footer.jsp" %>
</body>
</html>
