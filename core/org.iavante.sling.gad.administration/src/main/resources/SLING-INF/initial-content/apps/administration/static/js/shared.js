//########################################################################################################

	//Return a XmlHttp object
	function getXmlHttpObject(){
		var xmlHttp = null;
		// Mozilla, Opera, Safari, Internet Explorer 7
		if (typeof XMLHttpRequest != 'undefined') {
		    xmlHttp = new XMLHttpRequest();
		}
		if (!xmlHttp) {
		    // Internet Explorer 6
		    try {
		        xmlHttp  = new ActiveXObject("Msxml2.XMLHTTP");
		    } catch(e) {
		        try {
		            xmlHttp  = new ActiveXObject("Microsoft.XMLHTTP");
		        } catch(e) {
		            xmlHttp  = null;
		        }
		    }
		}
		return xmlHttp;
	}
	
//########################################################################################################

		function inicio(preview_flv,preview_ogv){
			mostrarPlayer(preview_flv,preview_ogv);
		}
		
		function inicio_img(preview_img){
			mostrarImage(preview_img);
		}
		
		function inicio_content(url_smil, url_logo){
			mostrarPlayerSMIL(url_smil, url_logo);
		}
	
//########################################################################################################
		
		function mostrarPlayer(preview_flv,preview_ogv){
//			if (supports_ogg_theora_video()){
//				
//				document.getElementById("video").innerHTML =  '<video width="260" height="210" src="'+preview_ogv+'" controls autoplay="true" >Video preview</video>';
//			}
//			else{
				var s1 = new SWFObject("/apps/administration/static/flash/player.swf","ply","260","210","9","#FFFFFF");
				s1.addParam("allowfullscreen","true");
				s1.addParam("allowscriptaccess","always");
				s1.addParam("flashvars","file="+preview_flv);
				s1.write("video");
//			}
			
		}
		
		function mostrarImage(preview_img){	
			document.getElementById("video").innerHTML =  '<img width="260" height="210" src="'+preview_img+'" alt="Thumbnail of source"/>';
			
			
		}
		
		function mostrarPlayerSMIL(url_smil, url_logo){
				
				var logo = "";
				if(url_logo.indexOf('no_logo_in_revision')!=-1 && url_smil.indexOf('/canales')==-1){
					logo = "/apps/administration/static/images/logo_flash.png";
				}else{
					logo = url_logo + "%26ext%3D%2Epng";
				}
			
				var s1 = new SWFObject("/apps/administration/static/flash/player.swf","ply","260","510","9","#FFFFFF");
				s1.addParam("allowfullscreen","true");
				s1.addParam("allowscriptaccess","always");
				
				myflashvars = "file="+url_smil;
				myflashvars += "&id=ply";
				
				
				myflashvars += "&playlist=bottom";
				myflashvars += "&repeat=list";
				myflashvars += "&logo=" + logo;
				myflashvars += "&playlistsize=300";
				myflashvars += "&autostart=false";
				
				myflashvars +="&plugins=captions";
								
				s1.addParam("flashvars",myflashvars);
				
				s1.write("video");
		
		}

//######################################################################################################## 

		function eliminar(url, aux){
		url = url + ".admin";
	
		var cont = url.split("/");
		cont = cont[cont.length-1];			

		if (confirm("Confirme el borrado de \"" + cont.replace(".admin", '') + "\"")){

			params = ":operation=delete";
			
			var xmlHttp = getXmlHttpObject();
			xmlHttp.open("POST", url, true);

			//Send the proper header information along with the request
			xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlHttp.setRequestHeader("Content-length", params.length);
			xmlHttp.setRequestHeader("Connection", "close");

			xmlHttp.onreadystatechange = function() {//Call a function when the state changes.
				if(xmlHttp.readyState == 4 && xmlHttp.status == 200) {

					if (aux=="false"){
						document.location.href="/content/catalogo/denegados.admin";}
					else if(aux=="true"){
						document.location.href="/content/catalogo/revisados.admin";}
					else{	
						document.location.href=document.location.href;}
				}
			}
			xmlHttp.send(params);
		}
		}

//######################################################################################################## 

		function pendiente(url){
			url=url+".admin";
			var cont = url.split("/");
			cont = cont[cont.length-1];			

			if (confirm("Confirme el envio a revisi\u00f3n de \"" + cont.replace(".admin", '') + "\"")){

			params = ":operation=copy&:dest=/content/catalogo/pendientes/&replace=true";
			 
			var xmlHttp = getXmlHttpObject();
			xmlHttp.open("POST", url, true);

			//Send the proper header information along with the request
			xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlHttp.setRequestHeader("Content-length", params.length);
			xmlHttp.setRequestHeader("Connection", "close");

			xmlHttp.onreadystatechange = function() {//Call a function when the state changes.
				if(xmlHttp.readyState == 4 && xmlHttp.status == 201) {
					//alert(xmlHttp.responseText);
					document.getElementById("spanok").innerHTML = "OK"; 	
					document.getElementById("spanok").style.color = "blue";
					setTimeout('document.location.href=document.location.href', 1000);
				}

				if(xmlHttp.readyState != 4 && xmlHttp.status != 201) {
					document.getElementById("spanok").innerHTML = "ERROR";
					document.getElementById("spanok").style.color = "red";						
				}
			}
			xmlHttp.send(params);
		}
		}

//######################################################################################################## 

		function revisar(url, aux){
			url=url+".admin";
			params = ":operation=move&:dest=/content/catalogo/revisados/&replace=true";

			var xmlHttp = getXmlHttpObject();
			xmlHttp.open("POST", url, true);

			//Send the proper header information along with the request
			xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlHttp.setRequestHeader("Content-length", params.length);
			xmlHttp.setRequestHeader("Connection", "close");

			xmlHttp.onreadystatechange = function() {//Call a function when the state changes.
				if(xmlHttp.readyState == 4 && xmlHttp.status == 201) {
					//alert(xmlHttp.responseText);
					
				if (aux=="false"){
					var cont = url.split("/");
					cont = cont[cont.length-2];	
					document.location.href="/content/catalogo/"+cont+".admin";
					}
				if (aux=="true"){
					document.location.href=document.location.href;}
			}
			}
			xmlHttp.send(params);  
		}

//######################################################################################################## 

		function denegar(url, aux){
			url=url+".admin";
			params = ":operation=move&:dest=/content/catalogo/denegados/&replace=true";
		 
			var xmlHttp = getXmlHttpObject();
			xmlHttp.open("POST", url, true);

			//Send the proper header information along with the request
			xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlHttp.setRequestHeader("Content-length", params.length);
			xmlHttp.setRequestHeader("Connection", "close");

			xmlHttp.onreadystatechange = function() {//Call a function when the state changes.
				if(xmlHttp.readyState == 4 && xmlHttp.status == 201) {
					//alert(xmlHttp.responseText);
				document.location.href=document.location.href;

				if (aux=="false"){
					var cont = url.split("/");
					cont = cont[cont.length-2];	
					document.location.href="/content/catalogo/"+cont+".admin";
					}
				if (aux=="true"){
					document.location.href=document.location.href;}
			} 
			}
			xmlHttp.send(params); 
		}
		
//######################################################################################################## 

		function publicar(url, dest, aux){
			url=url+".admin";
			params = ":operation=copy&:dest=" + dest + "/contents/&replace=true";

			var xmlHttp = getXmlHttpObject();
			xmlHttp.open("POST", url, true);

			//Send the proper header information along with the request
			xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlHttp.setRequestHeader("Content-length", params.length);
			xmlHttp.setRequestHeader("Connection", "close");

			xmlHttp.onreadystatechange = function() {//Call a function when the state changes.
				if(xmlHttp.readyState == 4 && xmlHttp.status == 201) {
					//alert(xmlHttp.responseText);
					
					if (aux=="false"){
						var cont = url.split("/");
						cont = cont[cont.length-2];	
						document.location.href="/content/catalogo/"+cont+".admin";
					}
					if (aux=="true"){
						document.location.href=document.location.href;
					}
				}
			}
			xmlHttp.send(params);  
		}

//######################################################################################################## 

		function directPublication(url, dest, aux){
			url=url+".publish.admin";

			var params = "";
			
			var select = document.getElementById('channels');
			
			if (select.type == 'select-multiple') {					
				 var j;
				 var identifier = select.id;
				 var values_concat = "";
				 for (j=0; j<select.options.length; j++) {						
				    if (select.options[j].selected) {
				      var value = select.options[j].value;
				      values_concat += value + ",";					      
				    }
				  }
				 if(values_concat == ""){
					 alert("Debe seleccionar al menos una opción de la lista '" + identifier + "'");
					 return;
				 }
				 values_concat = values_concat.replace(/,$/, "");
				 params += identifier + "=" + values_concat;
			}
			
			var xmlHttp = getXmlHttpObject();
			xmlHttp.open("GET", url+"?"+params, true);

			//Send the proper header information along with the request
			xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			//xmlHttp.setRequestHeader("Authorization", "Basic YWRtaW46YWRtaW4=");
			//xmlHttp.setRequestHeader("Content-length", params.length);
			xmlHttp.setRequestHeader("Connection", "close");

			xmlHttp.onreadystatechange = function() {//Call a function when the state changes.
				if(xmlHttp.readyState == 4 && xmlHttp.status==202) {
					//alert(xmlHttp.responseText);
					document.location.href=document.location.href;
				}
			}
			xmlHttp.send(params);  
		}
		
		
//########################################################################################################
		
		function copyContent(url, aux){
			
			var cont = url.split("/");
			contName = cont[cont.length-1];
			
			var index = document.getElementById("traduction_lang").selectedIndex;
			var lang = document.getElementById("traduction_lang").options[index].value;
			
			var dest = contName + "_" + lang; 
			
			alert("dest="+dest);
			
			url=url+".admin";
			
			if(""==dest){
				alert("Nombre de la copia no válido");
			}
			else{
			
				params = ":operation=copy&:dest=" + dest + "&:replace=true";
	
				var xmlHttp = getXmlHttpObject();
				xmlHttp.open("POST", url, true);
	
				//Send the proper header information along with the request
				xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				xmlHttp.setRequestHeader("Content-length", params.length);
				xmlHttp.setRequestHeader("Connection", "close");
	
				xmlHttp.onreadystatechange = function() {//Call a function when the state changes.
					if(xmlHttp.readyState == 4 && xmlHttp.status == 201) {
						//alert(xmlHttp.responseText);
						
						if (aux=="true"){
							document.location.href=document.location.href;
						}
					}
				}
				xmlHttp.send(params);
			}
		}		
		
		
//########################################################################################################
		
		function copySources(url, aux){
			url=url+".copysources.admin";
			
			var index = document.getElementById("contentToCopy").selectedIndex;
			var dest = document.getElementById("contentToCopy").options[index].value;
			
			if(""==dest){
				alert("Nombre de la copia no válido");
			}
			else{
				
				var params = "dest=" + dest;
				
				var xmlHttp = getXmlHttpObject();
				xmlHttp.open("GET", url+"?"+params, true);

				//Send the proper header information along with the request
				xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				//xmlHttp.setRequestHeader("Authorization", "Basic YWRtaW46YWRtaW4=");
				//xmlHttp.setRequestHeader("Content-length", params.length);
				xmlHttp.setRequestHeader("Connection", "close");

				xmlHttp.onreadystatechange = function() {//Call a function when the state changes.
					if(xmlHttp.readyState == 4 && xmlHttp.status==202) {
						//alert(xmlHttp.responseText);
						document.location.href=document.location.href;
					}
				}
				xmlHttp.send(params); 

			}
		}	
		
		
//########################################################################################################
		
		function copySource(url, aux){
			url=url+".copy.admin";
			
			var index = document.getElementById("contentToCopy").selectedIndex;
			var dest = document.getElementById("contentToCopy").options[index].value;
			var copyName = document.getElementById("copyName").value;
			
			if(""==dest){
				alert("Nombre de la copia no válido");
			}
			else{
				
				var params = "dest=" + dest + "&title=" + copyName;
				
				var xmlHttp = getXmlHttpObject();
				xmlHttp.open("GET", url+"?"+params, true);

				//Send the proper header information along with the request
				xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				//xmlHttp.setRequestHeader("Authorization", "Basic YWRtaW46YWRtaW4=");
				//xmlHttp.setRequestHeader("Content-length", params.length);
				xmlHttp.setRequestHeader("Connection", "close");

				xmlHttp.onreadystatechange = function() {//Call a function when the state changes.
					if(xmlHttp.readyState == 4 && xmlHttp.status==202) {
						//alert(xmlHttp.responseText);
						document.location.href=document.location.href;
					}
				}
				xmlHttp.send(params); 

			}
		}	
		
//########################################################################################################
		
		function copyTransformation(url, dest){
			url=url+".copy.admin";
			
			if (confirm("Se va a asignar este thumbnail a la fuente thumbnail_default. ¿Desea continuar?")){
			
				if(""==dest){
					alert("Nombre de la copia no válido");
				}
				else{
					
					var params = "dest=" + dest;
					params += "&overwritesourceprops=true";
					params += "&newtransname=image_preview";
					
					//alert("params: " + params);
					
					var xmlHttp = getXmlHttpObject();
					xmlHttp.open("GET", url+"?"+params, true);
	
					//Send the proper header information along with the request
					xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
					//xmlHttp.setRequestHeader("Authorization", "Basic YWRtaW46YWRtaW4=");
					//xmlHttp.setRequestHeader("Content-length", params.length);
					xmlHttp.setRequestHeader("Connection", "close");
	
					xmlHttp.onreadystatechange = function() {//Call a function when the state changes.
						if(xmlHttp.readyState == 4 && xmlHttp.status==202) {
							//alert(xmlHttp.responseText);
							document.location.href=document.location.href;
						}
					}
					xmlHttp.send(params); 
	
				}
			}
		}	
		
		
		
//######################################################################################################## 

		/**
		 * Validate regex with values.
		 * 
		 * @param exp regular expression
		 * @param value to validate
		 */
		function validateRegExp(exp,value) {
			var reg = new RegExp(exp);
			var result = reg.test(value);
			return result;
		}
		
		
		/**
		 * Unpublish a revision from a channel.
		 * 
		 * @param url to post
		 * @param aux to redirect to another url
		 */
		function unpublish(url, aux){
			
			dest = url.replace(/contents[\w\/]+/, 'unpublished/');

			params = ":operation=move&:dest=" + dest + "/&replace=true";
			
			url=url+".admin";

			var xmlHttp = getXmlHttpObject();
			xmlHttp.open("POST", url, true);

			//Send the proper header information along with the request
			xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlHttp.setRequestHeader("Content-length", params.length);
			xmlHttp.setRequestHeader("Connection", "close");

			xmlHttp.onreadystatechange = function() {//Call a function when the state changes.
				if(xmlHttp.readyState == 4 && xmlHttp.status == 201) {
					//alert(xmlHttp.responseText);
					
					if (aux=="false"){
						document.location.href=url.replace(/contents[\w\/]+/, 'contents')+".admin";
					}
					if (aux=="true"){
						document.location.href=document.location.href;
					}
				}
			}
			xmlHttp.send(params);  
		}
		
//########################################################################################################

		function guardar(url){
			url=url+".admin";
			var aux = document.getElementById('nodeProperties');
			aux.action = url; 
			//document.getElementById('myform').submit();

			var params = "";
		
			for(var i=0; i<aux.length; i++) {
				
				var input = aux.elements[i];
				
				if( input.type == 'text' || input.type == 'hidden'){
					if (input.id=="lengths")
						var identifier="length";
					else
						var identifier=input.id;	
						
					if (identifier == "tags") {
						var tags = new Array();  
						tags = input.value.split(",");
						for (var j=0; j<tags.length; j++) {
							var tag_name = tags[j].replace(/\s+/gi, " ");
							tag_name = tag_name.replace(/^\s+|\s+$/gi, "");
							params += identifier + "=" + tag_name + "&";
						}
					}
					else {
						params += identifier + "=" + input.value.replace(/^(\s|\t)*|(\s|\t)*$/g,"") + "&";
					}					
				}
				
				else if (input.type == 'select-one') {
					var index = input.selectedIndex;
					var value = input.options[index].value;
					value = value.replace(/^(\s|\t)*|(\s|\t)*$/g,"");
					
					var identifier = input.id;
					params += identifier + "=" + value + "&";
					}

				
			}

			params = params.replace(/\&$/,'');
			
			var xmlHttp = getXmlHttpObject();
			xmlHttp.open("POST", url, true);

			//Send the proper header information along with the request
			xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlHttp.setRequestHeader("Content-length", params.length);
			xmlHttp.setRequestHeader("Connection", "close");

			xmlHttp.onreadystatechange = function() {//Call a function when the state changes.
				if(xmlHttp.readyState == 4 && xmlHttp.status == 200) {
					//alert(xmlHttp.responseText);
					document.location.href=document.location.href;
				}
			}
			xmlHttp.send(params);
		}
		
		
//########################################################################################################

		function saveRevision(url){
			url=url+".admin";
			var aux = document.getElementById('nodeProperties');
			aux.action = url; 
			//document.getElementById('myform').submit();

			var params = "";
		
			for(var i=0; i<aux.length; i++) {
				
				var input = aux.elements[i];
				
				if(!input.disabled){
				
					if( input.type == 'text' || input.type == 'hidden'){
						if (input.id=="lengths")
							var identifier="length";
						else
							var identifier=input.id;	
							
						if (identifier == "tags") {
							var tags = new Array();  
							tags = input.value.split(",");
							for (var j=0; j<tags.length; j++) {
								var tag_name = tags[j].replace(/\s+/gi, " ");
								tag_name = tag_name.replace(/^\s+|\s+$/gi, "");
								params += identifier + "=" + tag_name + "&";
							}
						}
						else {
							params += identifier + "=" + input.value.replace(/^(\s|\t)*|(\s|\t)*$/g,"") + "&";
						}					
					}
					
					else if (input.type == 'select-one') {
						var index = input.selectedIndex;
						var value = input.options[index].value;
						value = value.replace(/^(\s|\t)*|(\s|\t)*$/g,"");
						
						var identifier = input.id;
						params += identifier + "=" + value + "&";
					}
				}
				
			}

			params = params.replace(/\&$/,'');
			
			var xmlHttp = getXmlHttpObject();
			xmlHttp.open("POST", url, true);

			//Send the proper header information along with the request
			xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlHttp.setRequestHeader("Content-length", params.length);
			xmlHttp.setRequestHeader("Connection", "close");

			xmlHttp.onreadystatechange = function() {//Call a function when the state changes.
				if(xmlHttp.readyState == 4 && xmlHttp.status == 200) {
					//alert(xmlHttp.responseText);
					document.location.href=document.location.href;
				}
			}
			xmlHttp.send(params);
		}

//########################################################################################################

		function crear(url){
			url+="/*.admin";	

			var aux = document.getElementById('nodeProperties');

			aux.action = url; 
			//document.getElementById('myform').submit();
			var params = "";
			
			for(var i=0; i<aux.length; i++){
				var input = aux.elements[i];
				//alert("input:" + input + ", input.type:" + input.type );
				if( input.type == 'text' || input.type == 'hidden' || input.type == "textarea"){
					
				  if (input.id=="lengths")
					var identifier="length";
				  else
					var identifier=input.id;	

				  if (identifier == "tags") {
					var tags = new Array();  
					tags = input.value.split(",");
					for (var j=0; j<tags.length; j++) {
						var tag_name = tags[j].replace(/\s+/gi, " ");
						tag_name = tag_name.replace(/^\s+|\s+$/gi, "");
						params += identifier + "=" + tag_name + "&";
					}
				  }					
				  else {
					params += identifier + "=" + input.value + "&";
				  }
				}
				
				else if (input.type == 'select-one') {
					var index = input.selectedIndex;
					var value = input.options[index].value;
					var identifier = input.id;
					params += identifier + "=" + value + "&";
				}		
				
				else if (input.type == 'select-multiple') {					
					 var j;
					 var identifier = input.id;
					 var values_concat = "";
					 for (j=0; j<input.options.length; j++) {						
					    if (input.options[j].selected) {
					      var value = input.options[j].value;
					      values_concat += value + ",";					      
					    }
					  }
					 if(values_concat == ""){
						 alert("Debe seleccionar al menos una opción de la lista '" + identifier + "'");
						 return;
					 }
					 values_concat = values_concat.replace(/,$/, "");
					 params += identifier + "=" + values_concat + "&";
				}
			}
			
			params = params.replace(/\&$/,'');
			
			var xmlHttp = getXmlHttpObject();
			xmlHttp.open("POST", url, true);

			//Send the proper header information along with the request
			xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlHttp.setRequestHeader("Content-length", params.length);
			xmlHttp.setRequestHeader("Connection", "close");

			xmlHttp.onreadystatechange = function() {//Call a function when the state changes.
				if(xmlHttp.readyState == 4 && xmlHttp.status == 201) {
					var dest_url = xmlHttp.getResponseHeader("location") + ".admin";
					
					//should wait for confirmation of ending
					
					document.location.href = dest_url ;
				}
			}
			xmlHttp.send(params);   
		}

//######################################################################################################## 

	function newSource(url){
			
			var oldUrl = url;

			var title = document.getElementById('newsource').value;
			if(document.getElementById('namesource').value != ''){
				title = document.getElementById('namesource').value;
			}
			
			var params = "";
			
			params="title=" + title + "&file=&mimetype=&text_encoding=&type=&bitrate=&tags=&" + 
			"tracks_number=&track_1_type=&track_1_encoding=&track_1_features=&" + 
			"track_2_type=&track_2_encoding=&track_2_features=&lang=&length=&" + 
			"size=&jcr:lastModifiedBy=&sling:resourceType=gad/source&jcr:createdBy=&jcr:lastModified=";
			
			params = params.replace(/\&$/,'');

			var xmlHttp = getXmlHttpObject();
//			xmlHttp.open("POST", url+="/*.admin", true);
			xmlHttp.open("POST", "/administration/uploader", true);

			//Send the proper header information along with the request
			xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlHttp.setRequestHeader("Content-length", params.length);
			xmlHttp.setRequestHeader("Connection", "close");

			xmlHttp.onreadystatechange = function() {//Call a function when the state changes.
				if(xmlHttp.readyState == 4 && xmlHttp.status == 201) {
					alert(xmlHttp.responseText);
					document.location.href= document.location.pathname + "#sources";
					document.location.reload(true);
				}
			}
			xmlHttp.send(params);   
		}

//######################################################################################################## 

	function show(element){
		if(document.getElementById(element).style.display == 'block'){
			document.getElementById(element).style.display = 'none';
		}
		else{
			javascript:document.getElementById(element).style.display='block';
		}
	}
	
	function votar(url){
		url=url+".admin";
		var params = "";
		
		params += "sling:resourceType=gad/revision&";
		params += "addvote=" + document.getElementById('addvote').value;

		params = params.replace(/\&$/,'');
		
		var xmlHttp = getXmlHttpObject();
		xmlHttp.open("POST", url, true);

		//Send the proper header information along with the request
		xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlHttp.setRequestHeader("Content-length", params.length);
		xmlHttp.setRequestHeader("Connection", "close");

		xmlHttp.onreadystatechange = function() {//Call a function when the state changes.
			if(xmlHttp.readyState == 4 && xmlHttp.status == 200) {
				//alert(xmlHttp.responseText);
				document.location.href=document.location.href;
			}
		}
		xmlHttp.send(params);
	}

//######################################################################################################## 

	function crearSubcategoria(url){
		url+="/*.admin";	

		var params = "";
		params += "sling:resourceType=gad/category&";
		params += "title=" + document.getElementById('subcat').value;
		
		params = params.replace(/\&$/,'');
		
		var xmlHttp = getXmlHttpObject();
		xmlHttp.open("POST", url, true);

		//Send the proper header information along with the request
		xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		xmlHttp.setRequestHeader("Content-length", params.length);
		xmlHttp.setRequestHeader("Connection", "close");

		xmlHttp.onreadystatechange = function() {//Call a function when the state changes.
			if(xmlHttp.readyState == 4 && xmlHttp.status == 201) {
				//alert(xmlHttp.responseText);
				url=url.replace("content/", '');
				document.location.href=url.replace("/*", '.admin');
			}
		}
		xmlHttp.send(params);   
	}
	
	function showDistributionServerConfig() {
		var distribution_server = document.getElementById('distribution_server').value;
		alert(distribution_server);
	}
	
	
	
	//######################################################################################################## 

		function asignarCategoria(url){
			url=url+".admin";
			var params = "";
			
			params += "sling:resourceType=gad/revision&";
			params += "categories@TypeHint=String[]&";
			
			//Seleccionar los checks marcados
			var div = document.getElementById('divcategorias').getElementsByTagName("input");
			var cont = 0;
			for(i=0;i<div.length;i++){
				if(div[i].checked){
					params += "categories=" + div[i].id + "&";
					cont++;
				}
			}
			if(cont==0){
				params += "categories=&";
			}

			params = params.replace(/\&$/,'');
			
			var xmlHttp = getXmlHttpObject();
			xmlHttp.open("POST", url, true);

			//Send the proper header information along with the request
			xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
			xmlHttp.setRequestHeader("Content-length", params.length);
			xmlHttp.setRequestHeader("Connection", "close");

			xmlHttp.onreadystatechange = function() {//Call a function when the state changes.
				if(xmlHttp.readyState == 4 && xmlHttp.status == 200) {
					//alert(xmlHttp.responseText);
					document.location.href=document.location.href;
				}
			}
			xmlHttp.send(params);
		}

	//######################################################################################################## 
		
		document.observe("dom:loaded", function() {
			/* example 1 */
			var button = $('button1'), interval, status=$('status');
			new Ajax_upload(button,{
				action: '/content/uploadservice',
				name: 'data',
				// Additional data to send
				data: {
//					content : '/content/colecciones/colecci_n_de_trol/contents/contentido_1/sources/*',
//					filename : 'IES'
				},
				onSubmit : function(file, ext){
					// change button text, when user selects file
					button.update('Uploading');

					var sourcename = $('sourcename').value;
					//alert('filename:' + filename);
					var content = $('sourcecontent').value;
					//alert('content:' + content);
					var filename = file;
					//alert('filename:' + file);
					
					if(sourcename != ''){
						this.setData({'filename':filename, 'content':content, 'sourcename':sourcename});
					}else{
						this.setData({'filename':filename, 'content':content, 'sourcename':filename});
					}
					
					// If you want to allow uploading only 1 file at time,
					// you can disable upload button
					this.disable();
					
					// Animating upload button
					// Uploding -> Uploading. -> Uploading...
					interval = window.setInterval(function(){
						var text = button.innerHTML;
						if (text.length < 13){
							button.update(text + '.');
						} else {
							button.update('Uploading');
						}
					}, 200);
				},
				onComplete: function(file, response){

					//console.log(response);
					button.update('Upload');
					window.clearInterval(interval);
					// enable upload button
					this.enable();

					if(response.startsWith("Location")){
						alert("Fichero subido correctamente");
					}else{
						alert("Se ha producido un error, " + response);
					}
					document.location.href= document.location.href;
				}
			}); 
		});