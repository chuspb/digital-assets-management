<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<div id="tags_box">
    <ul>
    <li class="shadow"><h4>Tags</h4></li>
	<% if (currentNode.hasProperty("tags")) {
		 Value[] tags = currentNode.getProperty("tags").getValues();													
		 for (int i=0; i<tags.length;i++) {
			 String tag = tags[i].getString();
			 String path = currentNode.getPath();
			 String tag_path = "";
			 if (path.startsWith("/content/colecciones"))
			    tag_path = "/colecciones/" + path.split("/")[3] + "/tags/" + tag + ".admin";
			 else if (path.startsWith("/content/canales"))				
			     tag_path = "/canales/" + path.split("/")[3] + "/tags/" + tag + ".admin";			 
			 %>
			 <li><a href="<%=tag_path%>"><span><%=tag%></span></a></li>
		 <%}
	}%>
	<li class="finli">&nbsp;</li>
	</ul>
</div>