<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page language="java" contentType="text/json; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.*,
        org.apache.sling.api.resource.Resource"
%>
<%@ page import="org.iavante.sling.searcher.services.SearchService"%>
<%@ page import="org.iavante.sling.searcher.services.SearchPrintService"%>
<%@ page import="java.io.PrintWriter"%>
<%@ page import="org.apache.commons.codec.binary.Base64"%>

<%@ taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />

<%
String key = "";
String encoding = "ascii";
String type = "gad/content";


if (request.getParameter("key") != null)
 	key = request.getParameter("key");

if (request.getParameter("encoding") != null)
 	encoding = request.getParameter("encoding");

if (request.getParameter("type") != null)
 	type = request.getParameter("type");


if (encoding.contentEquals("base64") == true)
	key = new String(Base64.decodeBase64(key.getBytes()));
	


 PrintWriter pw = response.getWriter();
 String[] mpath = {"/content/colecciones","/content/catalogo","/content/canales"};
 ArrayList<HashMap<String,String>> result = sling.getService(org.iavante.sling.searcher.services.SearchService.class).search(key,mpath, false,"AND");
 
 if (result == null)
 {}
 
 else{
	 if (result.isEmpty()){
		 
	 }
	 else{
		 sling.getService(SearchPrintService.class).writeSimpleOutput("json",result,pw,100);
	 }
 }
%>
