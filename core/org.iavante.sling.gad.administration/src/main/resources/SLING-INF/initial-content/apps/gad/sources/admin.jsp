<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.*"%>
<%@ page import="javax.jcr.Node"%>
<%@ page import="javax.jcr.NodeIterator"%>
<%@ page import="org.apache.sling.api.resource.Resource"%>
<%@ page import="org.iavante.sling.gad.content.ContentTools"%>
<%@ page import="javax.jcr.query.QueryManager"%>
<%@ page import="javax.jcr.query.Query"%>

<%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>

<sling:defineObjects />

<%
String url = request.getRequestURL().toString();
String uri = request.getRequestURI();
String aux = url.replaceAll(uri,"");
pageContext.setAttribute("urlBase", aux);
String [] pathSplit = currentNode.getPath().split("/");
boolean isMultimediaAssets = false;
if("content".equals(pathSplit[1]) 
		&& "canales".equals(pathSplit[2]) 
		&& "multimedia_assets".equals(pathSplit[pathSplit.length-1])){
	isMultimediaAssets = true;
}
%>

<html>

<head>
	<%@ include file="/apps/administration/header/html_header.jsp" %>
		<script type="text/javascript" src="/apps/administration/static/js/prototype.js"></script>
	<script type="text/javascript" src="/apps/administration/static/js/ajaxupload.3.5.js"></script> 

	<script type="text/javascript">
	document.observe("dom:loaded", function() {
		/* example 1 */
		var button = $('button1'), interval, status=$('status');
		new Ajax_upload(button,{
			action: '/content/uploadservice',
			name: 'data',
			// Additional data to send
			data: {
//				content : '/content/colecciones/colecci_n_de_trol/contents/contentido_1/sources/*',
//				filename : 'IES'
			},
			onSubmit : function(file, ext){
				// change button text, when user selects file
				button.update('Uploading'); 

				var sourcename = $('sourcename').value;
				//alert('filename:' + filename);
				var content = $('sourcecontent').value;
				//alert('content:' + content);
				var filename = file;
				//alert('filename:' + file);
				
				if(sourcename != ''){
					this.setData({'filename':filename, 'content':content, 'sourcename':sourcename});
				}else{
					this.setData({'filename':filename, 'content':content, 'sourcename':filename});
				}
				
				// If you want to allow uploading only 1 file at time,
				// you can disable upload button
				this.disable();
				
				// Animating upload button
				// Uploding -> Uploading. -> Uploading...
				interval = window.setInterval(function(){
					var text = button.innerHTML;
					if (text.length < 13){
						button.update(text + '.');
					} else {
						button.update('Uploading');
					}
				}, 200);
			},
			onComplete: function(file, response){

				//console.log(response);
				button.update('Upload');
				window.clearInterval(interval);
				// enable upload button
				this.enable();

				if(response.startsWith("Location")){
					alert("Fichero subido correctamente");
				}else{
					alert("Se ha producido un error, " + response);
				}
				document.location.href= document.location.href;
			}
		}); 
	});
	</script>
	
</head>

<body>

	<%@ include file="/apps/administration/header/header.jsp" %>

<div id="maincontainer">						
    <div id="container">
   		
	<div id="propiedades">
	<% PropertyIterator it = currentNode.getProperties(); 
	   while(it.hasNext()){
		Property p = it.nextProperty();
		if (!p.getDefinition().isMultiple()) {%>
		<input class="property" type="text" id="<%=p.getName()%>" value="<%=p.getValue().getString()%>" disabled="disabled"><br />
	  <%}}%>
	</div>

      <div id="caja">

	<div id="sources" class="corner-up">Fuentes:</div>

<%
String ORDER_BY = "order_by";
String ORDER = "order";
String OFFSET = "offset";
String ITEMS = "items";
String MIMETYPE = "mimetype";

String PathInfo = request.getPathInfo();
PathInfo = PathInfo.replace(".admin", "");


String order_by = "title";
if (request.getParameter(ORDER_BY) != null) {
    order_by = request.getParameter(ORDER_BY);   
}

String order = "descending";
if (request.getParameter(ORDER) != null) {
    order = request.getParameter(ORDER);
}

QueryManager queryManager = currentNode.getSession().getWorkspace().getQueryManager();
Query query;

if(request.getParameter(MIMETYPE) != null) {
	query = queryManager.createQuery("/jcr:root/" + PathInfo + "/element(*, nt:unstructured)[@sling:resourceType = 'gad/source' and @mimetype = '" + request.getParameter(MIMETYPE) + "'] order by @" + order_by + " " + order, "xpath");
}
else {
    query = queryManager.createQuery("/jcr:root/" + PathInfo + "/element(*, nt:unstructured)[@sling:resourceType = 'gad/source'] order by @" + order_by + " " + order, "xpath");
}
NodeIterator it2 = query.execute().getNodes();
Integer x=0;
%>


<% while(it2.hasNext()){
x++;
Node node = it2.nextNode(); %>
<div class="objeto">
	<%if(!isMultimediaAssets){ %>
	<a class="delete" href="javascript:eliminar('<%=node.getPath()%>')">eliminar<a/>
	<%} %> 
	<a class="url" href="<%=node.getPath() %>.admin"><%= node.getProperty("title").getString()%></a>&nbsp;->&nbsp;
	
	<% if ((!node.hasProperty("file")) || (node.getProperty("file").getValue().getString().equals(""))) { %>
	<a onclick="javascript:document.getElementById('<%= node.getName()%>').style.display='block';">+</a>
	<div id="<%= node.getName() %>" style="display:none; margin-left:65px;">
		<br /><input type="file" id="newfile" name="newfile" />
		<input type="button" onclick="uploader('<%= node.getName() %>','<%=node.getPath() %>');" value="Subir" />	
	</div>	
	<%} else {%>
		   <span><%= node.getProperty("file").getValue().getString() %></span>
	<%}%>
</div>
<%}%>

<div class="corner-down"></div>

      </div>
    </div>
 </div><!--End maincontainer-->
<%if(!isMultimediaAssets){ %>
		<div id="menu">
			<div id="crear">
				<input type="button" class="newsource" onclick="javascript:document.getElementById('dataupload').style.display='block';" value="" />
			</div>
			<div class="clear"></div>
			<div id="dataupload" class="example">
				<div class="wrapper">
					<label>[Nombre]:</label><br/>
					<input type="text" id="sourcename" name="sourcename" value=""></input><br/>
					<input type="hidden" id="sourcecontent" name="sourcecontent" value="<%=currentNode.getPath()%>/*"></input>
					<div id="button1" class="button">Upload</div>
					<span id="status"></span>
				</div>
			</div>
		</div>
<%} %>
    <%@ include file="/apps/administration/footer/footer.jsp" %>
	
</body>
</html>
