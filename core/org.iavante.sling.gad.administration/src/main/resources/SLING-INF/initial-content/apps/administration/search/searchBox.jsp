<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<script type="text/javascript" src="/apps/administration/static/js/webtoolkit.base64.js"></script>

<script type="text/javascript">
	function sendSearch(){
		document.getElementById("qt").value = Base64.encode(document.getElementById("qt_aux").value);
		document.getElementById("searchForm").submit();
	}

	function checkIntro(oEvento){
		var iAscii; 
		
		if (oEvento.which){ 
			iAscii = oEvento.which;
		} 
		else{
			if (oEvento.keyCode){ 
				iAscii = oEvento.keyCode;
			}
			else {
				return false;
			}
		} 

		if (iAscii == 13){ 
			sendSearch();
		} 

		return true; 
	}
</script>

<form id="searchForm" action="/administration/search" method="GET" enctype="multipart/form-data">
	|&nbsp;Administraci&oacute;n&nbsp;&nbsp;
	<input type="text" id="qt_aux" name="qt_aux" value="<%=request.getParameter("qt_aux") != null ? request.getParameter("qt_aux") : ""%>" size="24" />
	<input type="submit" value="Buscar" onclick="javascript:sendSearch()" />
	<input id="qt" name="qt" type="hidden"/>
	<a class="logout" href="/administration/logout">Salir<a/>
</form>
