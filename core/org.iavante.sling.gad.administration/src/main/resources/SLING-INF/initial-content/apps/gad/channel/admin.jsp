<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="java.util.Map"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.*"%>
<%@ page import="org.apache.sling.api.resource.Resource"%>
<%@ page import="org.iavante.sling.gad.content.ContentTools"%>
<%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />

<%
String url = request.getRequestURL().toString();
String uri = request.getRequestURI();
String aux = url.replaceAll(uri,"");
pageContext.setAttribute("urlBase", aux);
%>

<html>

<head>
<%@ include file="/apps/administration/header/html_header.jsp" %>
<link rel="stylesheet" href="/apps/administration/static/css/file.css">
</head>

<body>

<%@ include file="/apps/administration/header/header.jsp" %>

<div id="maincontainer">
    <div id="data">

<form action="<%=currentNode.getPath()%>" id="nodeProperties" method="post">

<%
	ArrayList etiquetas = new ArrayList();
	etiquetas.add("title");
	etiquetas.add("subtitle");
	etiquetas.add("description");
	etiquetas.add("picture");
	
 for(int i=0; i<etiquetas.size(); i++){
   
	String tagName = (String) etiquetas.get(i);%>

<%= tagName%><br />
<input class="input" type="text" id="<%= tagName%>" value="<% if(currentNode.hasProperty(tagName)){ %><%=currentNode.getProperty(tagName).getValue().getString()%><%}%>" /><br />
<%}%>

schema<br />
<input class="input" type="text" id="schemar" value="<% if(currentNode.hasProperty("schema")) { %><%=currentNode.getProperty("schema").getValue().getString()%><%}%>" disabled="disabled" /><br />

distribution_server<br />
<input class="input" type="text" id="distribution_server" value="<% if(currentNode.hasProperty("distribution_server")) { %><%=currentNode.getProperty("distribution_server").getValue().getString()%><%}%>" disabled="disabled" /><br />

distribution_format_video<br />
<input class="input" type="text" id="distribution_format_video" value="<% if(currentNode.hasProperty("distribution_format_video")) { %><%=currentNode.getProperty("distribution_format_video").getValue().getString()%><%}%>" disabled="disabled" /><br />

distribution_format_image<br />
<input class="input" type="text" id="distribution_format_image" value="<% if(currentNode.hasProperty("distribution_format_image")) { %><%=currentNode.getProperty("distribution_format_image").getValue().getString()%><%}%>" disabled="disabled" /><br />

sling:resourceType<br />
<input class="input" type="text" id="sling:resourceType" value="<% if(currentNode.hasProperty("sling:resourceType")) { %><%=currentNode.getProperty("sling:resourceType").getValue().getString()%><%}%>" disabled="disabled" /><br />


created<br />
<input class="input" type="text" id="jcr:created" value="<% if(currentNode.hasProperty("jcr:created")) { %><%=currentNode.getProperty("jcr:created").getValue().getString()%><%}%>" disabled="disabled" /><br />

createdBy<br />
<input class="input" type="text" id="jcr:createdBy" value="<% if(currentNode.hasProperty("jcr:createdBy")) { %><%=currentNode.getProperty("jcr:createdBy").getValue().getString()%><%}%>" disabled="disabled" /><br />

lastModified<br />
<input class="input" type="text" id="jcr:lastModified"value="<% if(currentNode.hasProperty("jcr:lastModified")) { %><%=currentNode.getProperty("jcr:lastModified").getValue().getString()%><%}%>" disabled="disabled" /><br />

lastModifiedBy<br />
<input class="input" type="text" id="jcr:lastModifiedBy" value="<% if(currentNode.hasProperty("jcr:lastModifiedBy")) { %><%=currentNode.getProperty("jcr:lastModifiedBy").getValue().getString()%><%}%>" disabled="disabled" /><br />

<input class="input" type="hidden" id="_charset_" value="utf-8" />

</form>

	<input class="guardar" type="button" onclick="javascript:guardar('<%=pageContext.getAttribute("urlBase")%><%=currentNode.getPath()%>')" value=""/>

  <div id="caja">
	<div class="corner-up"></div>	
		<a class="url" href="<%= currentNode.getName()%>/unpublished.admin">Unpublished</a><br />
		<a class="url" href="<%= currentNode.getPath()%>/contents.admin">Contenidos</a><br />
		<a class="url" href="<%= currentNode.getName()%>/cats.admin">Categor&iacute;as</a><br />
		<a class="url" href="<%= currentNode.getName()%>/tags.admin">Tags</a><br />
		<a class="url" href="<%= currentNode.getName()%>/ds_custom_props.admin">DS Custom props</a><br />
		<a class="url" href="<%= currentNode.getName()%>/multimedia_assets.admin">Multimedia Assets</a><br />
	<div class="corner-down"></div>
  </div>
</div> <!-- End data-->
</div> <!-- End maincontainer-->

	<%@ include file="/apps/administration/footer/footer.jsp" %>

</body>
</html>
