<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.*"%>
<%@ page import="org.apache.sling.api.resource.Resource"%>
<%@ page import="org.iavante.sling.gad.content.ContentTools"%>
<%@ page import="javax.jcr.Node"%>
<%@ page import="javax.jcr.NodeIterator"%>

<%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>

<sling:defineObjects />

<%
String url = request.getRequestURL().toString();
String uri = request.getRequestURI();
String aux = url.replaceAll(uri,"");
pageContext.setAttribute("urlBase", aux);
%>

<html>

<head>
<%@ include file="/apps/administration/header/html_header.jsp" %>
</head>

<body>

 <%@ include file="/apps/administration/header/header.jsp" %>
  
<div id="maincontainer">

  <div id="container">

	<div id="propiedades">

<%
PropertyIterator it = currentNode.getProperties(); 
	while(it.hasNext()){
		Property p = it.nextProperty();	
		if (!p.getDefinition().isMultiple()) {	%>						
		<input class="input" type="text" id="<%=p.getName()%>" value="<%=p.getValue().getString()%>"disabled="disabled" /><br />
<%}
}
%>
	</div>

  <div id="subcontainer">

	<div id="maintitle">Categor&iacute;as</div><br />
	
<%
String OFFSET = "offset";
String ITEMS = "items";

NodeIterator it3 = currentNode.getNodes();
%>

<%
if (request.getParameter(OFFSET) != null) {
    long skip = Long.parseLong(request.getParameter(OFFSET));
    while (skip > 0 && it3.hasNext()) {
        it3.next();
        skip--;
    }
}

long count = -1;
if (request.getParameter(ITEMS) != null) {
    count = Long.parseLong(request.getParameter(ITEMS));
}
%>

<div id="separador">
	<%
		Integer x=0;
		while (it3.hasNext() && count != 0) {
			x++;
			Node node = it3.nextNode(); 
	%>
 
  <% if(x!=1){%><div id="separador2"></div><%}%>

 	<a class="url" href="<%=node.getPath() %>.admin">&nbsp;&nbsp;&nbsp;<b><%= node.getProperty("title").getString()%></b></a><br />
 	<a class="delete" href="javascript:eliminar('<%=node.getPath()%>')">eliminar<a/><br />
	<%count--; %><%}%>
</div>

  	</div><!--End subcontainer-->
  </div><!--End container-->
</div><!--End maincontainer-->

<div id="menu">
	<div id="crear">
		<input type="button" class="newcategory" onclick="javascript:window.location='<%=currentNode.getPath()%>.create.admin'" value="" />
	</div>
</div>

 <%@ include file="/apps/administration/footer/footer.jsp" %>
	
</body>
</html>
