<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
    Node sources_node = null;

    if (currentNode.hasNode("sources")) {
	    sources_node = currentNode.getNode("sources");    			
    %>
	  
	  <!--  Schema sources -->
      <div id="caja">
      	<script type="text/javascript">
      	var subtitleUrl = null;
      	var thumbnailUrl = null;
      	</script>
      	
		<div id="sources" class="corner-up"><b>Esquema:</b></div>
   	    <%
		NodeIterator it2 = sources_node.getNodes();
		Integer x=0;
		while(it2.hasNext()){
			Node nodeFuente = it2.nextNode();
			if (sling.getService(org.iavante.sling.gad.source.SourceTools.class).is_a_playlist_source(nodeFuente)) continue; 
			x++;
			String rt = "";
		%>
		
		<div class="objeto">	
		<%if (nodeFuente.hasProperty("sling:resourceType") && "gad/source".compareTo(nodeFuente.getProperty("sling:resourceType").getValue().getString())==0) {%>
			<a class="delete" href="javascript:eliminar('<%=nodeFuente.getPath()%>')" >eliminar<a/>
			<a class="url" href="<%=nodeFuente.getPath()+ ".admin"%>"><%=nodeFuente.getName()%></a>&nbsp;<br />
			<% if ((!nodeFuente.hasProperty("file")) || (nodeFuente.getProperty("file").getValue().getString().equals(""))) { %>
			     <a onclick="javascript:document.getElementById('<%= nodeFuente.getName()%>').style.display='block';">+</a>
			     <div id="<%= nodeFuente.getName() %>" style="display:none; margin-left:65px;">
			     <br /><input type="file" id="newfile" name="newfile" />
			     <input type="button" onclick="uploader('<%= nodeFuente.getName() %>','<%=nodeFuente.getPath() %>');" value="Subir" />	
			     </div>	
			     <%} else {%>
			      <div class="source_files">
			      		<script type="text/javascript">
			      		if ('<%= nodeFuente.getName() %>' == 'subtitle_default'){
			      		//subtitleUrl = "<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).getOriginalUrl(nodeFuente)%>";
			      		subtitleUrl = mySubUrl;
			      		}
			      		if ('<%= nodeFuente.getName() %>' == 'thumbnail_default'){
			      		thumbnailUrl = myThumbUrl;
			      		}
      					</script>
				    <p><%= nodeFuente.getProperty("file").getValue().getString() %>&nbsp;
				    <a href="<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).getOriginalUrl(nodeFuente)%>"><img class="middle" title="Download" alt="Download" src="/apps/administration/static/images/download_icon.png" /></a></p>   
				  </div>
				 <%}%>
		    <%}%>
	    </div>
	    <%}%>

	<div class="corner-down"></div>
    </div><!-- fin caja -->
    
    <!--  Playlist sources -->    
    <div id="caja">
		<div id="sources" class="corner-up"><b>Playlist:</b></div>
   	    <%
		it2 = null;
   	    it2 = sources_node.getNodes();
		x=0;
		while(it2.hasNext()){
			Node nodeFuente = it2.nextNode();
			if (!sling.getService(org.iavante.sling.gad.source.SourceTools.class).is_a_playlist_source(nodeFuente)) continue; 
			x++;
		%>
		
		<div class="objeto">	
		<%if (nodeFuente.hasProperty("sling:resourceType") && "gad/source".compareTo(nodeFuente.getProperty("sling:resourceType").getValue().getString())==0) {%>
			<a class="delete" href="javascript:eliminar('<%=nodeFuente.getPath()%>')" >eliminar<a/>
			<a class="url" href="<%=nodeFuente.getPath()+ ".admin"%>"><%=nodeFuente.getName()%></a>&nbsp;<br />
			<% if ((!nodeFuente.hasProperty("file")) || (nodeFuente.getProperty("file").getValue().getString().equals(""))) { %>

			     <a onclick="javascript:document.getElementById('<%= nodeFuente.getName()%>').style.display='block';">+</a>
			     <div id="<%= nodeFuente.getName() %>" style="display:none; margin-left:65px;">
			     <br /><input type="file" id="newfile" name="newfile" />
			     <input type="button" onclick="uploader('<%= nodeFuente.getName() %>','<%=nodeFuente.getPath() %>');" value="Subir" />	
			     </div>	
			     <%} else {%>
			      <div class="source_files">

      					
				    <p><%= nodeFuente.getProperty("file").getValue().getString() %>&nbsp;
				    <a href="<%=sling.getService(org.iavante.sling.gad.content.ContentTools.class).getOriginalUrl(nodeFuente)%>"><img class="middle" title="Download" alt="Download" src="/apps/administration/static/images/download_icon.png" /></a></p>
				  </div>
				 <% }%>
		<%}%>
	    </div>
	    <%}%>

	  <div class="corner-down"></div>
      </div><!-- fin caja -->
    <%}%>