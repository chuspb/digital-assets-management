<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundaci�n Iavante
 * 
 * Authors: 
 *   Francisco Jos� Moreno Llorca <packo@assamita.net>
 *   Francisco Jes�s Gonz�lez Mata <chuspb@gmail.com>
 *   Juan Antonio Guzm�n Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel Jos� Cobo Fern�ndez <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or � as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page import="javax.jcr.Session"%>
<%@ page import="javax.jcr.Node"%>
<%@ page import="javax.jcr.Session"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="java.lang.String"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Iterator"%>

<%!
public static class NodeUtils {
	/** Path must be absolute */
	public static Node addNodeRecursively(String path, String type, Session session) throws Exception {
		Node currentRoot = session.getRootNode();
		String[] pathSegments = path.split("/");
		for(int i = 1; i < pathSegments.length; i++) {			
			if(!currentRoot.hasNode(pathSegments[i])) {
				currentRoot.addNode(pathSegments[i], "nt:unstructured");
			}
			currentRoot = currentRoot.getNode(pathSegments[i]);
		}
		return currentRoot;
	}
	
	public static String getThreadPath(Node node) throws Exception {
		// find the thread root 
		String[] tokenizer = node.getPath().split("/"); 
		return "/" + tokenizer[1] + "/" + tokenizer[2] + "/" + tokenizer[3];		
	}
	
	public static String getThreadUUID(Node node) throws Exception {
		return node.getSession().getRootNode().getNode(getThreadPath(node).substring(1)).getUUID();
	}	
}

public static class DateUtils {
	public static String format(Calendar cal) {
		SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd HH:mm" );		
		return df.format(cal.getTime()) ; 
	}
}

public static class MimeIcons{
	public static String getSourceBackgroundImage(Node nodo,String path){
		String file = "background-image: url(/apps/administration/static/images/";
		String mimetype = "";
		String type ="";
		try{
			Node node = nodo.getSession().getRootNode().getNode("path");
			mimetype = node.getProperty("mimetype").getValue().getString().trim();
			type = node.getProperty("type").getValue().getString().trim();
		}catch(Exception e){
			System.out.println(e.getMessage());
			System.out.println("ERROR: "+path);
			return "";
		}
		if ("video".compareToIgnoreCase(type) == 0){
			file+="gnome-mime-video.png";
		}
		else if ("image".compareToIgnoreCase(type) == 0){
			file+="gnome-mime-image.png";
		}
		else{
			if ("txt/plain".compareToIgnoreCase(mimetype) == 0)
				file+="gnome-mime-text.png";
			else if ("application/pdf".compareToIgnoreCase(mimetype) == 0)
				file+="gnome-mime-application-pdf.png";
			else if ("application/vnd.ms-office".compareToIgnoreCase(mimetype) == 0)
				file+="gnome-mime-application-msword.png";
			else
				file+="gnome-mime-application.png";
		}
		file+=")";
		return file;
	}
}

public static class ComboLang{
	
	public static String buildComboLang(String id, String value, String cssClass){

		String code = "<select id=\"" + id + "\" name=\"" + id + "\" class=\"" + cssClass + "\">";
	
		Map<String,String> langs = new HashMap<String,String>();
		langs.put("es_ES","Espa�ol");langs.put("en_US","Ingl�s USA");
		langs.put("en_GB","Ingl�s GB");langs.put("fr_FR","Franc�s");
		langs.put("de_DE","Alem�n");langs.put("it_IT","Italiano");
		langs.put("ja_JP","Japon�s");langs.put("da_DK","Dan�s");
		langs.put("fi_FI","Fin�s");langs.put("pt_PT","Portugu�s");
		langs.put("ru_RU","Ruso");langs.put("sv_SE","Sueco");
		langs.put("zh_CN","Chino");
	
		Set set = langs.entrySet();
		Iterator it = set.iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			
			code += "<option value=\"" + entry.getKey() + "\""; 
			if(value.compareTo(""+entry.getKey())==0){
				code += " selected=\"selected\"";
			}
			code += ">";
			code += entry.getValue();
			code += "</option>";
		}
		code += "</select>";
		
		return code;
	}
	
	public static String buildComboLang(String id, String value, String cssClass, List candidates, String comentario){

		String code = "<select id=\"" + id + "\" name=\"" + id + "\" class=\"" + cssClass + "\">";
		
		Map<String,String> langsReduce = new HashMap<String,String>();
		
		Map<String,String> langs = new HashMap<String,String>();
		langs.put("es_ES","Espa�ol");langs.put("en_US","Ingl�s USA");
		langs.put("en_GB","Ingl�s GB");langs.put("fr_FR","Franc�s");
		langs.put("de_DE","Alem�n");langs.put("it_IT","Italiano");
		langs.put("ja_JP","Japon�s");langs.put("da_DK","Dan�s");
		langs.put("fi_FI","Fin�s");langs.put("pt_PT","Portugu�s");
		langs.put("ru_RU","Ruso");langs.put("sv_SE","Sueco");
		langs.put("zh_CN","Chino");
		
		if(candidates.size()==0){
			langsReduce.put("",comentario);
		}else{
			for(int i=0; i<candidates.size(); i++){
				String candidate = (String)candidates.get(i);
				String [] ls = candidate.split("/");
				langsReduce.put(candidate, ((String)langs.get(ls[ls.length-3])) + " > " + ls[ls.length-1]);
			}
		}
	
		Set set = langsReduce.entrySet();
		Iterator it = set.iterator();
		while (it.hasNext()) {
			Map.Entry entry = (Map.Entry) it.next();
			
			code += "<option value=\"" + entry.getKey() + "\""; 
			if(value.compareTo(""+entry.getKey())==0){
				code += " selected=\"true\"";
			}
			code += ">";
			code += entry.getValue();
			code += "</option>";
		}
		code += "</select>";
		
		return code;
	}
}

%>