<%
/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.lang.String"%>
<%@ page import="javax.jcr.PropertyIterator, javax.jcr.Property, javax.jcr.Node"%>
<%@ page import="javax.jcr.NodeIterator"%> 
<%@taglib prefix="sling" uri="http://sling.apache.org/taglibs/sling/1.0"%>
<sling:defineObjects />

<%
	String url = request.getRequestURL().toString();
	String uri = request.getRequestURI();
	String aux = url.replaceAll(uri,"");
	pageContext.setAttribute("urlBase", aux);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <%@ include file="/apps/administration/header/html_header.jsp" %>
  <link rel="stylesheet" href="/apps/administration/static/css/file.css">
</head>

<body>
 <%@ include file="/apps/administration/header/header.jsp" %>

<div id="maincontainer">
	<br />
    <div id="maintitle">Propiedades de canal. DS: "<%=currentNode.getParent().getProperty("distribution_server").getValue().getString() %>"</div><br />
    <br />
	<%
	
   	   PropertyIterator propiter = currentNode.getProperties(); %>
		
		<form action="config/<%= currentNode %>.admin" id="<%= currentNode %>" method="post">				
		<div style="clear:both;width:800px;text-align:left;">
        <table width="800px">		
		<% while (propiter.hasNext()) {
			Property prop = (Property) propiter.next(); %>
			<tr>
			<td width="50%">
			<span><%= prop.getName() %></span><span></td>
			<td width="50%"><input class="input" type="text" name="<%= prop.getName() %>" value="<%= prop.getValue().getString() %>">
			</input>
			</td></tr>				
	    <%}%>
	    <tr>
	      <td></td>
	      <td></td>
	    </tr>
	    <tr>
	      <td width="50%"></td>
	      
		  <td width="50%"><input type="submit" value="Guardar"/></td>
		</tr>
		</table>	
		<input type="hidden" name=":redirect" value="<%= url %>" />
        </div>
        </form>		
</div>
<%@ include file="/apps/administration/footer/footer.jsp" %>
</body>