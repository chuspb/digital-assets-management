/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.source.impl;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Map;

import javax.jcr.AccessDeniedException;
import javax.jcr.InvalidItemStateException;
import javax.jcr.ItemExistsException;
import javax.jcr.ItemNotFoundException;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.ReferentialIntegrityException;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.nodetype.NoSuchNodeTypeException;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;
import javax.jcr.observation.ObservationManager;
import javax.jcr.version.VersionException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.sling.jcr.api.SlingRepository;
import org.iavante.sling.commons.services.FileToolsService;
import org.iavante.sling.commons.services.GADContentCreationService;
import org.iavante.sling.commons.services.ITranscoderService;
import org.iavante.sling.commons.services.PortalSetup;
import org.iavante.sling.gad.content.ContentTools;
import org.iavante.sling.gad.source.SourceService;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Listener implementation at /content/sources. Create transformation if needed
 * when a new source is created.
 * 
 * @scr.service
 * @scr.component immediate="true" metatype="false"
 * @scr.property name="service.description" value="Source listener"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */

public class SourceServiceImpl implements SourceService, EventListener,
		Serializable {
	private static final long serialVersionUID = 1L;

	/** JCR Session. */
	private Session session;

	/** JCR Observation Manager. */
	private ObservationManager observationManager;

	/** @scr.reference */
	private SlingRepository repository;

	/** @scr.reference */
	private ITranscoderService transcoderService;

	/** @scr.reference */
	private PortalSetup portalSetup;

	/** @scr.reference */
	private FileToolsService fileTool;

	/** @scr.reference */
	private GADContentCreationService gadContentTool;

	/** GAD Config properties. */
	private Map<String, String> globalProperties = null;
	private Map<String, String> s3backendProperties = null;

	/** Default Logger */
	private static final Logger log = LoggerFactory
			.getLogger(SourceServiceImpl.class);

	/** Commons variables. */
	private final String COLLECTIONS_PATH = "/content/colecciones";
	private final String TRANSFORMATIONS_FOLDER = "transformations";
	private final String TRANSFORMATION_PREVIEW_FLV = "preview";
	private final String TRANSFORMATION_PREVIEW_OGV = "preview_ogv";
	private final String TRANSFORMATION_PREVIEW_IMAGE = "image_preview";
	private final String TRANSFORMATION_PREVIEW_SUBTITLE = "subtitle_preview";
	private final String TRANSFORMATION_PREVIEW_AUDIO = "audio_preview";
	private final String TRANSFORMATION_NOCONVERSION = "noconversion";
	private final String TRANSFORMATION_PREVIEW_THUMBNAIL = "thumbnail_preview";
	private final String PREVIEW_MIMETYPE_FLV = "video/x-flv";
	private final String PREVIEW_MIMETYPE_OGV = "video/ogg";
	private final String PREVIEW_MIMETYPE_IMAGE = "image/jpeg";
	private final String PREVIEW_MIMETYPE_GIF = "image/gif";
	private final String PREVIEW_MIMETYPE_AUDIO = "audio/x-hx-aac-adts";
	private final String SOURCE_RESOURCE_TYPE = "gad/source";
	private final String TRANSFORMATION_RESOURCETYPE = "gad/transformation";
	private final int NUM_THUMBNAILS = 3;
	private final String PROP_DEFAULT_SUBTITLE = "default_subtitle";
	private final String PROP_DEFAULT_AUDIODESCRIPTION = "default_audiodescription";

	/** Default file folder. */
	private String defaultFileFolder;

	/** Sling url. */
	private String slingUrl;

	/** JCR root node. */
	private Node rootNode;

	/** Name of collection. */
	private String collection;

	/** External storage. */
	private String externalStorageServer;

	/** External storage. */
	private String externalStorageUrl;

	/** @scr.reference */
	private ContentTools contentTool;

	/** Validated prop. */
	private final String VALIDATED_PROP = "validated";

	public String getRepository() {
		return repository.getDescriptor(SlingRepository.REP_NAME_DESC);
	}

	protected void activate(ComponentContext context) {
		try {
			session = repository.loginAdministrative(null);
			if (repository.getDescriptor(Repository.OPTION_OBSERVATION_SUPPORTED)
					.equals("true")) {
				observationManager = session.getWorkspace().getObservationManager();
				String[] types = null;
				observationManager.addEventListener(this, Event.NODE_ADDED
						| Event.PROPERTY_REMOVED | Event.PROPERTY_ADDED, COLLECTIONS_PATH,
						true, types, null, true);
			}
		} catch (Exception e) {
			log.error("cannot start");
		}

		// Get config params
		try {
			this.rootNode = session.getRootNode();
			// Get properties
			this.globalProperties = portalSetup.get_config_properties("/sling");
			this.s3backendProperties = portalSetup
					.get_config_properties("/s3backend");
			this.defaultFileFolder = this.globalProperties.get("default_file_folder");
			this.slingUrl = this.globalProperties.get("sling_url");

			// S3 default
			this.externalStorageServer = "S3";

			this.externalStorageUrl = this.s3backendProperties.get("url")
					+ this.s3backendProperties.get("bucket");

		} catch (ValueFormatException e) {
			e.printStackTrace();
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
	}

	protected void deactivate(ComponentContext componentContext)
			throws RepositoryException {
		if (observationManager != null) {
			observationManager.removeEventListener(this);
		}
		if (session != null) {
			session.logout();
			session = null;
		}
	}

	/*
	 * @see javax.jcr.observation.EventListener
	 */
	public void onEvent(EventIterator eventIterator) {
		if (log.isInfoEnabled())
			log.info("EventIterator");
		while (eventIterator.hasNext()) {
			Event event = eventIterator.nextEvent();
			Node addedNode = null;

			String propPath = "";
			try {
				propPath = event.getPath();

				collection = propPath.split("/")[3];

			} catch (RepositoryException e1) {
				e1.printStackTrace();
			}

			try {

				if (event.getType() == Event.NODE_ADDED) {

					addedNode = session.getRootNode().getNode(
							event.getPath().substring(1));

					if (log.isInfoEnabled())
						log.info("onEvent, node added: " + addedNode.getPath());

					if (addedNode.hasProperty("sling:resourceType")) {
						if (addedNode.getProperty("sling:resourceType").getValue()
								.getString().equals(SOURCE_RESOURCE_TYPE)) {

							Node from = isCloned(addedNode);

							if (from == null) {
								// Add transformation folder
								addedNode.addNode(TRANSFORMATIONS_FOLDER);
								addedNode.setProperty("finish", "1");
								addedNode.save();
								addedNode.getNode(TRANSFORMATIONS_FOLDER).setProperty(
										"sling:resourceType", "gad/transformations");
								addedNode.save();
								if (log.isInfoEnabled())
									log.info("onEvent, new source node: " + addedNode.getPath());
							} else {
								if (addedNode.hasNode(TRANSFORMATIONS_FOLDER)) {
									// Uncomment in order to send new conversion jobs for the
									// cloned source
									// processSource(addedNode);
									if (log.isInfoEnabled())
										log.info("onEvent, node cloned: " + addedNode.getPath());
								}
							}
						}
					}
				}

				else if ((event.getType() == Event.PROPERTY_REMOVED)
						&& (propPath.endsWith("jcr:lastModified"))) {
					if (log.isInfoEnabled())
						log.info("PROPERTY REMOVED & last modifed");
					int pos = propPath.lastIndexOf('/');
					String nodePath = propPath.substring(0, pos);

					collection = propPath.split("/")[3];
					addedNode = rootNode.getNode(nodePath.substring(1));

					if ((addedNode.hasProperty("sling:resourceType"))
							&& (addedNode.hasProperty("file"))) {

						String file = addedNode.getProperty("file").getString();
						if ((addedNode.getProperty("sling:resourceType").getString()
								.equals(SOURCE_RESOURCE_TYPE))
								&& (file != "")) {
							if (log.isInfoEnabled())
								log.info("Edit source");
							processSource(addedNode);
						}

						if ((addedNode.getProperty("sling:resourceType").getString()
								.equals(TRANSFORMATION_RESOURCETYPE))
								&& (file != "")) {
							if (log.isInfoEnabled())
								log.info("Edit transformation");
							// copyTransformation(addedNode); //this was to synchronize with
							// collection/sources
						}
						if (log.isInfoEnabled())
							log.info("Process edited source finished");
					}
				}
			} catch (ValueFormatException e) {
				e.printStackTrace();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (PathNotFoundException e) {
				e.printStackTrace();
			} catch (RepositoryException e) {
				e.printStackTrace();
			}
		}
	}

	/*
	 * @see org.iavante.sling.gad.source.SourceService
	 */
	@SuppressWarnings("unchecked")
	public int copy(ServletRequest request, ServletResponse response,
			String nodePath) {

		if (log.isInfoEnabled())
			log.info("copy, ini");

		int code = 0;

		Enumeration en2 = request.getParameterNames();
		while (en2.hasMoreElements()) {
			String cad = (String) en2.nextElement();
			if (log.isInfoEnabled())
				log.info("copy, Parameter: " + cad);
		}

		String dest = request.getParameter("dest");
		String replace = request.getParameter("replace");
		if (replace == null) {
			replace = "true";
		}
		String title = request.getParameter("title");

		String[] aux = nodePath.split("/");
		String contentRelPath = aux[aux.length - 3];

		try {

			// copy the source to the dest sources folder
			Node source = rootNode.getNode(nodePath.substring(1));
			if (source.hasProperty("sling:resourceType")
					&& SOURCE_RESOURCE_TYPE.equals(source.getProperty(
							"sling:resourceType").getValue().getString())) {

				// New source name
				String name = "";
				if ((title != null) && !"".equals(title)) {
					name = title;
				} else {
					String[] cads = source.getPath().split("/");
					name = cads[cads.length - 1];
					name = name.endsWith("_default") ? name.replace("_default", "_from")
							: name;
					name += "_" + contentRelPath;
				}
				String destSourcePath = dest + "/sources/" + name;

				if (log.isInfoEnabled())
					log.info("copy, source: " + source.getPath() + ", dest:"
							+ destSourcePath);

				// Copy the source to 'dest' content
				rootNode.getSession().getWorkspace().copy(source.getPath(),
						destSourcePath);

				// Set property schema=playlist
				rootNode.getNode(destSourcePath.substring(1)).setProperty("schematype",
						"playlist");

				rootNode.getNode(destSourcePath.substring(1)).save();

			}

		} catch (ConstraintViolationException e) {
			e.printStackTrace();
		} catch (VersionException e) {
			e.printStackTrace();
		} catch (AccessDeniedException e) {
			e.printStackTrace();
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (ItemExistsException e) {
			e.printStackTrace();
		} catch (LockException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		if (log.isInfoEnabled())
			log.info("copy, end");

		return code;
	}

	/*
	 * @see org.iavante.sling.gad.source.SourceService
	 */
	public int copyTransformation(ServletRequest request,
			ServletResponse response, String nodePath) {

		int code = 0;

		String dest = request.getParameter("dest");
		String overwriteParam = request.getParameter("overwritesourceprops");
		String newTransName = request.getParameter("newtransname");
		boolean overwriteSourceProps = false;
		if (overwriteParam != null) {
			overwriteSourceProps = Boolean.parseBoolean(overwriteParam);
		}

		String[] aux = nodePath.split("/");

		String transName = aux[aux.length - 1];

		if (newTransName != null) {
			transName = newTransName;
		}

		if (log.isInfoEnabled())
			log.info("copyTransformation, transName:" + transName + ", overwrite:"
					+ overwriteSourceProps + ", dest:" + dest);

		try {

			// copy the transformation to the dest transformations folder
			Node trans = rootNode.getNode(nodePath.substring(1));
			if (trans.hasProperty("sling:resourceType")
					&& TRANSFORMATION_RESOURCETYPE.equals(trans.getProperty(
							"sling:resourceType").getString())) {

				String destTransPath = dest + "/transformations/" + transName;

				if (log.isInfoEnabled())
					log.info("copyTransformation, transformation: " + trans.getPath()
							+ ", dest:" + destTransPath);

				// Delete the transformation, if exists
				if (rootNode.hasNode(destTransPath.substring(1))) {
					Node nodeAux = rootNode.getNode(destTransPath.substring(1));
					Node nodeAuxParent = nodeAux.getParent();
					nodeAux.remove();
					nodeAuxParent.save();
				}

				// Copy the transformation to 'dest' source
				rootNode.getSession().getWorkspace().copy(trans.getPath(),
						destTransPath);

				// Overwrite source parent props
				if (overwriteSourceProps) {
					Node newTrans = rootNode.getNode(destTransPath.substring(1));
					Node source = newTrans.getParent().getParent();

					String mimetype = newTrans.getProperty("mimetype").getString();

					source.setProperty("file", newTrans.getProperty("file").getString());
					source.setProperty("mimetype", mimetype);
					source.setProperty("type", mimetype.substring(0, mimetype
							.indexOf("/")));

					source.save();

					if (log.isInfoEnabled())
						log.info("copyTransformation, source: " + source.getPath()
								+ " modified.");
				}

				Node newTrans = rootNode.getNode(destTransPath.substring(1));
				Node content = newTrans.getParent().getParent().getParent().getParent();

				if (contentTool.is_valid_content(content)) {
					content.setProperty(VALIDATED_PROP, "1");
				} else {
					content.setProperty(VALIDATED_PROP, "0");
				}
				content.save();

			}

		} catch (ConstraintViolationException e) {
			e.printStackTrace();
		} catch (VersionException e) {
			e.printStackTrace();
		} catch (AccessDeniedException e) {
			e.printStackTrace();
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (ItemExistsException e) {
			e.printStackTrace();
		} catch (LockException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		return code;
	}

	// -------------------- Internal ----------------------

	/**
	 * Check if a node is copied from another one
	 * 
	 * @param node
	 * @return The genuine node
	 */
	private Node isCloned(Node node) {
		@SuppressWarnings("unused")
		boolean res = false;
		Node from = null;

		String id = "gaduuid";

		// check if exists any content with the same gaduuid
		try {

			if (node.getParent().getParent().hasProperty(id)) {

				String nodeId = "";
				if (node.getParent().getParent().hasProperty("from")) {
					nodeId = node.getParent().getParent().getProperty("from").getValue()
							.getString();
					// log.info("isCloned, newNodeContent, from: " + node.getPath() +
					// ", gaduuid: " + nodeId);
				} else {
					nodeId = node.getParent().getParent().getProperty(id).getValue()
							.getString();
					// log.info("isCloned, newNodeContent, gaduuid: " + node.getPath() +
					// ", gaduuid: " + nodeId);
				}

				NodeIterator it = node.getParent().getParent().getParent().getNodes();
				// while (it.hasNext() && !res) {
				while (it.hasNext()) {
					Node child = it.nextNode();
					if (child.hasProperty(id)) {
						String childId = child.getProperty(id).getValue().getString();
						log.info("isCloned, nodeContent: "
								+ node.getParent().getParent().getPath() + ", otherContent: "
								+ child.getPath());
						if ((nodeId.compareTo(childId) == 0)
								&& (!child.getPath().equals(
										node.getParent().getParent().getPath()))) {
							res = true;
							from = child;
						}
					}
				}
			}

		} catch (ItemNotFoundException e) {
			e.printStackTrace();
		} catch (AccessDeniedException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		return from;
	}

	/**
	 * Update the preview node with the notification status code.
	 * 
	 * @param previewNode
	 * @param status
	 */
	private void update_trans_prew_notification(Node previewNode, int status) {
		try {
			previewNode.setProperty("notify_status_code", Integer.toString(status));
			previewNode.save();
		} catch (ValueFormatException e) {
			e.printStackTrace();
		} catch (VersionException e) {
			e.printStackTrace();
		} catch (LockException e) {
			e.printStackTrace();
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Process the source. Checks if it needs conversion.
	 * 
	 * @param source
	 * @throws RepositoryException
	 * @throws PathNotFoundException
	 * @throws ValueFormatException
	 */
	private void processSource(Node source) throws ValueFormatException,
			PathNotFoundException, RepositoryException {

		// 0. Check if is a transcription
		try {
			processIfTranscription(source);
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		// 1. Configure the conversions

		String sourceMimetype = "";

		if (source.hasProperty("type")) {
			sourceMimetype = source.getProperty("type").getString();
		}

		if (log.isInfoEnabled())
			log.info("processSource, type: " + sourceMimetype);

		// S3 default
		this.externalStorageServer = "S3";
		this.externalStorageUrl = this.s3backendProperties.get("url")
				+ this.s3backendProperties.get("bucket");

		Node collectionNode = rootNode.getNode(COLLECTIONS_PATH.substring(1) + "/"
				+ collection);

		if (collectionNode.hasProperty("extern_storage")) {

			String externStorageValue = collectionNode.getProperty("extern_storage")
					.getString();
			if ((!"on".equals(externStorageValue))
					&& (!"off".equals(externStorageValue))) {
				this.externalStorageUrl = "";
				this.externalStorageServer = "";
				if (log.isInfoEnabled())
					log
							.info("processSource, No external storage. Deactivated in collection");
			}
		}

		// Video
		if (sourceMimetype.contains("video")) {

			String previewNodePathFlv = gadContentTool.createTransformation(source,
					TRANSFORMATION_PREVIEW_FLV, PREVIEW_MIMETYPE_FLV);
			String previewNodePathOgv = gadContentTool.createTransformation(source,
					TRANSFORMATION_PREVIEW_OGV, PREVIEW_MIMETYPE_OGV);
			Node previewNodeFlv = rootNode.getNode(previewNodePathFlv.substring(1));
			Node previewNodeOgv = rootNode.getNode(previewNodePathOgv.substring(1));

			if (log.isInfoEnabled())
				log.info("processSource, Preview path: " + previewNodePathFlv);
			if (log.isInfoEnabled())
				log.info("processSource, Preview path: " + previewNodePathOgv);

			// Get file name
			String fileName = source.getProperty("file").getString();

			String outputFileNameFlv = fileTool.genTransformationFilename(fileName,
					PREVIEW_MIMETYPE_FLV, "flv");
			String outputFileNameOgv = fileTool.genTransformationFilename(fileName,
					PREVIEW_MIMETYPE_OGV, "ogv");

			String inputPath = this.defaultFileFolder + "/" + fileName;
			if (log.isInfoEnabled())
				log.info("processSource, Input path: " + inputPath);
			String outputPathFlv = this.defaultFileFolder + "/" + outputFileNameFlv;
			String outputPathOgv = this.defaultFileFolder + "/" + outputFileNameOgv;
			String notificationUrlFlv = this.slingUrl + previewNodePathFlv;
			String notificationUrlOgv = this.slingUrl + previewNodePathOgv;

			if (log.isInfoEnabled())
				log.info("processSource, Conversion params: " + inputPath + " "
						+ outputPathFlv + " " + notificationUrlFlv + " "
						+ externalStorageServer + " " + externalStorageUrl);
			int statusCodeFlv = transcoderService.sendConversionTask(
					TRANSFORMATION_PREVIEW_FLV, inputPath, outputPathFlv,
					notificationUrlFlv, externalStorageServer, externalStorageUrl);

			if (log.isInfoEnabled())
				log.info("processSource, Conversion params: " + inputPath + " "
						+ outputPathOgv + " " + notificationUrlOgv + " "
						+ externalStorageServer + " " + externalStorageUrl);
			int statusCodeOgv = transcoderService.sendConversionTask(
					TRANSFORMATION_PREVIEW_OGV, inputPath, outputPathOgv,
					notificationUrlOgv, externalStorageServer, externalStorageUrl);

			if (log.isInfoEnabled())
				log.info("processSource, Notification sent. Status code: "
						+ statusCodeFlv);
			if (log.isInfoEnabled())
				log.info("processSource, Notification sent. Status code: "
						+ statusCodeOgv);

			update_trans_prew_notification(previewNodeFlv, statusCodeFlv);
			update_trans_prew_notification(previewNodeOgv, statusCodeOgv);

			// Copy the source to /sources
			// if (source.getPath().split("/")[4].equals(CONTENTS_FOLDER)) {
			// Node destNode = rootNode.getNode(COLLECTIONS_PATH.substring(1) + "/"
			// + collection + "/" + SOURCES_FOLDER);
			// String nodeName = fileName;
			// jcrTool.copy(source, destNode, nodeName);
			// destNode.save();
			// Node dest_source = destNode.getNode(nodeName);
			// String source_title = generate_source_title(fileName);
			// dest_source.setProperty("title", source_title);
			// dest_source.save();
			// if (log.isInfoEnabled())
			// log.info("processSource, Copy the source to sources");
			// }

			// -----------------------------------
			// Automatic thumbnail generation
			// -----------------------------------
			if (source.hasProperty("lengths")) {

				String[] thumbFilesPath = new String[NUM_THUMBNAILS];

				// Calc hh:mm:ss when snapshots will be done
				String duration = source.getProperty("lengths").getString().trim();
				String[] hhmmss = duration.split(":");
				int durationInSeconds = Integer.parseInt(hhmmss[0]) * 3600
						+ Integer.parseInt(hhmmss[1]) * 60
						+ Integer.parseInt(hhmmss[2].substring(0, 2));
				log.info("processSource, durationInSeconds=" + durationInSeconds);

				String[] timeSnapshot = new String[NUM_THUMBNAILS];
				timeSnapshot[0] = "00:00:01";
				for (int i = 1; i < NUM_THUMBNAILS; i++) {
					int sss = durationInSeconds / NUM_THUMBNAILS * i;
					int hh = sss / 3600;
					int mm = (sss - hh * 3600) / 60;
					int ss = sss - hh * 3600 - mm * 60;
					timeSnapshot[i] = hh + ":" + mm + ":" + ss;
					log.info("processSource, timeSnapshot[" + i + "] = "
							+ timeSnapshot[i]);
				}

				for (int i = 0; i < timeSnapshot.length; i++) {

					String previewNodePath = gadContentTool.createTransformation(source,
							TRANSFORMATION_PREVIEW_THUMBNAIL + "_" + i,
							PREVIEW_MIMETYPE_IMAGE);
					Node previewNode = rootNode.getNode(previewNodePath.substring(1));

					if (log.isInfoEnabled())
						log.info("processSource, Preview path thumbnail: "
								+ previewNodePath);

					String outputFileName = fileTool.genTransformationFilename(fileName,
							PREVIEW_MIMETYPE_IMAGE + "_" + i, "jpg");
					String outputPath = this.defaultFileFolder + "/" + outputFileName;
					thumbFilesPath[i] = outputPath;
					String notificationUrl = this.slingUrl + previewNodePath;

					if (log.isInfoEnabled())
						log.info("processSource, Conversion params thumbnail: " + inputPath
								+ " " + outputPath + " " + notificationUrl + " "
								+ externalStorageServer + " " + externalStorageUrl);

					int statusCode = transcoderService.sendConversionTask2(
							TRANSFORMATION_PREVIEW_THUMBNAIL, inputPath, outputPath,
							notificationUrl, externalStorageServer, externalStorageUrl,
							" -ss " + timeSnapshot[i] + " -s 470x320 -f image2 -vframes 1 ");

					if (log.isInfoEnabled())
						log
								.info("processSource, Notification thumbnail sent. Status code: "
										+ statusCode);
					update_trans_prew_notification(previewNode, statusCode);

				} // end for

				// -------------------------------------------------
				// Generate the gif animation (The last thumbnail)
				// -------------------------------------------------
				String previewNodePath = gadContentTool.createTransformation(source,
						TRANSFORMATION_PREVIEW_THUMBNAIL + "_" + NUM_THUMBNAILS,
						PREVIEW_MIMETYPE_GIF);
				Node previewNode = rootNode.getNode(previewNodePath.substring(1));

				if (log.isInfoEnabled())
					log.info("processSource, Preview path thumbnail: " + previewNodePath);

				String outputFileName = fileTool.genTransformationFilename(fileName,
						PREVIEW_MIMETYPE_GIF + "_" + NUM_THUMBNAILS, "gif");
				String outputPath = this.defaultFileFolder + "/" + outputFileName;
				String notificationUrl = this.slingUrl + previewNodePath;

				if (log.isInfoEnabled())
					log.info("processSource, Conversion params thumbnail: " + inputPath
							+ " " + outputPath + " " + notificationUrl + " "
							+ externalStorageServer + " " + externalStorageUrl);

				// execute full command
				String params = "custom /convert -delay 100 -size 235x160";
				for (int i = 0; i < thumbFilesPath.length; i++) {
					params += " -page +0+0 " + thumbFilesPath[i];
				}
				params += " -loop 0 " + outputPath;

				int statusCode = transcoderService.sendConversionTask2(
						TRANSFORMATION_PREVIEW_IMAGE, inputPath, outputPath,
						notificationUrl, externalStorageServer, externalStorageUrl, params);

				if (log.isInfoEnabled())
					log.info("processSource, Notification thumbnail sent. Status code: "
							+ statusCode);
				update_trans_prew_notification(previewNode, statusCode);

			}// end if 'lengths'
		}

		// Image
		else if (sourceMimetype.contains("image")
				|| sourceMimetype.contains("application")) {

			String previewNodePath = gadContentTool.createTransformation(source,
					TRANSFORMATION_PREVIEW_IMAGE, sourceMimetype);
			Node previewNode = rootNode.getNode(previewNodePath.substring(1));
			if (log.isInfoEnabled())
				log.info("processSource, Preview path: " + previewNodePath);

			// Get file name
			String fileName = source.getProperty("file").getString();

			String outputFileName = fileTool.genTransformationFilename(fileName,
					PREVIEW_MIMETYPE_IMAGE, "jpg");

			String inputPath = this.defaultFileFolder + "/" + fileName;
			if (log.isInfoEnabled())
				log.info("processSource, Input path: " + inputPath);
			String outputPath = this.defaultFileFolder + "/" + outputFileName;
			String notificationUrl = this.slingUrl + previewNodePath;

			// Send conversion task
			int statusCode = transcoderService.sendConversionTask(
					TRANSFORMATION_PREVIEW_IMAGE, inputPath, outputPath, notificationUrl,
					externalStorageServer, externalStorageUrl);
			if (log.isInfoEnabled())
				log
						.info("processSource, Notification sent. Status code: "
								+ statusCode);

			update_trans_prew_notification(previewNode, statusCode);
		}

		else if (sourceMimetype.contains("text")) { // for subtitles

			String previewNodePath = gadContentTool.createTransformation(source,
					TRANSFORMATION_PREVIEW_SUBTITLE, sourceMimetype);
			Node previewNode = rootNode.getNode(previewNodePath.substring(1));
			if (log.isInfoEnabled())
				log.info("processSource, Preview path: " + previewNodePath);

			// Get file name
			String fileName = source.getProperty("file").getString();

			if (fileName.endsWith("srt")) {

				// String outputFileName = fileTool.genTransformationFilename(fileName,
				// PREVIEW_MIMETYPE_SUBTITLE, "");
				String outputFileName = fileName;

				String inputPath = this.defaultFileFolder + "/" + fileName;
				if (log.isInfoEnabled())
					log.info("processSource, Input path: " + inputPath);
				String outputPath = this.defaultFileFolder + "/" + outputFileName;
				String notificationUrl = this.slingUrl + previewNodePath;

				// Send conversion task
				int statusCode = transcoderService.sendConversionTask(
						TRANSFORMATION_NOCONVERSION, inputPath, outputPath,
						notificationUrl, externalStorageServer, externalStorageUrl);
				if (log.isInfoEnabled())
					log.info("processSource, Notification sent. Status code: "
							+ statusCode);

				update_trans_prew_notification(previewNode, statusCode);

			}
		}

		else if (sourceMimetype.contains("audio")) { // for subtitles

			String previewNodePath = gadContentTool.createTransformation(source,
					TRANSFORMATION_PREVIEW_AUDIO, sourceMimetype);
			Node previewNode = rootNode.getNode(previewNodePath.substring(1));
			if (log.isInfoEnabled())
				log.info("processSource, Preview path: " + previewNodePath);

			// Get file name
			String fileName = source.getProperty("file").getString();

			String outputFileName = fileTool.genTransformationFilename(fileName,
					PREVIEW_MIMETYPE_AUDIO, "aac");

			String inputPath = this.defaultFileFolder + "/" + fileName;
			if (log.isInfoEnabled())
				log.info("processSource, Input path: " + inputPath);
			String outputPath = this.defaultFileFolder + "/" + outputFileName;
			String notificationUrl = this.slingUrl + previewNodePath;

			// Send conversion task
			int statusCode = transcoderService.sendConversionTask(
					TRANSFORMATION_PREVIEW_AUDIO, inputPath, outputPath, notificationUrl,
					externalStorageServer, externalStorageUrl);
			if (log.isInfoEnabled())
				log
						.info("processSource, Notification sent. Status code: "
								+ statusCode);

			update_trans_prew_notification(previewNode, statusCode);
		}

		if (log.isInfoEnabled())
			log.info("processSource, Processs source finished");
	}

	/**
	 * @param source
	 * @throws RepositoryException
	 * @throws ItemNotFoundException
	 * @throws AccessDeniedException
	 * @throws ValueFormatException
	 * @throws PathNotFoundException
	 * @throws VersionException
	 * @throws LockException
	 * @throws ConstraintViolationException
	 * @throws ItemExistsException
	 * @throws InvalidItemStateException
	 * @throws ReferentialIntegrityException
	 * @throws NoSuchNodeTypeException
	 */
	private void processIfTranscription(Node source) throws RepositoryException,
			ItemNotFoundException, AccessDeniedException, ValueFormatException,
			PathNotFoundException, VersionException, LockException,
			ConstraintViolationException, ItemExistsException,
			InvalidItemStateException, ReferentialIntegrityException,
			NoSuchNodeTypeException {

		String[] pathSplit = source.getPath().split("/");
		// at least 5 elemets:
		// fuente_default/transcriptions/es_ES/subtitle/sub_srt
		if (pathSplit.length > 5
				&& "transcriptions".equals(pathSplit[pathSplit.length - 4])
				&& source.getParent().getParent().getParent().getParent().hasProperty(
						"sling:resourceType")
				&& SOURCE_RESOURCE_TYPE.equals(source.getParent().getParent()
						.getParent().getParent().getProperty("sling:resourceType")
						.getString())) {

			if (log.isInfoEnabled())
				log.info("processIfTranscription, transcription modified: "
						+ source.getPath());

			Node sourceParent = source.getParent().getParent().getParent()
					.getParent();

			// Reset prop in sourceParent if nodes don't exit
			if (sourceParent.hasProperty(PROP_DEFAULT_SUBTITLE)
					&& !"".equals(sourceParent.getProperty(PROP_DEFAULT_SUBTITLE)
							.getString())
					&& !sourceParent.hasNode(sourceParent.getProperty(
							PROP_DEFAULT_SUBTITLE).getString().substring(1))) {
				sourceParent.setProperty(PROP_DEFAULT_SUBTITLE, "");
				sourceParent.save();
				if (log.isInfoEnabled())
					log.info("processIfTranscription, prop " + PROP_DEFAULT_SUBTITLE
							+ " reset");
			}
			if (sourceParent.hasProperty(PROP_DEFAULT_AUDIODESCRIPTION)
					&& !"".equals(sourceParent.getProperty(PROP_DEFAULT_AUDIODESCRIPTION)
							.getString())
					&& !sourceParent.hasNode(sourceParent.getProperty(
							PROP_DEFAULT_AUDIODESCRIPTION).getString().substring(1))) {
				sourceParent.setProperty(PROP_DEFAULT_AUDIODESCRIPTION, "");
				sourceParent.save();
				if (log.isInfoEnabled())
					log.info("processIfTranscription, prop "
							+ PROP_DEFAULT_AUDIODESCRIPTION + " reset");
			}

			String prop = "";
			if ("subtitle".equals(pathSplit[pathSplit.length - 2])) {
				prop = PROP_DEFAULT_SUBTITLE;
			} else if ("audiodescription".equals(pathSplit[pathSplit.length - 2])) {
				prop = PROP_DEFAULT_AUDIODESCRIPTION;
			}

			String transType = pathSplit[pathSplit.length - 2];

			// Ej.: fuente_default.default_subtitle=""
			if (!sourceParent.hasProperty(prop)
					|| (sourceParent.hasProperty(prop) && "".equals(sourceParent
							.getProperty(prop).getString()))) {

				String value = "";

				if (log.isInfoEnabled())
					log
							.info("processIfTranscription, no default transcription assigned to  "
									+ sourceParent.getPath() + " yet");

				// get the content 'lang'
				String contentLang = "";
				if (sourceParent.getParent().getParent().hasProperty("lang")) {
					contentLang = sourceParent.getParent().getParent()
							.getProperty("lang").getString();
				}

				Node transcriptionNode = source.getParent().getParent().getParent();

				// assign this transcription as default
				value = "/" + pathSplit[pathSplit.length - 4] + "/"
						+ pathSplit[pathSplit.length - 3] + "/"
						+ pathSplit[pathSplit.length - 2] + "/"
						+ pathSplit[pathSplit.length - 1];

				// check if exits a transcription in the same lang that the content is
				if (transcriptionNode.hasNode(contentLang + "/" + transType)) {

					Node aux = transcriptionNode.getNode(contentLang).getNode(transType);
					if (aux.hasNodes()) {

						NodeIterator it = aux.getNodes();
						Node aux2 = it.nextNode();
						String[] aux3 = aux2.getPath().split("/");
						value = "/" + aux3[aux3.length - 4] + "/" + aux3[aux3.length - 3]
								+ "/" + aux3[aux3.length - 2] + "/" + aux3[aux3.length - 1];
					}
				}

				if (log.isInfoEnabled())
					log
							.info("processIfTranscription, default transcription assigned to  "
									+ sourceParent.getPath() + ": " + value);

				sourceParent.setProperty(prop, value);
				sourceParent.save();
			}

		}
	}

}
