/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.source.impl;

import java.io.Serializable;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;

import org.iavante.sling.gad.source.SourceTools;

/**
 * @see org.iavante.sling.gad.source.SourceTools
 * @scr.component
 * @scr.service interface="org.iavante.sling.gad.source.SourceTools"
 * @scr.property name="service.description" value="Source Tools Impl"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */
public class SourceToolsImpl implements SourceTools, Serializable {
	private static final long serialVersionUID = 1L;

	/** Defines if the source is a schema source or a playlist source. */
	private final String SCHEMATYPE_PROP = "schematype";

	/*
	 * @see org.iavante.sling.gad.source.SourceTools
	 */
	public boolean is_a_playlist_source(Node node) {

		Boolean isPlaylistSource = true;

		try {
			if (node.hasProperty(SCHEMATYPE_PROP)
					&& node.getProperty(SCHEMATYPE_PROP).getValue().getString().equals(
							"schema"))
				isPlaylistSource = false;
		} catch (ValueFormatException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		return isPlaylistSource;
	}
}
