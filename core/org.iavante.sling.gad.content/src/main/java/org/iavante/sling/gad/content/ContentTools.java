/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.content;

import java.util.List;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

/**
 * Commons content tools.
 * 
 * @scr.property name="service.description" value="IAVANTE - Content Tools"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */
public interface ContentTools {

	/**
	 * Returns the public content url.
	 * 
	 * 
	 * @param String
	 *          The current node path
	 * @return url
	 */
	public String getXSPFUrl(String contentPath);
	
	/**
	 * Returns the channel external url
	 * @param channel
	 * @param The transformation format
	 * @return
	 */
	public String getExternalUrl(Node content, String format);		

	
	/**
	 * Returns the public content url.
	 * 
	 * @param req
	 *          The client request
	 * @param node
	 *          The current node
	 * @return url
	 */
	public String get_url(ServletRequest req, Node node)
			throws RepositoryException;

	/**
	 * Returns a valid S3 address with read permissions the bucket is predefined
	 * in system.
	 * 
	 * @param node
	 *          to retrieve media
	 * @return url
	 */
	public String getS3Url(Node node);

	/**
	 * Returns the thumbnail default external url.
	 * 
	 * @param node
	 * @return The external url
	 */
	public String getThumbUrl(Node node);
	
	/**
	 * Returns the External URL for thumbnail number n 
	 * of source. The number n by default is 0.
	 * 
	 * @param node
	 * @param previewNumber
	 * @return The external url
	 */
	public String getThumbPreviewUrl(Node node, int previewNumber);
	
	
	/**
	 * Returns the subtitle external url. If lang is specified,
	 * returns that subtitle, otherwise it returns the default one
	 * (if exists).
	 * 
	 * @param node
	 * @param lang 
	 * @return The external url
	 */
	public String getSubtitleUrl(Node node, String lang);
	
	
	/**
	 * Returns the download url. If 'gad_download' transformation doesn't exit,
	 * checks if exists in catalog. If so, it copies the transformations into the
	 * revision already published. Then return the external url for that 
	 * transformation. 
	 * 
	 * @param node
	 * @return The external url
	 */
	public String getDownloadUrl(Node node);	
	
	
	/**
	 * Returns the audiodescription external url. If lang is specified,
	 * returns that audiodescription, otherwise it returns the default one
	 * (if exists).
	 * 
	 * @param node
	 * @param lang 
	 * @return The external url
	 */
	public String getAudiodescriptionUrl(Node node, String lang);
	
	/**
	 * Returns the original file url. 
	 * @param node	The source node
	 * @return
	 */
	public String getOriginalUrl(Node node);

	/**
	 * Returns the schema.smil with externa urls.
	 * 
	 * @param content
	 * @returns The schema.smil
	 */
	public String getSmil(Node content);

	/**
	 * Replaces the marks in the associated schema with the values of the
	 * determined content.
	 * 
	 * @param content
	 *          is the node to get the associated SMIL
	 * @param storage
	 *          "external" or "internal", where is the storage of the media
	 * @return the SMIL multimedia definition of the content
	 */
	public String getSmil(Node content, String storage);

	/**
	 * Returns the playlist with external storage.
	 * 
	 * @param content
	 *          the content to obtain the playlist
	 * @return playlist
	 */
	public String getPlaylist(Node content);

	/**
	 * Returns the playlist.
	 * 
	 * @param content
	 *          the content to obtain the playlist
	 * @param storage
	 *          "external" or "internal", where is the storage of the media
	 * @return playlist
	 */
	public String getPlaylist(Node content, String storage);

	/**
	 * Returns the XSPF playlist.
	 * 
	 * @param content
	 *          the content to obtain the playlist
	 * @param storage
	 *          "external" or "internal", where is the storage of the media
	 * @return a string with the playlist in xspf format
	 */
	public String getXSPF(Node content, String storage);
	/**
	 * Returns the schema of a node.
	 * 
	 * @param node
	 *          the node to get the schema
	 * @return a string with the schema associated to the content
	 */
	public String getContentSchema(Node node);

	/**
	 * Returns True or False if the node is playlist or not
	 * 
	 * @param contentNode
	 * @return
	 */
	public Boolean isPlaylist(Node contentNode);

	/**
	 * True of false if the content is a valid content. A valid content must have
	 * all its schema sources filled.
	 * 
	 * @param content
	 * @return
	 */
	public Boolean is_valid_content(Node content);
	
	/**
	 * Returns the playlist url
	 *
	 * @param content_path
	 * @return
	 */
	public String getPlaylistUrl(String content_path);
	
	/**
	 * Returns the Url for the endingVideo
	 *
	 * @param Content Node
	 * @return Url
	 */
	public String getEndingVideoUrl(Node content);
	
	/**
	 * Return the Url for the tvicon
	 * @param content
	 * @param storage - 'internal' or 'external'
	 * @return
	 */
	public String getTviconUrl(Node content, String storage);
	
	/**
	 * Returns playlist lines.
	 *  
	 * @param content
	 *          the node to get the sources media line of smil
	 * @param storage
	 *          if the storage is external or internal
	 * @return an ArrayList<String> with the lines of sources in SMIL format
	 */
	public List<String> getPlaylistLines(Node content, String storage);
	/**
	 * Return a Map with the Multimedia Assets.
	 *  
	 * @param content
	 *          the node to get the sources media line of smil
	 * 
	 * @return a Map with the lines of sources for the multimedia assets
	 */
	public Map<String, String> getMultimediaAssets(Node revision);
	/**
	 * Returns the number of votes
	 * @param content
	 * @return
	 */
	public String number_of_votes(Node content);
	
	/**
	 * Returns the sum of all votes
	 * @param content
	 * @return
	 */
	public String number_of_votes_points(Node content);
	
	/**
	 * Returns the space where the node is.
	 * @param Node The node to check
	 * @return The name of the space: collections, catalog or channels
	 * 
	 */
	public String get_node_space(Node node);
	
	/**
	 * Returns the player for this node. The player settings can be defined
	 * with the distribution server config, the player custom distribution
	 * server properties and the request.
	 * @param req The request
	 * @param node The node
	 * @return
	 */
	public String get_node_player(HttpServletRequest req, Node node);
	
}
