/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.content;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.*;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.*;

/**
 * Test content creation and schema association
 */
public class ContentServiceTestIT
		extends TestCase {

	private final String COLLECTIONS_URL = "/content/colecciones/";
	private final String SCHEMA_REL_PATH = "sources/schema.smil";
	private final String CONTENTS_FOLDER = "contents";
	private final String SOURCES_FOLDER = "sources";
	private final String SOURCES_RESOURCE_TYPE = "gad/sources";
	private final String TAGS_FOLDER = "tags";
	private final String TAGS_COUNT_PROP = "count";

	private String location;
	private String title;
	private String schema;
	private String slug_content;
	private String slug_content_dos;
	private String slug_collection;

	private String tag1_input;
	private String tag2_input;
	private String tag1_title;
	private String tag2_title;
	private String tag1_slug;
	private String tag2_slug;

	private Credentials defaultcreds;
	private List authPrefs = new ArrayList(2);

	private final String HOSTVAR = "SLINGHOST";
	private final String HOSTPREDEF = "localhost:8888";
	private String SLING_URL = "http://";

	HttpClient client;

	protected void setUp() {

		Map<String, String> envs = System.getenv();
		Set<String> keys = envs.keySet();

		Iterator<String> it = keys.iterator();
		boolean hashost = false;
		while (it.hasNext()) {
			String key = (String) it.next();

			if (key.compareTo(HOSTVAR) == 0) {
				SLING_URL = SLING_URL + (String) envs.get(key);
				hashost = true;
			}
		}

		if (hashost == false)
			SLING_URL = SLING_URL + HOSTPREDEF;

		client = new HttpClient();
		title = "Test case content";
		schema = "default";
		slug_collection = "admin";
		slug_content = "it_content";
		slug_content_dos = "it_content_dos";

		tag1_input = "AEIOU";
		tag2_input = "Recetas Equilibradas";

		tag1_title = "aeiou";
		tag2_title = "recetas equilibradas";

		tag1_slug = "aeiou";
		tag2_slug = "recetas-equilibradas";

		authPrefs.add(AuthPolicy.DIGEST);
		authPrefs.add(AuthPolicy.BASIC);
		defaultcreds = new UsernamePasswordCredentials("admin", "admin");

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		// Create collection
		PostMethod post_create_col = new PostMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection);

		post_create_col.setDoAuthentication(true);

		NameValuePair[] data_create_col = {
				new NameValuePair("sling:resourceType", "gad/collection"),
				new NameValuePair("title", title), new NameValuePair("subtitle", ""),
				new NameValuePair("schema", schema),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };

		post_create_col.setRequestBody(data_create_col);

		try {
			client.executeMethod(post_create_col);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		post_create_col.releaseConnection();

		// Create content in collection
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		PostMethod post_create = new PostMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content);
		post_create.setDoAuthentication(true);

		NameValuePair[] data_create = {
				new NameValuePair("sling:resourceType", "gad/content"),
				new NameValuePair("title", title),
				new NameValuePair("schema", schema),
				new NameValuePair(
						"description",
						"Content description generated by test case. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."),
				new NameValuePair("author", "Test case"),
				new NameValuePair("origin", "Test case"),
				new NameValuePair("lang", "es"), new NameValuePair("tags", tag1_input),
				new NameValuePair("tags", tag2_input),
				new NameValuePair("tags@TypeHint", "String[]"),
				new NameValuePair("state", "pending"),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", ""),
				new NameValuePair("_charset_", "utf-8") };

		post_create.setRequestBody(data_create);
		try {
			client.executeMethod(post_create);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		post_create.releaseConnection();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Edit the content
		PostMethod post_edit = new PostMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content);

		post_edit.setDoAuthentication(true);

		NameValuePair[] data_edit = {
				new NameValuePair("sling:resourceType", "gad/content"),
				new NameValuePair("title", title),
				new NameValuePair("schema", schema),
				new NameValuePair(
						"description",
						"Edited  aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."),
				new NameValuePair("author", "Test case"),
				new NameValuePair("origin", "Test case"),
				new NameValuePair("lang", "es"), new NameValuePair("state", "pending"),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", ""),
				new NameValuePair("_charset_", "utf-8") };

		post_edit.setRequestBody(data_edit);
		// post.setDoAuthentication(true);
		try {
			client.executeMethod(post_edit);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		// Get the content
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		post_edit.releaseConnection();

		// Edit the content again
		post_edit.setRequestBody(data_edit);

		try {
			client.executeMethod(post_edit);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		// Get the content
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		post_edit.releaseConnection();
	}

	protected void tearDown() {

		// Delete the content
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		PostMethod post_delete = new PostMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection);
		NameValuePair[] data_delete = { new NameValuePair(":operation", "delete"), };

		post_delete.setDoAuthentication(true);
		post_delete.setRequestBody(data_delete);

		try {
			client.executeMethod(post_delete);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertEquals(post_delete.getStatusCode(), 200);

		post_delete.releaseConnection();
	}

	public void test_getContent() {

		// Get the content

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		HttpMethod get_content = new GetMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content + "/"
				+ "title");
		try {
			this.client.executeMethod(get_content);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		// handle response.
		String response_body = "";
		try {
			response_body = get_content.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertEquals(response_body, title);
		get_content.releaseConnection();
	}

	public void test_get_content_inmediatly() {

		// Get the content inmediatly
		PostMethod post_create = new PostMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content_dos);
		post_create.setDoAuthentication(true);

		NameValuePair[] data_create = {
				new NameValuePair("sling:resourceType", "gad/content"),
				new NameValuePair("title", title),
				new NameValuePair("schema", schema),
				new NameValuePair(
						"description",
						"Content description generated by test case. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."),
				new NameValuePair("author", "Test case"),
				new NameValuePair("origin", "Test case"),
				new NameValuePair("lang", "es"), new NameValuePair("tags", tag1_input),
				new NameValuePair("tags", tag2_input),
				new NameValuePair("tags@TypeHint", "String[]"),
				new NameValuePair("state", "pending"),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", ""),
				new NameValuePair("_charset_", "utf-8") };

		post_create.setRequestBody(data_create);
		try {
			client.executeMethod(post_create);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		post_create.releaseConnection();

		HttpMethod get_content = new GetMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content_dos);

		try {
			this.client.executeMethod(get_content);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertEquals(get_content.getStatusCode(), 200);
		get_content.releaseConnection();
	}

	public void test_get_content_finish_prop() {

		// Get the content inmediatly
		PostMethod post_create = new PostMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content_dos);
		post_create.setDoAuthentication(true);

		NameValuePair[] data_create = {
				new NameValuePair("sling:resourceType", "gad/content"),
				new NameValuePair("title", title),
				new NameValuePair("schema", schema),
				new NameValuePair(
						"description",
						"Content description generated by test case. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."),
				new NameValuePair("author", "Test case"),
				new NameValuePair("origin", "Test case"),
				new NameValuePair("lang", "es"), new NameValuePair("tags", tag1_input),
				new NameValuePair("tags", tag2_input),
				new NameValuePair("tags@TypeHint", "String[]"),
				new NameValuePair("state", "pending"),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", ""),
				new NameValuePair("_charset_", "utf-8") };

		post_create.setRequestBody(data_create);

		try {
			client.executeMethod(post_create);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		post_create.releaseConnection();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		HttpMethod get_content = new GetMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content_dos);

		try {
			this.client.executeMethod(get_content);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		String response_body = "";
		try {
			response_body = get_content.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertTrue(response_body.contains("<finish>1"));
		get_content.releaseConnection();
	}

	public void test_get_embed() {

		// Get the content inmediatly
		PostMethod post_create = new PostMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content_dos);
		post_create.setDoAuthentication(true);

		NameValuePair[] data_create = {
				new NameValuePair("sling:resourceType", "gad/content"),
				new NameValuePair("title", title),
				new NameValuePair("schema", "playlist"),
				new NameValuePair(
						"description",
						"Content description generated by test case. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."),
				new NameValuePair("author", "Test case"),
				new NameValuePair("origin", "Test case"),
				new NameValuePair("lang", "es"), new NameValuePair("tags", tag1_input),
				new NameValuePair("tags", tag2_input),
				new NameValuePair("tags@TypeHint", "String[]"),
				new NameValuePair("state", "pending"),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", ""),
				new NameValuePair("_charset_", "utf-8") };

		post_create.setRequestBody(data_create);

		try {
			client.executeMethod(post_create);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		post_create.releaseConnection();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		HttpMethod get_content = new GetMethod(
				SLING_URL
						+ COLLECTIONS_URL
						+ slug_collection
						+ "/"
						+ CONTENTS_FOLDER
						+ "/"
						+ slug_content_dos
						+ "/?player_width=1000&player_height=200&player_playlist_layout=left&player_plugins=test-1,test-2&player_autostart=true&player_repeat=none");

		try {
			this.client.executeMethod(get_content);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		String response_body = "";
		try {
			response_body = get_content.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertTrue(response_body.contains("<embed>"));
		assertTrue(response_body.contains("</embed>"));
		assertTrue(response_body.contains("width='1000'"));
		assertTrue(response_body.contains("height='200'"));
		assertTrue(response_body.contains("playlist=left"));
		assertTrue(response_body.contains("plugins=test-1,test-2"));
		assertTrue(response_body.contains("autostart=true"));
		assertTrue(response_body.contains("repeat=none"));
		get_content.releaseConnection();
	}

	public void test_getSourcesFolder() {

		// Get the content
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		HttpMethod get_content = new GetMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content + "/"
				+ SOURCES_FOLDER + "/sling:resourceType");
		try {
			this.client.executeMethod(get_content);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		// handle response.
		String response_body = "";
		try {
			response_body = get_content.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertEquals(response_body, SOURCES_RESOURCE_TYPE);

		get_content.releaseConnection();
	}

	public void test_getVersion() {

		// Get the content
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		String version = "1.1";
		// Get the content
		HttpMethod get_content_version = new GetMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content + "/"
				+ "version");
		try {
			this.client.executeMethod(get_content_version);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		// handle response.
		String response_body = "";
		try {
			response_body = get_content_version.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertEquals(response_body, version);

		get_content_version.releaseConnection();
	}

	public void test_getContentHits() {

		// Get the content hits property
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		HttpMethod get_content = new GetMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content + "/"
				+ "hits");
		try {
			this.client.executeMethod(get_content);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		// Get the content hits property
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		String response_body = null;
		try {
			response_body = get_content.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertEquals(response_body, "0");

		get_content.releaseConnection();

		get_content = new GetMethod(SLING_URL + COLLECTIONS_URL + slug_collection
				+ "/" + CONTENTS_FOLDER + "/" + slug_content + "/");
		try {
			this.client.executeMethod(get_content);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		// Get the content hits property
		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		get_content.releaseConnection();

		get_content = new GetMethod(SLING_URL + COLLECTIONS_URL + slug_collection
				+ "/" + CONTENTS_FOLDER + "/" + slug_content + "/" + "hits");
		try {
			this.client.executeMethod(get_content);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		response_body = null;
		try {
			response_body = get_content.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertEquals(response_body, "1");

		get_content.releaseConnection();
	}

	public void test_getContentGadUuid() {

		// Get the content gaduuid property
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		HttpMethod get_content = new GetMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content + "/"
				+ "gaduuid");
		try {
			this.client.executeMethod(get_content);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertEquals(get_content.getStatusCode(), 200);
		get_content.releaseConnection();
	}

	public void test_getSMIL() {

		// Get the content
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}
		HttpMethod get_content = new GetMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content
				+ ".smil");
		try {
			this.client.executeMethod(get_content);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		// handle response.
		String response_body = "";
		try {
			response_body = get_content.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assert response_body
				.indexOf("<meta name=\"title\" content=\"Test case content\"/>") != -1;
		assert response_body
				.indexOf("<meta name=\"author\" content=\"Test case\"/>") != -1;
		assert response_body.indexOf("<video ") != -1;

		get_content.releaseConnection();
	}

	public void test_getPlaylist() {

		// Get the content
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}
		HttpMethod get_content = new GetMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content
				+ ".playlist");
		try {
			this.client.executeMethod(get_content);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		// handle response.
		String response_body = "";
		try {
			response_body = get_content.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assert response_body
				.indexOf("<meta name=\"title\" content=\"Test case content\"/>") != -1;
		assert response_body
				.indexOf("<meta name=\"author\" content=\"Test case\"/>") != -1;

		get_content.releaseConnection();
	}

	/**
	 * Tests for tags postprocessing
	 */
	public void test_getTagsCollection() {

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Get the collection tags
		HttpMethod get_col_tag1 = new GetMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + TAGS_FOLDER + "/" + tag1_slug + "/title");
		try {
			this.client.executeMethod(get_col_tag1);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		// handle response.
		String response_body = "";
		try {
			response_body = get_col_tag1.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertEquals(response_body, tag1_title);
		get_col_tag1.releaseConnection();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}
		// Get the collection tags
		HttpMethod get_col_tag2 = new GetMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + TAGS_FOLDER + "/" + tag2_slug + "/title");
		try {
			this.client.executeMethod(get_col_tag2);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		// handle response.
		try {
			response_body = get_col_tag2.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertEquals(response_body, tag2_title);
		get_col_tag2.releaseConnection();
	}

	/**
	 * Tests path property
	 */
	public void test_getPathProp() {

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Get path property
		String path_response = COLLECTIONS_URL + slug_collection + "/"
				+ CONTENTS_FOLDER + "/" + slug_content;
		String path_prop = "jcr_path";

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}
		// Get the content
		HttpMethod get_content_path = new GetMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content + "/"
				+ path_prop);
		try {
			this.client.executeMethod(get_content_path);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		// handle response.
		String response_body = "";
		try {
			response_body = get_content_path.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertEquals(response_body, path_response);
		get_content_path.releaseConnection();
	}

	/**
	 * Tests path property
	 */
	public void test_getLogNode() {

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		String log_node = "log";
		String log_resourceType = "gad/log";

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Get the log node
		HttpMethod get_content_log = new GetMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content + "/"
				+ log_node + "/sling:resourceType");
		try {
			this.client.executeMethod(get_content_log);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		// handle response.
		String response_body = "";
		try {
			response_body = get_content_log.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertEquals(response_body, log_resourceType);
		get_content_log.releaseConnection();

		// Get the url property
		HttpMethod get_content_log_url = new GetMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content + "/"
				+ log_node + "/url");
		try {
			this.client.executeMethod(get_content_log_url);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		// handle response.
		response_body = "";
		try {
			response_body = get_content_log_url.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		String log_url = COLLECTIONS_URL + slug_collection + "/" + CONTENTS_FOLDER
				+ "/" + slug_content;

		assertEquals(response_body, log_url);
		get_content_log_url.releaseConnection();

	}

	/**
	 * Test for content postprocesing. Associate schema
	 */
	public void test_sourceAssociation() {

		// Check sources content type created from schema
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}
		HttpMethod get_source = new GetMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content + "/"
				+ SCHEMA_REL_PATH);
		try {
			client.executeMethod(get_source);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		assertEquals(get_source.getStatusCode(), 200);

		get_source.releaseConnection();
	}

	/**
	 * Test the validated property
	 */
	public void test_content_validation() {

		String validated = "0";
		String response_body = "";

		String source_video = "fuente_default";
		String source_image = "thumbnail_default";

		String file_video = "test.avi";
		String mime_video = "video/avi";

		String file_image = "test.png";
		String mime_image = "image/png";

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		HttpMethod get_valid_prop = new GetMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content
				+ "/validated");
		try {
			client.executeMethod(get_valid_prop);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		try {
			response_body = get_valid_prop.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertEquals(response_body, validated);
		get_valid_prop.releaseConnection();

		// Edit source default
		PostMethod post_edit = new PostMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content + "/"
				+ SOURCES_FOLDER + "/" + source_video);
		post_edit.setDoAuthentication(true);

		NameValuePair[] data_edit = {
				new NameValuePair("sling:resourceType", "gad/source"),
				new NameValuePair("file", file_video),
				new NameValuePair("mimetype", mime_video),
				new NameValuePair("text_encoding", ""), new NameValuePair("lang", ""),
				new NameValuePair("length", ""), new NameValuePair("size", ""),
				new NameValuePair("type", ""), new NameValuePair("bitrate", ""),
				new NameValuePair("tracks_number", ""),
				new NameValuePair("track_1_type", ""),
				new NameValuePair("track_1_encoding", ""),
				new NameValuePair("track_1_features", ""),
				new NameValuePair("track_2_type", ""),
				new NameValuePair("track_2_encoding", ""),
				new NameValuePair("track_2_features", ""),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };
		post_edit.setRequestBody(data_edit);
		post_edit.setDoAuthentication(true);
		try {
			client.executeMethod(post_edit);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals(200, post_edit.getStatusCode());
		post_edit.releaseConnection();

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Edit thumbnail default
		post_edit = null;
		post_edit = new PostMethod(SLING_URL + COLLECTIONS_URL + slug_collection
				+ "/" + CONTENTS_FOLDER + "/" + slug_content + "/" + SOURCES_FOLDER
				+ "/" + source_image);
		post_edit.setDoAuthentication(true);

		NameValuePair[] data_edit_image = {
				new NameValuePair("sling:resourceType", "gad/source"),
				new NameValuePair("file", file_image),
				new NameValuePair("mimetype", mime_image),
				new NameValuePair("text_encoding", ""), new NameValuePair("lang", ""),
				new NameValuePair("length", ""), new NameValuePair("size", ""),
				new NameValuePair("type", ""), new NameValuePair("bitrate", ""),
				new NameValuePair("tracks_number", ""),
				new NameValuePair("track_1_type", ""),
				new NameValuePair("track_1_encoding", ""),
				new NameValuePair("track_1_features", ""),
				new NameValuePair("track_2_type", ""),
				new NameValuePair("track_2_encoding", ""),
				new NameValuePair("track_2_features", ""),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };
		post_edit.setRequestBody(data_edit_image);
		post_edit.setDoAuthentication(true);
		try {
			client.executeMethod(post_edit);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals(200, post_edit.getStatusCode());
		post_edit.releaseConnection();

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Test if the content is already validated
		validated = "1";
		get_valid_prop = new GetMethod(SLING_URL + COLLECTIONS_URL
				+ slug_collection + "/" + CONTENTS_FOLDER + "/" + slug_content
				+ "/validated");
		try {
			client.executeMethod(get_valid_prop);
		} catch (HttpException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}

		try {
			response_body = get_valid_prop.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertEquals(response_body, validated);
		get_valid_prop.releaseConnection();
	}
}
