/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.content;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Calendar;

import javax.jcr.ItemExistsException;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.nodetype.NoSuchNodeTypeException;
import javax.jcr.version.VersionException;

import org.apache.sling.commons.testing.jcr.RepositoryTestBase;
import org.apache.sling.jcr.api.SlingRepository;

import org.iavante.sling.gad.content.impl.ContentToolsImpl;

public class ContentToolsTest extends RepositoryTestBase {
	
	ContentToolsImpl content_tools;
	SlingRepository repo;
	Session session;
	
	private Node root_node;
	private Node sources_node;
	private Node content_node;
	
	private final String resources_path = "src/test/resources/";
	private final String SCHEMA_NODE_NAME = "schema.smil";
	
	private String schema_file_name;
	private File schema_file;

    @Override
    protected void setUp() throws Exception {
    	
    	super.setUp();
    	session = getSession();    	
    	
    	String[] prefixes = session.getWorkspace().getNamespaceRegistry().getPrefixes();
    	
    	boolean registered = false;
    	
    	for (int i=0; i< prefixes.length; i++) {
    		if (prefixes[i].equals("sling")) registered = true;    		
    	}
    	
    	if (!registered) session.getWorkspace().getNamespaceRegistry().registerNamespace("sling", "http://www.iavante.es/wiki/1.0");
    	
        root_node = getSession().getRootNode(); 
        
        // Add content node
        content_node = root_node.addNode("test" +  System.currentTimeMillis(), "nt:unstructured");
        root_node.save();        
        
        sources_node = content_node.addNode("sources", "nt:unstructured");
        
        // Save
        root_node.save();	     
        session.save();     
        
        content_tools = new ContentToolsImpl();
    }	
    
	@Override	
    protected void tearDown() throws Exception {
        super.tearDown();        
    }
    
	/*
	 * Test if is a valid default content
	 */
    public void test_is_valid_content_default() throws ItemExistsException, PathNotFoundException, NoSuchNodeTypeException, LockException, VersionException, ConstraintViolationException, RepositoryException, FileNotFoundException {
    	
    	schema_file_name = "default.smil";
    	schema_file = new File(resources_path + schema_file_name);
    	
        // Add schema file
        Node schema_node = sources_node.addNode (SCHEMA_NODE_NAME, "nt:file");
        
        //create the mandatory child node - jcr:content
        Node resNode = schema_node.addNode ("jcr:content", "nt:resource");
        resNode.setProperty ("jcr:mimeType", "text/xml");
        resNode.setProperty ("jcr:encoding", "utf-8");
        resNode.setProperty ("jcr:data", new FileInputStream (schema_file));
        Calendar lastModified = Calendar.getInstance ();
        lastModified.setTimeInMillis (schema_file.lastModified ());
        resNode.setProperty ("jcr:lastModified", lastModified);        
        
        sources_node.save();   
        
    	String file_name = "test.avi";
    	
    	sources_node.addNode("not_a_source", "nt:unstructured");
    	sources_node.save();

    	assertTrue(!content_tools.is_valid_content(content_node));    	
    	
    	Node source = sources_node.addNode("fuente_default", "nt:unstructured");
    	sources_node.save();
    	
    	source.setProperty("sling:resourceType", "gad/source");
    	source.save();
    	
    	assertTrue(!content_tools.is_valid_content(content_node));
    	source.setProperty("file", file_name);
    	source.save();
    	
    	assertTrue(!content_tools.is_valid_content(content_node));
    	
    	source = sources_node.addNode("subtitle_default", "nt:unstructured");
    	sources_node.save();
    	
    	source.setProperty("sling:resourceType", "gad/source");
    	source.save();
    	
    	assertTrue(!content_tools.is_valid_content(content_node));
    	source.setProperty("file", file_name);
    	source.save();
    	
    	assertTrue(!content_tools.is_valid_content(content_node));
    	
    	source = sources_node.addNode("thumbnail_default", "nt:unstructured");
    	sources_node.save();
    	
    	source.setProperty("sling:resourceType", "gad/source");
    	source.save();
    	
    	assertTrue(!content_tools.is_valid_content(content_node));
    	source.setProperty("file", file_name);
    	source.save();    
    	
    	assertTrue(content_tools.is_valid_content(content_node));
    }
    
    /* 
     * Test if it is a valid playlist content
     */
    public void test_is_valid_content_playlist() throws ItemExistsException, PathNotFoundException, NoSuchNodeTypeException, LockException, VersionException, ConstraintViolationException, RepositoryException, FileNotFoundException {
    	
    	schema_file_name = "playlist.smil";
    	schema_file = new File(resources_path + schema_file_name);
    	
        // Add schema file
        Node schema_node = sources_node.addNode (SCHEMA_NODE_NAME, "nt:file");
        
        //create the mandatory child node - jcr:content
        Node resNode = schema_node.addNode ("jcr:content", "nt:resource");
        resNode.setProperty ("jcr:mimeType", "text/xml");
        resNode.setProperty ("jcr:encoding", "utf-8");
        resNode.setProperty ("jcr:data", new FileInputStream (schema_file));
        Calendar lastModified = Calendar.getInstance ();
        lastModified.setTimeInMillis (schema_file.lastModified ());
        resNode.setProperty ("jcr:lastModified", lastModified);        
        
        sources_node.save();   
        
    	String file_name = "test.avi";
    	
    	sources_node.addNode("not_a_source", "nt:unstructured");
    	sources_node.save();
    	
    	System.out.println("--> Content node en test: " + content_node.getPath());
    	assertTrue(!content_tools.is_valid_content(content_node));    	
    	
    	Node source = sources_node.addNode("fuente_default", "nt:unstructured");
    	sources_node.save();
    	
    	source.setProperty("sling:resourceType", "gad/source");
    	source.save();
    	
    	assertTrue(!content_tools.is_valid_content(content_node));
    	source.setProperty("file", file_name);
    	source.save();
    	
    	assertTrue(!content_tools.is_valid_content(content_node));
    	
    	source = sources_node.addNode("thumbnail_default", "nt:unstructured");
    	sources_node.save();
    	
    	source.setProperty("sling:resourceType", "gad/source");
    	source.save();
    	
    	assertTrue(!content_tools.is_valid_content(content_node));
    	source.setProperty("file", file_name);
    	source.save();    
    	
    	assertTrue(content_tools.is_valid_content(content_node));
    }
}
