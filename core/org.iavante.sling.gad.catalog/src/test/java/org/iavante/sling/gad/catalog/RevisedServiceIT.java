/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.catalog;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthPolicy;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;

import junit.framework.TestCase;

public class RevisedServiceIT extends TestCase{
	
	private String COL_URL = "/content/colecciones/";
	private String CATALOG_URL = "/content/catalogo/";
	private String SCHEMA_REL_PATH = "sources/schema.smil";
	private String CONTENTS_FOLDER = "contents";
	private String SOURCES_FOLDER = "sources";
	private String PENDING_FOLDER = "pendientes";
	private String TAGS_FOLDER = "tags";
	private String LOG_FOLDER = "log";
	private String REVISED_FOLDER = "revisados";
	private String TAGS_RESOURCE_TYPE = "gad/tags";
	
	private String col_title;
	private String content_title;
	private String source_title;
	private String file;
	private String mimetype;
	private String location;
	private String title;
	private String schema;
	private String slug_content;
	private String slug_collection;
	private String tags_request;
	private String tags_response;
	
	private String tag1_input;
	private String tag2_input;
	private String tag1_title;
	private String tag2_title;
	private String tag1_slug;
	private String tag2_slug;

	
	
	private Credentials defaultcreds;
	
	private final String HOSTVAR = "SLINGHOST";
  private final String HOSTPREDEF = "localhost:8888";
  private String SLING_URL = "http://";
	private List authPrefs;
	
	HttpClient client;
	
	
	protected void setUp() {	
		
		Map<String,String> envs = System.getenv();
		Set<String> keys = envs.keySet();
		
	    Iterator<String> it = keys.iterator();
	    boolean hashost = false;
	    while (it.hasNext()){
	      String key = (String) it.next();
	      
	      if (key.compareTo(HOSTVAR) == 0 ){
	    	  SLING_URL = SLING_URL + (String) envs.get(key);
	    	  hashost = true;
	      }
	    }	    
		if (hashost == false)
			SLING_URL = SLING_URL + HOSTPREDEF;
		
		client = new HttpClient();
		col_title = "it_col";
		content_title = "it_content";
		source_title = "it_source";
		file = "it.flv";
		mimetype = "video/flv";
		schema = "default";
   	title = "Test case content";
   	schema = "default";
   	slug_collection = "admin";
   	slug_content = "it_content";
   	tags_request = "    test,     case";
   	tags_response = "test,case";
   	
   	tag1_input = "AEIOU";
 		tag2_input = "Recetas Equilibradas";

 		tag1_title = "aeiou";
 		tag2_title = "recetas equilibradas";

 		tag1_slug = "aeiou";
 		tag2_slug = "recetas-equilibradas";

   	defaultcreds = new UsernamePasswordCredentials("admin", "admin");
   	 	
		authPrefs = new ArrayList(2);   	 	
   	authPrefs.add(AuthPolicy.DIGEST);
   	authPrefs.add(AuthPolicy.BASIC);
  	 	
   	client.getParams().setAuthenticationPreemptive(true);
   	client.getState().setCredentials(AuthScope.ANY, defaultcreds);
   	client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);   	
   	 	
   	// Delete the collection
		System.out.println("Delete collection: " + SLING_URL + COL_URL + col_title);
		PostMethod post_delete = new PostMethod(SLING_URL + COL_URL + col_title);		
		NameValuePair[] data_delete = {
		           new NameValuePair(":operation", "delete"),		             
		         };
		
   	post_delete.setDoAuthentication(true);
		post_delete.setRequestBody(data_delete);


        try {
 			client.executeMethod(post_delete);
 		} catch (HttpException e1) {
 			e1.printStackTrace();
 		} catch (IOException e1) { 		
 			e1.printStackTrace();
 		} 		
 		post_delete.releaseConnection();
   	 	
   	// Create collection   	 	
   	System.out.println("Create collection");
   	PostMethod post_create_col = new PostMethod(SLING_URL + COL_URL + col_title);	    	 
	 	post_create_col.setDoAuthentication(true);    	 
	 
	 	NameValuePair[] data_create_col = {
	            new NameValuePair("sling:resourceType", "gad/collection"),
	            new NameValuePair("title", col_title),
	            new NameValuePair("schema", schema),
	            new NameValuePair("subtitle", ""),
	            new NameValuePair("extern_storage", "on"),
	            new NameValuePair("picture", ""),
	            new NameValuePair("jcr:created", ""),
	            new NameValuePair("jcr:createdBy", ""),
	            new NameValuePair("jcr:lastModified", ""),
	            new NameValuePair("jcr:lastModifiedBy", "")          
	 	};
	 	post_create_col.setRequestBody(data_create_col);
	 	//post.setDoAuthentication(true);
	 	try {
			client.executeMethod(post_create_col);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) { 		
			e1.printStackTrace();
		}
		
		// Collection created
		assertEquals(post_create_col.getStatusCode(), 201);
		post_create_col.releaseConnection();
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		// Create content in collection	
		System.out.println("Create content in collection");
		PostMethod post_create_content = new PostMethod(SLING_URL + COL_URL + col_title + "/" + CONTENTS_FOLDER + "/" + content_title);	    	 	
   	 	post_create_content.setDoAuthentication(true);    	 
  	 
        NameValuePair[] data_create_content = {
          new NameValuePair("sling:resourceType", "gad/content"),
          new NameValuePair("title", content_title),
          new NameValuePair("schema", schema),
          new NameValuePair("description", "Content description generated by test case. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."),
          new NameValuePair("author", "Test case"),
          new NameValuePair("origin", "Test case"),
          new NameValuePair("lang", "es"),
  				new NameValuePair("tags", tag1_input),
  				new NameValuePair("tags", tag2_input),
  				new NameValuePair("tags@TypeHint", "String[]"),
          new NameValuePair("state", "pending"),
          new NameValuePair("jcr:created", ""),
          new NameValuePair("jcr:createdBy", ""),
          new NameValuePair("jcr:lastModified", ""),
          new NameValuePair("jcr:lastModifiedBy", "")            
        };
        post_create_content.setRequestBody(data_create_content);
        //post.setDoAuthentication(true);
        try {
			client.executeMethod(post_create_content);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) { 		
			e1.printStackTrace();
		}
		
		// Content created
		assertEquals(post_create_content.getStatusCode(), 201);
		post_create_content.releaseConnection();    
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		// Create the source
   	System.out.println("Create source in content");
		PostMethod post_create = new PostMethod(SLING_URL + COL_URL + col_title + "/" + CONTENTS_FOLDER + "/" + content_title + "/" + SOURCES_FOLDER + "/" + source_title);   
   	System.out.println("Request: " + SLING_URL + COL_URL + col_title + "/" + CONTENTS_FOLDER + "/" + content_title + "/" + SOURCES_FOLDER + "/" + source_title);
   	post_create.setDoAuthentication(true);    	 
    NameValuePair[] data_create = {        		 
         		 	new NameValuePair("sling:resourceType", "gad/source"),
      	 			new NameValuePair("title", source_title),
      	 			new NameValuePair("file", ""),
      	 			new NameValuePair("mimetype", ""),
      	 			new NameValuePair("text_encoding", ""),
      	 			new NameValuePair("lang", ""),
      	 			new NameValuePair("length", ""),
      	 			new NameValuePair("size", ""),
      	 			new NameValuePair("type", ""),
      	 			new NameValuePair("bitrate", ""),
      	 		  new NameValuePair("tags", ""),
      	 			new NameValuePair("tracks_number", ""),
      	 			new NameValuePair("track_1_type", ""),
      	 			new NameValuePair("track_1_encoding", ""),
      	 			new NameValuePair("track_1_features", ""),
      	 			new NameValuePair("track_2_type", ""),
      	 			new NameValuePair("track_2_encoding", ""),
      	 			new NameValuePair("track_2_features", ""),   	
      	 			new NameValuePair("jcr:created", ""),
      	 			new NameValuePair("jcr:createdBy", ""),
      	 			new NameValuePair("jcr:lastModified", ""),
      	 			new NameValuePair("jcr:lastModifiedBy", "")            
        };
        post_create.setRequestBody(data_create);
        //post.setDoAuthentication(true);
        try {
			client.executeMethod(post_create);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) { 		
			e1.printStackTrace();
		}
		
		assertEquals(post_create.getStatusCode(), 201);
		post_create.releaseConnection();
		
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}		
		
		// Send the content to revision
		System.out.println("Send the content to revision");
		PostMethod post_send_to_revision = new PostMethod(SLING_URL + COL_URL + col_title + "/" + CONTENTS_FOLDER + "/" + content_title); 
		String catalog_pending_url = CATALOG_URL + PENDING_FOLDER + "/";
   	 	System.out.println("Catalog url: " + catalog_pending_url);
		System.out.println("Send to revision: " + SLING_URL + COL_URL + col_title + "/" + CONTENTS_FOLDER + "/" + content_title);
   	 	post_send_to_revision.setDoAuthentication(true);    	 
        
   	 	NameValuePair[] data_send_to_revision = {        		 
       		 		new NameValuePair(":operation", "copy"),
      	 			new NameValuePair(":dest", catalog_pending_url),
      	 			new NameValuePair("replace", "true")         
        };
        post_send_to_revision.setRequestBody(data_send_to_revision);
        try {
			client.executeMethod(post_send_to_revision);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) { 		
			e1.printStackTrace();
		}

		assertEquals(post_send_to_revision.getStatusCode(), 201);
		post_send_to_revision.releaseConnection();
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		// Revise the content
		System.out.println("Revise the content");
		PostMethod post_revise_content = new PostMethod(SLING_URL + CATALOG_URL + PENDING_FOLDER + "/" + content_title); 
		String catalog_revised_url = CATALOG_URL + REVISED_FOLDER + "/";
		System.out.println("Revise content: " + SLING_URL + CATALOG_URL + PENDING_FOLDER + "/" + content_title);
   	 	post_send_to_revision.setDoAuthentication(true);    	 
        
   	 	NameValuePair[] data_revise_content = {        		 
       		 		new NameValuePair(":operation", "move"),
      	 			new NameValuePair(":dest", catalog_revised_url),
      	 			new NameValuePair("replace", "true")         
        };
   	 	post_revise_content.setRequestBody(data_revise_content);
        try {
			client.executeMethod(post_revise_content);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) { 		
			e1.printStackTrace();
		}

		assertEquals(post_revise_content.getStatusCode(), 201);
		post_revise_content.releaseConnection();
    }
	
	
    protected void tearDown() {
    	
    	try {
			Thread.sleep(9000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
    	
    	// Delete the collection
    	System.out.println("Deleting collection");
		PostMethod post_delete = new PostMethod(SLING_URL + COL_URL + col_title);		
		NameValuePair[] data_delete = {
		           new NameValuePair(":operation", "delete"),		             
		         };
		
   	 	post_delete.setDoAuthentication(true);
		post_delete.setRequestBody(data_delete);


        try {
 			client.executeMethod(post_delete);
 		} catch (HttpException e1) {
 			e1.printStackTrace();
 		} catch (IOException e1) { 		
 			e1.printStackTrace();
 		} 		
 		assertEquals(post_delete.getStatusCode(), 200);
 		post_delete.releaseConnection();
 		
 		// Delete revision
		PostMethod post_delete_rev = new PostMethod(SLING_URL + CATALOG_URL + REVISED_FOLDER + "/" + content_title);		
		NameValuePair[] data_delete_rev = {
		           new NameValuePair(":operation", "delete"),		             
		         };
		
   	post_delete_rev.setDoAuthentication(true);
		post_delete_rev.setRequestBody(data_delete_rev);


        try {
 			client.executeMethod(post_delete_rev);
 		} catch (HttpException e1) {
 			e1.printStackTrace();
 		} catch (IOException e1) { 		
 			e1.printStackTrace();
 		} 		
 		assertEquals(post_delete_rev.getStatusCode(), 200);
 		post_delete_rev.releaseConnection();
 		
 		// Delete tags
		PostMethod post_delete_tag1 = new PostMethod(SLING_URL + CATALOG_URL + TAGS_FOLDER + "/" + tag1_slug);		
		NameValuePair[] data_delete_tag1 = {
		           new NameValuePair(":operation", "delete"),		             
		         };
		
   	post_delete_tag1.setDoAuthentication(true);
		post_delete_tag1.setRequestBody(data_delete_tag1);


        try {
 			client.executeMethod(post_delete_tag1);
 		} catch (HttpException e1) {
 			e1.printStackTrace();
 		} catch (IOException e1) { 		
 			e1.printStackTrace();
 		} 		
 		assertEquals(post_delete_tag1.getStatusCode(), 200);
 		post_delete_tag1.releaseConnection();
 		
		PostMethod post_delete_tag2 = new PostMethod(SLING_URL + CATALOG_URL + TAGS_FOLDER + "/" + tag2_slug);		
		NameValuePair[] data_delete_tag2 = {
		           new NameValuePair(":operation", "delete"),		             
		         };
		
   	post_delete_tag2.setDoAuthentication(true);
		post_delete_tag2.setRequestBody(data_delete_tag2);


        try {
 			client.executeMethod(post_delete_tag2);
 		} catch (HttpException e1) {
 			e1.printStackTrace();
 		} catch (IOException e1) { 		
 			e1.printStackTrace();
 		} 		
 		assertEquals(post_delete_tag2.getStatusCode(), 200);
 		post_delete_tag2.releaseConnection();
    }   
	
    
    public void test_revision_created() {
    	
    	System.out.println("TEST Revision created");
    	String revision_resource_type = "gad/revision";
    	
    	try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
   	 	
    	// Get source test not present in schema 	
		HttpMethod get_revision = new GetMethod(SLING_URL + CATALOG_URL + REVISED_FOLDER + "/" + content_title + "/sling:resourceType");
		System.out.println(SLING_URL + CATALOG_URL + REVISED_FOLDER + "/" + content_title);
		try {
			client.executeMethod(get_revision);
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
		// handle response.	
		String res_resource_type = "";
		try {
			res_resource_type = get_revision.getResponseBodyAsString();
		} catch (IOException e) {
			e.printStackTrace();
		}	
		
		System.out.println("Response: " + res_resource_type);
		
		assertEquals(res_resource_type, revision_resource_type);
		get_revision.releaseConnection();   	
    }    
    
    public void test_get_ref_content_state() {
    	
    	try {
			Thread.sleep(8000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
    	System.out.println("TEST content state updated");
		String pending_state = "revised";
		HttpMethod get_ref_content_state = new GetMethod(SLING_URL + COL_URL + col_title + "/" + CONTENTS_FOLDER + "/" + content_title + "/" + LOG_FOLDER + "/state");
		System.out.println("Get ref content status: " + SLING_URL + COL_URL + col_title + "/" + CONTENTS_FOLDER + "/" + content_title + "/" + LOG_FOLDER + "/state");
		try {
			this.client.executeMethod(get_ref_content_state);
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
         // handle response.	
		String response_body = "";
		try {
			response_body = get_ref_content_state.getResponseBodyAsString();
		} catch (IOException e) {
			e.printStackTrace();
		}		
		System.out.println("Response: " + response_body);
		assertEquals(response_body, pending_state);
	}
    
    public void test_get_revision_pending() {
    	
    	try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
    	System.out.println("TEST if the pending revision is present");

		HttpMethod get_pending_revision = new GetMethod(SLING_URL + CATALOG_URL + PENDING_FOLDER + "/" + content_title + "/sling:resourceType");
		System.out.println("Get if the content is pending " + SLING_URL + CATALOG_URL + PENDING_FOLDER + "/" + content_title + "/sling:resourceType");
		
		try {
			this.client.executeMethod(get_pending_revision);
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
        
		System.out.println("Response: " + get_pending_revision.getStatusCode());
		assertEquals(get_pending_revision.getStatusCode(), 404);
	}
    
    public void test_getTagsFolder() {
    	
  		try {
  			Thread.sleep(2000);
  		} catch (InterruptedException e1) {
  			// TODO Auto-generated catch block
  			e1.printStackTrace();
  		}
      	
      	String response_body = "";
      	HttpMethod get_col_tags = new GetMethod(SLING_URL + CATALOG_URL + TAGS_FOLDER + "/" + "sling:resourceType");
  		try {
  			client.executeMethod(get_col_tags);
  		} catch (HttpException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		} catch (IOException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
           
  		try {
  			response_body = get_col_tags.getResponseBodyAsString();
  		} catch (IOException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}	
  		
  		assertEquals(response_body, TAGS_RESOURCE_TYPE);			
  		get_col_tags.releaseConnection();
      }
    
    /**
  	 * Tests for tags postprocessing
  	 */
  	public void test_getTagsCatalog() {

  		try {
  			Thread.sleep(4000);
  		} catch (InterruptedException e1) {
  			// TODO Auto-generated catch block
  			e1.printStackTrace();
  		}

  		// Get the  tags
  		HttpMethod get_col_tag1 = new GetMethod(SLING_URL + CATALOG_URL
  				+ TAGS_FOLDER + "/" + tag1_slug + "/title");
  		try {
  			this.client.executeMethod(get_col_tag1);
  		} catch (HttpException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		} catch (IOException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
  		// handle response.
  		String response_body = "";
  		try {
  			response_body = get_col_tag1.getResponseBodyAsString();
  		} catch (IOException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
  		// System.out.println("Tag 1: " + response_body);
  		assertEquals(response_body, tag1_title);
  		get_col_tag1.releaseConnection();

  		try {
  			Thread.sleep(2000);
  		} catch (InterruptedException e1) {
  			// TODO Auto-generated catch block
  			e1.printStackTrace();
  		}
  		// Get the collection tags
  		HttpMethod get_col_tag2 = new GetMethod(SLING_URL + CATALOG_URL
  				+ TAGS_FOLDER + "/" + tag2_slug + "/title");
  		try {
  			this.client.executeMethod(get_col_tag2);
  		} catch (HttpException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		} catch (IOException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
  		// handle response.
  		try {
  			response_body = get_col_tag2.getResponseBodyAsString();
  		} catch (IOException e) {
  			// TODO Auto-generated catch block
  			e.printStackTrace();
  		}
  		// System.out.println("Tag 2: " + response_body);
  		assertEquals(response_body, tag2_title);
  		get_col_tag2.releaseConnection();
  	}
}
