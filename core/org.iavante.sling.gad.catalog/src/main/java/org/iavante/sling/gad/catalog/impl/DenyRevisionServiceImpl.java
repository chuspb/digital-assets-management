/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.catalog.impl;

import java.io.Serializable;

import javax.jcr.Node;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;
import javax.jcr.observation.ObservationManager;

import org.apache.sling.jcr.api.SlingRepository;
import org.iavante.sling.commons.services.GADContentCreationService;
import org.iavante.sling.gad.catalog.DenyRevisionService;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see org.iavante.sling.gad.catalog.DenyRevisionService
 * @scr.service
 * @scr.component immediate="true" metatype="false"
 * @scr.property name="service.description"
 *               value="Catalog deny revision listener"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */
public class DenyRevisionServiceImpl implements DenyRevisionService,
		EventListener, Serializable {
	private static final long serialVersionUID = 1L;

	/** JCR Session. */
	private Session session;

	/** JCR Observation Manager. */
	private ObservationManager observationManager;

	/** @scr.reference */
	private SlingRepository repository;

	/** @scr.reference */
	private GADContentCreationService gadContentTool;

	/** Registered JCR path. */
	private final String REGISTERED_PATH = "/content/catalogo/denegados";

	/** Revision content type. */
	private final String REVISION_RESOURCE_TYPE = "gad/revision";

	/** Property to save the original content path. */
	private final String PATH_PROP = "jcr_path";

	/** Log node. */
	private final String LOG_NODE = "log";

	/** State of the log node. */
	private final String STATE = "denied";

	/** Default logger. */
	private static final Logger log = LoggerFactory
			.getLogger(PendingServiceImpl.class);

	public void log(String text) {
		log.error(text);
	}

	protected void activate(ComponentContext context) {
		try {

			session = repository.loginAdministrative(null);
			if (repository.getDescriptor(Repository.OPTION_OBSERVATION_SUPPORTED)
					.equals("true")) {
				observationManager = session.getWorkspace().getObservationManager();
				String[] types = { "nt:unstructured" };
				observationManager.addEventListener(this, Event.NODE_ADDED,
						REGISTERED_PATH, true, null, types, false);
			}
		} catch (Exception e) {
			log("cannot start");
		}
	}

	protected void deactivate(ComponentContext componentContext)
			throws RepositoryException {
		if (observationManager != null) {
			observationManager.removeEventListener(this);
		}
		if (session != null) {
			session.logout();
			session = null;
		}
	}

	/*
	 * @see javax.jcr.observation.EventListener
	 */
	public void onEvent(EventIterator eventIterator) {
		while (eventIterator.hasNext()) {

			Event event = eventIterator.nextEvent();
			if (event.getType() == Event.NODE_ADDED) {
				try {

					if (log.isInfoEnabled())
						log.info("Node added");
					Node addedNode = session.getRootNode().getNode(
							event.getPath().substring(1));
					if (log.isInfoEnabled())
						log.info("Node removed: " + addedNode.getPath().toString());

					if (addedNode.hasProperty("sling:resourceType")) {
						if (addedNode.getProperty("sling:resourceType").getString().equals(
								REVISION_RESOURCE_TYPE)) {

							// Get log node in reference content
							log("Added node: " + addedNode.getPath());
							String destPath = addedNode.getPath();
							String refContentPath = addedNode.getProperty(PATH_PROP)
									.getString();
							Node refContentNode = session.getRootNode().getNode(
									refContentPath.substring(1));

							// Update the log node with the new state
							gadContentTool.updateLogNode(refContentNode, STATE, destPath);
							session.save();

							log("New status: " + STATE + "Ref: " + refContentPath);
						}
					}

				} catch (RepositoryException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
