/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.catalog.filters;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This filter is suscribed to POST requests to catalog paths (pendientes,
 * revisados y dengados) Only the revisions tag property can be changed in
 * catalog
 * 
 * @scr.component immediate="true" metatype="no"
 * @scr.property name="service.description"
 *               value="IAV Catalog Validation Filter"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.property name="filter.scope" value="request" private="true"
 * @scr.property name="filter.order" value="-1" type="Integer" private="true"
 * @scr.service
 */
public class CatalogValidationFilter implements Filter {

	/** Default log. */
	private final Logger log = LoggerFactory.getLogger(getClass());

	/** Catalog pending path. */
	private final String CAT_PENDING = "/catalogo/pendientes/";

	/** Catalog revised path. */
	private final String CAT_REVISED = "/catalogo/revisados/";

	/** Catalog denied path. */
	private final String CAT_DENIED = "/catalogo/pendientes";

	/** Allowed properties to edit. */
	private String allowedProperties[] = { "tags", "tags@TypeHint",
			"jcr:created", "jcr:createdBy", "jcr:lastModified", "jcr:lastModifiedBy",
			"sling:resourceType" };

	/** Allowed properties to edit. */
	private Set<String> allowedPropertiesSet = new HashSet();

	/*
	 * Does nothing. This filter has not initialization requirement.
	 * @see javax.servlet.Filter
	 */
	public void init(FilterConfig filterConfig) {

		for (int i = 0; i < allowedProperties.length; i++) {
			allowedPropertiesSet.add(allowedProperties[i]);
		}
	}

	/*
	 * Checks the request Authorization Headers and use this information to
	 * authenticate aginst LDAP
	 * @see javax.servlet.Filter
	 */
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {

		SlingHttpServletRequest request = (SlingHttpServletRequest) req;
		SlingHttpServletResponse response = (SlingHttpServletResponse) res;

		String requestedPath = request.getPathInfo();
		String [] pathSplit = requestedPath.split("/");
		
		if(log.isInfoEnabled()) log.info("doFilter, requestedPath:" + requestedPath);

		// Pass throught the filter if it is a POST request, a catalog request and
		// not and operation
		if ((request.getMethod() == "POST") && (is_catalog_url(requestedPath))
				&& (request.getParameter(":operation") == null)) {

			if ( ("transformations".equals(pathSplit[pathSplit.length-2]) 
					&& "gad_download".equals(pathSplit[pathSplit.length-1]) ) 
					|| valid_edit_params(request)) {
				chain.doFilter(request, response);
			} else {
				response
						.sendError(javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED);
			}
		} else {
			chain.doFilter(request, response);
		}
	}

	/*
	 * Does nothing. This filter has not destroyal requirement.
	 * @see javax.servlet.Filter
	 */
	public void destroy() {
	}

	//--------------- Internal -------------------------

	/**
	 * Returns if the params in the requests are allowed
	 * 
	 * @param The
	 *          input request
	 * @return Boolean if the params are valid or not
	 */
	private Boolean valid_edit_params(SlingHttpServletRequest request) {

		Enumeration params = request.getParameterNames();
		Boolean valid = true;
		int i = 0;
		
//	Enumeration en2 = request.getParameterNames();
//	while (en2.hasMoreElements()) {
//		String cad = (String) en2.nextElement();
//		if (log.isInfoEnabled())
//			log.info("valid_edit_params, Parameter: " + cad);
//	}

		while (params.hasMoreElements()) {
			i++;
			String paramName = (String) params.nextElement();
			if (!allowedPropertiesSet.contains(paramName)) {
				return false;
			}
		}

		// Checks if the number of params in the request are the same that the
		// allowed params
		if (i != allowedProperties.length) {
			return false;
		}
		return valid;
	}

	/**
	 * Returns if it is a catalog url.
	 * 
	 * @param The
	 *          requested path
	 * @return Booelan If it is a catalog path
	 */
	private Boolean is_catalog_url(String requestedPath) {
		return (requestedPath.contains(CAT_PENDING))
				|| (requestedPath.contains(CAT_REVISED))
				| (requestedPath.contains(CAT_DENIED));
	}
}
