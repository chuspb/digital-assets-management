/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.catalog.impl;

import java.io.Serializable;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.Value;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;
import javax.jcr.observation.ObservationManager;
import javax.jcr.version.VersionException;

import org.apache.jackrabbit.value.StringValue;
import org.apache.sling.jcr.api.SlingRepository;
import org.iavante.sling.commons.services.FileToolsService;
import org.iavante.sling.commons.services.GADContentCreationService;
import org.iavante.sling.commons.services.ITranscoderService;
import org.iavante.sling.commons.services.PortalSetup;
import org.iavante.sling.gad.catalog.RevisedService;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @scr.service
 * @scr.component immediate="true" metatype="false"
 * @scr.property name="service.description"
 *               value="Catalog Revised Listener Impl"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */

public class RevisedServiceImpl implements RevisedService, EventListener,
		Serializable {
	private static final long serialVersionUID = 1L;

	/** JCR Session. */
	private Session session;

	/** JCR Observation Manager. */
	private ObservationManager observationManager;

	/** @scr.reference */
	private SlingRepository repository;

	/** @scr.reference */
	private PortalSetup portalSetup;

	/** @scr.reference */
	private FileToolsService fileTool;

	/** @scr.reference */
	private GADContentCreationService gadContentTool;
	
	/** @scr.reference */
	private ITranscoderService transcoderService;

	/** Registered JCR path. */
	private final String REGISTERED_PATH = "/content/catalogo/revisados";

	/** Revision content type. */
	private final String REVISION_RESOURCE_TYPE = "gad/revision";

	/** Property to save the original content path. */
	private final String PATH_PROP = "jcr_path";

	/** Log node. */
	private final String LOG_NODE = "log";

	/** State of the log node. */
	private final String STATE = "revised";
	
	/** Sling url. */
	private String slingUrl;

	/** GAD Config properties. */
	private Map<String, String> globalProperties = null;
	private Map<String, String> s3backendProperties = null;

	/** Name of collection. */
	private String collection;

	/** External storage. */
	private String externalStorageServer;

	/** External storage. */
	private String externalStorageUrl;

	/** JCR root node. */
	private Node rootNode;

	/** Default file folder. */
	private String defaultFileFolder;

	/** Commons variables. */
	private final String TRANSFORMATION = "gad_download";
	private final String TRANS_MIMETYPE = "video/x-msvideo";
	private final String TAG_PROP = "tags";

	/** Default Logger. */
	private static final Logger log = LoggerFactory
			.getLogger(RevisedServiceImpl.class);

	public void log(String text) {
		log.error(text);
	}

	protected void activate(ComponentContext context) {
		try {

			session = repository.loginAdministrative(null);
			if (repository.getDescriptor(Repository.OPTION_OBSERVATION_SUPPORTED)
					.equals("true")) {
				observationManager = session.getWorkspace().getObservationManager();
				String[] types = { "nt:unstructured" };
				observationManager.addEventListener(this, Event.NODE_ADDED,
						REGISTERED_PATH, true, null, types, false);
			}
		} catch (Exception e) {
			log("cannot start");
		}

		try {
			this.rootNode = session.getRootNode();
			this.globalProperties = portalSetup.get_config_properties("/sling");
			this.s3backendProperties = portalSetup
					.get_config_properties("/s3backend");
			this.defaultFileFolder = this.globalProperties.get("default_file_folder");
			this.slingUrl = this.globalProperties.get("sling_url");
			this.externalStorageServer = "S3";
			this.externalStorageUrl = this.s3backendProperties.get("url")
					+ this.s3backendProperties.get("bucket");
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

	}

	protected void deactivate(ComponentContext componentContext)
			throws RepositoryException {
		if (observationManager != null) {
			observationManager.removeEventListener(this);
		}
		if (session != null) {
			session.logout();
			session = null;
		}
	}

	/*
	 * @see javax.jcr.observation.EventListener
	 */
	public void onEvent(EventIterator eventIterator) {
		while (eventIterator.hasNext()) {

			Event event = eventIterator.nextEvent();
			if (event.getType() == Event.NODE_ADDED) {
				try {
					Node addedNode = session.getRootNode().getNode(
							event.getPath().substring(1));

					log("Node added: " + addedNode.getPath().toString());
					if (addedNode.hasProperty("sling:resourceType")) {
						if (addedNode.getProperty("sling:resourceType").getString().equals(
								REVISION_RESOURCE_TYPE)) {

							// Get log node in reference content
							log("Added node: " + addedNode.getPath());
							String destPath = addedNode.getPath();
							String refContentPath = addedNode.getProperty(PATH_PROP)
									.getString();
							Node refContentNode = session.getRootNode().getNode(
									refContentPath.substring(1));
							
							if (addedNode.hasProperty(TAG_PROP)) {

								Value[] tags = addedNode.getProperty(TAG_PROP).getValues();
								Value[] tagsNormal = new Value[tags.length];								
								Node catalogNode = addedNode.getParent().getParent();
								for (int i = 0; i < tags.length; i++) {
									
									String tagNormal = gadContentTool.normalizeTag(tags[i]
											.getString());
									tagsNormal[i] = new StringValue(tagNormal);
									gadContentTool.updateTags(catalogNode, tagsNormal[i]
											.getString());
								}

								addedNode.setProperty(TAG_PROP, tagsNormal);
								addedNode.save();
							}

							// Update the log node with the new state
							gadContentTool.updateLogNode(refContentNode, STATE, destPath);
							log("New status: " + STATE + "Ref: " + refContentPath);

							// Create a new transformation
							NodeIterator it = addedNode.getNode("sources").getNodes();
							while (it.hasNext()) {
								Node source = it.nextNode();
								if (source.hasProperty("type")
										&& "video".equals(source.getProperty("type").getString())
										&& !source.hasNode("transformations/gad_download")) {
									// ok, let's create 'gad_transformation'
									createTransformation(source);
								}
							}
						}
					}

				} catch (RepositoryException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void createTransformation(Node source)
			throws ValueFormatException, PathNotFoundException, RepositoryException {

		// S3 default
		this.externalStorageServer = "S3";
		this.externalStorageUrl = this.s3backendProperties.get("url")
				+ this.s3backendProperties.get("bucket");

		String transNodePath = gadContentTool.createTransformation(source,
				TRANSFORMATION, TRANS_MIMETYPE);
		Node transNode = rootNode.getNode(transNodePath.substring(1));

		if (log.isInfoEnabled())
			log.info("createTransformation, Trans path: " + transNodePath);

		// Get file name
		String fileName = source.getProperty("file").getString();

		String outputFileName = fileTool.genTransformationFilename(fileName,
				TRANS_MIMETYPE, "avi");
		
		String inputPath = this.defaultFileFolder + "/" + fileName;
		String outputPath = this.defaultFileFolder + "/" + outputFileName;
		
		if (log.isInfoEnabled())
			log.info("createTransformation, Input path: " + inputPath);
		if (log.isInfoEnabled())
			log.info("createTransformation, Output path: " + outputPath);
		
		String notificationUrl = this.slingUrl + transNodePath;
		
		if (log.isInfoEnabled())
			log.info("createTransformation, Conversion params: " + inputPath + " "
					+ outputPath + " " + notificationUrl + " "
					+ externalStorageServer + " " + externalStorageUrl);
		
		int statusCode = transcoderService.sendConversionTask(
				TRANSFORMATION, inputPath, outputPath,
				notificationUrl, externalStorageServer, externalStorageUrl);
		
		if (log.isInfoEnabled())
			log.info("createTransformation, Notification sent. Status code: "
					+ statusCode);
		
		update_trans_prew_notification(transNode, statusCode);

	}
	
	/**
	 * Update the preview node with the notification status code.
	 * 
	 * @param previewNode
	 * @param status
	 */
	private void update_trans_prew_notification(Node previewNode, int status) {
		try {
			previewNode.setProperty("notify_status_code", Integer.toString(status));
			previewNode.save();
		} catch (ValueFormatException e) {
			e.printStackTrace();
		} catch (VersionException e) {
			e.printStackTrace();
		} catch (LockException e) {
			e.printStackTrace();
		} catch (ConstraintViolationException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
	}
}
