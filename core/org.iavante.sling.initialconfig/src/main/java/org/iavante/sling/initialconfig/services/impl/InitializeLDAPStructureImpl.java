/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.initialconfig.services.impl;

import java.io.Serializable;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;

import org.apache.sling.jcr.api.SlingRepository;
import org.iavante.sling.commons.services.LDAPConnector;
import org.iavante.sling.initialconfig.services.InitializeLDAPStructure;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class to load and initialize the gad structure into LDAP Groups.
 * 
 * @scr.component immediate="true" metatype="false"
 * @scr.property name="service.description"
 *               value="IAVANTE - Intialize LDAP Structure Impl"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.service 
 *              interface="org.iavante.sling.initialconfig.services.InitializeLDAPStructure"
 */
public class InitializeLDAPStructureImpl implements InitializeLDAPStructure,
		Serializable {
	private static final long serialVersionUID = 1L;

	/** @scr.reference */
	private SlingRepository repository;

	/** Default Logger. */
	private final Logger log = LoggerFactory.getLogger(getClass());

	/** @scr.reference */
	private LDAPConnector myLdapConection;

	protected void activate(ComponentContext context) {
		createInitialGADStructure();
	}

	// -------------- Internal -------------------

	/**
	 * This method is used to replace some characters to avoid errors on LDAP
	 * names and groups.
	 * 
	 * @param target
	 * @param from
	 * @param to
	 * @return
	 */
	private static String replace(String target, String from, String to) {
		int start = target.indexOf(from);
		if (start == -1)
			return target;
		int lf = from.length();
		char[] targetChars = target.toCharArray();
		StringBuffer buffer = new StringBuffer();
		int copyFrom = 0;
		while (start != -1) {
			buffer.append(targetChars, copyFrom, start - copyFrom);
			buffer.append(to);
			copyFrom = start + lf;
			start = target.indexOf(from, copyFrom);
		}
		buffer.append(targetChars, copyFrom, targetChars.length - copyFrom);
		return buffer.toString();
	}

	/**
	 * This method returns the right path to ensure compatibility if the base node
	 * calls "content".
	 * 
	 * @param target
	 * @param from
	 * @param to
	 */
	private String transformName(String cn) {
		String myString = cn;
		myString = replace(myString, "/", "|");
		String[] temp = null;
		String stringOut = "";

		temp = myString.split("\\|");

		for (int i = 0; i < temp.length; i++) {
			// Change 'content' for 'gad'
			if (temp[i] == "content") {
				temp[i] = "gad";
			}

			// Add '|' if needed
			if (stringOut.length() == 0) {// do nothing
			} else {
				stringOut = stringOut + "|";
			}

			// Regroup blocks 
			if (temp[i].length() == 0) {
			} else {
				stringOut = stringOut + temp[i];
			}

		}
		return stringOut;
	}

	/**
	 * This method checks the gad structure from the RootNode and create a group
	 * for each subNode under the root Node.
	 */
	private void createInitialGADStructure() {

		Session session = null;
		Node rootNode = null;

		try {
			session = repository.loginAdministrative(null);
		} catch (RepositoryException e) {

			if (log.isInfoEnabled()) {
				log.info("Error in session login Administrative");
			}
			e.printStackTrace();
		}

		try {
			rootNode = session.getRootNode();
		} catch (RepositoryException e) {
			if (log.isInfoEnabled()) {
				log.info("Error in getRootNode");
			}

			e.printStackTrace();
		}

		NodeIterator nodes = null;
		try {
			nodes = rootNode.getNodes();

			String LDAPgroup = "";

			while (nodes.hasNext()) {
				Node node = nodes.nextNode();

				if ((node.getPath().equals("/content"))
						| (node.getPath().equals("/gad"))) {

					String prefix = node.getName();
					String sufix = "";

					if (prefix.equals("content")) {
						prefix = "gad";
					}

					NodeIterator subnodes = node.getNodes();

					while (subnodes.hasNext()) {
						Node newNode = subnodes.nextNode();
						sufix = newNode.getName();
						LDAPgroup = prefix + "|" + sufix;
						myLdapConection.createGroup(LDAPgroup);

						if (sufix.equals("catalogo")) {
							NodeIterator ssubnodes = newNode.getNodes();
							while (ssubnodes.hasNext()) {
								Node nnewNode = ssubnodes.nextNode();
								String sufix2 = nnewNode.getName();
								LDAPgroup = prefix + "|" + sufix + "|" + sufix2;
								myLdapConection.createGroup(LDAPgroup);

							}

						}

					}
				}

			}
		} catch (RepositoryException e) {
			if (log.isInfoEnabled()) {
				log.info("Error. Not available sunnodes in RootNode");
			}
			e.printStackTrace();
		}

		// Create a group to organize administration
		myLdapConection.createGroup("gad|admin");
	}

}
