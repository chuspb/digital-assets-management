/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.initialconfig.services.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

import javax.jcr.Node;
import javax.jcr.Session;

import org.apache.sling.jcr.api.SlingRepository;
import org.iavante.sling.commons.services.PortalSetup;
import org.iavante.sling.initialconfig.services.InitializeJCRAuthorization;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see org.iavante.sling.initialconfig.services.InitializeJCRAuthorization
 * @scr.component immediate="true" metatype="false"
 * @scr.property name="service.description"
 *               value="IAVANTE - Initialize JCR Authorization Mechanism Impl"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.service 
 * interface="org.iavante.sling.initialconfig.services.InitializeJCRAuthorization"
 */
public class InitializeJCRAuthorizationImpl implements
		InitializeJCRAuthorization, Serializable {
	private static final long serialVersionUID = 1L;

	/** Default Logger. */
	private final Logger log = LoggerFactory.getLogger(getClass());

	/** @scr.reference */
	private PortalSetup portalSetup;

	/** Authentication properties. */
	private Map<String, String> authProperties = null;

	/** @scr.reference */
	private SlingRepository repository;
	
	/** JCR Session. */
	private Session session;
	
	/** JCR root node. */
	private Node rootNode;

	// Default constructor
	public InitializeJCRAuthorizationImpl() {
	}

	protected void activate(ComponentContext context) throws IOException {

		if (log.isInfoEnabled())
			log.info("activate, ini");

		// getting global configuration service
		try {
			session = repository.loginAdministrative(null);
			authProperties = portalSetup.get_config_properties("/sling");
			AbstractHttpOperation.HTTP_BASE_URL = authProperties.get("sling_url");
			if (log.isInfoEnabled())
				log.info("activate, AbstractHttpOperation.HTTP_BASE_URL= "
						+ AbstractHttpOperation.HTTP_BASE_URL);
			this.rootNode = session.getRootNode();
		} catch (Exception e) {
			log.error("activate, error getting sling_url from config, "
					+ e.getMessage());
			AbstractHttpOperation.HTTP_BASE_URL = "http://localhost:8888";
			log.error("activate, using default sling_url="
					+ AbstractHttpOperation.HTTP_BASE_URL);
		}

		// Creating the object that assigns permissions and create users
		new JCROperations(authProperties, this.rootNode);

		if (log.isInfoEnabled())
			log.info("activate, end");

	}
}
