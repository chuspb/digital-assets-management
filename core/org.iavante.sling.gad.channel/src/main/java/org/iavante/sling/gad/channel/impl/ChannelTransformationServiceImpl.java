/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.channel.impl;

import java.io.Serializable;

import javax.jcr.Node;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.observation.Event;
import javax.jcr.observation.EventIterator;
import javax.jcr.observation.EventListener;
import javax.jcr.observation.ObservationManager;

import org.apache.sling.jcr.api.SlingRepository;
import org.iavante.sling.commons.distributionserver.IDistributionServer;
import org.iavante.sling.commons.services.DistributionServiceProvider;
import org.iavante.sling.gad.channel.ChannelService;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Channel Listener implementation.
 * 
 * @scr.service
 * @scr.component immediate="true" metatype="false"
 * @scr.property name="service.description" value="Channel Transformation Listener"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */

public class ChannelTransformationServiceImpl implements ChannelService, EventListener,
		Serializable {	
	
	private static final long serialVersionUID = 1L;

	/** JCR Session. */
	private Session session;

	/** JCR Observation Manager. */
	private ObservationManager observationManager;
	
	  /** @scr.reference */
	  DistributionServiceProvider myDSProvider = null ;

	/** @scr.reference */
	private SlingRepository repository;

	/** Path to channels. */
	private final String DOCUMENT_PATH = "/content/canales";
	
	/** Transformation resource type */
	private final String TRANS_RESOURCE_TYPE = "gad/transformation";
	
	/** Distirbution server property */
	private static final String DIST_SERVER_PROP = "distribution_server";
	
	/** Contents folder */
	private static final String CONTENTS_FOLDER = "contents";
	
	/** Sling resource type prop */
	private static final String RESOURCE_TYPE = "sling:resourceType";
	
	/** Root node */
	Node rootNode = null;

	public String getRepository() {
		return repository.getDescriptor(SlingRepository.REP_NAME_DESC);
	}

	/** Default Logger. */
	private final Logger log = LoggerFactory.getLogger(getClass());

	protected void activate(ComponentContext context) {
		try {

			session = repository.loginAdministrative(null);
			rootNode = session.getRootNode();			

			if (repository.getDescriptor(Repository.OPTION_OBSERVATION_SUPPORTED)
					.equals("true")) {
				observationManager = session.getWorkspace().getObservationManager();
				String[] types = { "nt:unstructured" };
				observationManager.addEventListener(this, Event.NODE_ADDED
						| Event.PROPERTY_REMOVED, DOCUMENT_PATH, true, null, types, false);
			}
		} catch (Exception e) {
			log.error("cannot start");
		}
	}

	protected void deactivate(ComponentContext componentContext)
			throws RepositoryException {
		if (observationManager != null) {
			observationManager.removeEventListener(this);
		}
		if (session != null) {
			session.logout();
			session = null;
		}
	}

	/*
	 * @see javax.jcr.observation.EventListener
	 */
	public void onEvent(EventIterator eventIterator) {
		while (eventIterator.hasNext()) {

			Event event = eventIterator.nextEvent();
			String event_path = null;
			boolean isMultimediaAssets = false;
			
			try {
				event_path = event.getPath();
				String[] pathSplit = event.getPath().split("/"); // multimedia_assets
				if (pathSplit.length > 4 && "content".equals(pathSplit[1])
						&& "canales".equals(pathSplit[2])
						&& "multimedia_assets".equals(pathSplit[4])) {
					isMultimediaAssets = true;
				}
			} catch (RepositoryException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			if ((event.getType() == Event.PROPERTY_REMOVED) 
					&& (event_path.endsWith("jcr:lastModified"))) {

				try {
					if (log.isInfoEnabled()) log.info("Node edited " + event_path);
					String nodePath = event_path.substring(1, event_path.lastIndexOf("/"));
					Node editedNode = session.getRootNode().getNode(nodePath);
					
					if ((editedNode.hasProperty(RESOURCE_TYPE)) 
						&& (TRANS_RESOURCE_TYPE.equals(editedNode.getProperty(RESOURCE_TYPE).getValue().getString()))
						&& !isMultimediaAssets) {
						
						if (log.isInfoEnabled()) log.info("Transformation edited " + event_path);
						String channel_name = event_path.split("/")[3]; 
						Node channel = rootNode.getNode(DOCUMENT_PATH.substring(1) + "/" + 
								channel_name);
						
						// Get the content
						String content_name = event_path.split("/")[5];
						Node content = rootNode.getNode((DOCUMENT_PATH.substring(1) + "/" 
								+ channel_name + "/" + CONTENTS_FOLDER + "/" + content_name));
						
						if (log.isInfoEnabled()) log.info("Channel name: " + channel_name);
						if (log.isInfoEnabled()) log.info("Content name: " + content_name);
						
						String dist_server = null;
						if (channel.hasProperty(DIST_SERVER_PROP)) {
							dist_server = channel.getProperty(DIST_SERVER_PROP).getValue().getString();	
							if (log.isInfoEnabled()) log.info("Dist server: " + dist_server);
						}
						
						if (!"".equals(dist_server)) {	
							if (log.isInfoEnabled()) log.info("Calling " + dist_server 
									+ " upload content method");
							IDistributionServer myDServer 
							    = myDSProvider.get_distribution_server(dist_server);
							myDServer.upload(content);
							if (log.isInfoEnabled()) log.info("Upload finished");
						}
					}					
				} catch (RepositoryException e) {
					e.printStackTrace();
				}
			} // end 'else if'
		}
	}
}
