/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.channel.filters;

import java.io.IOException;
import java.io.Serializable;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This filter is suscribed to POST requests to contents in a channel Only the
 * categories and votes can be edited.
 * 
 * @scr.component immediate="true" metatype="no"
 * @scr.property name="service.description"
 *               value="IAV Channel Validation Filter"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.property name="filter.scope" value="request" private="true"
 * @scr.property name="filter.order" value="-2" type="Integer" private="true"
 * @scr.service
 */
public class ChannelValidationFilter implements Filter, Serializable {
	private static final long serialVersionUID = 1L;

	/** Default Logger. */
	private final Logger log = LoggerFactory.getLogger(getClass());

	/** Allowed properties to edit. */
	private String allowedProperties[] = { "categories", "addvote",
			"categories@TypeHint", "jcr:created", "jcr:createdBy", "tags",
			"jcr:lastModified", "jcr:lastModifiedBy", "sling:resourceType",
			"needEnding", "needTvicon", "votes@TypeHint", "tags@TypeHint" };

	private Set<String> allowedPropertiesSet = new HashSet();

	/** Transformation resource type */
	private static final String TRANS_TYPE = "gad/transformation";

	/** Sling resource type property */
	private static final String RES_TYPE_PROP = "sling:resourceType";

	/*
	 * Does nothing. This filter has not initialization requirement.
	 * @see javax.servlet.Filter
	 */
	public void init(FilterConfig filterConfig) {

		for (int i = 0; i < allowedProperties.length; i++) {
			allowedPropertiesSet.add(allowedProperties[i]);
		}
	}

	/*
	 * Checks the request Authorization Headers and use this information to
	 * authenticate aginst LDAP.
	 * @see javax.servlet.Filter
	 */
	public void doFilter(ServletRequest req, ServletResponse res,
			FilterChain chain) throws IOException, ServletException {

		if (log.isInfoEnabled())
			log.info("Channel validation filter");
		SlingHttpServletRequest request = (SlingHttpServletRequest) req;
		SlingHttpServletResponse response = (SlingHttpServletResponse) res;

		String requestedPath = request.getPathInfo();

		// Pass throught the filter if it is a POST request, a channel request and
		// not and operation
		if ((request.getMethod() == "POST")
				&& (is_channel_content_url(requestedPath))
				&& (request.getParameter(":operation") == null)
				&& (!TRANS_TYPE.equals(request.getParameter(RES_TYPE_PROP)))) {
			if (log.isInfoEnabled())
				log.info("Is a channel content request");
			if (valid_edit_params(request)) {
				if (log.isInfoEnabled())
					log.info("Valid params");
				chain.doFilter(request, response);
			} else {
				if (log.isInfoEnabled())
					log.info("Invalid params");
				response
						.sendError(javax.servlet.http.HttpServletResponse.SC_UNAUTHORIZED);
			}
		} else {
			chain.doFilter(request, response);
		}
	}

	/*
	 * Does nothing. This filter has not destroyal requirement.
	 * @see javax.servlet.Filter
	 */
	public void destroy() {
	}

	// ---------------- Internal -------------------------

	/**
	 * Returns if the params in the requests are allowed.
	 * 
	 * @param request
	 *          The input request
	 * @return Boolean if the params are valid or not
	 */
	private Boolean valid_edit_params(SlingHttpServletRequest request) {

		Enumeration params = request.getParameterNames();
		Boolean valid = true;
		int i = 0;

		while (params.hasMoreElements()) {
			i++;
			String paramName = (String) params.nextElement();
			if (!allowedPropertiesSet.contains(paramName)) {
				return false;
			}
		}

		return valid;
	}

	/**
	 * Returns if it is a channel contents folder
	 * 
	 * @param requestedPath
	 *          The requested path
	 * @return boolean
	 */
	private Boolean is_channel_content_url(String requestedPath) {

		String expression = "/canales/[a-zA-Z0-9_-]+/contents/[a-zA-Z0-9_-]+";
		Pattern channelUrl = Pattern.compile(expression);
		Matcher channelContentMatch = channelUrl.matcher(requestedPath);
		return channelContentMatch.find();
	}
}
