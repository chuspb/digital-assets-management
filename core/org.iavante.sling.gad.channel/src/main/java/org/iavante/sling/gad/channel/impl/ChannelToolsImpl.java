/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.channel.impl;

import java.io.Serializable;
import java.util.ListIterator;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;

import org.iavante.sling.commons.services.GADContentCreationService;
import org.iavante.sling.gad.channel.ChannelTools;
import org.iavante.sling.gad.content.ContentTools;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Channel Listener implementation.
 * 
 * @scr.component
 * @scr.property name="service.description" value="Channel tools"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 * @scr.service interface="org.iavante.sling.gad.channel.ChannelTools"
 */
public class ChannelToolsImpl implements ChannelTools, Serializable{
	
	private static final long serialVersionUID = 1L;	


    /** @scr.reference */
    private ContentTools contenttool;

    /** Default Logger. */
    private final Logger log = LoggerFactory.getLogger(getClass());    

    /** playlist head */
    private String playlist_head = "<?xml version=\"1.0\"?>\n"
			+ "<smil xmlns=\"http://www.w3.org/2001/SMIL20/Language\">\n"
			+ "  <head>\n"
			+ "    <meta name=\"title\" content=\"Channel content \"/>\n"
			+ "    <meta name=\"generator\" content=\"GAD\"/>\n"
			+ "    <meta name=\"author\" content=\"Fundacion Iavante\"/>\n"
			+ "    <layout>\n"
			+ "      <root-layout id=\"Welcome\" backgroundColor=\"gray\" width=\"800\" height=\"600\"/>\n"
			+ "      <region id=\"background\" width=\"800\" height=\"600\"/>\n"
			+ "    </layout>\n" + "  </head>\n" + "  <body>\n" + "   <par>\n"
			+ "    <img src=\"/etc/X11/xdm/FondoXDM.jpg\" region=\"Welcome\"/>\n"
			+ "        <seq >\n";
	
    
    /** playlist foot */
    private String playlist_foot = "        </seq>\n" + "   </par>\n"
			+ "  </body>\n" + "</smil>\n";

    protected void activate(ComponentContext context) {    	
    }
    
    protected void deactivate(ComponentContext componentContext){}
	
    /*
     * (non-Javadoc)
     * @see org.iavante.sling.gad.channel.ChannelTools#getPlaylist(javax.jcr.Node, java.lang.String)
     */
    public String getPlaylist(Node channel,String storage){

   	  String playlist_output = playlist_head;
	  /* get all the media lines in playlist of all contents */
	  try {
	    NodeIterator it = channel.getNode("contents").getNodes();
		while(it.hasNext()){
		  ListIterator<String> itline = contenttool.getPlaylistLines(it.nextNode(),storage).listIterator();
			while(itline.hasNext()){
			  playlist_output += itline.next()+"\n";
			}
		}
	  } catch (PathNotFoundException e) {
		  log.error("Error getting channel playlist. PathNotFoundException: "+e.getMessage());
		  return null;
	  } catch (RepositoryException e) {
		  log.error("Error getting channel playlist. RepositoryException: "+e.getMessage());
		  return null;
	  }
	  return playlist_output+playlist_foot;
	}	
}
