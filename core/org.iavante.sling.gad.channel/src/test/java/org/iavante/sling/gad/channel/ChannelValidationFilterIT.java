/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.channel;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.*;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.*;

/**
 * Test channel creation and source association
 */
public class ChannelValidationFilterIT
		extends TestCase {

	private String CHANNEL_URL = "/content/canales/";
	private String COL_URL = "/content/colecciones/";
	private String CATALOG_URL = "/content/catalogo/";
	private String PENDING_FOLDER = "pendientes";
	private String REVISED_FOLDER = "revisados";
	private String CONTENTS_FOLDER = "contents";
	private String SOURCES_FOLDER = "sources";
	private String UNPUBLISHED_FOLDER = "unpublished";
	private String TRANSFORMATIONS_FOLDER = "transformations";
	private String LOG_FOLDER = "log";
	private String TAGS_FOLDER = "tags";
	private String CATEGORY_FOLDER = "cats";
	private String TAGS_RESOURCE_TYPE = "gad/tags";
	private String REVISION_RESOURCE_TYPE = "gad/revision";
	private String CATEGORY_RESOURCE_TYPE = "gad/categories";
	private String REVISIONS_RESOURCE_TYPE = "gad/revisions";

	private String col_title;
	private String content_title;
	private String source_title;
	private String file;
	private String mimetype;
	private String location;
	private String slug_content;
	private String slug_collection;
	private String tags_request;
	private String tags_response;
	private String title;
	private String subtitle;
	private String schema;
	private String slug;
	private String revision_title;
	private String channel_title;
	private String channel_subtitle;
	private String channel_schema;
	private String channel_slug;
	private String extern_storage;
	private String channel_distribution_format;
	private String channel_distribution_server;
	private Credentials defaultcreds;
	private String source_default_slug;
	private List authPrefs = new ArrayList(2);

	private final String HOSTVAR = "SLINGHOST";
	private final String HOSTPREDEF = "localhost:8888";
	private String SLING_URL = "http://";

	HttpClient client;

	@org.junit.Before
	protected void setUp() {

		Map<String, String> envs = System.getenv();
		Set<String> keys = envs.keySet();

		Iterator<String> it = keys.iterator();
		boolean hashost = false;
		while (it.hasNext()) {
			String key = (String) it.next();

			if (key.compareTo(HOSTVAR) == 0) {
				SLING_URL = SLING_URL + (String) envs.get(key);
				hashost = true;
			}
		}
		if (hashost == false)
			SLING_URL = SLING_URL + HOSTPREDEF;

		client = new HttpClient();
		col_title = "it_col";
		content_title = "it_content";
		source_title = "it_source";
		file = "it_for_channel_tests.mpeg";
		mimetype = "video/flv";
		schema = "default";
		title = "Test case content";
		schema = "default";
		slug_collection = "admin";
		slug_content = "it_content";
		tags_request = "    test,     case";
		tags_response = "test,case";
		channel_title = "IT Channel";
		channel_slug = "it_channel";
		channel_subtitle = "it_channel";
		channel_schema = "default";
		channel_distribution_format = "preview";
		channel_distribution_server = "s3";
		source_default_slug = "fuente_default";
		defaultcreds = new UsernamePasswordCredentials("admin", "admin");

		authPrefs = new ArrayList(2);
		authPrefs.add(AuthPolicy.DIGEST);
		authPrefs.add(AuthPolicy.BASIC);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		// Delete the collection
		PostMethod post_delete = new PostMethod(SLING_URL + COL_URL + col_title);
		NameValuePair[] data_delete = { new NameValuePair(":operation", "delete"), };

		post_delete.setDoAuthentication(true);
		post_delete.setRequestBody(data_delete);

		try {
			client.executeMethod(post_delete);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		post_delete.releaseConnection();

		// Create collection
		PostMethod post_create_col = new PostMethod(SLING_URL + COL_URL + col_title);
		post_create_col.setDoAuthentication(true);

		NameValuePair[] data_create_col = {
				new NameValuePair("sling:resourceType", "gad/collection"),
				new NameValuePair("title", col_title),
				new NameValuePair("schema", schema), new NameValuePair("subtitle", ""),
				new NameValuePair("extern_storage", "on"),
				new NameValuePair("picture", ""), new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };
		post_create_col.setRequestBody(data_create_col);
		// post.setDoAuthentication(true);
		try {
			client.executeMethod(post_create_col);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		// Collection created
		assertEquals(post_create_col.getStatusCode(), 201);
		post_create_col.releaseConnection();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Create content in collection
		PostMethod post_create_content = new PostMethod(SLING_URL + COL_URL
				+ col_title + "/" + CONTENTS_FOLDER + "/" + content_title);
		post_create_content.setDoAuthentication(true);

		NameValuePair[] data_create_content = {
				new NameValuePair("sling:resourceType", "gad/content"),
				new NameValuePair("title", content_title),
				new NameValuePair("schema", schema),
				new NameValuePair(
						"description",
						"Content description generated by test case. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."),
				new NameValuePair("author", "Test case"),
				new NameValuePair("origin", "Test case"),
				new NameValuePair("lang", "es"),
				new NameValuePair("tags", "test case"),
				new NameValuePair("tags@TypeHint", "String[]"),
				new NameValuePair("state", "pending"),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };

		post_create_content.setRequestBody(data_create_content);

		try {
			client.executeMethod(post_create_content);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		// Content created
		assertEquals(post_create_content.getStatusCode(), 201);
		post_create_content.releaseConnection();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Create the source
		PostMethod post_create = new PostMethod(SLING_URL + COL_URL + col_title
				+ "/" + CONTENTS_FOLDER + "/" + content_title + "/" + SOURCES_FOLDER
				+ "/" + source_title);
		post_create.setDoAuthentication(true);
		NameValuePair[] data_create = {
				new NameValuePair("sling:resourceType", "gad/source"),
				new NameValuePair("title", source_title),
				new NameValuePair("file", file), new NameValuePair("mimetype", ""),
				new NameValuePair("text_encoding", ""), new NameValuePair("lang", ""),
				new NameValuePair("length", ""), new NameValuePair("size", ""),
				new NameValuePair("type", ""), new NameValuePair("bitrate", ""),
				new NameValuePair("tags", ""), new NameValuePair("tracks_number", ""),
				new NameValuePair("track_1_type", ""),
				new NameValuePair("track_1_encoding", ""),
				new NameValuePair("track_1_features", ""),
				new NameValuePair("track_2_type", ""),
				new NameValuePair("track_2_encoding", ""),
				new NameValuePair("track_2_features", ""),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };

		post_create.setRequestBody(data_create);

		try {
			client.executeMethod(post_create);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals(post_create.getStatusCode(), 201);
		post_create.releaseConnection();

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Send the content to revision
		PostMethod post_send_to_revision = new PostMethod(SLING_URL + COL_URL
				+ col_title + "/" + CONTENTS_FOLDER + "/" + content_title);
		String catalog_pending_url = CATALOG_URL + PENDING_FOLDER + "/";

		post_send_to_revision.setDoAuthentication(true);

		NameValuePair[] data_send_to_revision = {
				new NameValuePair(":operation", "copy"),
				new NameValuePair(":dest", catalog_pending_url),
				new NameValuePair("replace", "true") };

		post_send_to_revision.setRequestBody(data_send_to_revision);
		try {
			client.executeMethod(post_send_to_revision);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals(post_send_to_revision.getStatusCode(), 201);
		post_send_to_revision.releaseConnection();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Revise the content
		PostMethod post_revise_content = new PostMethod(SLING_URL + CATALOG_URL
				+ PENDING_FOLDER + "/" + content_title);
		String catalog_revised_url = CATALOG_URL + REVISED_FOLDER + "/";
		post_send_to_revision.setDoAuthentication(true);

		NameValuePair[] data_revise_content = {
				new NameValuePair(":operation", "move"),
				new NameValuePair(":dest", catalog_revised_url),
				new NameValuePair("replace", "true") };
		post_revise_content.setRequestBody(data_revise_content);

		try {
			client.executeMethod(post_revise_content);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals(post_revise_content.getStatusCode(), 201);
		post_revise_content.releaseConnection();

		// Create channel
		PostMethod post_create_channel = new PostMethod(SLING_URL + CHANNEL_URL
				+ channel_slug);
		post_create_channel.setDoAuthentication(true);

		NameValuePair[] data_create_channel = {
				new NameValuePair("sling:resourceType", "gad/channel"),
				new NameValuePair("title", channel_title),
				new NameValuePair("schema", channel_schema),
				new NameValuePair("distribution_format", channel_distribution_format),
				new NameValuePair("distribution_server", channel_distribution_server),
				new NameValuePair("subtitle", channel_subtitle),
				new NameValuePair("picture", ""), new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };
		post_create_channel.setRequestBody(data_create_channel);

		try {
			client.executeMethod(post_create_channel);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		post_create_channel.releaseConnection();

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Publish the content
		PostMethod post_publish = new PostMethod(SLING_URL + CATALOG_URL
				+ REVISED_FOLDER + "/" + content_title);
		String channel = CHANNEL_URL + channel_slug + "/contents/";
		post_publish.setDoAuthentication(true);

		NameValuePair[] data_publish = { new NameValuePair(":operation", "copy"),
				new NameValuePair(":dest", channel),
				new NameValuePair("replace", "true") };
		post_publish.setRequestBody(data_publish);
		try {
			client.executeMethod(post_publish);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals(post_publish.getStatusCode(), 201);
		post_publish.releaseConnection();
	}

	@org.junit.After
	protected void tearDown() {

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Delete the collection
		PostMethod post_delete = new PostMethod(SLING_URL + COL_URL + col_title);
		NameValuePair[] data_delete = { new NameValuePair(":operation", "delete"), };

		post_delete.setDoAuthentication(true);
		post_delete.setRequestBody(data_delete);

		try {
			client.executeMethod(post_delete);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals(post_delete.getStatusCode(), 200);
		post_delete.releaseConnection();

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Delete revision
		PostMethod post_delete_rev = new PostMethod(SLING_URL + CATALOG_URL
				+ REVISED_FOLDER + "/" + content_title);
		NameValuePair[] data_delete_rev = { new NameValuePair(":operation",
				"delete"), };

		post_delete_rev.setDoAuthentication(true);
		post_delete_rev.setRequestBody(data_delete_rev);

		try {
			client.executeMethod(post_delete_rev);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		post_delete_rev.releaseConnection();

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Delete the channel
		PostMethod post_delete_channel = new PostMethod(SLING_URL + CHANNEL_URL
				+ channel_slug);
		NameValuePair[] data_delete_channel = { new NameValuePair(":operation",
				"delete"), };

		post_delete_channel.setDoAuthentication(true);
		post_delete_channel.setRequestBody(data_delete_channel);

		try {
			client.executeMethod(post_delete_channel);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		post_delete_channel.releaseConnection();
	}

	@org.junit.Test
	public void test_edit_revision() {

		// Edit revision bad
		PostMethod post_edit_revision = new PostMethod(SLING_URL + CHANNEL_URL
				+ channel_slug + "/" + CONTENTS_FOLDER + "/" + content_title);
		post_edit_revision.setDoAuthentication(true);

		NameValuePair[] data_edit_revision = {
				new NameValuePair("sling:resourceType", "gad/revision"),
				new NameValuePair("title", content_title),
				new NameValuePair("schema", schema),
				new NameValuePair(
						"description",
						"Content description generated by test case. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."),
				new NameValuePair("author", "Test case"),
				new NameValuePair("origin", "Test case"),
				new NameValuePair("lang", "es"),
				new NameValuePair("catetories@TypeHint", "String[]"),
				new NameValuePair("category", "/test"),
				new NameValuePair("state", "pending"),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };

		post_edit_revision.setRequestBody(data_edit_revision);

		try {
			client.executeMethod(post_edit_revision);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		// Unautorized
		assertEquals(post_edit_revision.getStatusCode(), 401);
		post_edit_revision.releaseConnection();

		// Edit revision bad
		PostMethod post_edit_revision_ok = new PostMethod(SLING_URL + CHANNEL_URL
				+ channel_slug + "/" + CONTENTS_FOLDER + "/" + content_title);
		post_edit_revision.setDoAuthentication(true);

		NameValuePair[] data_edit_revision_ok = {
				new NameValuePair("sling:resourceType", "gad/revision"),
				new NameValuePair("categories@TypeHint", "String[]"),
				new NameValuePair("categories", "/cat-test"),
				new NameValuePair("categories", "/cat-test2"),
				new NameValuePair("addvote", "5"),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:created", "") };

		post_edit_revision_ok.setRequestBody(data_edit_revision_ok);

		try {
			client.executeMethod(post_edit_revision_ok);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		// Unautorized
		assertEquals(post_edit_revision_ok.getStatusCode(), 200);
		post_edit_revision_ok.releaseConnection();

	}
}