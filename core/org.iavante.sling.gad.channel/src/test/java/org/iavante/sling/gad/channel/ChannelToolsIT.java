/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.sling.gad.channel;

import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.*;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.*;

/**
 * Test channel tools
 */
public class ChannelToolsIT
		extends TestCase {

	private String CHANNEL_URL = "/content/canales/";
	private String COL_URL = "/content/colecciones/";
	private String CATALOG_URL = "/content/catalogo/";
	private String PENDING_FOLDER = "pendientes";
	private String REVISED_FOLDER = "revisados";
	private String CONTENTS_FOLDER = "contents";
	private String SOURCES_FOLDER = "sources";
	private String CHANNEL_FOLDER = "canales";
	private String DS_CUSTOM_PROPS_FOLDER = "ds_custom_props";

	private String col_title;
	private String content_title;
	private String source_title;
	private String file;
	private String mimetype;
	private String location;
	private String slug_content;
	private String slug_collection;
	private String tags_request;
	private String tags_response;
	private String title;
	private String subtitle;
	private String schema;
	private String slug;
	private String revision_title;
	private String channel_title;
	private String channel_subtitle;
	private String channel_schema;
	private String channel_slug;
	private String extern_storage;
	private String channel_distribution_format;
	private String channel_distribution_server;
	private Credentials defaultcreds;
	private String source_default_slug;
	private String tag1_input;
	private String tag2_input;
	private String tag1_title;
	private String tag2_title;
	private String tag1_slug;
	private String tag2_slug;
	private String file_trans_preview;
	private String file_trans_ies;

	private List authPrefs = new ArrayList(2);

	private final String HOSTVAR = "SLINGHOST";
	private final String HOSTPREDEF = "localhost:8888";
	private String SLING_URL = "http://";

	HttpClient client;

	@org.junit.Before
	protected void setUp() {

		Map<String, String> envs = System.getenv();
		Set<String> keys = envs.keySet();

		Iterator<String> it = keys.iterator();
		boolean hashost = false;
		while (it.hasNext()) {
			String key = (String) it.next();

			if (key.compareTo(HOSTVAR) == 0) {
				SLING_URL = SLING_URL + (String) envs.get(key);
				hashost = true;
			}
		}
		if (hashost == false)
			SLING_URL = SLING_URL + HOSTPREDEF;

		client = new HttpClient();
		col_title = "it_col";
		content_title = "it_content";
		source_title = "it_source";
		file = "it_for_channel_tests.mpeg";
		mimetype = "video/flv";
		schema = "default";
		title = "Test case content";
		schema = "default";
		slug_collection = "admin";
		slug_content = "it_content";
		tags_request = "    test,     case";
		tags_response = "test,case";
		channel_title = "IT Channel";
		channel_slug = "it_channel";
		channel_subtitle = "it_channel";
		channel_schema = "default";
		channel_distribution_format = "ies_web_medium,preview";
		channel_distribution_server = "s3";
		source_default_slug = "fuente_default";

		tag1_input = "AEIOU";
		tag2_input = "Recetas Equilibradas";

		tag1_title = "aeiou";
		tag2_title = "recetas equilibradas";

		tag1_slug = "aeiou";
		tag2_slug = "recetas-equilibradas";
		defaultcreds = new UsernamePasswordCredentials("admin", "admin");

		file_trans_preview = "file_preview.flv";
		file_trans_ies = "file_ies.flv";

		authPrefs = new ArrayList(2);
		authPrefs.add(AuthPolicy.DIGEST);
		authPrefs.add(AuthPolicy.BASIC);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		// Delete the collection
		PostMethod post_delete = new PostMethod(SLING_URL + COL_URL + col_title);
		NameValuePair[] data_delete = { new NameValuePair(":operation", "delete"), };

		post_delete.setDoAuthentication(true);
		post_delete.setRequestBody(data_delete);

		try {
			client.executeMethod(post_delete);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		post_delete.releaseConnection();

		// Create collection
		PostMethod post_create_col = new PostMethod(SLING_URL + COL_URL + col_title);
		post_create_col.setDoAuthentication(true);

		NameValuePair[] data_create_col = {
				new NameValuePair("sling:resourceType", "gad/collection"),
				new NameValuePair("title", col_title),
				new NameValuePair("schema", schema), new NameValuePair("subtitle", ""),
				new NameValuePair("extern_storage", "on"),
				new NameValuePair("picture", ""), new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };
		post_create_col.setRequestBody(data_create_col);
		// post.setDoAuthentication(true);
		try {
			client.executeMethod(post_create_col);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		// Collection created
		assertEquals(201, post_create_col.getStatusCode());
		post_create_col.releaseConnection();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Create content in collection
		PostMethod post_create_content = new PostMethod(SLING_URL + COL_URL
				+ col_title + "/" + CONTENTS_FOLDER + "/" + content_title);
		post_create_content.setDoAuthentication(true);

		NameValuePair[] data_create_content = {
				new NameValuePair("sling:resourceType", "gad/content"),
				new NameValuePair("title", content_title),
				new NameValuePair("schema", schema),
				new NameValuePair(
						"description",
						"Content description generated by test case. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur."),
				new NameValuePair("author", "Test case"),
				new NameValuePair("origin", "Test case"),
				new NameValuePair("lang", "es"),
				new NameValuePair("tags@TypeHint", "String[]"),
				new NameValuePair("tags", tag1_input),
				new NameValuePair("tags", tag2_input),
				new NameValuePair("state", "pending"),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };

		post_create_content.setRequestBody(data_create_content);

		try {
			client.executeMethod(post_create_content);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		// Content created
		assertEquals(201, post_create_content.getStatusCode());
		post_create_content.releaseConnection();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Create the source
		PostMethod post_create = new PostMethod(SLING_URL + COL_URL + col_title
				+ "/" + CONTENTS_FOLDER + "/" + content_title + "/" + SOURCES_FOLDER
				+ "/" + source_title);
		post_create.setDoAuthentication(true);
		NameValuePair[] data_create = {
				new NameValuePair("sling:resourceType", "gad/source"),
				new NameValuePair("title", source_title),
				new NameValuePair("file", file), new NameValuePair("mimetype", ""),
				new NameValuePair("text_encoding", ""), new NameValuePair("lang", ""),
				new NameValuePair("length", ""), new NameValuePair("size", ""),
				new NameValuePair("type", ""), new NameValuePair("bitrate", ""),
				new NameValuePair("tags", ""), new NameValuePair("tracks_number", ""),
				new NameValuePair("track_1_type", ""),
				new NameValuePair("track_1_encoding", ""),
				new NameValuePair("track_1_features", ""),
				new NameValuePair("track_2_type", ""),
				new NameValuePair("track_2_encoding", ""),
				new NameValuePair("track_2_features", ""),
				new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", "") };

		post_create.setRequestBody(data_create);

		try {
			client.executeMethod(post_create);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals(201, post_create.getStatusCode());
		post_create.releaseConnection();

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Create transformation
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Create the trans preview preview
		PostMethod post_create_trans = new PostMethod(SLING_URL + COL_URL
				+ col_title + "/" + CONTENTS_FOLDER + "/" + content_title + "/"
				+ SOURCES_FOLDER + "/fuente_default/transformations/preview");
		post_create.setDoAuthentication(true);
		NameValuePair[] data_create_trans = {
				new NameValuePair("sling:resourceType", "gad/transformation"),
				new NameValuePair("file", file_trans_preview)

		};
		post_create_trans.setRequestBody(data_create_trans);

		try {
			client.executeMethod(post_create_trans);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals(201, post_create_trans.getStatusCode());
		post_create_trans.releaseConnection();

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Create transformation
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Create the trans ies
		PostMethod post_create_trans2 = new PostMethod(SLING_URL + COL_URL
				+ col_title + "/" + CONTENTS_FOLDER + "/" + content_title + "/"
				+ SOURCES_FOLDER + "/fuente_default/transformations/ies_web_medium");
		post_create.setDoAuthentication(true);
		NameValuePair[] data_create_trans2 = {
				new NameValuePair("sling:resourceType", "gad/transformation"),
				new NameValuePair("file", file_trans_ies)

		};
		post_create_trans2.setRequestBody(data_create_trans2);

		try {
			client.executeMethod(post_create_trans2);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		// assertEquals(201,post_create_trans2.getStatusCode());
		post_create_trans2.releaseConnection();

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Send the content to revision
		PostMethod post_send_to_revision = new PostMethod(SLING_URL + COL_URL
				+ col_title + "/" + CONTENTS_FOLDER + "/" + content_title);
		String catalog_pending_url = CATALOG_URL + PENDING_FOLDER + "/";

		post_send_to_revision.setDoAuthentication(true);

		NameValuePair[] data_send_to_revision = {
				new NameValuePair(":operation", "copy"),
				new NameValuePair(":dest", catalog_pending_url),
				new NameValuePair("replace", "true") };

		post_send_to_revision.setRequestBody(data_send_to_revision);
		try {
			client.executeMethod(post_send_to_revision);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals(201, post_send_to_revision.getStatusCode());
		post_send_to_revision.releaseConnection();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Revise the content
		PostMethod post_revise_content = new PostMethod(SLING_URL + CATALOG_URL
				+ PENDING_FOLDER + "/" + content_title);
		String catalog_revised_url = CATALOG_URL + REVISED_FOLDER + "/";
		post_send_to_revision.setDoAuthentication(true);

		NameValuePair[] data_revise_content = {
				new NameValuePair(":operation", "move"),
				new NameValuePair(":dest", catalog_revised_url),
				new NameValuePair("replace", "true") };
		post_revise_content.setRequestBody(data_revise_content);

		try {
			client.executeMethod(post_revise_content);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals(201, post_revise_content.getStatusCode());
		post_revise_content.releaseConnection();

		// Create channel
		PostMethod post_create_channel = new PostMethod(SLING_URL + CHANNEL_URL
				+ channel_slug);
		post_create_channel.setDoAuthentication(true);

		NameValuePair[] data_create_channel = {
				new NameValuePair("sling:resourceType", "gad/channel"),
				new NameValuePair("title", channel_title),
				new NameValuePair("schema", channel_schema),
				new NameValuePair("distribution_server_config",
						"{'bucket':'test', 'public_read':'1'}"),
				new NameValuePair("distribution_format_video",
						channel_distribution_format),
				new NameValuePair("distribution_format_image", "preview"),
				new NameValuePair("distribution_server", channel_distribution_server),
				new NameValuePair("subtitle", channel_subtitle),
				new NameValuePair("picture", ""), new NameValuePair("jcr:created", ""),
				new NameValuePair("jcr:createdBy", ""),
				new NameValuePair("jcr:lastModified", ""),
				new NameValuePair("jcr:lastModifiedBy", ""), };
		post_create_channel.setRequestBody(data_create_channel);

		try {
			client.executeMethod(post_create_channel);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		post_create_channel.releaseConnection();

		try {
			Thread.sleep(5000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Publish the content
		PostMethod post_publish = new PostMethod(SLING_URL + CATALOG_URL
				+ REVISED_FOLDER + "/" + content_title);
		String channel = CHANNEL_URL + channel_slug + "/contents/";
		post_publish.setDoAuthentication(true);

		NameValuePair[] data_publish = { new NameValuePair(":operation", "copy"),
				new NameValuePair(":dest", channel),
				new NameValuePair("replace", "true") };
		post_publish.setRequestBody(data_publish);
		try {
			client.executeMethod(post_publish);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals(201, post_publish.getStatusCode());
		post_publish.releaseConnection();
	}

	@org.junit.After
	protected void tearDown() {

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Delete the collection
		PostMethod post_delete = new PostMethod(SLING_URL + COL_URL + col_title);
		NameValuePair[] data_delete = { new NameValuePair(":operation", "delete"), };

		post_delete.setDoAuthentication(true);
		post_delete.setRequestBody(data_delete);

		try {
			client.executeMethod(post_delete);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals(200, post_delete.getStatusCode());
		post_delete.releaseConnection();

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Delete revision
		PostMethod post_delete_rev = new PostMethod(SLING_URL + CATALOG_URL
				+ REVISED_FOLDER + "/" + content_title);
		NameValuePair[] data_delete_rev = { new NameValuePair(":operation",
				"delete"), };

		post_delete_rev.setDoAuthentication(true);
		post_delete_rev.setRequestBody(data_delete_rev);

		try {
			client.executeMethod(post_delete_rev);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		post_delete_rev.releaseConnection();

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Delete channel
		PostMethod post_delete_channel = new PostMethod(SLING_URL + CHANNEL_URL
				+ channel_slug);
		NameValuePair[] data_delete_channel = { new NameValuePair(":operation",
				"delete"), };

		post_delete_channel.setDoAuthentication(true);
		post_delete_channel.setRequestBody(data_delete_channel);

		try {
			client.executeMethod(post_delete_channel);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		post_delete_channel.releaseConnection();

		try {
			Thread.sleep(4000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}
	}

	@org.junit.Test
	public void test_get_channel_url() {

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Post vote
		GetMethod get_revision = new GetMethod(SLING_URL + CHANNEL_URL
				+ channel_slug + "/" + CONTENTS_FOLDER + "/" + content_title);

		try {
			client.executeMethod(get_revision);
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String body = "";
		try {
			body = get_revision.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assert (body.contains("<url format=\"preview\"><![CDATA[http://test.s3.amazonaws.com/file_preview.flv&ext=.flv]]></url>"));
		assert (body.contains("<url format=\"ies_web_medium\"><![CDATA[http://test.s3.amazonaws.com/file_ies.flv&ext=.flv]]></url>"));
		get_revision.releaseConnection();
	}

	@org.junit.Test
	public void test_get_channel_embed_request() {

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Get the revision
		GetMethod get_revision_simple = new GetMethod(SLING_URL + CHANNEL_URL
				+ channel_slug + "/" + CONTENTS_FOLDER + "/" + content_title);

		try {
			client.executeMethod(get_revision_simple);
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String body = "";
		try {
			body = get_revision_simple.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertTrue(body.contains("width='400'"));
		assertTrue(body.contains("height='400'"));

		// Edit ds custom props
		PostMethod post_edit_channel = new PostMethod(SLING_URL + CHANNEL_URL
				+ channel_slug + "/" + DS_CUSTOM_PROPS_FOLDER);
		post_edit_channel.setDoAuthentication(true);

		NameValuePair[] data_edit_channel = {
				new NameValuePair("player_width", "1000"),
				new NameValuePair("player_height", "800") };
		        new NameValuePair("player_playlist_layout", "top");
		post_edit_channel.setRequestBody(data_edit_channel);

		try {
			client.executeMethod(post_edit_channel);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		post_edit_channel.releaseConnection();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Get the revision
		GetMethod get_revision = new GetMethod(SLING_URL + CHANNEL_URL
				+ channel_slug + "/" + CONTENTS_FOLDER + "/" + content_title);

		try {
			client.executeMethod(get_revision);
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		body = "";
		try {
			body = get_revision.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertTrue(body.contains("width='1000'"));
		assertTrue(body.contains("height='800'"));
		assertTrue(body.contains("playlist=none"));

		get_revision.releaseConnection();

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Get the revision
		GetMethod get_revision_override = new GetMethod(SLING_URL + CHANNEL_URL
				+ channel_slug + "/" + CONTENTS_FOLDER + "/" + content_title);

		NameValuePair[] get_params = { new NameValuePair("player_width", "200"),
				new NameValuePair("player_height", "200"),
				new NameValuePair("player_repeat", "true"),
				new NameValuePair("player_playlist_layout", "left"),
				new NameValuePair("player_autostart", "true") };

		get_revision_override.setQueryString(get_params);

		try {

			client.executeMethod(get_revision_override);
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		body = "";
		try {
			body = get_revision_override.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertTrue(body.contains("width='200'"));
		assertTrue(body.contains("height='200'"));
		assertTrue(body.contains("repeat=true"));
		assertTrue(body.contains("playlist=left"));
		assertTrue(body.contains("autostart=true"));

		get_revision_override.releaseConnection();
	}

	@org.junit.Test
	public void test_get_show_list_api() {

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Get the list from ids
		GetMethod get_list = new GetMethod(SLING_URL + CHANNEL_URL + channel_slug
				+ "/" + CONTENTS_FOLDER + ".show.list/");

		NameValuePair[] get_params = { new NameValuePair("path", content_title),
				new NameValuePair("path", "not-exists") };

		get_list.setQueryString(get_params);

		try {
			client.executeMethod(get_list);
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String body = "";
		try {
			body = get_list.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		assertTrue(body.contains("<title>" + content_title + "</title>"));
		assertTrue(body.contains("<title>not-exists</title>"));
		assertTrue(body.contains("<path>404</path>"));

		get_list.releaseConnection();
	}
	
	
	@org.junit.Test
	public void test_get_pub_date() {

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Get revision
		GetMethod get_revision = new GetMethod(SLING_URL + CHANNEL_URL
				+ channel_slug + "/" + CONTENTS_FOLDER + "/" + content_title);

		try {
			client.executeMethod(get_revision);
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String body = "";
		try {
			body = get_revision.getResponseBodyAsString();
		} catch (IOException e) {

			e.printStackTrace();
		}

		Calendar cal = Calendar.getInstance();
		String year = Integer.toString(cal.get(Calendar.YEAR));
		assert (body
				.contains("<pubDate>" + year + "-"));
		
		get_revision.releaseConnection();
	}
	
	/*
	 * Tests if the xspf request with tickets or not is working
	 */
	@org.junit.Test
	public void test_get_xspf_public() {

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}

		// Get revision
		GetMethod get_revision = new GetMethod(SLING_URL + CHANNEL_URL
				+ channel_slug + "/" + CONTENTS_FOLDER + "/" + content_title 
				+ ".xspf.xml");

		NameValuePair[] get_params = { new NameValuePair("ticket", "abcd")};
		get_revision.setQueryString(get_params);
		
		try {
			client.executeMethod(get_revision);
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		assertEquals(get_revision.getStatusCode(), 200);		
		get_revision.releaseConnection();
		
		// Edit channel. Public read = 0
		PostMethod post_edit_channel = new PostMethod(SLING_URL + CHANNEL_URL
				+ channel_slug + "/" + DS_CUSTOM_PROPS_FOLDER);
		NameValuePair[] data_custom_props = { new NameValuePair("public_read", 
				"0"), };

		post_edit_channel.setDoAuthentication(true);
		post_edit_channel.setRequestBody(data_custom_props);

		try {
			client.executeMethod(post_edit_channel);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals(200, post_edit_channel.getStatusCode());
		post_edit_channel.releaseConnection();
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {

			e1.printStackTrace();
		}
		
		// Get revision again
		GetMethod get_revision_again = new GetMethod(SLING_URL + CHANNEL_URL
				+ channel_slug + "/" + CONTENTS_FOLDER + "/" + content_title 
				+ ".xspf.xml");

		NameValuePair[] get_params_again = { new NameValuePair("ticket", 
				"abcd")};
		get_revision_again.setQueryString(get_params_again);

		
		try {
			client.executeMethod(get_revision_again);
		} catch (HttpException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		assertEquals(get_revision_again.getStatusCode(), 403);		
		get_revision_again.releaseConnection();
	}

}