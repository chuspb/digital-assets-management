/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.transcodingServer;

import java.io.File;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;

import org.apache.sling.commons.testing.jcr.RepositoryTestBase;
import org.apache.sling.jcr.api.SlingRepository;
import org.iavante.sling.transcodingServer.modules.impl.Convert;
import org.iavante.sling.transcodingServer.*;

public class ConvertTest extends RepositoryTestBase {
	
	Convert ConvertInstance;
	SlingRepository repo;
	Session session;
	
	private Node content_node;
	private Node rootNode;
	
	private final String resources_path = "src/test/resources/";
	private final String SOURCE_IMAGE_FILE_NAME = "test.jpg";
	private final String JPG_IMAGE_FILE_NAME = "test.jpg";
	private final String PNG_IMAGE_FILE_NAME = "test.png";
	private final String PDF_IMAGE_FILE_NAME = "test.pdf";
	private final String ODT_IMAGE_FILE_NAME = "test.odt";
	private final String RESIZEPARAMS = "-size 470x320";
	private final String WRONGPARAMS = "-f wrongcodec -b 512k -ar 44100";
	
	
	
	private File imageFile;
	

   
    protected void setUp() throws Exception {
    	//System.out.println("___________SetupTest");
    	super.setUp();
    	session = getSession();
    	
    	String[] prefixes = session.getWorkspace().getNamespaceRegistry().getPrefixes();
    	
    	boolean registered = false;
    	
    	for (int i=0; i< prefixes.length; i++) {
    		if (prefixes[i].equals("sling")) registered = true;
    		
    	}
    	
    	if (!registered)  session.getWorkspace().getNamespaceRegistry().registerNamespace("sling", "http://www.iavante.es/wiki/1.0");


        rootNode = getSession().getRootNode(); 
        
        // Add a jobNode
        content_node = rootNode.addNode("testjob" +  System.currentTimeMillis(), "nt:unstructured");

        
        rootNode.save();
        
	
		
        session.save();     
        
        ConvertInstance = new Convert();
        //System.out.println("___________FfmpegInstance...");
    }	
    
	@Override	
    protected void tearDown() throws Exception {
        super.tearDown();        
    }
    
    public void test_workingConvertProcess() {
   	System.out.println("__________________test_workingConvertProcess...");
    	
    	imageFile = new File(resources_path + JPG_IMAGE_FILE_NAME);
      
    	if (imageFile.exists()){
    		try{
      	content_node.setProperty("source_location", imageFile.getAbsolutePath());
      	content_node.setProperty("temp_source_location", imageFile.getAbsolutePath());
      	content_node.setProperty("target_location", resources_path + "output"+PNG_IMAGE_FILE_NAME);
      	content_node.setProperty("executable", "Convert");
      	content_node.setProperty("params", RESIZEPARAMS);
      	content_node.setProperty("extern_storage_server", "S3");
      	content_node.setProperty("extern_storage_url", "http://s3.amazonaws.com/bucket");
      	content_node.setProperty("tempDestination", resources_path);
      	content_node.setProperty("source_file", JPG_IMAGE_FILE_NAME);
      	content_node.setProperty("target_file", "ouput"+PNG_IMAGE_FILE_NAME);
      	content_node.setProperty("notification_url", "www.google.es");
      	content_node.save();
      	
    		}
    		 catch( ArrayIndexOutOfBoundsException e ) {
    			 e.printStackTrace(); 
    		 } catch (ValueFormatException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (VersionException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (LockException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (ConstraintViolationException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		} catch (RepositoryException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}
      }
    	else{
    		
    		
    	}
      
      try {
  			Thread.sleep(1000);
  		} catch (InterruptedException e1) {
  			// TODO Auto-generated catch block
  			e1.printStackTrace();
  		}
      
  		String response = ConvertInstance.runProcess(content_node);
  		System.out.println("response:"+response);
  		assertTrue(response.toUpperCase().startsWith("OK"));
    	
    	
    }
    
    public void test_wrongParamsConvertProcess() {

     	System.out.println("_________________test_wrongParamsConvertProcess...");
      	
      	imageFile = new File(resources_path + JPG_IMAGE_FILE_NAME);
        
      	if (imageFile.exists()){
      		try{
        	content_node.setProperty("source_location", imageFile.getAbsolutePath());
        	content_node.setProperty("temp_source_location", imageFile.getAbsolutePath());
        	content_node.setProperty("target_location", resources_path + "output"+PNG_IMAGE_FILE_NAME);
        	content_node.setProperty("executable", "Convert");
        	content_node.setProperty("params", WRONGPARAMS);
        	content_node.setProperty("extern_storage_server", "S3");
        	content_node.setProperty("extern_storage_url", "http://s3.amazonaws.com/bucket");
        	content_node.setProperty("tempDestination", "/tmp");
        	content_node.setProperty("source_file", JPG_IMAGE_FILE_NAME);
        	content_node.setProperty("target_file", "ouput"+PNG_IMAGE_FILE_NAME);
        	content_node.setProperty("notification_url", "www.google.es");
        	content_node.save();
        	
      		}
      		 catch( ArrayIndexOutOfBoundsException e ) {
      			 e.printStackTrace(); 
      		 } catch (ValueFormatException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		} catch (VersionException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		} catch (LockException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		} catch (ConstraintViolationException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		} catch (RepositoryException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		}
        }
      	else{
      		
      		
      	}
        
        try {
    			Thread.sleep(1000);
    		} catch (InterruptedException e1) {
    			// TODO Auto-generated catch block
    			e1.printStackTrace();
    		}
        
    		String response = ConvertInstance.runProcess(content_node);
    		System.out.println("response:"+response);
    		assertTrue(!response.toUpperCase().startsWith("OK"));
      	
      
    }
    
    public void test_wrongInputPathConvertProcess() {


     	System.out.println("___________test_wrongInputPathConvertProcess...");
      	
      	imageFile = new File(resources_path + JPG_IMAGE_FILE_NAME);
        
      	if (imageFile.exists()){
      		try{
        	content_node.setProperty("source_location", "/wrongdir"+imageFile.getAbsolutePath());
        	content_node.setProperty("temp_source_location", "/wrongdir"+imageFile.getAbsolutePath());
        	content_node.setProperty("target_location", resources_path + "output"+PNG_IMAGE_FILE_NAME);
        	content_node.setProperty("executable", "Convert");
        	content_node.setProperty("params", RESIZEPARAMS);
        	content_node.setProperty("extern_storage_server", "S3");
        	content_node.setProperty("extern_storage_url", "http://s3.amazonaws.com/bucket");
        	content_node.setProperty("tempDestination", "/tmp");
        	content_node.setProperty("source_file", JPG_IMAGE_FILE_NAME);
        	content_node.setProperty("target_file", "ouput"+PNG_IMAGE_FILE_NAME);
        	content_node.setProperty("notification_url", "www.google.es");
        	content_node.save();
        	
      		}
      		 catch( ArrayIndexOutOfBoundsException e ) {
      			 e.printStackTrace(); 
      		 } catch (ValueFormatException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		} catch (VersionException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		} catch (LockException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		} catch (ConstraintViolationException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		} catch (RepositoryException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		}
        }
      	else{
      		
      		
      	}
        
        try {
    			Thread.sleep(1000);
    		} catch (InterruptedException e1) {
    			// TODO Auto-generated catch block
    			e1.printStackTrace();
    		}
        
    		String response = ConvertInstance.runProcess(content_node);
    		System.out.println("response:"+response);
    		assertTrue(!response.toUpperCase().startsWith("OK"));
      	
    	
    	
    	
    		assertTrue(!response.toUpperCase().startsWith("OK"));
    	    	
    }
    
    public void test_wrongInputFileTypeConvertProcess() {
    	System.out.println("___________test_wrongInputFileTypeConvertProcess...");
  		

     	
      	
      	imageFile = new File(resources_path + ODT_IMAGE_FILE_NAME);
        
      	if (imageFile.exists()){
      		try{
          	content_node.setProperty("source_location", imageFile.getAbsolutePath());
          	content_node.setProperty("temp_source_location", imageFile.getAbsolutePath());
          	content_node.setProperty("target_location", resources_path + "output"+PNG_IMAGE_FILE_NAME);
          	content_node.setProperty("executable", "Convert");
          	content_node.setProperty("params", RESIZEPARAMS);
          	content_node.setProperty("extern_storage_server", "S3");
          	content_node.setProperty("extern_storage_url", "http://s3.amazonaws.com/bucket");
          	content_node.setProperty("tempDestination", resources_path);
          	content_node.setProperty("source_file", ODT_IMAGE_FILE_NAME);
          	content_node.setProperty("target_file", "ouput"+PNG_IMAGE_FILE_NAME);
          	content_node.setProperty("notification_url", "www.google.es");
          	content_node.save();
        	
      		}
      		 catch( ArrayIndexOutOfBoundsException e ) {
      			 e.printStackTrace(); 
      		 } catch (ValueFormatException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		} catch (VersionException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		} catch (LockException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		} catch (ConstraintViolationException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		} catch (RepositoryException e) {
      			// TODO Auto-generated catch block
      			e.printStackTrace();
      		}
        }
      	else{
      		
      		
      	}
        
        try {
    			Thread.sleep(1000);
    		} catch (InterruptedException e1) {
    			// TODO Auto-generated catch block
    			e1.printStackTrace();
    		}
        
    		String response = ConvertInstance.runProcess(content_node);
    		System.out.println("response:"+response);
    		assertTrue(!response.toUpperCase().startsWith("OK"));
      	
    	
    	
    	
    		
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    	
    }
    

   
   
    
}