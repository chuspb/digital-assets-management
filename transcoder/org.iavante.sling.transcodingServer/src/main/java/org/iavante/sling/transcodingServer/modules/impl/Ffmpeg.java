/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.transcodingServer.modules.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.ValueFormatException;
import javax.jcr.lock.LockException;
import javax.jcr.nodetype.ConstraintViolationException;
import javax.jcr.version.VersionException;

import org.iavante.sling.transcodingServer.*;
import org.iavante.sling.transcodingServer.impl.TranscodingManagerImpl;
import org.iavante.sling.transcodingServer.modules.*;

import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see gadJobInterface. Ffmpeg Class makes all the Sytem calls to extract
 *      metadata and convert the Video
 * */

public class Ffmpeg implements gadJobInterface {
	/**
	 * Log
	 */
	private static final Logger log = LoggerFactory.getLogger(Ffmpeg.class);

	private static final String call_in_test_mode = "ffmpeg -y -i ";
	private static final String tempdir_in_test_mode = "src/test/resources/";

	protected void activate(ComponentContext context) {
		log.info("Activated Ffmpeg Class");
	}

	private void initialize() {

	}

	private String extractInputMetadata(Node jobNode) {
		// Metadata Extract
		String inputMimeType = "";
		String inputDuration = "";
		String inputBitrate = "";
		String inputVideo = "";
		String inputAudio = "";
		String nombre = null;
		try {
			nombre = jobNode.getProperty("source_file").getValue().getString();
			log.info("Extrackting Metadata of " + nombre);
		} catch (ValueFormatException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}
		String tempDestination = null;
		try {
			tempDestination = jobNode.getProperty("tempDestination").getValue()
					.getString();
		} catch (ValueFormatException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		Proceso p0 = new Proceso();
		String orden0 = Constantes.getpathFile() + "/file -i " + tempDestination;
		Map responseMime = p0.start(orden0);

		Iterator it = responseMime.entrySet().iterator();

		try {
			while (it.hasNext()) {
				Map.Entry e = (Map.Entry) it.next();
				String mimeLine = e.getValue().toString();
				if (mimeLine.contains(": ")) {
					if (mimeLine.indexOf(": ") < mimeLine.indexOf(";")) {
						inputMimeType = mimeLine.substring(mimeLine.indexOf(": "), mimeLine
								.indexOf(";"));
					}
					jobNode.setProperty("inputMimeType", inputMimeType);
					jobNode.save();
				}

			}

		} catch (ArrayIndexOutOfBoundsException e) {
			return "OK. But some Metadata not found.";

		} catch (ValueFormatException e) {

			e.printStackTrace();
		} catch (VersionException e) {

			e.printStackTrace();
		} catch (LockException e) {

			e.printStackTrace();
		} catch (ConstraintViolationException e) {

			e.printStackTrace();
		} catch (RepositoryException e) {

			e.printStackTrace();
		}

		String DurationLabelBegin = "Duration: ";
		String DurationLabelEnd = ", ";
		String VideoLabelBegin = "Video: ";
		String VideoLabelEnd = ", ";
		String AudioLabelBegin = "Audio: ";
		String AudioLabelEnd = ", ";
		String BitrateLabelBegin = "bitrate: ";
		String BitrateLabelEnd = "s";

		Proceso p1 = new Proceso();
		// calling to make
		String orden = Constantes.getpathFFMPEG() + "/ffmpeg -i " + tempDestination;
		Map responseMeta = p1.start(orden);
		Iterator it2 = responseMeta.entrySet().iterator();

		try {
			while (it2.hasNext()) {
				int init = 0;
				int end = 0;
				Map.Entry myentry = (Map.Entry) it2.next();
				String metaLine = myentry.getValue().toString();
				if (metaLine.contains(DurationLabelBegin)) {
					log.info("contains " + DurationLabelBegin + " this line?" + metaLine);
					init = 0;
					end = 0;
					init = metaLine.indexOf(DurationLabelBegin)
							+ DurationLabelBegin.length();
					end = metaLine.indexOf(DurationLabelEnd);
					log.info("init" + init);
					log.info("end" + end);
					if (init < end) {
						inputDuration = metaLine.substring(init, end);
					}

				}
				if (metaLine.contains(BitrateLabelBegin)) {
					log.info("contains " + BitrateLabelBegin + " this line?" + metaLine);
					// inputBitrate = metaLine.substring(, );
					init = 0;
					end = 0;
					init = metaLine.indexOf(BitrateLabelBegin)
							+ BitrateLabelBegin.length();
					end = metaLine.lastIndexOf(BitrateLabelEnd) + 1;
					log.info("init" + init);
					log.info("end" + end);
					if (init < end) {
						inputBitrate = metaLine.substring(init, end);
					}
				}
				if (metaLine.contains(VideoLabelBegin)) {
					log.info("contains " + VideoLabelBegin + " this line?" + metaLine);
					// inputVideo = metaLine.substring(, );
					init = 0;
					end = 0;
					init = metaLine.indexOf(VideoLabelBegin) + VideoLabelBegin.length();
					end = metaLine.indexOf(VideoLabelEnd);
					log.info("init" + init);
					log.info("end" + end);
					if (init < end) {
						inputVideo = metaLine.substring(init, end);
					}
				}

				if (metaLine.contains(AudioLabelBegin)) {
					log.info("contains " + AudioLabelBegin);
					// inputAudio = metaLine.substring(,);
					init = 0;
					end = 0;
					init = metaLine.indexOf(AudioLabelBegin) + AudioLabelBegin.length();
					end = metaLine.indexOf(AudioLabelEnd);
					log.info("init" + init);
					log.info("end" + end);
					if (init < end) {
						inputAudio = metaLine.substring(init, end);
					}

				}

			}
			jobNode.setProperty("inputDuration", inputDuration);
			jobNode.setProperty("inputBitrate", inputBitrate);
			jobNode.setProperty("inputVideo", inputVideo);
			jobNode.setProperty("inputAudio", inputAudio);
			jobNode.save();

		} catch (ArrayIndexOutOfBoundsException e) {
			return "OK. But some Metadata not found.";
		} catch (ValueFormatException e) {

			e.printStackTrace();
		} catch (VersionException e) {

			e.printStackTrace();
		} catch (LockException e) {

			e.printStackTrace();
		} catch (ConstraintViolationException e) {

			e.printStackTrace();
		} catch (RepositoryException e) {

			e.printStackTrace();
		}

		log.info("inPutMimeType is " + inputMimeType);
		log.info("inputDuration is " + inputDuration);
		log.info("inputBitrate is " + inputBitrate);
		log.info("inputVideo is " + inputVideo);
		log.info("inputAudio is " + inputAudio);

		return "OK. MimeType & Metadata extracted.";
	}

	private String extractOutputMetadata(Node jobNode) {
		// Metadata Extract
		String outputMimeType = "";
		String outputDuration = "";
		String outputBitrate = "";
		String outputVideo = "";
		String outputAudio = "";
		String nombre = null;

		String temp_target_location = null;
		try {

			temp_target_location = jobNode.getProperty("temp_target_location")
					.getValue().getString();
		} catch (ValueFormatException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (PathNotFoundException e) {
			e.printStackTrace();
		} catch (RepositoryException e) {
			e.printStackTrace();
		}

		Proceso p0 = new Proceso();
		String orden0 = Constantes.getpathFile() + "/file -i "
				+ temp_target_location;
		Map responseMime = p0.start(orden0);

		Iterator it = responseMime.entrySet().iterator();

		try {
			while (it.hasNext()) {
				Map.Entry e = (Map.Entry) it.next();
				String mimeLine = e.getValue().toString();
				if (mimeLine.contains(": ")) {
					outputMimeType = mimeLine.substring(mimeLine.indexOf(": "), mimeLine
							.indexOf(";"));
					jobNode.setProperty("outputMimeType", outputMimeType);
					jobNode.save();
				}

			}

		} catch (ArrayIndexOutOfBoundsException e) {
			return "OK. But some Metadata not found.";

		} catch (ValueFormatException e) {

			e.printStackTrace();
		} catch (VersionException e) {

			e.printStackTrace();
		} catch (LockException e) {

			e.printStackTrace();
		} catch (ConstraintViolationException e) {

			e.printStackTrace();
		} catch (RepositoryException e) {

			e.printStackTrace();
		}

		String DurationLabelBegin = "Duration: ";
		String DurationLabelEnd = ", ";
		String VideoLabelBegin = "Video: ";
		String VideoLabelEnd = ", ";
		String AudioLabelBegin = "Audio: ";
		String AudioLabelEnd = ", ";
		String BitrateLabelBegin = "bitrate: ";
		String BitrateLabelEnd = "s";

		Proceso p1 = new Proceso();
		// calling to make
		String orden = Constantes.getpathFFMPEG() + "/ffmpeg -i "
				+ temp_target_location;
		Map responseMeta = p1.start(orden);
		Iterator it2 = responseMeta.entrySet().iterator();
		try {
			while (it2.hasNext()) {
				int init = 0;
				int end = 0;
				Map.Entry myentry = (Map.Entry) it2.next();
				String metaLine = myentry.getValue().toString();
				if (metaLine.contains(DurationLabelBegin)) {
					log.info("contains " + DurationLabelBegin + " this line?" + metaLine);
					init = 0;
					end = 0;
					init = metaLine.indexOf(DurationLabelBegin)
							+ DurationLabelBegin.length();
					end = metaLine.indexOf(DurationLabelEnd);
					log.info("init" + init);
					log.info("end" + end);
					if (init < end) {
						outputDuration = metaLine.substring(init, end);
					}

				}
				if (metaLine.contains(BitrateLabelBegin)) {
					log.info("contains " + BitrateLabelBegin + " this line?" + metaLine);
					// inputBitrate = metaLine.substring(, );
					init = 0;
					end = 0;
					init = metaLine.indexOf(BitrateLabelBegin)
							+ BitrateLabelBegin.length();
					end = metaLine.lastIndexOf(BitrateLabelEnd) + 1;
					log.info("init" + init);
					log.info("end" + end);
					if (init < end) {
						outputBitrate = metaLine.substring(init, end);
					}
				}
				if (metaLine.contains(VideoLabelBegin)) {
					log.info("contains " + VideoLabelBegin + " this line?" + metaLine);
					// inputVideo = metaLine.substring(, );
					init = 0;
					end = 0;
					init = metaLine.indexOf(VideoLabelBegin) + VideoLabelBegin.length();
					end = metaLine.indexOf(VideoLabelEnd);
					log.info("init" + init);
					log.info("end" + end);
					if (init < end) {
						outputVideo = metaLine.substring(init, end);
					}
				}

				if (metaLine.contains(AudioLabelBegin)) {
					log.info("contains " + AudioLabelBegin);
					// inputAudio = metaLine.substring(,);
					init = 0;
					end = 0;
					init = metaLine.indexOf(AudioLabelBegin) + AudioLabelBegin.length();
					end = metaLine.indexOf(AudioLabelEnd);
					log.info("init" + init);
					log.info("end" + end);
					if (init < end) {
						outputAudio = metaLine.substring(init, end);
					}

				}

			}
			jobNode.setProperty("outputDuration", outputDuration);
			jobNode.setProperty("outputBitrate", outputBitrate);
			jobNode.setProperty("outputVideo", outputVideo);
			jobNode.setProperty("outputAudio", outputAudio);
			jobNode.save();

		} catch (ArrayIndexOutOfBoundsException e) {
			return "OK. But some Metadata not found.";
		} catch (ValueFormatException e) {

			e.printStackTrace();
		} catch (VersionException e) {

			e.printStackTrace();
		} catch (LockException e) {

			e.printStackTrace();
		} catch (ConstraintViolationException e) {

			e.printStackTrace();
		} catch (RepositoryException e) {

			e.printStackTrace();
		}

		log.info("outPutMimeType is " + outputMimeType);
		log.info("outputDuration is " + outputDuration);
		log.info("outputBitrate is " + outputBitrate);
		log.info("outputVideo is " + outputVideo);
		log.info("outputAudio is " + outputAudio);

		return "OK. MimeType & Metadata extracted.";

	}

	public String runProcess(Node jobNode) {

		// Checking Paths
		log.info("FFmpeg:runProcess");

		@SuppressWarnings("unused")
		String response = "";
		@SuppressWarnings("unused")
		Boolean error = false;

		
		@SuppressWarnings("unused")
		String my_source_location;
		@SuppressWarnings("unused")
		String my_target_location;
		@SuppressWarnings("unused")
		String my_executable;
		@SuppressWarnings("unused")
		String my_params = null;
		@SuppressWarnings("unused")
		String my_notification_url;
		@SuppressWarnings("unused")
		String my_extern_storage_url;
		@SuppressWarnings("unused")
		String my_extern_storage_server;

		@SuppressWarnings("unused")
		String my_source_file;
		@SuppressWarnings("unused")
		String my_temp_source_location = null;
		@SuppressWarnings("unused")
		String my_target_file = null;

		try {
			log.info("FFmpeg:runProcess...getting properties");
			my_source_location = jobNode.getProperty("source_location").getValue()
					.getString();
			my_target_location = jobNode.getProperty("target_location").getValue()
					.getString();
			my_temp_source_location = jobNode.getProperty("temp_source_location")
					.getValue().getString();

			my_executable = jobNode.getProperty("executable").getValue().getString();
			my_params = jobNode.getProperty("params").getValue().getString();
			my_notification_url = jobNode.getProperty("notification_url").getValue()
					.getString();
			my_extern_storage_server = jobNode.getProperty("extern_storage_server")
					.getValue().getString();
			my_extern_storage_url = jobNode.getProperty("extern_storage_url")
					.getValue().getString();

			my_source_file = jobNode.getProperty("source_file").getValue()
					.getString();
			my_target_file = jobNode.getProperty("target_file").getValue()
					.getString();

		} catch (ValueFormatException e1) {

			e1.printStackTrace();
			error = true;
			log.info(response);
			response = "Error. ValueFormatException" + e1;
			return response;
		} catch (IllegalStateException e1) {

			e1.printStackTrace();
			error = true;
			log.info(response);
			response = "Error. IllegalStateException" + e1;
			return response;
		} catch (PathNotFoundException e1) {

			e1.printStackTrace();
			error = true;
			log.info(response);
			response = "Error. PathNotFoundException" + e1;
			return response;
		} catch (RepositoryException e1) {

			e1.printStackTrace();
			error = true;
			log.info(response);
			response = "Error. RepositoryException" + e1;
			return response;
		}

		if (!error) {

			List ErrorStrings = new ArrayList();
			ErrorStrings.add("Unknown input or output");
			ErrorStrings.add("no such file");
			ErrorStrings.add("Unknown encoder");
			ErrorStrings.add("Unknown format");
			ErrorStrings.add("is not a suitable output format");
			ErrorStrings.add("Error while opening encoder");
			ErrorStrings.add("Could not write header for output");
			ErrorStrings.add("Error while parsing header");
			ErrorStrings.add("missing argument");
			ErrorStrings.add("java.io.IOException");
			ErrorStrings.add("Could not open");
			ErrorStrings.add("Video encoding failed");

			Proceso p0 = new Proceso();
			String orden0 = "";
			if (!my_params.endsWith(" ")) {
				my_params = my_params + " ";
			}
			if (!(Constantes.getpathFFMPEG() == null)) {
				orden0 = Constantes.getpathFFMPEG() + "/ffmpeg -y -i "
						+ my_temp_source_location + " " + my_params
						+ Constantes.getworkingDir() + "/" + my_target_file;

			}

			else {
				// only for test
				String tempPath = my_temp_source_location.substring(0,
						my_temp_source_location.lastIndexOf("/"));
				log.info("Test Mode ON: " + orden0);
				orden0 = tempPath + "/" + call_in_test_mode + my_temp_source_location
						+ " " + my_params + " " + tempPath + "/" + my_target_file;

			}

			log.info("Making the call: '" + orden0);
			Map salidaFF = p0.start(orden0);

			Iterator itr = salidaFF.entrySet().iterator();

			while (itr.hasNext()) {

				Map.Entry e = (Map.Entry) itr.next();
				Iterator errorsIt = ErrorStrings.iterator();
				while (errorsIt.hasNext()) {
					String thisError = (String) errorsIt.next();
					if (e.getValue().toString().toLowerCase().contains(
							thisError.toLowerCase())) {
						response = "Error in FFMPEG process. " + e.getValue().toString();
						error = true;
						log.info("Se ha detectado un error en la llamada:"
								+ e.getValue().toString());
						return response;
					}
				}

			}
		}

		// we've to check if the file has been created.

		if (!error) {
			String salida = "";

			if ((Constantes.getpathFFMPEG() == null)) {
				// test mode
				log.info("Modo test activo");
				salida = tempdir_in_test_mode;
			} else {
				salida = Constantes.getworkingDir();

			}
			salida = salida + "/" + my_target_file;

			File my_output_file = new File(salida);

			if (my_output_file.exists()) {
				if (my_output_file.length() > 1) {
					response = "OK. File converted.";
					try {
						log.info("saving temp_target_location");
						jobNode.setProperty("temp_target_location", salida);
						jobNode.save();
						response = "OK. File converted.";
						return response;
					} catch (ValueFormatException e) {

						e.printStackTrace();
					} catch (VersionException e) {

						e.printStackTrace();
					} catch (LockException e) {

						e.printStackTrace();
					} catch (ConstraintViolationException e) {

						e.printStackTrace();
					} catch (RepositoryException e) {

						e.printStackTrace();
					}

				} else {
					response = "1 Error converting the file. Check ffmpeg codecs and configuration parameters.";
					error = true;
					return response;
				}

			} else {
				response = "2 Error converting the file. Check ffmpeg codecs and configuration parameters.";
				error = true;
				return response;
			}

		}

		return response;
	}

	private String checkFiles(Node JobNode) {
		log.info("CheckingFiles...");
		String response = "";
		Boolean error = false;
		Node myNode = null;
		String my_source_location = null;
		String my_target_location = null;
		myNode = JobNode;

		try {
			my_source_location = myNode.getProperty("source_location").getValue()
					.getString();

		} catch (PathNotFoundException e1) {

			my_source_location = "";
			response = "Error. File not Found.";
			e1.printStackTrace();
			return response;
		} catch (RepositoryException e1) {

			e1.printStackTrace();
			response = "Error. File not Found.";
			e1.printStackTrace();
			return response;
		}

		try {
			my_target_location = myNode.getProperty("target_location").getValue()
					.getString();

		} catch (PathNotFoundException e1) {

			my_target_location = "";
			e1.printStackTrace();
			response = "Error. File not Found.";
			return response;
		} catch (RepositoryException e1) {

			e1.printStackTrace();
			response = "Error. File not Found.";
			return response;
		}

		String source_file = "";
		String tempDestination = "";
		String source_folder = null;
		String target_folder = null;

		source_folder = (String) my_source_location.subSequence(0,
				my_source_location.lastIndexOf("/"));

		target_folder = (String) my_target_location.subSequence(0,
				my_target_location.lastIndexOf("/"));

		File my_source_folder = new File(source_folder);
		File my_source_file = new File(my_source_location.toString());
		File my_target_folder = new File(target_folder);

		if (!my_source_folder.isDirectory()) {
			error = true;
			response = "Error. Source Folder doesn't exist (" + source_folder + ")";
			log.info("Error in checkFiles: " + response);
			return response;
		} else if (!my_source_file.exists()) {
			error = true;
			response = "Error. File doesn't exist (" + my_source_location + ")";
			log.info("Error in checkFiles: " + response);
			return response;
		} else if (!my_target_folder.isDirectory()) {
			error = true;
			response = "Error. Target Folder doesn't exist (" + target_folder + ")";
			log.info("Error in checkFiles: " + response);
			return response;
		}

		if (!error) {
			try {

				myNode.setProperty("source_file", my_source_file.getName());
				myNode.setProperty("source_folder", source_folder);
				myNode.setProperty("target_folder", target_folder);
				myNode.save();
				log.info("CheckingFiles... OK!");
				response = "OK";

			} catch (ValueFormatException e) {

				e.printStackTrace();
				response = "Some error in checking Files";
				return response;
			} catch (VersionException e) {

				e.printStackTrace();
				response = "Some error in checking Files";
				return response;
			} catch (LockException e) {

				e.printStackTrace();
				response = "Some error in checking Files";
				return response;
			} catch (ConstraintViolationException e) {

				e.printStackTrace();
				response = "Some error in checking Files";
				return response;
			} catch (RepositoryException e) {

				e.printStackTrace();
				response = "Some error in checking Files";
				return response;
			}

		}

		return response;

	}

	private String copyFiletoWorkingFolder(Node jobNode) {
		log.info("copyFiletoWorkingFolder");
		Boolean error = false;
		String thisresponse = null;
		String my_source_location = null;
		String my_target_location = null;
		String source_file = null;
		String target_file = null;
		String tempDestination = null;

		try {

			my_source_location = jobNode.getProperty("source_location").getValue()
					.getString();

		} catch (PathNotFoundException e1) {

			e1.printStackTrace();
			error = true;
			thisresponse = "Error. The property source_location property not accesible.";
			log.info("copyFiletoWorkingFolder:" + thisresponse);
			return thisresponse;
		} catch (RepositoryException e1) {

			e1.printStackTrace();
			error = true;
			thisresponse = "Error. RepositoryException.";
			log.info("copyFiletoWorkingFolder:" + thisresponse);
			return thisresponse;
		}

		try {
			my_target_location = jobNode.getProperty("target_location").getValue()
					.getString();

		} catch (PathNotFoundException e1) {

			e1.printStackTrace();
			error = true;
			thisresponse = "Error. The property target_location property not accesible.";
			log.info("copyFiletoWorkingFolder:" + thisresponse);
			return thisresponse;

		} catch (RepositoryException e1) {

			e1.printStackTrace();
			error = true;
			thisresponse = "Error. RepositoryException.";
			log.info("copyFiletoWorkingFolder:" + thisresponse);
			return thisresponse;

		}

		if (error) {
			thisresponse = "Error. Checking the job files and Folders";
			log.info("copyFiletoWorkingFolder:" + thisresponse);
		} else {
			source_file = (String) my_source_location.subSequence(my_source_location
					.lastIndexOf("/") + 1, my_source_location.length());
			target_file = (String) my_target_location.subSequence(my_target_location
					.lastIndexOf("/") + 1, my_target_location.length());
			tempDestination = Constantes.getworkingDir();

			if (!tempDestination.endsWith("/")) {
				tempDestination = tempDestination + "/";
			}

			tempDestination = tempDestination + source_file;

			try {
				log.info("trying to Copy to " + tempDestination);
				CopyFile.copy(my_source_location, tempDestination);
				thisresponse = "OK. Copied to WorkingFolder";
				log.info(thisresponse);
			} catch (IOException e) {
				error = true;
				thisresponse = "Error copying the file to workingFolder."
						+ e.getMessage();
				log.info("Error:" + thisresponse);
				e.printStackTrace();
				return thisresponse;
			}

		}

		try {
			jobNode.setProperty("temp_source_location", tempDestination);
			jobNode.setProperty("tempDestination", tempDestination);
			jobNode.setProperty("source_file", source_file);
			jobNode.setProperty("target_file", target_file);
			jobNode.save();
		} catch (ValueFormatException e) {

			e.printStackTrace();
			thisresponse = "Error. Some error copying file.";
			return thisresponse;
		} catch (VersionException e) {

			e.printStackTrace();
			thisresponse = "Error. Some error copying file.";
			return thisresponse;
		} catch (LockException e) {

			e.printStackTrace();
			thisresponse = "Error. Some error copying file.";
			return thisresponse;
		} catch (ConstraintViolationException e) {

			e.printStackTrace();
			thisresponse = "Error. Some error copying file.";
			return thisresponse;
		} catch (RepositoryException e) {

			e.printStackTrace();
			thisresponse = "Error. Some error copying file.";
			return thisresponse;
		}
		return thisresponse;

	}

	private String copyFiletoTargetFolder(Node jobNode) {

		String response = "";
		boolean error = false;
		String my_target_file = null;
		String my_target_fullpath = null;
		String my_output_fullpath = null;

		try {
			my_target_file = jobNode.getProperty("target_file").getValue()
					.getString();
			my_target_fullpath = Constantes.getworkingDir() + "/" + my_target_file;
			my_output_fullpath = Constantes.getoutputDir() + "/" + my_target_file;
			jobNode.setProperty("output_fullpath", my_output_fullpath);
			jobNode.save();
			CopyFile.copy(my_target_fullpath, my_output_fullpath);
			response = "OK. Copied to OutPutFolder";
			log.info(response);

		} catch (ValueFormatException e) {

			e.printStackTrace();
			error = true;
			response = "Error. ValueFormatException.";
			log.info(response);
		} catch (IllegalStateException e) {

			e.printStackTrace();
			error = true;
			response = "Error. IllegalStateException.";
			log.info(response);
		} catch (PathNotFoundException e) {

			e.printStackTrace();
			error = true;
			response = "Error. PathNotFoundException.";
			log.info(response);
		} catch (RepositoryException e) {

			e.printStackTrace();
			error = true;
			response = "Error. RepositoryException.";
			log.info(response);
		} catch (IOException e) {

			e.printStackTrace();
			error = true;
			response = "Error. IOException.";
			log.info(response);
		}
		return response;
	}

	public String startJob(Node jobNode) {

		log.info("startJob");
		initialize();
		Boolean error = false;
		String response_checking = null;
		String response_copy = null;
		String response_copy_out = null;
		String response_metadata = null;
		String response_process = null;
		String response = null;

		log.info(">>1: checkingFiles...");

		response_checking = checkFiles(jobNode);

		response_checking = "OK.";

		if (response_checking.startsWith("OK")) {
			// copyFiletoWorkingFolder
			log.info(">>1: checkingFiles:OK");
			log.info(">>2: copyingFiles:...");
			response_copy = copyFiletoWorkingFolder(jobNode);
		} else {
			// if response_cheking is error
			error = true;
			response = response_checking;
			log.info(">>1: checkingFiles:Error");
			return response;

		}
		if ((!error) && (response_copy.startsWith("OK"))) {
			// extractMetadata
			log.info(">>2: copyingFiles:OK");
			log.info(">>3: extractingMetadata:...");
			response_metadata = extractInputMetadata(jobNode);

		} else {
			// if response_copy is error or there is another error before
			error = true;
			response = response_copy;
			log.info(">>2: copyingFiles:Error");
			return response;
		}

		if ((!error) && (response_metadata.startsWith("OK"))) {
			// runProcess
			log.info(">>3: extractingMetadata:OK");
			log.info(">>4: runningProcess");
			response_process = runProcess(jobNode);
		} else {
			// if response_metadata is error
			error = true;
			response = response_metadata;
			log.info(">>3: extractingMetadata:Error");
			return response;

		}
		if ((!error) && (response_process.startsWith("OK"))) {
			// externalStorage
			log.info(">>4: runningProcess:OK");
			log.info(">>4.5: extract outputMetadata...");
			extractOutputMetadata(jobNode);
			log.info(">>4.5: OK.");
			log.info(">>5: copyToOutputFolder");
			// we've to copy the file to outputFolder
			response_copy_out = copyFiletoTargetFolder(jobNode);

		} else {
			// if response_process is error
			error = true;
			response = response_process;
			log.info(">>4: runningProcess:Error");
			return response;
		}

		if ((!error) && (response_copy_out.startsWith("OK"))) {
			// everything was OK
			log.info(">>5: copyToOutputFolder:OK");
		} else {
			// if error in response_copy_out
			error = true;
			response = response_copy_out;
			log.info(">>5: copyToOutputFolder:Error");
			return response;
		}

		if (!error) {
			response = "OK.";

		}

		return response;
	}

}
