/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.transcodingServer.impl;

import java.io.Serializable;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.observation.EventIterator;

import org.iavante.sling.commons.services.PortalSetup;
import org.iavante.sling.transcodingServer.Constantes;
import org.iavante.sling.transcodingServer.ITranscodingServer;
import org.osgi.service.component.ComponentContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @see org.iavante.sling.transcodingServer.ITrasncodingServer
 * @scr.service
 * @scr.component immediate="true" metatype="false"
 * @scr.property name="service.description" value="IAVANTE - Transcoding Server"
 * @scr.property name="service.vendor" value="IAVANTE"
 * @scr.service 
 *              interface="org.iavante.sling.transcodingServer.ITranscodingServer"
 */

public class TranscodingServerImpl implements ITranscodingServer, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/** @scr.reference */
	private PortalSetup portalSetup;
	/**
	 * Log
	 */
	private static final Logger log = LoggerFactory
			.getLogger(TranscodingServerImpl.class);

	/**
	 * Initital Activation to Read the useful data.
	 * 
	 * @param context
	 */
	protected void activate(ComponentContext context) {
		log.info("TranscodingServerImpl Activation");
		Map<String, String> properties = portalSetup
				.get_config_properties("/transcodingServer");
		String pathFFMPEG = null;
		String pathImageMagick = null;
		String pathFile = null;
		String sourceDir = null;
		String workingDir = null;
		String outputDir = null;
		String jobsPath = null;
		String GAD_user = null;
		String GAD_password = null;
		String threadsTotal = null;
		String threadsH264 = null;
		String threadsOthers = null;

		if (properties.containsKey("pathFFMPEG"))
			Constantes.setpathFFMPEG(properties.get("pathFFMPEG"));
		if (properties.containsKey("pathImageMagick"))
			Constantes.setpathImageMagick(properties.get("pathImageMagick"));

		if (properties.containsKey("pathFile"))
			Constantes.setpathFile(properties.get("pathFile"));

		if (properties.containsKey("sourceDir"))
			Constantes.setsourceDir(properties.get("sourceDir"));

		if (properties.containsKey("workingDir"))
			Constantes.setworkingDir(properties.get("workingDir"));

		if (properties.containsKey("outputDir"))
			Constantes.setoutputDir(properties.get("outputDir"));

		if (properties.containsKey("jobsPath"))
			Constantes.setjobsPath(properties.get("jobsPath"));

		if (properties.containsKey("GAD_user"))
			Constantes.setGADuser(properties.get("GAD_user"));

		if (properties.containsKey("GAD_password"))
			Constantes.setGAD_password(properties.get("GAD_password"));

		if (properties.containsKey("threadsTotal"))
			Constantes.setthreadsTotal(properties.get("threadsTotal"));

		if (properties.containsKey("threadsH264"))
			Constantes.setthreadsH264(properties.get("threadsH264"));

		if (properties.containsKey("threadsOthers"))
			Constantes.setthreadsOthers(properties.get("threadsOthers"));

	}

	protected void deactivate(ComponentContext componentContext)
			throws RepositoryException {
		log.info("TranscodingServerImpl Deactivation");
	}

	public void onEvent(EventIterator arg0) {

	}
}
