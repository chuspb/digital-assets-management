/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.transcodingServer;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Make a system call and search for an error stream
 * 
 * 
 */
public class Proceso implements Runnable {

	private final Logger out = LoggerFactory.getLogger(Proceso.class);

	Process p; // Process
	Thread t;

	String orden; // System Call
	List cadenas; // Strings that we're going to search
	Map found_in_errorstream; // Matches in error stream
	Map found_in_inputstream; // Matches in input stream

	private boolean stream_error = false;

	/** Creates a new instance of Proceso */
	public Proceso() {
	}

	/**
	 * 
	 * @param String
	 *          - System call
	 * @param List
	 *          - Strings to search
	 * @return Map - key=cadena, values=matches. null if exception
	 */
	public Map start(String orden) {

		Map resultado = new HashMap();
		Map error = new HashMap();
		error.put("1", "Error");

		if (orden.length() <= 0) {
			out.error("Error. Need command to run");
			return error;
		}

		orden = orden.replace("  ", " ");
		String dir = "";
		String executable = "";
		String[] ordenSplited = orden.split(" ");
		if (ordenSplited[0].contains("/")) {
			dir = ordenSplited[0].substring(0, ordenSplited[0].lastIndexOf("/"));

			executable = ordenSplited[0].substring(ordenSplited[0].lastIndexOf("/"),
					ordenSplited[0].length());
			executable = executable.substring(1);

		} else {
			executable = ordenSplited[0];
		}

		List<String> myExecArray = new ArrayList<String>();

		String OSNAME = System.getProperty("os.name");

		if (OSNAME.equals("SunOS")) {
			out.error("OS Name:" + OSNAME + " adding the dot to Executable");
			myExecArray.add("./" + executable);
		} else {
			out.error("OS Name:" + OSNAME + " Leaving the executable as is.");
			myExecArray.add("" + executable);
		}

		for (int i = 1; i <= ordenSplited.length - 1; i++) {
			myExecArray.add(ordenSplited[i]);
		}
		ProcessBuilder pb = new ProcessBuilder(myExecArray);

		File file;

		file = new File(dir);
		pb.directory(file);
		pb.redirectErrorStream(true);

		Process process;

		try {
			out.error("Executing..." + myExecArray);

			process = pb.start();
		} catch (IOException e1) {

			out.error("Exception..." + e1);
			e1.printStackTrace();
			error.put("io/exception", e1);
			return error;
		}
		InputStream is = process.getInputStream();
		InputStream es = process.getErrorStream();

		InputStreamReader isr = new InputStreamReader(is);
		InputStreamReader esr = new InputStreamReader(es);
		BufferedReader br = new BufferedReader(isr);
		BufferedReader br2 = new BufferedReader(esr);
		String line;
		String line2;
		int i = 1;
		try {
			while ((line = br.readLine()) != null) {
				out.info("____" + line);
				resultado.put(i, line);
				i = i + 1;
			}
		} catch (IOException e) {

			e.printStackTrace();
			out.info("____>io exception: " + e);
		}
		try {
			while ((line2 = br2.readLine()) != null) {
				out.info("____" + line2);
				resultado.put(i, line2);
				i = i + 1;
			}
		} catch (IOException e) {

			out.info("____>io exception: " + e);
			e.printStackTrace();
		}
		process.destroy();

		return resultado;
	}

	@Override
	public void run() {

	}

}
