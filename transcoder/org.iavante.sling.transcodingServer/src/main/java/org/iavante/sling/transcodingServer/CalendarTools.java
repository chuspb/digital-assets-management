/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.sling.transcodingServer;

import java.util.Calendar;

public class CalendarTools {
	public static String getDateFormated() {
		String dateString = "";
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int day = cal.get(Calendar.DATE);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int min = cal.get(Calendar.MINUTE);
		int sec = cal.get(Calendar.SECOND);
		int milis = cal.get(Calendar.MILLISECOND);

		dateString = dateString + year + " ";
		if (month < 10) {
			dateString = dateString + "0";
		}
		dateString = dateString + month + " ";
		if (day < 10) {
			dateString = dateString + "0";
		}
		dateString = dateString + day + " ";
		if (hour < 10) {
			dateString = dateString + "0";
		}
		dateString = dateString + hour + ":";
		if (min < 10) {
			dateString = dateString + "0";
		}
		dateString = dateString + min + ":";
		if (sec < 10) {
			dateString = dateString + "0";
		}
		dateString = dateString + sec + ".";
		if (milis < 100) {
			dateString = dateString + "0";
		}
		if (milis < 10) {
			dateString = dateString + "0";
		}
		dateString = dateString + milis;
		return dateString = dateString;
	}

	public static String getTimeStamp() {

		String dateString = "";
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int day = cal.get(Calendar.DATE);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int min = cal.get(Calendar.MINUTE);
		int sec = cal.get(Calendar.SECOND);
		int milis = cal.get(Calendar.MILLISECOND);

		dateString = dateString + year;
		if (month < 10) {
			dateString = dateString + "0";
		}
		dateString = dateString + month;
		if (day < 10) {
			dateString = dateString + "0";
		}
		dateString = dateString + day;
		if (hour < 10) {
			dateString = dateString + "0";
		}
		dateString = dateString + hour;
		if (min < 10) {
			dateString = dateString + "0";
		}
		dateString = dateString + min;
		if (sec < 10) {
			dateString = dateString + "0";
		}
		dateString = dateString + sec;
		if (milis < 100) {
			dateString = dateString + "0";
		}
		if (milis < 10) {
			dateString = dateString + "0";
		}
		dateString = dateString + milis;
		return dateString;

	}

}