#!/usr/bin/env python
# encoding: utf-8
"""
GAD Integration.py

/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
"""

import logging
import sys
import os
import commands
import getopt
import xml.dom.minidom
import re
from urllib import quote

# Config params
gad_user = ""
gad_pass = ""

# Path to exported files
file_path = "/var/gadintegration/tmp_library"
# Path to FCS working media directory
media_dir = "/Production/Media"
gad_url = ""
# Uploader API entry point
uploader_url = ""

def main():
    title = ""
    mime = ""
    extension = ""
    # parse command line options
    print "Entra 1"
    try:
        opts, args = getopt.getopt(sys.argv[1:], "ht:f:", ["help", "title", "file"])
    except getopt.error, msg:
        print msg
        print "for help use --help"
        sys.exit(2)
    # process options
    for o, a in opts:
        if o in ("-h", "--help"):
            print __doc__
            sys.exit(0)
        if o == "-t":
            title = a
            print title
        if o == "-f":
            file = a
    process_file(title, file)

def process_file(title, file):
    """
       Process parse the xml file and register in GAD collection
    """
    print "Entra"
    xml_file = os.path.join(file_path, title + ".xml")
    logging.debug('Processing %s' % xml_file)

    metadata_dict = {}
    doc = xml.dom.minidom.parse(xml_file)
    metadata_props = doc.getElementsByTagName("mdValue")
    for metadata in metadata_props:
      if metadata.hasChildNodes():
        att = metadata.getAttribute("fieldName") 
        value = metadata.childNodes[0].data
        metadata_dict[att] = value

    # Create the content 
    url_splited = gad_url.split("://")
    path = "/content/colecciones/%s/contents/*" % metadata_dict[u"Registrar en colecci\xf3n"]
    composed_url = url_splited[0] + "://" + gad_user + ":" + gad_pass + "@" + url_splited[1] + path
    prop_tags = ""
    if metadata_dict.has_key("Keywords"):
        tags = metadata_dict['Keywords'].split(",")
        for tag in tags:
            prop_tags += ' -F"tags=%s"' % quote(tag.strip())
    description = ""
    if metadata_dict.has_key("Description"):
        description = metadata_dict['Description']
    command = 'curl -F"sling:resourceType=gad/content" -F"title=%s" -F"schema=default" -F"lang=es_ES" -F"origin=%s" -F"tags@TypeHing=String[]" -F"author=%s" -F"description=%s" %s -F"jcr:created=" -F"jcr:createdBy=" -F"jcr:lastModified=" -F"jcr:lastModifiedBy=" "%s"' % (quote(metadata_dict['Title']), "Final Cut Server", "Final Cut Server", quote(description), prop_tags, composed_url)
    
    command_log = command.replace(gad_user, "*****")
    command_log = command_log.replace(gad_pass, "******")
    logging.debug('Creating content: %s' % command_log)
    
    salida = commands.getoutput(command)
    print salida 
    m = re.search('\<h1\>Content created (\W|\D|\d).*\<\/h1>', salida)
    if m:
        match = m.group(0)
        location = match.replace("<h1>Content created ", "")
        location = location.replace("</h1>", "")
        logging.debug('Content created at %s' % location)    
        print "Content created"
    else:        
        logging.error('Creating content %s' % command)
        sys.exit(0)

    # Upload the binary file
    url_splited = uploader_url.split("://")
    uploader_composed_url = url_splited[0] + "://" + gad_user + ":" + gad_pass + "@" + url_splited[1]
    path = os.path.join(media_dir, file)
    print path

    source_path = os.path.join(location, "sources/fuente_default")

    command = 'curl -F"data=@%s" -F"filename=%s" -F"content=%s" "%s"' % (path, quote(metadata_dict['File Name']).replace("%","_"), source_path, uploader_composed_url)
    command_log = command.replace(gad_user, "********")
    command_log = command_log.replace(gad_pass, "********")
    logging.debug('Uploading binary file: %s' % command_log)
    salida = commands.getoutput(command)
    if "OK" in salida:
        logging.debug("Uploading ok %s" % path)
    else:
        logging.error("Uploading bad %s %s" % (path, salida))    

    # Upload the thumbnail
    path_thumb = os.path.join(file_path, title + ".jpg" )
    print path_thumb

    thumb_path = os.path.join(location, "sources/thumbnail_default")

    command = 'curl -F"data=@%s" -F"filename=%s" -F"content=%s" "%s"' % (path_thumb, quote(file.split(".")[0].replace("%", "_") + ".jpg"), thumb_path, uploader_composed_url)
    command_log = command.replace(gad_user, "*****")
    command_log = command_log.replace(gad_pass, "*****")
    logging.debug('Uploading thumbnail file: %s' % command_log)
    salida = commands.getoutput(command)
    if "OK" in salida:
        logging.debug("Uploading thumbnail ok %s" % path_thumb)
    else:
        logging.error("Uploading thumbnail %s %s" % (path_thumb, salida))


if __name__ == "__main__":

    # Configure log
    LOG_FILENAME = os.path.join(file_path, "gad_integration.log")    
    logging.basicConfig(filename=LOG_FILENAME,level=logging.DEBUG, format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    main()
    #process_file("AtuSalud_libre_eleccion_medico")

