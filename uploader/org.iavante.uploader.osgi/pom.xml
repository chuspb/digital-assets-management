<?xml version="1.0" encoding="UTF-8"?>
<!-- 
  /*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>org.iavante.uploader</groupId>
	<artifactId>org.iavante.uploader.osgi</artifactId>
	<version>1.0</version>
	<name>IAVANTE - Uploader</name>
	<packaging>bundle</packaging>
	<description />

	<!-- Default properties and values -->
	<properties>
		<user>admin</user>
		<password>admin</password>
		<port>8888</port>
		<host>localhost</host>
		<path></path>
	</properties>

	<build>
		<plugins>
			<!-- Instruct the maven compiler to compile sources 1.5 syntax -->
			<plugin>
				<groupId>org.apache.felix</groupId>
				<artifactId>maven-scr-plugin</artifactId>
				<version>1.2.0</version>
				<executions>
					<execution>
						<id>generate-scr-scrdescriptor</id>
						<goals>
							<goal>scr</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<configuration>
					<source>1.6</source>
					<target>1.6</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.felix</groupId>
				<artifactId>maven-bundle-plugin</artifactId>
				<version>1.4.3</version>
				<extensions>true</extensions>
				<configuration>
					<instructions>
						<Export-Package>org.iavante.uploader.ifaces</Export-Package>
						<Import-Package>*;resolution:=optional</Import-Package>
						<Private-Package>org.iavante.uploader.*</Private-Package>
						<Bundle-SymbolicName>${pom.artifactId}</Bundle-SymbolicName>
						<Bundle-Name>${pom.name}</Bundle-Name>
						<Bundle-Vendor>IAVANTE</Bundle-Vendor>
						<Embed-Dependency>*;scope=compile|runtime</Embed-Dependency>
						<Sling-Initial-Content>
						SLING-INF/initial-content,
						SLING-INF/initial-content/uploader;path:=/uploader;overwrite:=true;uninstall:=true,							
						SLING-INF/initial-content/apps/uploader;path:=/apps/uploader;overwrite:=true;uninstall:=true
						</Sling-Initial-Content>
					</instructions>
				</configuration>
			</plugin>

			<plugin>
				<groupId>org.apache.sling</groupId>
				<artifactId>maven-sling-plugin</artifactId>
				<version>2.0.4-incubator</version>
				<executions>
					<execution>
						<id>install-bundle</id>
						<goals>
							<goal>install</goal>
						</goals>
						<configuration>
							<slingUrl>http://${host}:${port}${path}/system/console/install</slingUrl>
							<skipTests>true</skipTests>
							<user>${user}</user>
							<password>${password}</password>
						</configuration>
					</execution>
				</executions>
			</plugin>


			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<configuration>
					<skip>true</skip>
				</configuration>
				<executions>
					<execution>
						<id>surefire-it</id>
						<phase>integration-test</phase>
						<goals>
							<goal>test</goal>
						</goals>
						<configuration>
							<skip>false</skip>
							<includes>
								<include>**/*IT.class</include>
							</includes>
						</configuration>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>

	<dependencies>
		<dependency>
			<groupId>org.apache.felix</groupId>
			<artifactId>org.osgi.core</artifactId>
			<version>1.0.1</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.sling</groupId>
			<artifactId>org.apache.sling.api</artifactId>
			<version>2.0.4-incubator</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.sling</groupId>
			<artifactId>org.apache.sling.engine</artifactId>
			<version>2.0.4-incubator</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.apache.felix</groupId>
			<artifactId>org.osgi.compendium</artifactId>
			<version>1.0.1</version>
			<scope>provided</scope>
			<exclusions>
				<exclusion>
					<groupId>org.apache.felix</groupId>
					<artifactId>javax.servlet</artifactId>
				</exclusion>
			</exclusions>
		</dependency>
		<dependency>
			<groupId>javax.servlet</groupId>
			<artifactId>servlet-api</artifactId>
			<version>2.4</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-simple</artifactId>
			<version>1.5.0</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.6</version>
		</dependency>
		<dependency>
			<groupId>commons-fileupload</groupId>
			<artifactId>commons-fileupload</artifactId>
			<version>1.2.1</version>
		</dependency>
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-io</artifactId>
			<version>1.3.2</version>
		</dependency>
		<dependency>
			<groupId>commons-lang</groupId>
			<artifactId>commons-lang</artifactId>
			<version>20030203.000129</version>
		</dependency>
	    <dependency>
	        <groupId>commons-codec</groupId>
	        <artifactId>commons-codec</artifactId>
	        <version>1.3</version>
	    </dependency>
		<dependency>
			<groupId>httpunit</groupId>
			<artifactId>httpunit</artifactId>
			<version>1.6.2</version>
		</dependency>
		<dependency>
			<groupId>commons-httpclient</groupId>
			<artifactId>commons-httpclient</artifactId>
			<version>3.1</version>
		</dependency>
    <dependency>
      <groupId>org.apache.sling</groupId>
      <artifactId>org.apache.sling.commons.mime</artifactId>
      <version>2.0.2-incubator</version>
      <type>bundle</type>
      <scope>provided</scope>
    </dependency>
    <dependency>
			<groupId>org.apache.sling</groupId>
			<artifactId>org.apache.sling.commons.json</artifactId>
			<version>2.0.4-incubator</version>
			<scope>provided</scope>
		</dependency>
		<!-- Apache Tika Dependencies -->
		<dependency>
    	<groupId>org.apache.tika</groupId>
    	<artifactId>tika-parsers</artifactId>
    	<version>0.4</version>
    	<type>bundle</type>
    </dependency>
    <dependency>
    	<groupId>org.apache.tika</groupId>
    	<artifactId>tika-core</artifactId>
    	<version>0.4</version>
    </dependency>
    <dependency>
    	<groupId>org.apache.commons</groupId>
    	<artifactId>commons-compress</artifactId>
    	<version>1.0</version>
    </dependency>
    <dependency>
    	<groupId>pdfbox</groupId>
    	<artifactId>pdfbox</artifactId>
    	<version>0.7.3</version>
    </dependency>
    <dependency>
    	<groupId>org.fontbox</groupId>
    	<artifactId>fontbox</artifactId>
    	<version>0.1.0</version>
    </dependency>
    <dependency>
    	<groupId>org.jempbox</groupId>
    	<artifactId>jempbox</artifactId>
    	<version>0.2.0</version>
    </dependency>
    <dependency>
    	<groupId>bouncycastle</groupId>
    	<artifactId>bcmail-jdk14</artifactId>
    	<version>136</version>
    </dependency>
    <dependency>
    	<groupId>bouncycastle</groupId>
    	<artifactId>bcprov-jdk14</artifactId>
    	<version>136</version>
    </dependency>
    <dependency>
    	<groupId>org.apache.poi</groupId>
    	<artifactId>poi</artifactId>
    	<version>3.5-beta6</version>
    </dependency>
    <dependency>
    	<groupId>org.apache.poi</groupId>
    	<artifactId>poi-scratchpad</artifactId>
    	<version>3.5-beta6</version>
    </dependency>
    <dependency>
    	<groupId>org.apache.poi</groupId>
    	<artifactId>poi-ooxml</artifactId>
    	<version>3.5-beta6</version>
    </dependency>
    <dependency>
    	<groupId>org.apache.poi</groupId>
    	<artifactId>ooxml-schemas</artifactId>
    	<version>1.0</version>
    </dependency>
<!--    <dependency>-->
<!--    	<groupId>org.apache.xmlbeans</groupId>  depende de xml-apis -->
<!--    	<artifactId>xmlbeans</artifactId>-->
<!--    	<version>2.3.0</version>-->
<!--    </dependency>-->
    <dependency>
    	<groupId>dom4j</groupId>
    	<artifactId>dom4j</artifactId>
    	<version>1.6.1</version>
    </dependency>
<!--    <dependency> Launch exception -->
<!--    	<groupId>xml-apis</groupId>-->
<!--    	<artifactId>xml-apis</artifactId>-->
<!--    	<version>1.0.b2</version>-->
<!--    </dependency>-->
    <dependency>
    	<groupId>org.apache.geronimo.specs</groupId>
    	<artifactId>geronimo-stax-api_1.0_spec</artifactId>
    	<version>1.0</version>
    </dependency>
    <dependency>
    	<groupId>commons-logging</groupId>
    	<artifactId>commons-logging</artifactId>
    	<version>1.1.1</version>
    </dependency>
    <dependency>
    	<groupId>net.sourceforge.nekohtml</groupId>
    	<artifactId>nekohtml</artifactId>
    	<version>1.9.9</version>
    </dependency>
    <dependency>
    	<groupId>asm</groupId>
    	<artifactId>asm</artifactId>
    	<version>3.1</version>
    </dependency>
<!--    <dependency>-->
<!--    	<groupId>log4j</groupId>-->
<!--    	<artifactId>log4j</artifactId>-->
<!--    	<version>1.2.14</version>-->
<!--    </dependency>-->
<!--    <dependency>-->
<!--    	<groupId>xalan</groupId>-->
<!--    	<artifactId>xalan</artifactId>-->
<!--    	<version>2.1.0</version>-->
<!--    </dependency>-->
    <dependency>
    	<groupId>xerces</groupId>
    	<artifactId>xercesImpl</artifactId>
    	<version>2.9.1</version>
    </dependency>
    <dependency>
    	<groupId>org.apache.lucene</groupId>
    	<artifactId>lucene-core</artifactId>
    	<version>2.3.2</version>
    </dependency>

    
    
	</dependencies>

</project>
