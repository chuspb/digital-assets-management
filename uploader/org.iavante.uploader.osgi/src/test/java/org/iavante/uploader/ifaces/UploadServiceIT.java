/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.uploader.ifaces;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import junit.framework.TestCase;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthPolicy;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.methods.multipart.StringPart;
import org.iavante.uploader.ifaces.IUploadService;
import org.iavante.uploader.ifaces.impl.UploadServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.meterware.httpunit.WebConversation;
import com.meterware.httpunit.WebResponse;

public class UploadServiceIT
		extends TestCase {

	private final String HOSTVAR = "SLINGHOST";
	private final String HOSTPREDEF = "localhost:8888";
	private String SLING_URL = "http://";
	private String UPLOADER_URL = "/uploader";

	private Credentials defaultcreds;
	private List authPrefs = new ArrayList(2);

	private HttpClient client;

	private IUploadService uploadService;

	/** good video input */
	private static final String VIDEODIR = "./src/test/resources/";
	private static final String VIDEONAME = "videoTest1.ogg";

	/** good content input */
	private static final String CONTENT = "/uploader/tmp/uploaderTests/source_1";
	private static final String CORE_URL = "http://admin:admin@localhost:8888";

	/** display page */
	private String displayPageTitle = "GadUploaderForm";

	/** log */
	private final Logger out = LoggerFactory.getLogger(getClass());

	public UploadServiceIT(String name) {
		super(name);
		uploadService = new UploadServiceImpl();
	}

	protected void setUp() {

		System.out.println("setup, inicio");
		System.out.println("setup, IUploadService=" + uploadService);

		Map<String, String> envs = System.getenv();
		Set<String> keys = envs.keySet();

		Iterator<String> it = keys.iterator();
		boolean hashost = false;
		while (it.hasNext()) {
			String key = (String) it.next();

			if (key.compareTo(HOSTVAR) == 0) {
				SLING_URL = SLING_URL + (String) envs.get(key);
				hashost = true;
			}
		}
		if (hashost == false)
			SLING_URL = SLING_URL + HOSTPREDEF;

		client = new HttpClient();
		authPrefs.add(AuthPolicy.DIGEST);
		authPrefs.add(AuthPolicy.BASIC);
		defaultcreds = new UsernamePasswordCredentials("admin", "admin");

		System.out.println("setup, fin");

	}

	protected void tearDown() {
	}

	/**
	 * Tests a valid upload action.
	 * 
	 * @exception Exception
	 *              Invalid httpRequest Exception
	 */
	public void test_ValidUpload() throws Exception {

		PostMethod post0 = new PostMethod(SLING_URL + CONTENT);
		Part[] parts0 = { new StringPart("title", "Prueba") };
		post0.setRequestEntity((RequestEntity) new MultipartRequestEntity(parts0,
				post0.getParams()));
		post0.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		try {
			client.executeMethod(post0);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		assertEquals(201, post0.getStatusCode());

		File f = new File(VIDEODIR + VIDEONAME);
		PostMethod post = new PostMethod(SLING_URL + UPLOADER_URL);
		Part[] parts = { new StringPart("coreurl", CORE_URL),
				new StringPart("content", CONTENT),
				new StringPart("filename", f.getName()), new FilePart("data", f) };
		post.setRequestEntity((RequestEntity) new MultipartRequestEntity(parts,
				post.getParams()));
		post.setDoAuthentication(true);

		client.getParams().setAuthenticationPreemptive(true);
		client.getState().setCredentials(AuthScope.ANY, defaultcreds);
		client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY, authPrefs);

		try {
			client.executeMethod(post);
		} catch (HttpException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		assertEquals(200, post.getStatusCode());
		if (post.getStatusCode() == 200) {

			// ---------------------------------------------------
			// ---------------------------------------------------

			uploadService.removeFile("/mnt/gad/input_repository/" + VIDEONAME);
			PostMethod post2 = new PostMethod(SLING_URL + CONTENT);
			Part[] parts2 = { new StringPart(":operation", "delete") };
			post2.setRequestEntity((RequestEntity) new MultipartRequestEntity(parts2,
					post2.getParams()));
			post2.setDoAuthentication(true);

			client.getParams().setAuthenticationPreemptive(true);
			client.getState().setCredentials(AuthScope.ANY, defaultcreds);
			client.getParams().setParameter(AuthPolicy.AUTH_SCHEME_PRIORITY,
					authPrefs);

			try {
				client.executeMethod(post2);
			} catch (HttpException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			assertEquals(post2.getStatusCode(), 200);
		}

		post.releaseConnection();

	}

}
