/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.uploader.ifaces;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.sling.commons.json.JSONArray;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.iavante.uploader.ifaces.impl.AbstractHttpOperation;


/**
 * Tests for the 'InitializaJCRAuthorizationService'
 */
public class InitializeJCRAuthorizationIT extends AbstractHttpOperation {
	
	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	public void testCheckPermissionsAnonymous() throws IOException, JSONException {
	
		Credentials creds = new UsernamePasswordCredentials("admin", "admin");
		
		HTTP_BASE_URL = "http://localhost:8888";
		
		//Root nodes will be applied priveleges from
		List<String> rootNodes = new ArrayList<String>();
		rootNodes.add("/uploader");
		
		//Assigning privileges in every node
		for(String node : rootNodes){
			
			String getUrl = HTTP_BASE_URL + node + ".acl.json";
			String json = getAuthenticatedContent(creds, getUrl, CONTENT_TYPE_JSON, null, HttpServletResponse.SC_OK);
			assertNotNull(json);
			JSONObject jsonObj = new JSONObject(json);
			String aceString = jsonObj.getString("anonymous");
			assertNotNull(aceString);
			
			JSONObject aceObject = new JSONObject(aceString); 
			assertNotNull(aceObject);
			
			JSONArray grantedArray = aceObject.getJSONArray("granted");
			assertNotNull(grantedArray);
			assertEquals("jcr:all", grantedArray.getString(0));
			
		}
	}

}