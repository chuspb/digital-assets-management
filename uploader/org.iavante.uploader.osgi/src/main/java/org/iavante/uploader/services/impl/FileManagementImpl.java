/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.uploader.services.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;

import org.iavante.uploader.services.IFileManagement;
import org.iavante.uploader.services.ServiceException;

/**
 * IFileManagement Implementation.
 */
public class FileManagementImpl implements IFileManagement, Serializable {
	private static final long serialVersionUID = 1L;

	// ----------- Constructors -------------------
	public FileManagementImpl() {
	}

	/*
	 * @see org.iavante.uploader.services.IFileManagement
	 */
	public int writeFile(String path, byte[] stream) throws ServiceException {

		FileOutputStream fostream = null;
		try {
			String basePath = path.replaceAll("file:", "");
			File file = new File(basePath);
			fostream = new FileOutputStream(file);
			fostream.write(stream);
		} catch (FileNotFoundException e) {
			throw new ServiceException(
					"writeFile, Error: FileNotFoundException");
		} catch (IOException e) {
			throw new ServiceException(
					"writeFile, Error: IOException");
		} finally {
			try {
				if (fostream != null)
					fostream.close();
			} catch (IOException e1) {
				throw new ServiceException("Error saving file: " + path
						+ "-> " + e1.getMessage());
			}
		}
		return stream.length;
	}

	/*
	 * @see org.iavante.uploader.services.IFileManagement
	 */
	public byte[] readFile(String path) throws ServiceException {

		byte bytes[] = null;
		FileInputStream fistream = null;

		try {
			String basePath = path.replaceAll("file:", "");
			File file = new File(basePath);

			String sLongitud = String.valueOf(file.length());
			bytes = new byte[Integer.parseInt(sLongitud)];

			fistream = new FileInputStream(file);

			fistream.read(bytes);
		} catch (Exception err) {
			throw new ServiceException("readFile, Error: " + err.getMessage());
		} finally {
			try {
				if (fistream != null)
					fistream.close();
			} catch (IOException e1) {
				throw new ServiceException("readFile, Error reading file");
			}
		}
		return bytes;
	}

	/*
	 * @see org.iavante.uploader.services.IFileManagement
	 */
	public void deleteFile(String path) throws ServiceException {
		try {
			String basePath = path.replaceAll("file:", "");
			File file = new File(basePath);
			file.delete();
		} catch (Exception e) {
			throw new ServiceException("deleteFile, Error deleting file");
		}
	}

	/*
	 * @see org.iavante.uploader.services.IFileManagement
	 */
	public void renameFile(String oldPath, String newPath)
			throws ServiceException {
		try {
			String basePath = oldPath.replaceAll("file:", "");
			String newBasePath = newPath.replaceAll("file:", "");
			File source = new File(basePath);
			File destination = new File(newBasePath);
			source.renameTo(destination);
		} catch (Exception e) {
			throw new ServiceException("Error renaming file");
		}
	}

}
