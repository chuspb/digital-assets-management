/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */


package org.iavante.uploader.ifaces;

import java.util.List;

/**
 * Start - stop default Sling Bundles.
 * 
 * @scr.property name="service.description"
 *               value="IAVANTE - Initialize Bundles State"
 * @scr.property name="service.vendor" value="IAVANTE Foundation"
 */
public interface InitializeBundlesState {

	/**
	 * Stop a set of bundles by default. This set are needed in order to stablish
	 * a properly initial configuration of Sling.
	 * 
	 * @param symbolicNames list of symbolic names of bundles to stop
	 */
	public void stopBundles(List symbolicNames);
}
