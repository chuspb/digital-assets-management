/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */
package org.iavante.uploader.ifaces.impl.indexer;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.XHTMLContentHandler;
import org.iavante.uploader.services.Constants;
import org.iavante.uploader.util.SystemProcess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;

/**
 * This class implements a tika parser. To activate the parser we have to change
 * the tika-config.xml.
 */
public class VideoParser implements Parser, Serializable {
	private static final long serialVersionUID = 1L;

	/** Default Logger. */
	private final Logger logger = LoggerFactory.getLogger(getClass());

	public void parse(InputStream stream, ContentHandler handler,
			Metadata metadata) throws IOException, SAXException, TikaException {

		String type = metadata.get(Metadata.CONTENT_TYPE);

		if (type != null) {
			// hey we get a video lets read the exif
			//if (type.startsWith("video/")) {
				extractVideoMetaData(stream, metadata);
			//}
		}

		XHTMLContentHandler xhtml = new XHTMLContentHandler(handler, metadata);
		xhtml.startDocument();
		xhtml.characters(extractBody(stream, metadata.get("filename")));
		xhtml.endDocument();
	}

	// -------------------- Internal -------------------------

	/**
	 * Get additional metadata for video files
	 * 
	 * @param inputStream
	 * @param tikaMetaData
	 */
	private void extractVideoMetaData(InputStream stream, Metadata metadata) {

		if (logger.isInfoEnabled())
			logger.info("extractVideoMetaData, ini");

		List tags = new ArrayList();
		tags.add("Duration");
		tags.add("bitrate");
		tags.add("Video");
		tags.add("Audio");

		SystemProcess p1 = new SystemProcess();
		String orden = Constants.PATHFFMPEG + "/ffmpeg -i "
				+ metadata.get("filename");

		Map aux = p1.start(orden, tags);

		for (int i = 0; i < tags.size(); i++) {
			// only look for the first match of the searched string ->
			// (String) ((List) aux.get(cadenas.get(i))).get(0)
			if (((List) aux.get(tags.get(i))).size() > 0) {
				String linea = (String) ((List) aux.get(tags.get(i))).get(0);
				String regex = tags.get(i) + "(\\:)([ ]+)[a-zA-Z(\\:)(\\.)0-9]+";
				String res = "";

				Pattern p = Pattern.compile(regex);
				Matcher m = p.matcher(linea);
				if (m.find()) {
					res = m.group();
					String[] aux2 = res.split("\\:[ ]+");
					metadata.set(aux2[0].toLowerCase(), aux2[1]);
				}
				if (logger.isInfoEnabled())
					logger.info(res);
			}
		}

		if (logger.isInfoEnabled())
			logger.info("extractVideoMetaData, end");
	}

	/**
	 * Build the body of the content.
	 * 
	 * @param stream
	 * @param filename
	 * @return
	 */
	private String extractBody(InputStream stream, String filename) {

		SystemProcess p1 = new SystemProcess();
		String orden = Constants.PATHFFMPEG + "/ffmpeg -i " + filename;

		return p1.start(orden);
	}
}
