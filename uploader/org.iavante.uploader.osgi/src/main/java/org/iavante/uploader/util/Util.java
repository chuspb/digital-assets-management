/*
 * Digital Assets Management
 * =========================
 * 
 * Copyright 2009 Fundación Iavante
 * 
 * Authors: 
 *   Francisco José Moreno Llorca <packo@assamita.net>
 *   Francisco Jesús González Mata <chuspb@gmail.com>
 *   Juan Antonio Guzmán Hidalgo <juan@guzmanhidalgo.com>
 *   Daniel de la Cuesta Navarrete <cues7a@gmail.com>
 *   Manuel José Cobo Fernández <ranrrias@gmail.com>
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * http://ec.europa.eu/idabc/eupl
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and 
 * limitations under the Licence.
 * 
 */

package org.iavante.uploader.util;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Date utilities.
 */
public class Util implements Serializable {
	private static final long serialVersionUID = 1L;

	/** Default date. */
	public static Date DEFECT_DATE;

	/** Default date. */
	public static Date dateDefecto;

	static {
		try {
			DEFECT_DATE = Util.stringToDate("01/01/0001");
			dateDefecto = Util.stringToDate("01/01/0001");
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	// --------------- Constructor --------------
	public Util() {
		super();
	}

	public static void setDateDefecto(Date dateDefecto) {
		Util.dateDefecto = dateDefecto;
	}

	public static Date getDateDefecto() {
		return dateDefecto;
	}

	public static String getExtensionFichero(String file) {
		if (file != null && file.length() > 0 && file.indexOf('.') != -1) {
			String ext = "."
					+ file.substring(file.lastIndexOf('.') + 1, file.length());
			return ext.toUpperCase();
		} else {
			return "";
		}
	}

	/**
	 * Return current year.
	 * 
	 * @return
	 */
	public static int getCurrentYear() {
		Calendar oCalendar = Calendar.getInstance();
		oCalendar.setTime(new Date(System.currentTimeMillis()));
		int year = oCalendar.get(Calendar.YEAR);
		return year;
	}

	/**
	 * Return current date.
	 * 
	 * @return
	 */
	public static Date getCurrentDate() {
		Calendar oCalendar = Calendar.getInstance();
		oCalendar.setTime(new Date(System.currentTimeMillis()));
		Date date = oCalendar.getTime();
		return date;
	}

	/**
	 * From java.util.Date to java.sql.Date.
	 * 
	 * @param dat
	 * @return
	 */
	public static java.sql.Date dateToSql(Date dat) {
		java.sql.Date dSql = null;

		if (dat != null)
			dSql = new java.sql.Date(dat.getTime());

		return dSql;
	}

	/**
	 * Format a date.
	 * 
	 * @param oDate
	 * @return
	 */
	public static String formatDate(Date oDate) {
		String sDate = "";

		if (oDate != null && Util.dateIqualTo(oDate, Util.DEFECT_DATE) == false) {
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			sDate = format.format(oDate);
		}

		return sDate;
	}

	/**
	 * From string to date.
	 * 
	 * @param sDate
	 * @return
	 * @throws ParseException
	 */
	public static Date stringToDate(String sDate) throws ParseException {
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
		Date oDate = formato.parse(sDate);
		return oDate;
	}

	/**
	 * Format a date with dashes.
	 * 
	 * @param oDate
	 * @return
	 */
	public static String formatDateDashes(Date oDate) {
		String sDate = "";

		if (oDate != null) {
			SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
			sDate = format.format(oDate);
		}

		return sDate;
	}

	/**
	 * String to Date with dashes.
	 * 
	 * @param sDate
	 * @throws ParseException
	 * @return
	 */
	public static Date stringToDateDashes(String sDate) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		Date oDate = format.parse(sDate);

		return oDate;
	}

	/**
	 * Compare two dates.
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static boolean dateIqualTo(Date a, Date b) {
		if (a.compareTo(b) == 0)
			return true;
		else
			return false;
	}

	/**
	 * Returns the mimetype of a file using URL methods.
	 * 
	 * @param fileUrl
	 * @return
	 * @throws java.io.IOException
	 * @throws MalformedURLException
	 */
	public static String getMimeType(String fileUrl) throws java.io.IOException,
			MalformedURLException {
		String type = null;
		URL u = new URL(fileUrl);
		URLConnection uc = null;
		uc = u.openConnection();
		type = uc.getContentType();
		return type;
	}
}
